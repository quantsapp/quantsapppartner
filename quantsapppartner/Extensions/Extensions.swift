//
//  Extensions.swift
//  quantsapppartner
//
//  Created by Quantsapp on 12/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation
import UIKit
import ZAlertView
import SwifterSwift

extension Sequence where Element == UInt8 {
    public var hexString: String {
        var hexadecimalString = ""
        var iterator = makeIterator()
        while let value = iterator.next() {
            hexadecimalString += String(format: "%02x", value)
        }
        return hexadecimalString
    }
}

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

extension CGRect {
    static func * (lhs: CGRect, rhs: CGFloat) -> CGRect {
        return CGRect(x: lhs.minX * rhs, y: lhs.minY * rhs, width: lhs.width * rhs, height: lhs.height * rhs)
    }
}

extension UIView {
    public func resignAllResponders() {
        self.endEditing(true)
    }
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func getSnapshot(of rect: CGRect? = nil, afterScreenUpdates: Bool = true) -> UIImage? {
        if #available(iOS 10.0, *) {
            return UIGraphicsImageRenderer(bounds: rect ?? bounds).image { _ in
                drawHierarchy(in: bounds, afterScreenUpdates: true)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0)
            drawHierarchy(in: bounds, afterScreenUpdates: afterScreenUpdates)
            let wholeImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // if no `rect` provided, return image of whole view
            
            guard let image = wholeImage, let rect = rect else { return wholeImage }
            
            // otherwise, grab specified `rect` of image
            
            guard let cgImage = image.cgImage?.cropping(to: rect * image.scale) else { return nil }
            return UIImage(cgImage: cgImage, scale: image.scale, orientation: .up)
        }
    }
    
    var firstResponder: UIView? {
        guard !isFirstResponder else { return self }
        
        for subview in subviews {
            if let firstResponder = subview.firstResponder {
                return firstResponder
            }
        }
        
        return nil
    }
}

extension UIViewController {
    
    public func addGradientLayer(topColor:UIColor, bottomColor:UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        view.layer.addSublayer(gradientLayer)
    }
    
    public func showZAlertView(withTitle title: String, andMessage message: String, cancelTitle cancel: String, isError: Bool = true, completion: @escaping(() -> Void)) {
        let dialog = ZAlertView()
        dialog.alertTitle = title
        dialog.message = message
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        dialog.addButton(cancel, hexColor: (isError) ? Constants.ZAlertViewUI.errorButtonColor.hexString : Constants.ZAlertViewUI.successButtonColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
            alertView.dismissAlertView()
            completion()
        }
        
        dialog.show()
    }
    
    public var rootViewController: UIViewController {
        return parent?.rootViewController ?? self
    }
    
    public func logout() {
        if let nc = self.navigationController {
            SessionTokenManager.sharedInstance.setToken(token: nil)
            SessionTokenManager.sharedInstance.setUsername(username: nil)
            nc.popToRootViewController(animated: true)
        }
    }
}

extension UIViewController: UITextFieldDelegate {
    
    func addToolBar(textField: UITextField) {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76 / 255, green: 217 / 255, blue: 100 / 255, alpha: 1)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    
    @objc func donePressed() {
        view.endEditing(true)
    }
    
    @objc func cancelPressed() {
        view.endEditing(true) // or do something
    }
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
    
    func toString() -> String {
        let intValue = self.int
        let decimal = self.truncatingRemainder(dividingBy: 1)
        
        if decimal == 0 {
            return "\(intValue)"
        } else {
            return "\(self)"
        }
    }
    
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    
    func toLocalCurrencyFormat() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .decimal // .currency
        
        if let formattedString = formatter.string(from: self as NSNumber) {
            return "₹ \(formattedString)"
        } else {
            return "₹ \(self)"
        }
    }
    
    func toLocalNumberFormat() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .decimal
        
        if let formattedString = formatter.string(from: self as NSNumber) {
            return formattedString
        } else {
            return "\(self)"
        }
    }
}

extension String {
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    var condensedWhitespace: String {
        let components = self.components(separatedBy: .whitespaces)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func convertToDate() -> Date? {
        
        if self.count == 10 {
            if let date = self.date(withFormat: "yyyy-MM-dd") {
                return date
            } else {
                return nil
            }
        } else if self.count == 11 {
            if let date = self.date(withFormat: "dd-MMM-yyyy") {
                return date
            } else {
                return nil
            }
        } else {
            if let date = self.date(withFormat: "yyyy-MM-dd HH:mm:ss.S") {
                return date
            } else {
                return nil
            }
        }
    }
    
    func percentEncoded() -> String {
        let customAllowedSet =  NSCharacterSet(charactersIn:":=&\"#%/<>?@\\^`{|}").inverted
        let string = self.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        //let string = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        return string!
    }
    
    func removingUrls() -> String {
        guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else {
            return self
        }
        
        return detector.stringByReplacingMatches(in: self,
                                                 options: [],
                                                 range: NSRange(location: 0, length: self.utf16.count),
                                                 withTemplate: "")
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func digits() -> String {
        return self.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890.").inverted)
    }
    
    func removeZero() -> String {
        if let dbl = self.double() {
            return dbl.toString()
        } else {
            return self
        }
    }
    
    func stripUnicodeCharacters() -> String {
        if self.canBeConverted(to: String.Encoding.ascii) {
            var newString = ""
            
            for char in self {
                if char == "\n" {
                    newString.append(" ")
                } else {
                    newString.append(char)
                }
            }
            
            return newString.condensedWhitespace
        } else {
            var newString = ""
            
            for char in self {
                if "\(char)".canBeConverted(to: String.Encoding.ascii) {
                    if char == ";" {
                        newString.append(",")
                    } else if char == "|" {
                        newString.append(" ")
                    } else if char == "*" {
                        newString.append(" ")
                    } else if char == "\n" {
                        newString.append(" ")
                    } else {
                        newString.append(char)
                    }
                } else {
                    newString.append("")
                }
            }
            
            return newString.condensedWhitespace
        }
    }
}

extension UITextField {
    
    func modifyClearButtonWithImage(image : UIImage) {
        let clearButton : UIButton = self.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor(white: 1.0, alpha: 0.4)
        clearButton.backgroundColor = UIColor.clear
    }
    
    func modifyClearButton(with image: UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor(white: 1.0, alpha: 0.4)
        clearButton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(_:)), for: .touchUpInside)
        rightView = clearButton
        rightViewMode = .whileEditing
    }
    
    @objc func clear(_ sender : AnyObject) {
        self.text = ""
        sendActions(for: .editingChanged)
    }
    
    func showIconOnLeftWithImage(image: UIImage) {
        let imageView = UIImageView(frame: CGRect(x: 4, y: 0, width: 36, height: 20))
        imageView.image = image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        self.leftView = imageView
        self.leftViewMode = .always
    }
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}

extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !self.text.isEmpty// > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = UIFont.systemFont(ofSize: 13)
        placeholderLabel.textColor = UIColor(white: 1.0, alpha: 0.3)
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = !self.text.isEmpty// > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        } else {
            return true
        }
    }
    
}

extension NSError {
    
    public convenience init(type: OSOError) {
        self.init(domain: SwifterSwift.appBundleID!, code: type.rawValue, userInfo: type.localizedUserInfo())
    }
}

extension UIResponder {
    
    func getParentViewController() -> UIViewController? {
        if self.next is UIViewController {
            return self.next as? UIViewController
        } else {
            if self.next != nil {
                return (self.next!).getParentViewController()
            }
            else {return nil}
        }
    }
    
}

extension Notification.Name {
    
    static let didRegisterForPushNotification = Notification.Name("didRegisterForPushNotification")
    static let didFailToRegisterForPushNotification = Notification.Name("didFailToRegisterForPushNotification")
    static let appBecomeActive = Notification.Name("appBecomeActive")
    static let notificationsRead = Notification.Name("notificationsRead")
    static let notificationReceived = Notification.Name("notificationReceived")
    static let paymentSuccessful = Notification.Name("paymentSuccessful")
    static let invalidSession = Notification.Name("invalidSession")
    static let didAccessWithoutLogin = Notification.Name("didAccessWithoutLogin")
}

