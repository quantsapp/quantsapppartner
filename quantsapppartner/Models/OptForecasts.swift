//
//  OptForecasts.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 27/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OptForecasts {
    
    public var id: Int?
    public var symbol: String?
    public var direction: Int?
    public var validTill: Date?
    public var level1: String?
    public var level2: String?
    public var level3: String?
    public var level4: String?
    public var dateCreated: Date?
    
    init() {
        
    }
    
    /*init(id: Int, symbol: String, direction: Int, validTill: Date, level1: String, level2: String, level3: String, level4: String, dateCreated: Date) {
        self.id = id
        self.symbol = symbol
        self.direction = direction
        self.validTill = validTill
        self.level1 = level1
        self.level2 = level2
        self.level3 = level3
        self.level4 = level4
        self.dateCreated = dateCreated
    }*/
    
}
