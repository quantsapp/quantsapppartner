//
//  StrategistResult.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 28/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class StrategistResult {
    
    public var symbol: String?
    public var days: String?
    public var volMean: String?
    public var volUp: String?
    public var volDn: String?
    public var strategyName: String?
    public var riskProfile: String?
    public var leg1: String?
    public var leg2: String?
    public var leg3: String?
    public var leg4: String?
    public var rewardToRisk: String?
    public var margin: String?
    public var meanProfit: String?
    public var meanLoss: String?
    public var l1Mean: String?
    public var l1Best: String?
    public var l1Worst: String?
    public var l2Mean: String?
    public var l2Best: String?
    public var l2Worst: String?
    public var l3Mean: String?
    public var l3Best: String?
    public var l3Worst: String?
    public var l4Mean: String?
    public var l4Best: String?
    public var l4Worst: String?
    public var level1: String?
    public var level2: String?
    public var level3: String?
    public var level4: String?
    public var type: Int?
    public var tradeSpecific: String? //
    public var no: Int? //
    public var drawDn: String?
    public var drawUp: String?
    public var drawDnLvl: String?
    public var drawUpLvl: String?
    public var dataTime: Date?
    
    
    init() {
        
    }

}
