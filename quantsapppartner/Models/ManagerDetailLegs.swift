//
//  ManagerDetailLegs.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 23/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class ManagerDetailLegs: NSCopying {
    
    public var bookType: String?
    public var srno: String?
    public var cmp: String?
    public var mtm: String?
    public var delta: String?
    public var theta: String?
    public var gamma: String?
    public var vega: String?
    public var expired: String?
    public var expiry: String?
    public var instrument: String?
    public var strike: String?
    public var optType: String?
    public var price: String?
    public var qty: String?
    public var closingDate: Date?
    
    init() {
        
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = ManagerDetailLegs()
        copy.bookType = bookType
        copy.srno = srno
        copy.cmp = cmp
        copy.mtm = mtm
        copy.delta = delta
        copy.theta = theta
        copy.gamma = gamma
        copy.vega = vega
        copy.expired = expired
        copy.expiry = expiry
        copy.instrument = instrument
        copy.strike = strike
        copy.optType = optType
        copy.price = price
        copy.qty = qty
        copy.closingDate = closingDate
        
        return copy
    }
    
    
    public func printData() {
        
        if let value = bookType {
            print("bookType = \(value)")
        }
        
        if let value = srno {
            print("srno = \(value)")
        }
        
        if let value = cmp {
            print("cmp = \(value)")
        }
        
        if let value = mtm {
            print("mtm = \(value)")
        }
        
        if let value = expired {
            print("expired = \(value)")
        }
        
        if let value = expiry {
            print("expiry = \(value)")
        }
        
        if let value = instrument {
            print("instrument = \(value)")
        }
        
        if let value = strike {
            print("strike = \(value)")
        }
        
        if let value = optType {
            print("optType = \(value)")
        }
        
        if let value = price {
            print("price = \(value)")
        }
        
        if let value = qty {
            print("qty = \(value)")
        }
    }
    
}
