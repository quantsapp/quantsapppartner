//
//  AnalyzerSummary.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 17/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class AnalyzerSummary {
    
    public var bookType: String?
    public var positionNo: String?
    public var srno: String?
    public var positionName: String?
    public var strategyName: String?
    public var dateCreated: Date?
    public var dateClosed: Date?
    public var Papi: String?
    public var instrument: String?
    public var symbol: String?
    public var strike: String?
    public var price: String?
    public var qty: String?
    public var margin: String?
    public var expiry: String?
    public var alertOne: String?
    public var alertTwo: String?
    public var syncNo: String?
    public var tradeStatus: String?
    public var optType: String?
    public var expired: String?
    public var dateLegClosed: String?
    public var initiationPrice: String?
    public var closingPrice: String?
    public var fixedProfit: String?
    public var spreadTarget: String?
    public var spreadStopLoss: String?
    
    init() {
        
    }
    
}
