//
//  AddEditPositions.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 25/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class AddEditPositions {
    
    public var fullInstrument: String?
    public var instrument: String?
    public var expiry: String?
    public var strike: String?
    public var optType: String?
    public var price: String?
    public var quantity: String?
    public var expired: String?
    
    public var symbol: String?
    
    init() {
        
    }
}
