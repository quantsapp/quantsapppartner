//
//  PositionDetails.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class PositionDetails {
    
    public var positionNo: String?
    public var acType: String?
    public var symbolCmp: String?
    public var delta: String?
    public var theta: String?
    public var gamma: String?
    public var vega: String?
    public var iv: String?
    public var cmp: [String]?
    public var srno: [String]?
    public var legDelta: [String]?
    public var legTheta: [String]?
    public var legGamma: [String]?
    public var legVega: [String]?
    public var dataTime: String?
    
    
    init() {
        
    }
}
