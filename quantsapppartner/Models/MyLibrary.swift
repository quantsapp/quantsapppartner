//
//  MyLibrary.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 09/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class MyLibrary {
    
    public var no: Int?
    public var direction: Int?
    public var proficiency: Int?
    public var riskType: Int?
    public var optimizer: Int?
    public var legs: Int?
    public var riskSubType: Int?
    public var name: String?
    public var trade: String?
    public var intro: String?
    public var execute: String?
    public var tradeText: String?
    public var maxProfit: String?
    public var maxLoss: String?
    public var advantages: String?
    public var disadvantages: String?
    
    init() {
        
    }
    
}
