//
//  OptMaster.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 13/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OptMaster {
    
    public var symbol: String?
    public var expiry: Date?
    public var lot: String?
    public var strikes: [String]?
    
    init () {
        
    }
}
