//
//  AnalyzerSummaryList.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 19/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation


class AnalyzerSummaryList {
    
    public var bookType: String?
    public var symbol: String?
    public var positionNo: String?
    public var positionName: String?
    public var strategyName: String?
    public var mtm: String?
    public var cmp: String?
    public var margin: String?
    public var roi: String?
    public var alertOne: String?
    public var alertTwo: String?
    public var tradeStatus: String?
    public var dateCreated: Date?
    public var dateClosed: Date?
    public var expired: Int?
    public var spreadTarget: String?
    public var spreadStopLoss: String?
    
    init() {
        
    }
}
