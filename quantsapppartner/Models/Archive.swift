//
//  Archive.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 24/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class Archive {
    
    public var symbol: String?
    public var strategy: String?
    public var createdOn: Date?
    public var closedOn: Date?
    public var pnl: String?
    public var margin: String?
    public var tradeStatus: String?
    public var legData: String?
    public var id: String?
    
    init() {
        
    }
}
