//
//  NotificationMessageViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 28/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift
import ZAlertView
import SVProgressHUD

class NotificationMessageViewController: UIViewController {
    
    private static let idleTimerDuration: Double = 30.0
    
    private var osoNavBar: OSONavigationBar!
    private let screenWidth = SwifterSwift.screenWidth
    
    public var positionNos: [String]?
    public var notificationMessage: String?
    public var bookName: [String]?
    public var notificationTitle: String?
    public var dataValues: String?
    
    private var idleTimer: Timer?
    
    private let lblHeading: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 24)
        label.text = "Notification Message"
        label.textColor = .white
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Constants.UIConfig.themeColor
        label.numberOfLines = 0
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblMessage: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = UIColor(white: 1.0, alpha: 0.7)
        label.numberOfLines = 0
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let btnContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let btnSend: UIButton = {
        let button = UIButton()
        button.setTitle("SEND", for: [])
        button.backgroundColor = Constants.UIConfig.themeColor
        button.setTitleColor(Constants.UIConfig.bottomColor, for: .normal) //
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = Constants.UIConfig.buttonCornerRadius
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(actionSend(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnCancel: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.setTitle("CANCEL", for: [])
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.5), for: [])
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = Constants.UIConfig.buttonCornerRadius
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(actionCancel(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startTimer()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        // initial setup of all views
        setupViews()
    }
    

    private func setupViews() {
        // btnContainer
        view.addSubview(btnContainer)
        btnContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        btnContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        btnContainer.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight + 32).isActive = true
        if #available(iOS 11.0, *) {
            btnContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            btnContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        
        // btnCancel
        btnContainer.addSubview(btnCancel)
        btnCancel.leftAnchor.constraint(equalTo: btnContainer.leftAnchor, constant: 16).isActive = true
        btnCancel.rightAnchor.constraint(equalTo: btnContainer.centerXAnchor, constant: -8).isActive = true
        btnCancel.centerYAnchor.constraint(equalTo: btnContainer.centerYAnchor, constant: 0).isActive = true
        btnCancel.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        
        // btnSend
        btnContainer.addSubview(btnSend)
        btnSend.rightAnchor.constraint(equalTo: btnContainer.rightAnchor, constant: -16).isActive = true
        btnSend.leftAnchor.constraint(equalTo: btnContainer.centerXAnchor, constant: 8).isActive = true
        btnSend.centerYAnchor.constraint(equalTo: btnContainer.centerYAnchor, constant: 0).isActive = true
        btnSend.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        
        // lblHeading
        view.addSubview(lblHeading)
        lblHeading.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        lblHeading.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        lblHeading.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 16).isActive = true
        lblHeading.heightAnchor.constraint(equalToConstant: (lblHeading.text?.height(constraintedWidth: screenWidth - 32, font: lblHeading.font))!).isActive = true
        
        // lblTitle
        view.addSubview(lblTitle)
        lblTitle.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        lblTitle.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        
        if let title = notificationTitle {
            lblTitle.text = title
            lblTitle.topAnchor.constraint(equalTo: lblHeading.bottomAnchor, constant: 16).isActive = true
            lblTitle.heightAnchor.constraint(equalToConstant: title.height(constraintedWidth: screenWidth - 32, font: lblTitle.font)).isActive = true
        } else {
            lblTitle.topAnchor.constraint(equalTo: lblHeading.bottomAnchor, constant: 0).isActive = true
            lblTitle.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        
        // lblMessage
        if let msg = notificationMessage {
            view.addSubview(lblMessage)
            lblMessage.text = msg
            lblMessage.sizeToFit()
            lblMessage.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
            lblMessage.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
            lblMessage.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 4).isActive = true
            lblMessage.heightAnchor.constraint(equalToConstant: msg.height(constraintedWidth: screenWidth - 32, font: lblMessage.font)).isActive = true
            view.layoutIfNeeded()
        }
    }
    
    private func startTimer() {
        if let timer = idleTimer {
            timer.invalidate()
            idleTimer = nil
        }
        
        idleTimer = Timer.scheduledTimer(timeInterval: NotificationMessageViewController.idleTimerDuration, target: self, selector: #selector(timerFired(_:)), userInfo: nil, repeats: false)
    }
    
    @objc private func timerFired(_ sender: Timer) {
        sender.invalidate()
        
        let message = "Looks like you have been idle for long time. Prices might have changed. Would you like to cancel the notification?"
        
        let dialog = ZAlertView(title: "Warning",
                                message: message,
                                isOkButtonLeft: false,
                                okButtonText: "YES",
                                cancelButtonText: "NO",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
                                    self.sendNotification(confirm: false)
                                    
        }) { (alertView) in
            alertView.dismissAlertView()
            self.startTimer()
        }
        dialog.show()
    }
    
    private func stopTimer() {
        if let timer = idleTimer {
            timer.invalidate()
            idleTimer = nil
        }
    }
    
    
    
    @objc private func actionCancel(_ sender: UIButton) {
        print("Cancel")
        
        setButtonInteractivity(enable: false)
        
        stopTimer()
        
        self.sendNotification(confirm: false)
    }
    
    @objc private func actionSend(_ sender: UIButton) {
        print("Send")
        
        self.setButtonInteractivity(enable: false)
        
        stopTimer()
        
        confirm(title: "Confirm",
                message: "Do you want to send notification?") { (confirmed) in
                    if confirmed {
                        self.sendNotification(confirm: confirmed)
                    } else {
                        self.setButtonInteractivity(enable: true)
                        self.startTimer()
                    }
        }
    }
    
    @objc private func sendNotification(confirm: Bool) {
        
        guard let positions = positionNos else {
            showZAlertView(withTitle: "Error", andMessage: "Position number not found.", cancelTitle: "OK", isError: true, completion: {})
            return
        }
        
        guard let name = bookName else {
            showZAlertView(withTitle: "Error", andMessage: "Book type not found.", cancelTitle: "OK", isError: true, completion: {})
            return
        }
        
        guard let title = notificationTitle else {
            showZAlertView(withTitle: "Error", andMessage: "Notification title not found.", cancelTitle: "OK", isError: true, completion: {})
            return
        }
        
        guard let body = notificationMessage else {
            showZAlertView(withTitle: "Error", andMessage: "Notification message not found.", cancelTitle: "OK", isError: true, completion: {})
            return
        }
        
        guard let values = dataValues else {
            showZAlertView(withTitle: "Error", andMessage: "Data values not found.", cancelTitle: "OK", isError: true, completion: {})
            return
        }
        
        SVProgressHUD.show()
        
        
        OSOWebService.sharedInstance.sendNotification(positions: positions, bookName: name, notfTitle: title, notfBody: body, dataValues: values, confirm: confirm) { (success, error, message) in
            if let err = error {
                SVProgressHUD.dismiss()
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    self.setButtonInteractivity(enable: true)
                })
            } else {
                if success {
                    var msg = confirm ? "Notification has been sent successfully." : "Notification has been cancelled."
                    
                    if let messageString = message {
                        msg = messageString
                    }
                    
                    if confirm {
                        self.deletePositions(positions: positions, completion: { (success) in
                            if success {
                                print("Position deleted successfully")
                            } else {
                                print("Failed to delete position")
                            }
                            
                            SVProgressHUD.dismiss()
                            self.showZAlertView(withTitle: "", andMessage: msg, cancelTitle: "OK", isError: false, completion: {
                                self.showHomeScreen()
                            })
                        })
                    } else {
                        SVProgressHUD.dismiss()
                        self.showZAlertView(withTitle: "", andMessage: msg, cancelTitle: "OK", isError: false, completion: {
                            self.showPreviousScreen()
                        })
                    }
                } else {
                    SVProgressHUD.dismiss()
                    self.showZAlertView(withTitle: "Error", andMessage: "Failed to send notification", cancelTitle: "OK", isError: true, completion: {
                        self.setButtonInteractivity(enable: true)
                    })
                }
            }
        }
    }
    
    private func deletePositions(positions: [String], completion: @escaping((_ success: Bool) -> Void)) {
        DatabaseManager.sharedInstance.deletePositions(positions: positions, bookType: "-") { (success) in
            completion(success)
        }
    }
    
    private func showHomeScreen() {
        let homeVC = self.navigationController!.viewControllers.filter { $0 is HomeViewController }.first!
        navigationController!.popToViewController(homeVC, animated: true)
    }
    
    private func showPreviousScreen() {
        stopTimer()
        self.navigationController?.popViewController()
    }
    
    private func confirm(title: String?, message: String, completion: @escaping ((_ confirmed: Bool) -> Void)) {
        
        let dialog = ZAlertView(title: title,
                                message: message,
                                isOkButtonLeft: false,
                                okButtonText: "Yes",
                                cancelButtonText: "No",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissWithDuration(0.3)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                                        completion(true)
                                    })
        }) { (alertView) in
            completion(false)
            alertView.dismissAlertView()
        }
        dialog.show()
    }
    
    private func setButtonInteractivity(enable: Bool) {
        btnSend.isEnabled = enable
        btnCancel.isEnabled = enable
    }
    
    private func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect.zero, title: "Push Notification", subTitle: "Confirm", leftbuttonImage: nil, rightButtonImage: nil)
        osoNavBar.translatesAutoresizingMaskIntoConstraints = false
        osoNavBar.delegate = self
        view.addSubview(osoNavBar)
        
        osoNavBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        osoNavBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        osoNavBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        osoNavBar.heightAnchor.constraint(equalToConstant: Constants.OSONavigationBarConstants.barHeight).isActive = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

// MARK:- OSONavigationBarDelegate

extension NotificationMessageViewController: OSONavigationBarDelegate {
    
    func rightButtonTapped(sender: OSONavigationBar) {
        print("Close")
    }
    
}
