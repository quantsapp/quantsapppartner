//
//  SplashViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 12/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift
import ZAlertView
import Alamofire

class SplashViewController: UIViewController {
    
    private var masterApiCallAttemptCounter: Int = 0
    private var downloadProcessCounter = 0

    private let splashView: SplashView = {
        let view = SplashView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // initial setup of all views
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // init database
        self.initDatabase { (error) in
            if let err = error {
                self.splashView.loadingActivityIndicatorView.stopAnimating()
                self.showZAlertView(withTitle: "Database Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
            } else {
                self.splashView.loadingActivityIndicatorView.startAnimating()
                self.callMasterAPI()
            }
        }
    }
    
    private func setupViews() {
        // splashView
        view.addSubview(splashView)
        splashView.loadingActivityIndicatorView.startAnimating()
        splashView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        splashView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        splashView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        if #available(iOS 11.0, *) {
            splashView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            splashView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
    }
    
    private func initDatabase(completion: @escaping((_ error: Error?) -> Void)) {
        
        print("► initDatabase()")
        
        DatabaseManager.sharedInstance.checkForDatabase(databaseName: Constants.DB.dbName, dbExtension: Constants.DB.dbExtension) { (success, error) in
            if let _ = error {
                DatabaseManager.sharedInstance.createDatabase(databaseName: Constants.DB.dbName, completion: { (success, error) in
                    if let err = error {
                        completion(err)
                    } else {
                        self.createAllTables(completion: { (error) in
                            completion(error)
                        })
                    }
                })
            } else {
                completion(nil)
            }
        }
        
    }
    
    private func createAllTables(completion: @escaping((_ error: NSError?) -> Void)) {
        
        print("► createAllTables()")
        
        if DatabaseManager.sharedInstance.isConnectedToDB {
            
            let tables = [Constants.DBTables.AnalyzerSummary,
                          Constants.DBTables.CallHistory,
                          Constants.DBTables.AppLog,
                          Constants.DBTables.Archive,
                          Constants.DBTables.ErrorLog,
                          Constants.DBTables.FutMaster,
                          Constants.DBTables.GcmRefresh,
                          Constants.DBTables.GlobalNotification,
                          Constants.DBTables.HelpCase,
                          Constants.DBTables.MyLibrary,
                          Constants.DBTables.Notifications,
                          Constants.DBTables.OptForecasts,
                          Constants.DBTables.OptMaster,
                          Constants.DBTables.PageInfo,
                          Constants.DBTables.PendingApis,
                          Constants.DBTables.StrategistMaster,
                          Constants.DBTables.StrategistResult,
                          Constants.DBTables.Books]
            
            
            var tableCounter = 0
            
            for table in tables {
                DatabaseManager.sharedInstance.createTable(tableName: table) { (success, error) in
                    if let error = error {
                        print("Failed to create table \"\(table)\" [Error: \(error)]")
                        completion(error)
                        return
                    } else {
                        tableCounter += 1
                    }
                }
            }
            
            if tableCounter == tables.count {
                print("All tables are created")
                self.insertDefaultMasterValues { (error) in
                    if let err = error {
                        completion(err)
                    } else {
                        completion(nil)
                    }
                }
            } else {
                let error = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Failed to create all the tables."])
                completion(error)
            }
        } else {
            let error = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "No connection to database found"])
            completion(error)
        }
    }
    
    private func insertDefaultMasterValues(completion: @escaping((_ error: NSError?) -> Void)) {
        print("► insertDefaultMasterValues()")
        
        DatabaseManager.sharedInstance.insertDefaultsInTableStrategistMaster { (success, error) in
            if let err = error {
                print("✖︎ Failed to insert default values in StrategistMaster. [Error: \(err)]")
                completion(err)
            } else {
                print("✔︎ Default values inserted successfully in StrategistMaster")
                completion(nil)
            }
        }
    }
    
    private func callMasterAPI() {
        print("► callMasterAPI()")
        
        downloadProcessCounter = 0
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        
        let masterVersions = DatabaseManager.sharedInstance.getMasterVersions()
        let version: String = masterVersions["version"]! // dataMaster
        let infoVersion: String = masterVersions["infoVersion"]!
        let scripMaster: String = masterVersions["scripMaster"]!
        let versionLibrary: String = masterVersions["versionLibrary"]!
        
        let accountDetails = DatabaseManager.sharedInstance.getAccountDetails()
        let sAcType: String = accountDetails["sAcType"]!
        let sValidity: String = accountDetails["sValidity"]!
        let sLegs: String = accountDetails["sLegs"]!
        let sOutputs: String = accountDetails["sOutputs"]!
        let aAcType: String = accountDetails["aAcType"]!
        let aValidity: String = accountDetails["aValidity"]!
        let aLegs: String = accountDetails["aLegs"]!
        let aOutputs: String = accountDetails["aOutputs"]!
        
        
        OSOWebService.sharedInstance.masterAPI(version: version, infoVersion: infoVersion, sAcType: sAcType, sValidity: sValidity, sLegs: sLegs, sOutputs: sOutputs, aAcType: aAcType, aValidity: aValidity, aLegs: aLegs, aOutputs: aOutputs) { (error, jsonData) in
            
            if let err = error {
                print("Error (callMasterAPI): \(err.localizedDescription)")
                self.retryCallMasterAPI(errorMessage: err.localizedDescription)
            } else {
                if let json = jsonData as? [String: String] {
                    self.handleMasterAPIResponse(json: json, userId: userId, token: token, scripMaster: scripMaster, versionLibrary: versionLibrary, version: version, infoVersion: infoVersion)
                } else {
                    print("Error (callMasterAPI): No response")
                    self.retryCallMasterAPI()
                }
            }
        }
    }
    
    private func retryCallMasterAPI(errorMessage: String = "") {
        print("► retryCallMasterAPI()")
        
        masterApiCallAttemptCounter += 1
        
        if masterApiCallAttemptCounter > Constants.NetworkingConfig.maximumAttempts {
            splashView.loadingActivityIndicatorView.stopAnimating()
            
            
            // OPTION TO RESET SERVER SETTINGS
            let dialog = ZAlertView()
            dialog.alertTitle = "Error"
            dialog.message = "Could not load data from the server. Make sure you are connected to internet."
            dialog.alertType = .multipleChoice
            dialog.allowTouchOutsideToDismiss = false
            dialog.addButton("TRY AGAIN", hexColor: Constants.ZAlertViewUI.errorButtonColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
                alertView.dismissAlertView()
                self.masterApiCallAttemptCounter = 0
                if self.splashView.loadingActivityIndicatorView.isHidden {
                    self.splashView.loadingActivityIndicatorView.isHidden = false
                }
                self.splashView.loadingActivityIndicatorView.startAnimating()
                self.callMasterAPI()
            }
            dialog.show()
        } else {
            
            print("========================================================")
            print("=================== RE-TRYING (\(masterApiCallAttemptCounter))=======================")
            print("========================================================")
            callMasterAPI()
        }
    }
    
    private func handleMasterAPIResponse(json: [String: String], userId: String, token: String, scripMaster: String, versionLibrary: String, version: String, infoVersion: String) {
        print("► handleMasterAPIResponse()")
        
        if let receivedApi = json["api"] {
            let receivedApiString = receivedApi.components(separatedBy: "|")
            print("receivedApiString = \(receivedApiString)")
            
            if receivedApiString[0] != "0" {
                var response = [String: String]()
                response["url"] = receivedApiString[1]
                response["scripFileName"] = receivedApiString[2]
                response["versionLibrary"] = receivedApiString[3]
                response["version"] = receivedApiString[4] //dataMaster
                response["appCompatible"] = receivedApiString[5]
                response["sessionValid"] = receivedApiString[6] // if 0-show login / 1-show home
                response["userData"] = receivedApiString[7]
                response["infoVersion"] = receivedApiString[8]
                response["accountData"] = receivedApiString[9] // check for 8 elements in accountData is not 0
                response["gcmAsked"] = receivedApiString[10]
                
                if receivedApiString.indices.contains(11) {
                    response["whatsapp"] = receivedApiString[11]
                }
                
                if response["accountData"] != "0" {
                    
                    if let temp = response["accountData"]?.components(separatedBy: ",") {
                        var keyValuePairs = [String:String]()
                        keyValuePairs["sAcType"] = temp[0]
                        keyValuePairs["sValidity"] = temp[1]
                        keyValuePairs["sLegs"] = temp[2]
                        keyValuePairs["sOutputs"] = temp[3]
                        keyValuePairs["aAcType"] = temp[4]
                        keyValuePairs["aValidity"] = temp[5]
                        keyValuePairs["aLegs"] = temp[6]
                        keyValuePairs["aOutputs"] = temp[7]
                        
                        // session validity
                        keyValuePairs["sessionValid"] = receivedApiString[6]
                        
                        DatabaseManager.sharedInstance.bulkUpdateInStrategicMaster(keyValuePairs: keyValuePairs) { (success, error) in
                            if let err = error {
                                print("Error occurred while saving account data (\(err.localizedDescription)")
                            } else {
                                print("Account Data saved successfully.")
                            }
                        }
                    }
                }
                
                if let whatsappNumber = response["whatsapp"] {
                    DatabaseManager.sharedInstance.insertIntoStrategistMaster(key: "whatsapp", value: whatsappNumber) { (success, error) in
                        if let err = error {
                            print("Error occurred while saving whatsapp number (\(err.localizedDescription)")
                        } else {
                            print("Whatsapp number saved successfully.")
                        }
                    }
                }
                
                
                // scripMaster
                /*if scripMaster != response["scripFileName"]! {
                    self.downloadFile(url: response["url"]! + "/scripMaster.txt", completion: { (success, filePath) in
                        if success {
                            if let path = filePath {
                                print("*** scripMaster.txt is saved at \(path)")
                                self.updateScripMaster(filePath: path, newScripMaster: response["scripFileName"]!, completion: { (success) in
                                    if success {
                                        print(">>> Finished updateScripMaster")
                                        self.updateDownloadProcess()
                                    } else {
                                        self.retryCallMasterAPI()
                                    }
                                })
                            } else {
                                print("scripMaster.txt is saved but url not found")
                                self.retryCallMasterAPI()
                            }
                        } else {
                            print("failed to download scripMaster.txt")
                            self.retryCallMasterAPI()
                        }
                    })
                } else {
                    print("scripMaster unchanged")
                    self.updateDownloadProcess()
                }*/
                
                // FIXME:- don't force download scripmaster
                self.downloadFile(url: response["url"]! + "/scripMaster.txt", completion: { (success, filePath) in
                    if success {
                        if let path = filePath {
                            print("*** scripMaster.txt is saved at \(path)")
                            self.updateScripMaster(filePath: path, newScripMaster: response["scripFileName"]!, completion: { (success) in
                                if success {
                                    print(">>> Finished updateScripMaster")
                                    self.updateDownloadProcess()
                                } else {
                                    self.retryCallMasterAPI()
                                }
                            })
                        } else {
                            print("scripMaster.txt is saved but url not found")
                            self.retryCallMasterAPI()
                        }
                    } else {
                        print("failed to download scripMaster.txt")
                        self.retryCallMasterAPI()
                    }
                })
                
                
                // versionLibrary
                if versionLibrary != response["versionLibrary"]! {
                    self.downloadFile(url: response["url"]! + "/library.txt", completion: { (success, filePath) in
                        if success {
                            if let path = filePath {
                                print("*** library.txt is saved at \(path)")
                                self.updateVersionLibrary(filePath: path, newVersionLibrary: response["versionLibrary"]!, completion: { (success) in
                                    if success {
                                        print(">>> Finished updateVersionLibrary")
                                        self.updateDownloadProcess()
                                    } else {
                                        self.retryCallMasterAPI()
                                    }
                                })
                            } else {
                                print("library.txt is saved but url not found")
                                self.retryCallMasterAPI()
                            }
                        } else {
                            print("failed to download library.txt")
                            self.retryCallMasterAPI()
                        }
                    })
                } else {
                    print("versionLibrary unchanged")
                    self.updateDownloadProcess()
                }
                
                
                // version (dataMaster)
                if version != response["version"]! {
                    self.downloadFile(url: response["url"]! + "/dataMaster.txt", completion: { (success, filePath) in
                        if success {
                            if let path = filePath {
                                print("*** dataMaster.txt is saved at \(path)")
                                self.updateStrategistMaster(filePath: path, newVersion: response["version"]!, userId: userId, token: token, scripMaster: response["scripFileName"]!, versionLibrary: response["versionLibrary"]!, userData: response["userData"]!, completion: { (success) in
                                    if success {
                                        print(">>> Finished updateStrategistMaster")
                                        self.updateDownloadProcess()
                                    } else {
                                        self.retryCallMasterAPI()
                                    }
                                })
                            } else {
                                print("dataMaster.txt is saved but url not found")
                                self.retryCallMasterAPI()
                            }
                        } else {
                            print("failed to download dataMaster.txt")
                            self.retryCallMasterAPI()
                        }
                    })
                } else {
                    print("version unchanged")
                    self.updateDownloadProcess()
                }
                
                
                // infoVersion
                if infoVersion != response["infoVersion"]! {
                    self.downloadFile(url: response["url"]! + "/info.txt", completion: { (success, filePath) in
                        if success {
                            if let path = filePath {
                                print("*** info.txt is saved at \(path)")
                                self.updateInfoVersion(filePath: path, newInfoVersion: response["infoVersion"]!, completion: { (success) in
                                    if success {
                                        print(">>> Finished updateInfoVersion")
                                        self.updateDownloadProcess()
                                    } else {
                                        self.retryCallMasterAPI()
                                    }
                                })
                            } else {
                                print("info.txt is saved but url not found")
                                self.retryCallMasterAPI()
                            }
                        } else {
                            print("failed to download info.txt")
                            self.retryCallMasterAPI()
                        }
                    })
                } else {
                    print("infoVersion unchanged")
                    self.updateDownloadProcess()
                }
            } else {
                print("Error: receivedApiString[0] is empty")
                self.retryCallMasterAPI()
            }
        } else {
            print("Error: receivedApiString is empty")
            self.retryCallMasterAPI()
        }
    }
    
    private func downloadFile(url: String, completion: @escaping (_ success: Bool, _ filePath: URL? ) -> Void) {
        let fileName = url.lastPathComponent
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(fileName)
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(url, to: destination).downloadProgress { (progress) in
            print("Download Progress: \(progress.fractionCompleted)")
            }.responseData { (response) in
                if let _ = response.result.error {
                    print("Error downloading file")
                    completion(false, nil)
                } else {
                    if let destinationUrl = response.destinationURL {
                        //print("File downloaded Successfully at \(destinationUrl)")
                        completion(true, destinationUrl)
                    } else {
                        completion(false, nil)
                    }
                }
        }
    }
    
    private func updateScripMaster(filePath: URL, newScripMaster: String, completion: @escaping((_ success: Bool) -> Void)) {
        do {
            let content = try String(contentsOf: filePath, encoding: String.Encoding.utf8)
            let allData = content.components(separatedBy: "|")
            
            // empty table FutMaster
            DatabaseManager.sharedInstance.emptyTable(tableName: Constants.DBTables.FutMaster) { (success, error) in
                if let error = error {
                    print("Failed to empty FutMaster. [Error: \(error)]")
                } else {
                    print("Emptied FutMaster successfully")
                }
            }
            
            // empty table OptMaster
            DatabaseManager.sharedInstance.emptyTable(tableName: Constants.DBTables.OptMaster) { (success, error) in
                if let error = error {
                    print("Failed to empty OptMaster. [Error: \(error)]")
                } else {
                    print("Emptied OptMaster successfully")
                }
            }
            
            
            var rowsFutMaster = [[String: Any]]()
            var rowsOptMaster = [[String: Any]]()
            
            for data in allData {
                let symbolData = data.components(separatedBy: ";")
                let symbol = symbolData[0]
                
                for i in 1..<symbolData.count {
                    var rowFut = [String: Any]()
                    var rowOpt = [String: Any]()
                    
                    let expiryData = symbolData[i].components(separatedBy: ":")
                    let expiry = expiryData[0]
                    let lot = expiryData[1]
                    let hasFut = expiryData[2]
                    let strikes = expiryData[3]
                    
                    // check for futures (FutMaster)
                    if hasFut == "1" {
                        rowFut["symbol"] = symbol
                        
                        if let expDate = expiry.date(withFormat: "dd-MMM-yyyy") {
                            rowFut["expiry"] = expDate
                        } else {
                            print("► SplashViewController - updateScripMaster() Error: \(expiry) is not convertible to Date()")
                        }
                        
                        rowFut["lot"] = lot
                        rowsFutMaster.append(rowFut)
                    }
                    
                    // check for options (OptMaster)
                    if !strikes.isEmpty {
                        rowOpt["symbol"] = symbol
                        rowOpt["expiry"] = expiry.date(withFormat: "dd-MMM-yyyy")!
                        rowOpt["lot"] = lot
                        rowOpt["strikes"] = strikes
                        rowsOptMaster.append(rowOpt)
                    }
                }
            }
            
            DatabaseManager.sharedInstance.bulkInsertIntoFutMaster(rowsFutMaster: rowsFutMaster) { (success, error) in
                if let err = error {
                    print("Error: \(err.localizedDescription)")
                } else {
                    print("+++ bulkInsertIntoFutMaster successful +++")
                }
            }
            
            DatabaseManager.sharedInstance.bulkInsertIntoOptMaster(rowsOptMaster: rowsOptMaster) { (success, error) in
                if let err = error {
                    print("Error: \(err.localizedDescription)")
                } else {
                    print("+++ bulkInsertIntoOptMaster successful +++")
                }
            }
            
            
            
            // update StrategistMaster table for key scripMaster with value of newScripMaster
            DatabaseManager.sharedInstance.updateValueForKeyInStrategistMaster(key: "scripMaster", value: newScripMaster) { (success, error) in
                if let error = error {
                    print("Failed to update StrategistMaster table for key \"scripMaster\". [Error: \(error)]")
                } else {
                    print("StrategistMaster table updated for key \"scripMaster\" with value \"\(newScripMaster)\"")
                    completion(true)
                }
            }
            
        } catch {
            print("Error: \(error)")
        }
    }
    
    private func updateVersionLibrary(filePath: URL, newVersionLibrary: String, completion: @escaping((_ success: Bool) -> Void)) {
        
        do {
            let content = try String(contentsOf: filePath, encoding: String.Encoding.windowsCP1252)
            let allData = content.components(separatedBy: "|")
            
            // empty tables MyLibrary
            DatabaseManager.sharedInstance.emptyTable(tableName: Constants.DBTables.MyLibrary) { (success, error) in
                if let error = error {
                    print("Failed to empty MyLibrary. [Error: \(error)]")
                } else {
                    print("Emptied MyLibrary successfully")
                }
            }
            
            var rowsMyLibrary = [[String: Any]]()
            
            for data in allData {
                let row = data.components(separatedBy: "$")
                var rowMyLibrary = [String: Any]()
                
                rowMyLibrary["no"] = row[0].int!
                rowMyLibrary["direction"] = row[1].int!
                rowMyLibrary["proficiency"] = row[2].int!
                rowMyLibrary["riskType"] = row[3].int!
                rowMyLibrary["optimizer"] = row[4].int!
                rowMyLibrary["legs"] = row[5].int!
                rowMyLibrary["riskSubType"] = row[6].int!
                rowMyLibrary["name"] = row[7]
                rowMyLibrary["trade"] = row[8]
                rowMyLibrary["intro"] = row[9]
                rowMyLibrary["execute"] = row[10]
                rowMyLibrary["tradeText"] = row[11]
                rowMyLibrary["maxProfit"] = row[12]
                rowMyLibrary["maxLoss"] = row[13]
                rowMyLibrary["advantages"] = row[14]
                rowMyLibrary["disadvantages"] = row[15]
                
                rowsMyLibrary.append(rowMyLibrary)
            }
            
            
            DatabaseManager.sharedInstance.bulkInsertIntoMyLibrary(rows: rowsMyLibrary) { (success, error) in
                if let err = error {
                    print("Error (bulkInsertIntoMyLibrary): \(err.localizedDescription)")
                } else {
                    print("+++ bulkInsertIntoMyLibrary successful +++")
                }
            }
            
            
            // update StrategistMaster table for key versionLibrary with value of newVersionLibary
            DatabaseManager.sharedInstance.updateValueForKeyInStrategistMaster(key: "versionLibrary", value: newVersionLibrary) { (success, error) in
                if let error = error {
                    print("Failed to update StrategistMaster table for key \"versionLibrary\" [Error: \(error)]")
                } else {
                    print("StrategistMaster table updated for key \"versionLibrary\" with value \"\(newVersionLibrary)\"")
                    completion(true)
                }
            }
            
        } catch {
            print("Error: \(error)")
        }
    }
    
    
    private func updateStrategistMaster(filePath: URL, newVersion: String, userId: String, token: String, scripMaster: String, versionLibrary: String, userData: String, completion: @escaping((_ success: Bool) -> Void)) {
        
        do {
            let content = try String(contentsOf: filePath, encoding: String.Encoding.utf8)
            let allData = content.components(separatedBy: "|")
            
            let appVersion = DatabaseManager.sharedInstance.getValueForKeyInStrategistMaster(key: "appVersion")
            let users = DatabaseManager.sharedInstance.getValueForKeyInStrategistMaster(key: "users")
            let firstName = DatabaseManager.sharedInstance.getValueForKeyInStrategistMaster(key: "firstName")
            let lastName = DatabaseManager.sharedInstance.getValueForKeyInStrategistMaster(key: "lastName")
            let infoVersion = DatabaseManager.sharedInstance.getValueForKeyInStrategistMaster(key: "infoVersion")
            let signUp = DatabaseManager.sharedInstance.getValueForKeyInStrategistMaster(key: "signup")
            
            // empty tables StrategistMaster
            DatabaseManager.sharedInstance.emptyTable(tableName: Constants.DBTables.StrategistMaster) { (success, error) in
                if let error = error {
                    print("Failed to empty StrategistMaster. [Error: \(error)]")
                } else {
                    print("Emptied StrategistMaster successfully")
                }
            }
            
            var rowsStrategistMaster = [[String: Any]]()
            
            for data in allData {
                let row = data.components(separatedBy: "$")
                var rowStrategistMaster = [String:Any]()
                
                rowStrategistMaster["key"] = row[0]
                rowStrategistMaster["value"] = row[1]
                
                rowsStrategistMaster.append(rowStrategistMaster)
            }
            
            DatabaseManager.sharedInstance.bulkInsertIntoStrategistMaster(rows: rowsStrategistMaster) { (success, error) in
                if let err = error {
                    print("Error (bulkInsertIntoStrategistMaster): \(err.localizedDescription)")
                } else {
                    print("+++ bulkInsertIntoStrategistMaster successful +++")
                }
            }
            
            
            // update other data
            var keyValuePairs = [String: String]()
            keyValuePairs["appVersion"] = appVersion
            keyValuePairs["firstName"] = firstName
            keyValuePairs["lastName"] = lastName
            keyValuePairs["infoVersion"] = infoVersion
            keyValuePairs["versionLibrary"] = versionLibrary
            keyValuePairs["userId"] = userId
            keyValuePairs["token"] = token
            keyValuePairs["scripMaster"] = scripMaster
            keyValuePairs["version"] = newVersion
            keyValuePairs["users"] = users
            
            DatabaseManager.sharedInstance.bulkUpdateInStrategicMaster(keyValuePairs: keyValuePairs) { (success, error) in
                if let error = error {
                    print("Bulk update1 failed. [Error: \(error)]")
                } else {
                    print("Bulk update1 done")
                }
            }
            
            // update all user values
            if userData != "0" {
                let uData = userData.components(separatedBy: ";")
                var newKeyValuePairs = [String: String]()
                newKeyValuePairs["email"] = uData[0]
                newKeyValuePairs["sAcType"] = uData[1]
                newKeyValuePairs["aAcType"] = uData[2]
                newKeyValuePairs["sValidity"] = uData[3]
                newKeyValuePairs["aValidity"] = uData[4]
                newKeyValuePairs["mobile"] = uData[5]
                newKeyValuePairs["firstName"] = uData[6]
                newKeyValuePairs["lastName"] = uData[7]
                newKeyValuePairs["sLegs"] = uData[8]
                newKeyValuePairs["sOutputs"] = uData[9]
                newKeyValuePairs["OptSetBackRatio"] = uData[10]
                newKeyValuePairs["OptSetRatio"] = uData[11]
                newKeyValuePairs["usrExposure"] = uData[12]
                newKeyValuePairs["alertMethod"] = uData[13]
                newKeyValuePairs["OptSetMaxExpiry"] = uData[14]
                newKeyValuePairs["aOutputs"] = uData[15]
                newKeyValuePairs["aLegs"] = uData[16]
                newKeyValuePairs["OptSetLotCapital"] = uData[17]
                newKeyValuePairs["OptSetRiskCapitalRatio"] = uData[18]
                newKeyValuePairs["OptSetCapital"] = uData[19]
                newKeyValuePairs["OptSetRisk"] = uData[20]
                newKeyValuePairs["OptSetUpdateCapital"] = uData[21]
                newKeyValuePairs["OptSetFiltration"] = uData[22]
                newKeyValuePairs["OptSetBestWorst"] = uData[23]
                newKeyValuePairs["OptSetAutoSaveForecast"] = uData[24]
                newKeyValuePairs["OptSetOverwriteForecast"] = uData[25]
                newKeyValuePairs["OptSetAutoImportForecast"] = uData[26]
                newKeyValuePairs["OptSetSortSummaryBy"] = uData[27]
                newKeyValuePairs["OptSetsLegs"] = uData[28]
                newKeyValuePairs["OptSetsOutputs"] = uData[29]
                newKeyValuePairs["OptSetLimitedRisk"] = uData[30]
                newKeyValuePairs["OptSetUpdateRisk"] = uData[31]
                newKeyValuePairs["OptSetUpdateRiskCapitalRatio"] = uData[32]
                newKeyValuePairs["manSetSorting"] = uData[33]
                newKeyValuePairs["dataType"] = uData[34]
                newKeyValuePairs["OptSetMinRoi"] = uData[35]
                newKeyValuePairs["promo"] = uData[36]
                newKeyValuePairs["OptSetMinLots"] = uData[37]
                
                DatabaseManager.sharedInstance.bulkUpdateInStrategicMaster(keyValuePairs: newKeyValuePairs) { (success, error) in
                    if let error = error {
                        print("Bulk update2 failed. [Error: \(error)]")
                    } else {
                        print("Bulk update2 done")
                    }
                    
                    completion(true)
                }
            } else {
                completion(true)
            }
            
        } catch {
            print("Error: \(error)")
        }
        
    }
    
    
    private func updateInfoVersion(filePath: URL, newInfoVersion: String, completion: @escaping((_ success: Bool) -> Void)) {
        
        do {
            let content = try String(contentsOf: filePath, encoding: String.Encoding.windowsCP1252)
            let allData = content.components(separatedBy: "|")
            
            // empty tables PageInfo
            DatabaseManager.sharedInstance.emptyTable(tableName: Constants.DBTables.PageInfo) { (success, error) in
                if let error = error {
                    print("Failed to empty PageInfo. [Error: \(error)]")
                } else {
                    print("Emptied PageInfo successfully")
                }
            }
            
            var rowsPageInfo = [[String: Any]]()
            
            for data in allData {
                let row = data.components(separatedBy: "$")
                var rowPageInfo = [String: Any]()
                
                rowPageInfo["id"] = row[0].int!
                rowPageInfo["pageName"] = row[1]
                rowPageInfo["segments"] = row[2]
                rowPageInfo["label"] = row[3]
                rowPageInfo["value"] = row[4]
                
                rowsPageInfo.append(rowPageInfo)
            }
            
            DatabaseManager.sharedInstance.bulkInsertIntoPageInfo(rows: rowsPageInfo) { (success, error) in
                if let err = error {
                    print("Error (bulkInsertIntoPageInfo): \(err.localizedDescription)")
                } else {
                    print("+++ bulkInsertIntoPageInfo successful +++")
                }
            }
            
            
            
            // update StrategistMaster table for key versionLibrary with value of newVersionLibary
            DatabaseManager.sharedInstance.updateValueForKeyInStrategistMaster(key: "infoVersion", value: newInfoVersion) { (success, error) in
                if let error = error {
                    print("Failed to update StrategistMaster table for key \"infoVersion\". [Error: \(error)]")
                } else {
                    print("StrategistMaster table updated for key \"infoVersion\" with value \"\(newInfoVersion)\"")
                    completion(true)
                }
            }
            
        } catch {
            print("Error: \(error)")
        }
    }
    
    private func updateDownloadProcess() {
        downloadProcessCounter += 1
        
        if downloadProcessCounter == 4 {
            // show Login or Home
            self.perform(#selector(showLoginOrHome), with: nil, afterDelay: 0.1)
        } else {
            print("Waiting for \(4 - downloadProcessCounter) processes to finish")
        }
    }
    
    @objc private func showLoginOrHome() {
        // show Login
        if let _ = SessionTokenManager.sharedInstance.token {
            showHome()
        } else {
            showLogin()
        }
    }
    
    private func showLogin() {
        splashView.loadingActivityIndicatorView.stopAnimating()
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
    private func showHome() {
        splashView.loadingActivityIndicatorView.stopAnimating()
        self.navigationController?.pushViewController(HomeViewController(), animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
