//
//  MyPositionsStepsPages.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 18/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class MyPositionsStepsPages: UIPageViewController {
    
    public var osoNavBar: OSONavigationBar?
    
    public var osoStepProgress: OSOStepProgress? {
        didSet {
            if let stepProgress = osoStepProgress {
                stepProgress.delegate = self
            }
        }
    }
    
    private var currentPageIndex: Int = -1
    
    fileprivate lazy var pages: [UIViewController] = {
        return [
            self.getViewController(forStep: "step_a")
        ]
    }()
    
    var pageControl = UIPageControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        //self.dataSource = self
        self.delegate = self
        
        if let firstVC = pages.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
            currentPageIndex = 0
        }
        
        //configurePageControl()
    }
    
    fileprivate func getViewController(forStep step: String) -> UIViewController
    {
        switch step {
        case "step_a":
            let vc = MyPositionsStepA()
            vc.updateType = "1"
            vc.delegate = self
            return vc
            
        default:
            return UIViewController()
        }
    }
    
    
    
    func configurePageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 150, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = pages.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor(white: 1.0, alpha: 0.2)
        self.view.addSubview(pageControl)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func toggleOSONavBar(enable: Bool) {
        if let navBar = osoNavBar {
            navBar.rightBarButtonEnabled(isEnabled: enable)
        }
    }
    
    public func showOptions() {
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: MyPositionsStepB.self) && i == currentPageIndex {
                let vc = pages[i] as! MyPositionsStepB
                vc.showMoreTools()
            } else if pages[i].isKind(of: MyPositionsStepA.self) && i == currentPageIndex {
                let vc = pages[i] as! MyPositionsStepA
                vc.refresh()
            }
        }
    }
    
    public func actionBack() {
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: MyPositionsStepB.self) && i == currentPageIndex {
                let vc = pages[i] as! MyPositionsStepB
                vc.actionBack()
            } else if pages[i].isKind(of: MyPositionsStepA.self) && i == currentPageIndex {
                let vc = pages[i] as! MyPositionsStepA
                vc.actionBack()
            }
        }
    }
    
    public func refresh() {
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: MyPositionsStepA.self) && i == currentPageIndex {
                let vc = pages[i] as! MyPositionsStepA
                vc.refresh()
            } else if pages[i].isKind(of: MyPositionsStepB.self) && i == currentPageIndex {
                let vc = pages[i] as! MyPositionsStepB
                vc.refresh()
            }
        }
    }
    
    /*public func share() {
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: MyPositionsStepB.self) && i == currentPageIndex {
                let vc = pages[i] as! MyPositionsStepB
                vc.share()
            }
        }
    }*/

}

// MARK:- MyPositionsStepADelegate

extension MyPositionsStepsPages: MyPositionsStepADelegate {
    
    func stepACompleted(summary: AnalyzerSummaryList, book: OSOBook) {
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: MyPositionsStepB.self) {
                pages[i].removeFromParent()
                pages.remove(at: i)
            }
        }
        
        let vc = MyPositionsStepB()
        vc.analyzerSummary = summary
        vc.selectedBook = book
        vc.osoNavBar = osoNavBar
        vc.delegate = self
        pages.append(vc)
        pageControl.numberOfPages = pages.count
        
        setViewControllers([pages[1]], direction: .forward, animated: true) { (finished) in
            if let stepProgress = self.osoStepProgress {
                stepProgress.setCurrentStepAsCompleted()
            }
            
            self.currentPageIndex = 1
        }
        
        toggleOSONavBar(enable: true)
        
        if let image = UIImage(named: "icon_refresh") {
            osoNavBar?.updateRightBarButtonImage(image: image)
        }
        
        if let bookTitle = book.title {
            osoNavBar?.showSubTitle(subTitleText: bookTitle)
        }
    }
    
}

// MARK:- MyPositionsStepBDelegate

extension MyPositionsStepsPages: MyPositionsStepBDelegate {
    
    /*func stepBAnalyze(positionDetails: PositionDetails, analyzerSummary: AnalyzerSummaryList, xAxes: String, yAxes: String, maxDays: Int, upperBound: Double, lowerBound: Double) {
        print("stepBAnalyze")
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: FindStrategyStepD.self) {
                pages[i].removeFromParent()
                pages.remove(at: i)
            }
        }
        
        pageControl.numberOfPages = pages.count
        
        let vc = FindStrategyStepD()
        let data: [String: Any] = ["xAxes": xAxes,
                                   "yAxes": yAxes,
                                   "maxDays": maxDays,
                                   "lowerBound": lowerBound,
                                   "upperBound": upperBound,
                                   "positionDetails": positionDetails,
                                   "analyzerSummary": analyzerSummary]
        vc.stepDData = data
        pages.append(vc)
        pageControl.numberOfPages = pages.count
        
        setViewControllers([pages[2]], direction: .forward, animated: true) { (finished) in
            if let stepProgress = self.osoStepProgress {
                stepProgress.setCurrentStepAsCompleted()
            }
            
            self.currentPageIndex = 2
        }
        
        toggleOSONavBar(enable: false)
    }*/
    
    /*func stepBErrorOccurred() {
        if let stepProgress = self.osoStepProgress {
            stepProgress.setCurrentStepAsIncomplete()
        }
    }*/
}

// MARK:- OSOStepProgressDelegate

extension MyPositionsStepsPages: OSOStepProgressDelegate {
    
    func stepSelected(stepIndex: Int, isPreviousStep: Bool) {
        
        currentPageIndex = stepIndex
        
        setViewControllers([pages[stepIndex]], direction: isPreviousStep ? .reverse : .forward, animated: true) { (finished) in
            
            for i in (0..<self.pages.count).reversed() {
                if i > stepIndex {
                    /*if self.pages[i].isKind(of: FindStrategyStepD.self) {
                        self.pages[i].removeFromParent()
                        self.pages.remove(at: i)
                        self.toggleOSONavBar(enable: true)
                    } else*/
                    if self.pages[i].isKind(of: MyPositionsStepB.self) {
                        let vc = self.pages[i] as! MyPositionsStepB
                        vc.actionBack()
                        
                        self.pages[i].removeFromParent()
                        self.pages.remove(at: i)
                        self.toggleOSONavBar(enable: true)
                        
                        if let image = UIImage(named: "icon_refresh") {
                            self.osoNavBar?.updateRightBarButtonImage(image: image)
                        }
                        
                        self.osoNavBar?.hideSubTitle()
                    }
                }
            }
            
            self.pageControl.numberOfPages = self.pages.count
            //self.dataSource = nil
            
        }
    }
}

// MARK:- UIPageViewControllerDelegate

extension MyPositionsStepsPages: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let pageContentViewController = pageViewController.viewControllers![0]
        
        self.pageControl.currentPage = pages.index(of: pageContentViewController)!
    }
    
    
}

// MARK:- UIPageViewControllerDataSource

extension MyPositionsStepsPages: UIPageViewControllerDataSource {
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0          else { return nil /*pages.last*/ }
        guard pages.count > previousIndex else { return nil }
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < pages.count else { return nil /*pages.first*/ }
        guard pages.count > nextIndex else { return nil }
        return pages[nextIndex]
    }
    
    
}
