//
//  AddNewLegViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 01/04/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SearchTextField
import SVProgressHUD
import ZAlertView
import Sheeeeeeeeet
import ActionSheetPicker_3_0
import DLRadioButton

protocol AddNewLegDelegate: class {
    func newLegAdded(managerDetailLeg: ManagerDetailLegs)
}

extension AddNewLegDelegate {
    func newLegAdded(managerDetailLeg: ManagerDetailLegs) {}
}

class AddNewLegViewController: UIViewController {
    
    weak var delegate: AddNewLegDelegate?
    
    // navigation bar
    var osoNavBar: OSONavigationBar!
    
    public var analyzerSummary: AnalyzerSummaryList?
    public var toolViewController: OpenCallToolViewController?
    
    // constraints
    private var scrollViewContainerBottomAnchorConstraint: NSLayoutConstraint!
    private var formHeightConstraint: NSLayoutConstraint!
    private var lblStrikeTopAnchorConstraint: NSLayoutConstraint!
    private var lblStrikeHeightAnchorConstraint: NSLayoutConstraint!
    private var borderStrikeHeightAnchorConstraint: NSLayoutConstraint!
    private var lblCallPutOptionTopAnchorConstraint: NSLayoutConstraint!
    private var lblCallPutOptionHeightAnchorConstraint: NSLayoutConstraint!
    private var borderCallPutOptionHeightAnchorConstraint: NSLayoutConstraint!
    private var lblQuantityValueWidthAnchorConstraint: NSLayoutConstraint!
    
    private var fieldVerticalGap: CGFloat = 4
    private var fieldHeight: CGFloat = 30
    private var strikes: [String]?
    public var cmp: String?
    
    private var radioButtons = [DLRadioButton]()
    private var selectedRadioButton: Int = 0
    
    // lot
    private var lot: Int = -1
    private var lots: [Int]?
    private var selectedLot = 0
    
    // expiries
    private var selectedExpiry: Int = 0
    private var expiries: [String]?
    
    private let scrollViewContainer: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let formView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let btnAdd: UIButton = {
        let button = UIButton()
        button.setTitle("ADD", for: .normal)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblPositionName: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Position Name"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPositionNameValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.text = "Position Name"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSymbol: UILabel = {
        let label = UILabel()
        label.text = "Symbol"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let borderSymbol: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblSymbolValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let borderPositionName: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.text = "CMP"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMPValue: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.text = "-"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblMargin: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Margin"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfMargin: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.attributedPlaceholder = NSAttributedString(string: "(Optional)", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.3), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderMargin: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblPositionType: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Type"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let borderPositionType: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let dropdownPositionType: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblBaseExpiry: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Expiry"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownBaseExpiry: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderExpiry: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblStrike: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Strike"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dropdownStrike: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.clipsToBounds = true
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderStrike: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblCallPutOption: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Option"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownCallPutOption: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.clipsToBounds = true
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderCallPutOption: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblPrice: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Price"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfPrice: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let btnFetchCMP: FetchCMPButton = {
        let button = FetchCMPButton()
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.7), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.setImage(UIImage(named: "icon_import")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.layer.cornerRadius = 3
        button.layer.masksToBounds = true
        button.clipsToBounds = true
        button.setTitle("Get CMP", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderPrice: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblQuantity: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Quantity"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQuantityValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = Constants.UIConfig.themeColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfQuantity: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let btnAddQty: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_increment")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDeleteQty(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnDeleteQty: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_decrement")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDeleteQty(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderQuantity: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let radioButtonBuy: DLRadioButton = {
        let radioButton = DLRadioButton()
        radioButton.isMultipleSelectionEnabled = false
        radioButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 14)
        radioButton.setTitle("Buy", for: [])
        radioButton.setTitleColor(UIColor(white: 1.0, alpha: 0.6), for: .normal)
        radioButton.setTitleColor(Constants.UIConfig.themeColor, for: UIControl.State.selected)
        radioButton.iconSize = 20
        radioButton.indicatorSize = 10
        radioButton.marginWidth = 10
        radioButton.iconColor = UIColor(white: 1.0, alpha: 0.6)
        radioButton.indicatorColor = Constants.UIConfig.themeColor
        radioButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        radioButton.addTarget(self, action: #selector(radioButtonSelected(_:)), for: UIControl.Event.touchUpInside)
        radioButton.tag = 1
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        return radioButton
    }()
    
    let radioButtonSell: DLRadioButton = {
        let radioButton = DLRadioButton()
        radioButton.isMultipleSelectionEnabled = false
        radioButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 14)
        radioButton.setTitle("Sell", for: [])
        radioButton.setTitleColor(UIColor(white: 1.0, alpha: 0.6), for: .normal)
        radioButton.setTitleColor(Constants.UIConfig.themeColor, for: UIControl.State.selected)
        radioButton.iconSize = 20
        radioButton.indicatorSize = 10
        radioButton.marginWidth = 10
        radioButton.iconColor = UIColor(white: 1.0, alpha: 0.6)
        radioButton.indicatorColor = Constants.UIConfig.themeColor
        radioButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        radioButton.addTarget(self, action: #selector(radioButtonSelected(_:)), for: UIControl.Event.touchUpInside)
        radioButton.tag = 2
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        return radioButton
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let summary = analyzerSummary else { return }
        
        if let symbol = summary.symbol {
            lblSymbolValue.text = symbol
            getStrikesExpiriesAndLot(symbolName: symbol)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        setupViews()
    }
    

    private func setupViews() {
        
        guard let summary = analyzerSummary else { return }
        
        // btnAdd
        view.addSubview(btnAdd)
        btnAdd.addTarget(self, action: #selector(actionAdd(_:)), for: .touchUpInside)
        btnAdd.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        btnAdd.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnAdd.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        if #available(iOS 11.0, *) {
            btnAdd.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        } else {
            btnAdd.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        }
        
        // MARK: scrollview container
        view.addSubview(scrollViewContainer)
        scrollViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollViewContainer.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 0).isActive = true
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnAdd, attribute: .top, multiplier: 1.0, constant: -10)
        scrollViewContainerBottomAnchorConstraint.isActive = true
        
        // MARK: form view
        scrollViewContainer.addSubview(formView)
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .top, relatedBy: .equal, toItem: scrollViewContainer, attribute: .top, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .left, relatedBy: .equal, toItem: scrollViewContainer, attribute: .left, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .right, relatedBy: .equal, toItem: scrollViewContainer, attribute: .right, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: scrollViewContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
        formHeightConstraint = NSLayoutConstraint(item: formView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 284)
        formHeightConstraint.isActive = true
        
        scrollViewContainer.contentSize = formView.size
        
        // MARK: Position Name
        // lblPositionName
        formView.addSubview(lblPositionName)
        lblPositionName.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblPositionName.topAnchor.constraint(equalTo: formView.topAnchor, constant: fieldVerticalGap + 4).isActive = true
        lblPositionName.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPositionName.widthAnchor.constraint(equalToConstant: (lblPositionName.text?.sizeOfString(usingFont: lblPositionName.font).width)! + 4).isActive = true
        
        
        
        
        // lblPositionNameValue
        formView.addSubview(lblPositionNameValue)
        lblPositionNameValue.leftAnchor.constraint(equalTo: lblPositionName.rightAnchor, constant: 8).isActive = true
        lblPositionNameValue.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        lblPositionNameValue.topAnchor.constraint(equalTo: lblPositionName.topAnchor, constant: 0).isActive = true
        lblPositionNameValue.heightAnchor.constraint(equalTo: lblPositionName.heightAnchor, multiplier: 1.0).isActive = true
        
        if let positionName = summary.positionName {
            lblPositionNameValue.text = positionName
        }
        
        // borderPositionName
        /*formView.addSubview(borderPositionName)
        borderPositionName.leftAnchor.constraint(equalTo: lblPositionNameValue.leftAnchor, constant: 0).isActive = true
        borderPositionName.rightAnchor.constraint(equalTo: lblPositionNameValue.rightAnchor, constant: 0).isActive = true
        borderPositionName.bottomAnchor.constraint(equalTo: lblPositionNameValue.bottomAnchor, constant: 0).isActive = true
        borderPositionName.heightAnchor.constraint(equalToConstant: 1).isActive = true*/
        
        // MARK: Symbol
        formView.addSubview(lblSymbol)
        lblSymbol.topAnchor.constraint(equalTo: lblPositionName.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblSymbol.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSymbol.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblSymbol.widthAnchor.constraint(equalToConstant: (lblSymbol.text?.sizeOfString(usingFont: lblSymbol.font).width)! + 4).isActive = true
        
        // lblSymbolValue
        formView.addSubview(lblSymbolValue)
        lblSymbolValue.topAnchor.constraint(equalTo: lblSymbol.topAnchor, constant: 0).isActive = true
        lblSymbolValue.heightAnchor.constraint(equalTo: lblSymbol.heightAnchor, multiplier: 1.0).isActive = true
        lblSymbolValue.leftAnchor.constraint(equalTo: lblSymbol.rightAnchor, constant: 8).isActive = true
        lblSymbolValue.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        
        // bottomBorder
        /*formView.addSubview(borderSymbol)
        borderSymbol.topAnchor.constraint(equalTo: lblSymbolValue.bottomAnchor, constant: 0).isActive = true
        borderSymbol.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderSymbol.leftAnchor.constraint(equalTo: lblSymbolValue.leftAnchor, constant: -4).isActive = true
        borderSymbol.rightAnchor.constraint(equalTo: lblSymbolValue.rightAnchor, constant: 0).isActive = true*/
        
        
        // MARK: CMP
        // lblCMP
        formView.addSubview(lblCMP)
        lblCMP.topAnchor.constraint(equalTo: lblSymbol.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblCMP.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCMP.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblCMP.widthAnchor.constraint(equalToConstant: (lblCMP.text?.sizeOfString(usingFont: lblCMP.font).width)! + 4).isActive = true
        
        // lblCMPValue
        formView.addSubview(lblCMPValue)
        lblCMPValue.topAnchor.constraint(equalTo: lblCMP.topAnchor, constant: 0).isActive = true
        lblCMPValue.heightAnchor.constraint(equalTo: lblCMP.heightAnchor, multiplier: 1.0).isActive = true
        lblCMPValue.leftAnchor.constraint(equalTo: lblCMP.rightAnchor, constant: 8).isActive = true
        lblCMPValue.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        // MARK: lblPositionType
        formView.addSubview(lblPositionType)
        lblPositionType.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblPositionType.topAnchor.constraint(equalTo: lblCMP.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblPositionType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPositionType.widthAnchor.constraint(equalToConstant: (lblPositionType.text?.sizeOfString(usingFont: lblPositionType.font).width)! + 8).isActive = true
        
        // dropdownPositionType
        formView.addSubview(dropdownPositionType)
        dropdownPositionType.setTitle("Fut", for: .normal)
        dropdownPositionType.addTarget(self, action: #selector(showPositionType(_:)), for: .touchUpInside)
        dropdownPositionType.leftAnchor.constraint(equalTo: lblPositionType.rightAnchor, constant: 8).isActive = true
        dropdownPositionType.centerYAnchor.constraint(equalTo: lblPositionType.centerYAnchor, constant: 0).isActive = true
        dropdownPositionType.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownPositionType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // borderPositionType
        formView.addSubview(borderPositionType)
        borderPositionType.leftAnchor.constraint(equalTo: dropdownPositionType.leftAnchor, constant: 0).isActive = true
        borderPositionType.rightAnchor.constraint(equalTo: dropdownPositionType.rightAnchor, constant: 0).isActive = true
        borderPositionType.bottomAnchor.constraint(equalTo: dropdownPositionType.bottomAnchor, constant: 0).isActive = true
        borderPositionType.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // MARK: lblBaseExpiry
        formView.addSubview(lblBaseExpiry)
        lblBaseExpiry.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblBaseExpiry.topAnchor.constraint(equalTo: lblPositionType.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblBaseExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblBaseExpiry.widthAnchor.constraint(equalToConstant: (lblBaseExpiry.text?.sizeOfString(usingFont: lblBaseExpiry.font).width)! + 8).isActive = true
        
        // dropdownBaseExpiry
        formView.addSubview(dropdownBaseExpiry)
        dropdownBaseExpiry.addTarget(self, action: #selector(showBaseExpiry(_:)), for: .touchUpInside)
        dropdownBaseExpiry.leftAnchor.constraint(equalTo: lblBaseExpiry.rightAnchor, constant: 8).isActive = true
        dropdownBaseExpiry.centerYAnchor.constraint(equalTo: lblBaseExpiry.centerYAnchor, constant: 0).isActive = true
        dropdownBaseExpiry.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownBaseExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // borderExpiry
        formView.addSubview(borderExpiry)
        borderExpiry.leftAnchor.constraint(equalTo: dropdownBaseExpiry.leftAnchor, constant: 0).isActive = true
        borderExpiry.rightAnchor.constraint(equalTo: dropdownBaseExpiry.rightAnchor, constant: 0).isActive = true
        borderExpiry.bottomAnchor.constraint(equalTo: dropdownBaseExpiry.bottomAnchor, constant: 0).isActive = true
        borderExpiry.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // MARK: Strike
        formView.addSubview(lblStrike)
        lblStrike.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        //lblStrike.topAnchor.constraint(equalTo: lblBaseExpiry.bottomAnchor, constant: fieldVerticalGap).isActive = true
        //lblStrike.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblStrike.widthAnchor.constraint(equalToConstant: (lblStrike.text?.sizeOfString(usingFont: lblStrike.font).width)! + 8).isActive = true
        lblStrikeTopAnchorConstraint = NSLayoutConstraint(item: lblStrike, attribute: .top, relatedBy: .equal, toItem: lblBaseExpiry, attribute: .bottom, multiplier: 1.0, constant: 0)
        lblStrikeTopAnchorConstraint.isActive = true
        lblStrikeHeightAnchorConstraint = NSLayoutConstraint(item: lblStrike, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        lblStrikeHeightAnchorConstraint.isActive = true
        
        
        // dropdownStrike
        formView.addSubview(dropdownStrike)
        dropdownStrike.addTarget(self, action: #selector(showStrikeForInstrument(_:)), for: .touchUpInside)
        dropdownStrike.leftAnchor.constraint(equalTo: lblStrike.rightAnchor, constant: 8).isActive = true
        dropdownStrike.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        dropdownStrike.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownStrike.heightAnchor.constraint(equalTo: lblStrike.heightAnchor, constant: 0).isActive = true
        
        // borderStrike
        formView.addSubview(borderStrike)
        borderStrike.leftAnchor.constraint(equalTo: dropdownStrike.leftAnchor, constant: 0).isActive = true
        borderStrike.rightAnchor.constraint(equalTo: dropdownStrike.rightAnchor, constant: 0).isActive = true
        borderStrike.bottomAnchor.constraint(equalTo: dropdownStrike.bottomAnchor, constant: 0).isActive = true
        borderStrikeHeightAnchorConstraint = NSLayoutConstraint(item: borderStrike, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        borderStrikeHeightAnchorConstraint.isActive = true
        
        // MARK: CE/PE Option
        formView.addSubview(lblCallPutOption)
        lblCallPutOption.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        //lblCallPutOption.topAnchor.constraint(equalTo: lblStrike.bottomAnchor, constant: fieldVerticalGap).isActive = true
        //lblCallPutOption.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCallPutOption.widthAnchor.constraint(equalToConstant: (lblCallPutOption.text?.sizeOfString(usingFont: lblCallPutOption.font).width)! + 8).isActive = true
        lblCallPutOptionTopAnchorConstraint = NSLayoutConstraint(item: lblCallPutOption, attribute: .top, relatedBy: .equal, toItem: lblStrike, attribute: .bottom, multiplier: 1.0, constant: 0)
        lblCallPutOptionTopAnchorConstraint.isActive = true
        lblCallPutOptionHeightAnchorConstraint = NSLayoutConstraint(item: lblCallPutOption, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        lblCallPutOptionHeightAnchorConstraint.isActive = true
        
        // dropdownCallPutOption
        formView.addSubview(dropdownCallPutOption)
        dropdownCallPutOption.setTitle("CE", for: .normal)
        dropdownCallPutOption.addTarget(self, action: #selector(showCallPutOption(_:)), for: .touchUpInside)
        dropdownCallPutOption.leftAnchor.constraint(equalTo: lblCallPutOption.rightAnchor, constant: 8).isActive = true
        dropdownCallPutOption.topAnchor.constraint(equalTo: lblCallPutOption.topAnchor, constant: 0).isActive = true
        dropdownCallPutOption.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownCallPutOption.heightAnchor.constraint(equalTo: lblCallPutOption.heightAnchor, constant: 0).isActive = true
        
        // borderCallPutOption
        formView.addSubview(borderCallPutOption)
        borderCallPutOption.leftAnchor.constraint(equalTo: dropdownCallPutOption.leftAnchor, constant: 0).isActive = true
        borderCallPutOption.rightAnchor.constraint(equalTo: dropdownCallPutOption.rightAnchor, constant: 0).isActive = true
        borderCallPutOption.bottomAnchor.constraint(equalTo: dropdownCallPutOption.bottomAnchor, constant: 0).isActive = true
        borderCallPutOptionHeightAnchorConstraint = NSLayoutConstraint(item: borderCallPutOption, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        borderCallPutOptionHeightAnchorConstraint.isActive = true
        
        // MARK: Price
        formView.addSubview(lblPrice)
        lblPrice.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblPrice.topAnchor.constraint(equalTo: lblCallPutOption.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblPrice.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPrice.widthAnchor.constraint(equalToConstant: (lblPrice.text?.sizeOfString(usingFont: lblPrice.font).width)! + 4).isActive = true
        
        formView.addSubview(btnFetchCMP)
        btnFetchCMP.addTarget(self, action: #selector(fetchCMP(_:)), for: .touchUpInside)
        btnFetchCMP.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        btnFetchCMP.bottomAnchor.constraint(equalTo: lblPrice.bottomAnchor, constant: 0).isActive = true
        btnFetchCMP.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        btnFetchCMP.widthAnchor.constraint(equalToConstant: (btnFetchCMP.titleLabel?.text?.sizeOfString(usingFont: (btnFetchCMP.titleLabel?.font)!).width)! + 44).isActive = true
        
        formView.addSubview(tfPrice)
        addToolBar(textField: tfPrice)
        tfPrice.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 8).isActive = true
        tfPrice.rightAnchor.constraint(equalTo: btnFetchCMP.leftAnchor, constant: -8).isActive = true
        tfPrice.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        tfPrice.heightAnchor.constraint(equalTo: lblPrice.heightAnchor, multiplier: 1).isActive = true
        
        
        
        formView.addSubview(borderPrice)
        borderPrice.leftAnchor.constraint(equalTo: tfPrice.leftAnchor, constant: 0).isActive = true
        borderPrice.rightAnchor.constraint(equalTo: tfPrice.rightAnchor, constant: 0).isActive = true
        borderPrice.bottomAnchor.constraint(equalTo: tfPrice.bottomAnchor, constant: 0).isActive = true
        borderPrice.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // MARK: Quantity
        formView.addSubview(lblQuantity)
        lblQuantity.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblQuantity.topAnchor.constraint(equalTo: lblPrice.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblQuantity.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblQuantity.widthAnchor.constraint(equalToConstant: (lblQuantity.text?.sizeOfString(usingFont: lblQuantity.font).width)! + 4).isActive = true
        
        formView.addSubview(btnAddQty)
        btnAddQty.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        btnAddQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btnAddQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btnAddQty.centerYAnchor.constraint(equalTo: lblQuantity.centerYAnchor, constant: 0).isActive = true
        
        formView.addSubview(lblQuantityValue)
        lblQuantityValue.text = "0"
        lblQuantityValue.rightAnchor.constraint(equalTo: btnAddQty.leftAnchor, constant: -10).isActive = true
        lblQuantityValue.bottomAnchor.constraint(equalTo: lblQuantity.bottomAnchor, constant: 0).isActive = true
        lblQuantityValue.heightAnchor.constraint(equalTo: lblQuantity.heightAnchor, constant: 0).isActive = true
        lblQuantityValueWidthAnchorConstraint = NSLayoutConstraint(item: lblQuantityValue, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (lblQuantityValue.text?.sizeOfString(usingFont: lblQuantityValue.font).width)! + 2)
        lblQuantityValueWidthAnchorConstraint.isActive = true
        
        formView.addSubview(btnDeleteQty)
        btnDeleteQty.rightAnchor.constraint(equalTo: lblQuantityValue.leftAnchor, constant: -10).isActive = true
        btnDeleteQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btnDeleteQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btnDeleteQty.centerYAnchor.constraint(equalTo: lblQuantity.centerYAnchor, constant: 0).isActive = true
        
        /*formView.addSubview(tfQuantity)
        addToolBar(textField: tfQuantity)
        tfQuantity.leftAnchor.constraint(equalTo: lblQuantity.rightAnchor, constant: 8).isActive = true
        tfQuantity.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfQuantity.topAnchor.constraint(equalTo: lblQuantity.topAnchor, constant: 0).isActive = true
        tfQuantity.heightAnchor.constraint(equalTo: lblQuantity.heightAnchor, multiplier: 1).isActive = true
        
        formView.addSubview(borderQuantity)
        borderQuantity.leftAnchor.constraint(equalTo: tfQuantity.leftAnchor, constant: 0).isActive = true
        borderQuantity.rightAnchor.constraint(equalTo: tfQuantity.rightAnchor, constant: 0).isActive = true
        borderQuantity.bottomAnchor.constraint(equalTo: tfQuantity.bottomAnchor, constant: 0).isActive = true
        borderQuantity.heightAnchor.constraint(equalToConstant: 1).isActive = true*/
        
        // lblBookType
        /*formView.addSubview(lblBookType)
         lblBookType.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
         lblBookType.topAnchor.constraint(equalTo: lblQuantity.bottomAnchor, constant: fieldVerticalGap).isActive = true
         lblBookType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
         lblBookType.widthAnchor.constraint(equalToConstant: (lblBookType.text?.sizeOfString(usingFont: lblBookType.font).width)! + 8).isActive = true
         
         // dropdownBookType
         formView.addSubview(dropdownBookType)
         dropdownBookType.setTitle("Select Book Type", for: .normal)
         //dropdownBookType.addTarget(self, action: #selector(showBookTypePicker(_:)), for: .touchUpInside)
         dropdownBookType.leftAnchor.constraint(equalTo: lblBookType.rightAnchor, constant: 8).isActive = true
         dropdownBookType.centerYAnchor.constraint(equalTo: lblBookType.centerYAnchor, constant: 0).isActive = true
         dropdownBookType.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
         dropdownBookType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
         
         // borderBookType
         formView.addSubview(borderBookType)
         borderBookType.leftAnchor.constraint(equalTo: dropdownBookType.leftAnchor, constant: 0).isActive = true
         borderBookType.rightAnchor.constraint(equalTo: dropdownBookType.rightAnchor, constant: 0).isActive = true
         borderBookType.bottomAnchor.constraint(equalTo: dropdownBookType.bottomAnchor, constant: 0).isActive = true
         borderBookType.heightAnchor.constraint(equalToConstant: 1).isActive = true*/
        
        // MARK: Radio Button (Buy/Sell)
        formView.addSubview(radioButtonBuy)
        radioButtonBuy.isSelected = true
        selectedRadioButton = radioButtonBuy.tag
        radioButtonBuy.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        radioButtonBuy.topAnchor.constraint(equalTo: lblQuantity.bottomAnchor, constant: 8).isActive = true
        radioButtonBuy.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        radioButtonBuy.rightAnchor.constraint(equalTo: formView.centerXAnchor, constant: -5).isActive = true
        
        formView.addSubview(radioButtonSell)
        radioButtonSell.isSelected = false
        radioButtons.append(radioButtonSell)
        radioButtonBuy.otherButtons = radioButtons
        radioButtonSell.leftAnchor.constraint(equalTo: formView.centerXAnchor, constant: 5).isActive = true
        radioButtonSell.topAnchor.constraint(equalTo: radioButtonBuy.topAnchor, constant: 0).isActive = true
        radioButtonSell.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        radioButtonSell.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        view.layoutSubviews()
        view.layoutIfNeeded()
        
        
    }
    
    @objc private func fetchCMP(_ sender: UIButton) {
        print("fetchCMP")
        resignAllResponders()
        
        if let symbol = lblSymbolValue.text?.trimmed {
            if symbol.isEmpty {
                showZAlertView(withTitle: "Error", andMessage: "Please enter a symbol", cancelTitle: "OK", isError: true, completion: {
                    self.lblSymbolValue.becomeFirstResponder()
                })
            } else {
                if let instrumentType = dropdownPositionType.titleLabel?.text {
                    
                    if instrumentType.isEmpty {
                        showZAlertView(withTitle: "Error", andMessage: "Please select an Instrument.", cancelTitle: "OK", isError: true, completion: {})
                    } else {
                        if let expiry = dropdownBaseExpiry.titleLabel?.text {
                            if expiry.isEmpty {
                                showZAlertView(withTitle: "Error", andMessage: "Please select Expiry.", cancelTitle: "OK", isError: true, completion: {})
                            } else {
                                if instrumentType.uppercased() == "FUT" {
                                    
                                    //----------------------------- NEW CODE: START --------------------------//
                                    var instruments = [String]()
                                    instruments.append("\(symbol.uppercased())_\(expiry)_1")
                                    
                                    SVProgressHUD.show()
                                    OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, price, oi, volume) in
                                        SVProgressHUD.dismiss()
                                        if let err = error {
                                            self.tfPrice.text = "-"
                                            self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                                        } else {
                                            if let priceArray = price {
                                                if priceArray.indices.contains(0) {
                                                    self.tfPrice.text = priceArray[0].roundToDecimal(2).toString()
                                                } else {
                                                    self.tfPrice.text = "-"
                                                }
                                            } else {
                                                self.tfPrice.text = "-"
                                                self.showZAlertView(withTitle: "Error", andMessage: "Could not get price. Please try again.", cancelTitle: "OK", isError: true, completion: {})
                                            }
                                        }
                                    }
                                    //------------------------------ NEW CODE: END ---------------------------//
                                    
                                    //----------------------------- OLD CODE: START --------------------------//
                                    /*SVProgressHUD.show()
                                     OSOWebService.sharedInstance.getCMPForInstrument(instrumentType: instrumentType, symbol: symbol, expiry: expiry, strike: "", optType: "") { (cmpValue) in
                                     SVProgressHUD.dismiss()
                                     self.tfPrice.text = cmpValue
                                     }*/
                                    //----------------------------- OLD CODE: END --------------------------//
                                } else if instrumentType.uppercased() == "OPT" {
                                    guard let strike = dropdownStrike.titleLabel?.text else {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select strike.", cancelTitle: "OK", isError: true, completion: {})
                                        return
                                    }
                                    
                                    guard let strikeDblVal = strike.double() else  {
                                        showZAlertView(withTitle: "Error", andMessage: "Strike value is not valid.", cancelTitle: "OK", isError: true, completion: {})
                                        return
                                    }
                                    
                                    guard let optType = dropdownCallPutOption.titleLabel?.text else {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select Option Type.", cancelTitle: "OK", isError: true, completion: {})
                                        return
                                    }
                                    
                                    if strike.isEmpty {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select strike.", cancelTitle: "OK", isError: true, completion: {})
                                    } else if optType.isEmpty {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select Option Type.", cancelTitle: "OK", isError: true, completion: {})
                                    } else {
                                        
                                        //----------------------------- NEW CODE: START --------------------------//
                                        var instruments = [String]()
                                        instruments.append("\(symbol.uppercased())_\(expiry)_1_\(optType.uppercased())_\(strikeDblVal.toString())")
                                        
                                        SVProgressHUD.show()
                                        OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, price, oi, volume) in
                                            SVProgressHUD.dismiss()
                                            if let err = error {
                                                self.tfPrice.text = "-"
                                                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                                            } else {
                                                if let priceArray = price {
                                                    if priceArray.indices.contains(0) {
                                                        self.tfPrice.text = priceArray[0].roundToDecimal(2).toString()
                                                    } else {
                                                        self.tfPrice.text = "-"
                                                    }
                                                } else {
                                                    self.tfPrice.text = "-"
                                                    self.showZAlertView(withTitle: "Error", andMessage: "Could not get price. Please try again.", cancelTitle: "OK", isError: true, completion: {})
                                                }
                                            }
                                        }
                                        //------------------------------ NEW CODE: END ---------------------------//
                                        
                                        //----------------------------- OLD CODE: START --------------------------//
                                        /*SVProgressHUD.show()
                                         OSOWebService.sharedInstance.getCMPForInstrument(instrumentType: instrumentType, symbol: symbol, expiry: expiry, strike: strike, optType: optType) { (cmpValue) in
                                         SVProgressHUD.dismiss()
                                         self.tfPrice.text = cmpValue
                                         }*/
                                        //----------------------------- OLD CODE: END --------------------------//
                                    }
                                }
                            }
                        } else {
                            showZAlertView(withTitle: "Error", andMessage: "Please select Expiry.", cancelTitle: "OK", isError: true, completion: {})
                        }
                    }
                }
            }
        }
    }
    
    private func getStrikesExpiriesAndLot(symbolName: String) {
        if let instrument = dropdownPositionType.titleLabel?.text {
            if instrument.isEmpty {
                self.showZAlertView(withTitle: "Error", andMessage: "Instrument not found.", cancelTitle: "OK", isError: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                DatabaseManager.sharedInstance.getExpiriesStrikesAndLot(symbol: symbolName, instrument: instrument) { (expiries, strikes, lots) in
                    if let expiriesArray = expiries {
                        self.expiries = expiriesArray
                        self.selectedExpiry = 0
                        self.dropdownBaseExpiry.setTitle("\(expiriesArray[self.selectedExpiry])", for: .normal)
                    }
                    
                    if let strikesArray = strikes {
                        self.strikes = strikesArray
                        
                        if let cmpValue = self.lblCMPValue.text?.double() {
                            let closestIndex = self.closestIndexInStrikes(strikes: strikesArray, CMP: cmpValue)
                            self.dropdownStrike.setTitle(strikesArray[closestIndex], for: .normal)
                        }
                    }
                    
                    if let lotsArray = lots {
                        self.lots = lotsArray
                        self.selectedLot = 0
                        
                        self.lot = lotsArray[self.selectedLot]
                        
                        DispatchQueue.main.async {
                            self.lblQuantityValue.text = "\(self.lot)"
                        }
                        
                        self.lblQuantityValueWidthAnchorConstraint.constant = "\(self.lot)".sizeOfString(usingFont: self.lblQuantityValue.font).width + 2
                        self.view.layoutIfNeeded()
                    }
                    
                    /*if let lotValue = lot?.int {
                        self.lot = lotValue
                        //self.tfQuantity.text = lotValue
                        
                        DispatchQueue.main.async {
                            self.lblQuantityValue.text = "\(lotValue)"
                        }
                        
                        self.lblQuantityValueWidthAnchorConstraint.constant = "\(lotValue)".sizeOfString(usingFont: self.lblQuantityValue.font).width + 2
                        self.view.layoutIfNeeded()
                    }*/
                    
                    if let firstExpiry = self.expiries?.first {
                        self.getCMP(forSymbol: symbolName, expiry: firstExpiry)
                    } else {
                        self.showZAlertView(withTitle: "Error", andMessage: "Expiry not found", cancelTitle: "OK", isError: true, completion: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "Instrument not found.", cancelTitle: "OK", isError: true, completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    private func getCMP(forSymbol symbol: String, expiry: String) {
        
        SVProgressHUD.show()
        
        OSOWebService.sharedInstance.getPrice(forInstrument: "\(symbol)_\(expiry)_1") { (error, price) in
            SVProgressHUD.dismiss()
            if let err = error {
                
                print("Error: \(err.localizedDescription)")
                SVProgressHUD.dismiss(completion: {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription == "0" ? "Invalid Session. Please login again." : err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                        //self.delegate?.stepCErrorOccured()
                    })
                })
                
            } else {
                if let cmpValue = price {
                    print("CMP = \(cmpValue)")
                    self.lblCMPValue.text = cmpValue.toString()
                } else {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func updateFieldConstraints(positionType: String) {
        if positionType == "Fut" {
            lblStrikeTopAnchorConstraint.constant = 0
            lblStrikeHeightAnchorConstraint.constant = 0
            lblCallPutOptionTopAnchorConstraint.constant = 0
            lblCallPutOptionHeightAnchorConstraint.constant = 0
            borderStrikeHeightAnchorConstraint.constant = 0
            borderCallPutOptionHeightAnchorConstraint.constant = 0
            formHeightConstraint.constant = 318
        } else if positionType == "Opt" {
            lblStrikeTopAnchorConstraint.constant = fieldVerticalGap
            lblStrikeHeightAnchorConstraint.constant = fieldHeight
            lblCallPutOptionTopAnchorConstraint.constant = fieldVerticalGap
            lblCallPutOptionHeightAnchorConstraint.constant = fieldHeight
            borderStrikeHeightAnchorConstraint.constant = 1
            borderCallPutOptionHeightAnchorConstraint.constant = 1
            formHeightConstraint.constant = 386
        }
        
        self.formView.layoutIfNeeded()
        self.scrollViewContainer.contentSize = self.formView.size
    }
    
    func closestIndexInStrikes(strikes: [String], CMP: Double) -> Int {
        
        var smallestDifference = (CMP - strikes[0].double()!).abs
        var closest = 0
        
        for i in 0..<strikes.count {
            let currentDifference = (CMP - strikes[i].double()!).abs
            
            if currentDifference < smallestDifference {
                smallestDifference = currentDifference
                closest = i
            }
        }
        
        return closest
    }
    
    @objc private func actionAdd(_ sender: UIButton) {
        
        resignAllResponders()
        guard let summary = analyzerSummary else { return }
        
        guard let symbol = summary.symbol else { return }
        guard let positionName = summary.positionName else { return }
        guard let price = tfPrice.text?.trimmed else { return }
        //guard let quantity = tfQuantity.text?.trimmed else { return }
        guard let quantity = lblQuantityValue.text?.trimmed else { return }
        guard let instrument = dropdownPositionType.titleLabel?.text else { return }
        
        if price.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter price.", cancelTitle: "OK", isError: true, completion: {
                self.tfPrice.becomeFirstResponder()
            })
        } else if price == "0" {
            showZAlertView(withTitle: "Error", andMessage: "Price cannot be zero.", cancelTitle: "OK", isError: true, completion: {
                self.tfPrice.becomeFirstResponder()
            })
        } else if quantity.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter quantity.", cancelTitle: "OK", isError: true, completion: {
                self.tfQuantity.becomeFirstResponder()
            })
        } else if quantity == "0" {
            showZAlertView(withTitle: "Error", andMessage: "Quantity cannot be zero.", cancelTitle: "OK", isError: true, completion: {
                self.tfQuantity.becomeFirstResponder()
            })
        } else {
            let leg = ManagerDetailLegs()
            
            guard let expiry = dropdownBaseExpiry.titleLabel?.text else { return }
            
            leg.instrument = instrument
            leg.expiry = expiry
            leg.expired = "0"
            leg.price = price
            leg.qty = "\(selectedRadioButton == 1 ? "" : "-")\(quantity)"
            
            
            if instrument == "Fut" {
                leg.strike = "-1"
                leg.optType = "XX"
            } else if instrument == "Opt" {
                guard let strike = dropdownStrike.titleLabel?.text else { return }
                guard let optType = dropdownCallPutOption.titleLabel?.text else { return }
                
                leg.strike = strike
                leg.optType = optType
            }
            
            if let toolVC = self.toolViewController {
                let index = toolVC.legExists(leg: leg)
                
                if index != -1 {
                    self.showZAlertView(withTitle: "Error", andMessage: "Position already exists.", cancelTitle: "OK", isError: true, completion: {})
                } else {
                    self.dismiss(animated: true) {
                        self.delegate?.newLegAdded(managerDetailLeg: leg)
                    }
                }
            }
            
            
        }
        
    }
    
    @objc private func actionAddOrDeleteQty(_ sender: UIButton) {
        guard let currentQty = lblQuantityValue.text?.int else { return }
        
        var tempQty = currentQty
        
        if lot == -1 { return }
        
        if sender == btnAddQty {
            if tempQty == 0 || tempQty/lot == -2 || tempQty.abs/lot == 1 {
                tempQty += lot
            } else if (tempQty.abs/lot) % 2 == 0 {
                tempQty += (lot * 2)
            }
        } else if sender == btnDeleteQty {
            if tempQty == 0 || tempQty/lot == 2 || tempQty.abs/lot == 1 {
                tempQty -= lot
            } else if (tempQty.abs/lot) % 2 == 0 {
                tempQty -= (lot * 2)
            }
        }
        
        
        lblQuantityValue.text = "\(tempQty)"
        lblQuantityValueWidthAnchorConstraint.constant = "\(tempQty)".sizeOfString(usingFont: lblQuantityValue.font).width + 2
        self.view.layoutIfNeeded()
        
        if tempQty == lot {
            btnDeleteQty.isEnabled = false
        } else {
            btnDeleteQty.isEnabled = true
        }
    }
    
    @objc private func radioButtonSelected(_ radioButton: DLRadioButton) {
        
        resignAllResponders()
        
        guard let tag = radioButton.selected()?.tag else { return }
        selectedRadioButton = tag
    }
    
    @objc private func showPositionType(_ sender: UIButton) {
        
        resignAllResponders()
        
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Select Type"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        let itemFut = ActionSheetItem(title: "Fut", value: "Fut", image: nil)
        items.append(itemFut)
        
        let itemOpt = ActionSheetItem(title: "Opt", value: "Opt", image: nil)
        items.append(itemOpt)
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            print("selected = \(value)")
            sender.setTitle(value, for: .normal)
            
            if let symbol = self.lblSymbolValue.text?.trimmed {
                if !symbol.isEmpty {
                    SVProgressHUD.show()
                    self.getStrikesExpiriesAndLot(symbolName: symbol)
                }
            }
            
            self.tfPrice.clear()
            
            
            self.updateFieldConstraints(positionType: value)
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
    }
    
    @objc private func showCallPutOption(_ sender: UIButton) {
        
        resignAllResponders()
        
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Select Option"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        let itemCE = ActionSheetItem(title: "CE", value: "CE", image: nil)
        items.append(itemCE)
        
        let itemPE = ActionSheetItem(title: "PE", value: "PE", image: nil)
        items.append(itemPE)
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            guard let previousValue = sender.titleLabel?.text else { return }
            if previousValue != value {
                print("selected = \(value)")
                sender.setTitle(value, for: .normal)
                self.tfPrice.clear()
            }
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
    }
    
    @objc private func showBaseExpiry(_ sender: UIButton) {
        
        resignAllResponders()
        
        if let expiryDates = self.expiries {
            let picker = ActionSheetStringPicker(title: "Select Expiry", rows: expiryDates, initialSelection: selectedExpiry, doneBlock: { (picker, value, index) in
                if self.selectedExpiry != value {
                    self.selectedExpiry = value
                    sender.setTitle(index as? String, for: .normal)
                    self.tfPrice.clear()
                }
            }, cancel: { (picker) in
                //
            }, origin: sender)
            
            picker?.show()
        }
    }
    
    @objc private func showStrikeForInstrument(_ sender: UIButton) {
        
        resignAllResponders()
        
        if let selectedOption = sender.titleLabel?.text, let strikesArray = self.strikes {
            
            if let initialSelection = strikesArray.index(of: selectedOption) {
                
                let picker = ActionSheetStringPicker(title: "Select Strike", rows: strikesArray, initialSelection: initialSelection, doneBlock: { (picker, value, index) in
                    guard let previousStrike = sender.titleLabel?.text else { return }
                    guard let selectedStrike = index as? String else { return }
                    if previousStrike != selectedStrike {
                        sender.setTitle(index as? String, for: .normal)
                        self.tfPrice.clear()
                    }
                    
                }, cancel: { (picker) in
                    //
                }, origin: sender)
                
                picker?.show()
            }
        }
    }
    
    private func resignAllResponders() {
        view.endEditing(true)
    }
    
    func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: Constants.OSONavigationBarConstants.barHeight), title: "Add Leg", subTitle: nil, leftbuttonImage: nil, rightButtonImage: UIImage(named: "icon_close"))
        view.addSubview(osoNavBar)
        osoNavBar.delegate = self
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

// MARK:- OSONavigationBarDelegate

extension AddNewLegViewController: OSONavigationBarDelegate {
    
    func rightButtonTapped(sender: OSONavigationBar) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
