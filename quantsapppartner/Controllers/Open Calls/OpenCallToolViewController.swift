//
//  OpenCallToolViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 28/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift
import ZAlertView
import SVProgressHUD
import ActiveLabel

class OpenCallToolViewController: UIViewController {
    
    private var osoNavBar: OSONavigationBar!
    private var screenWidth = SwifterSwift.screenWidth
    
    public var symbol: String?
    public var lot: Int = -1
    public var analyzerSummary: AnalyzerSummaryList?
    public var managerDetailLegs: [ManagerDetailLegs]?
    private var tempManagerDetailLegs = [ManagerDetailLegs]()
    private var tempManagerDetailLegsSelection = [Bool]()
    private var legContainers = [LegBookFullProfit]()
    public var currentSpread: Double = 0
    public var targetSpread: Double?
    public var stopLossSpread: Double?
    public var selectedTool: OpenCallToolType?
    public var selectedBook: OSOBook?
    
    private var instruments = [String]()
    private var instrumentsAndCmp: [String: Double] = [:]
    
    // constraints
    private var scrollViewContainerBottomAnchorConstraint: NSLayoutConstraint!
    private var formHeightConstraint: NSLayoutConstraint!
    private var lblCommentTopConstraint: NSLayoutConstraint!
    private var lblTargetSpreadTopConstraint: NSLayoutConstraint!
    
    private let scrollViewContainer: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let formView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let btnConfirm: UIButton = {
        let button = UIButton()
        button.setTitle("CONFIRM", for: .normal)
        button.backgroundColor = Constants.UIConfig.themeColor//UIColor(white: 1.0, alpha: 0.1)
        button.setTitleColor(Constants.UIConfig.bottomColor, for: .normal) //
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(actionConfirm(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblHeading: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.textColor = Constants.UIConfig.themeColor
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPositionName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCurrentSpread: UILabel = {
        let label = UILabel()
        label.text = "Current Spread"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCurrentSpreadValue: UILabel = {
        let label = UILabel()
        label.text = "0"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTargetSpread: UILabel = {
        let label = UILabel()
        label.text = "Target Spread"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfTargetSpread: OSOTextField = {
        let textField = OSOTextField()
        textField.addPaddingLeft(10)
        textField.textColor = Constants.UIConfig.themeColor
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.autocorrectionType = .no
        textField.layer.borderColor = Constants.UIConfig.borderColor.cgColor
        textField.layer.cornerRadius = 4
        textField.layer.borderWidth = 0.5
        textField.backgroundColor = UIColor(white: 1.0, alpha: 0.05)
        textField.returnKeyType = .done
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let lblStopLossSpread: UILabel = {
        let label = UILabel()
        label.text = "Stop Loss Spread"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfStopLossSpread: OSOTextField = {
        let textField = OSOTextField()
        textField.addPaddingLeft(10)
        textField.textColor = Constants.UIConfig.themeColor
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.autocorrectionType = .no
        textField.layer.borderColor = Constants.UIConfig.borderColor.cgColor
        textField.layer.cornerRadius = 4
        textField.layer.borderWidth = 0.5
        textField.backgroundColor = UIColor(white: 1.0, alpha: 0.05)
        textField.returnKeyType = .done
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let lblComment: UILabel = {
        let label = UILabel()
        label.text = "Comment"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tvComment: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textColor = UIColor.white
        textView.layer.borderColor = Constants.UIConfig.borderColor.cgColor
        textView.layer.cornerRadius = 4
        textView.layer.borderWidth = 0.5
        textView.backgroundColor = UIColor(white: 1.0, alpha: 0.05)
        textView.returnKeyType = .done
        textView.keyboardType = .asciiCapable
        textView.placeholder = "Add comment..."
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        // initial setup of all views
        setupViews()
        
        // prepare instruments list
        if let summary = analyzerSummary {
            prepareInstrumentsList(summary: summary)
        }
    }
    
    private func setupViews() {
        
        //guard let toolType = selectedTool else { return }
        
        // btnConfirm
        view.addSubview(btnConfirm)
        btnConfirm.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        btnConfirm.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnConfirm.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        if #available(iOS 11.0, *) {
            btnConfirm.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        } else {
            btnConfirm.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        }
        
        if let toolType = selectedTool {
            btnConfirm.setTitle(toolType.title().uppercased(), for: .normal)
        }
        
        
        // lblHeading
        view.addSubview(lblHeading)
        lblHeading.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        lblHeading.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        lblHeading.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 10).isActive = true
        
        if let symbol = analyzerSummary?.symbol {
            lblHeading.text = symbol
            lblHeading.heightAnchor.constraint(equalToConstant: symbol.height(constraintedWidth: screenWidth - 32, font: lblHeading.font)).isActive = true
        } else {
            lblHeading.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        // lblPositionName
        view.addSubview(lblPositionName)
        lblPositionName.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        lblPositionName.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        lblPositionName.topAnchor.constraint(equalTo: lblHeading.bottomAnchor, constant: 0).isActive = true
        
        if let positionName = analyzerSummary?.positionName {
            lblPositionName.text = positionName
            lblPositionName.heightAnchor.constraint(equalToConstant: positionName.height(constraintedWidth: screenWidth - 32, font: lblPositionName.font)).isActive = true
        } else {
            lblPositionName.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        // MARK: scrollview container
        view.addSubview(scrollViewContainer)
        scrollViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollViewContainer.topAnchor.constraint(equalTo: lblPositionName.bottomAnchor, constant: 10).isActive = true
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnConfirm, attribute: .top, multiplier: 1.0, constant: -16)
        scrollViewContainerBottomAnchorConstraint.isActive = true
        
        // MARK: form view
        scrollViewContainer.addSubview(formView)
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .top, relatedBy: .equal, toItem: scrollViewContainer, attribute: .top, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .left, relatedBy: .equal, toItem: scrollViewContainer, attribute: .left, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .right, relatedBy: .equal, toItem: scrollViewContainer, attribute: .right, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: scrollViewContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
        formHeightConstraint = NSLayoutConstraint(item: formView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        formHeightConstraint.isActive = true
        
        scrollViewContainer.contentSize = formView.size
    }
    
    private func prepareInstrumentsList(summary: AnalyzerSummaryList) {
        print("► prepareInstrumentsList()")
        
        if let positionNo = summary.positionNo {
            DatabaseManager.sharedInstance.getPositionDetails(positionNo: positionNo, completion: { (error, positions) in
                if let err = error {
                    print("Error: \(err.localizedDescription)")
                } else {
                    if let positionsList = positions {
                        for pos in positionsList {
                            if let symbol = summary.symbol, let expiry = pos.expiry, let instrument = pos.instrument, let strike = pos.strike, let optType = pos.optType {
                                var instrumentString = "\(symbol)_\(expiry)_1"
                                if instrument == "Opt" {
                                    instrumentString.append("_\(optType)_\(strike)")
                                }
                                
                                if !self.instruments.contains(instrumentString) {
                                    self.instruments.append(instrumentString)
                                }
                            }
                        }
                    }
                }
            })
        }
        
        print("► instruments[\(instruments.count)] = \(instruments)")
        
        prepareInstrumentsPriceList()
    }
    
    private func prepareInstrumentsPriceList() {
        print("► prepareInstrumentsPriceList()")
        
        if instruments.count > 0 {
            SVProgressHUD.show()
            
            OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, priceList, oiList, volumeList) in
                
                SVProgressHUD.dismiss()
                
                if let priceArray = priceList {
                    print("priceArray[\(priceArray.count)] = \(priceArray)")
                    for i in 0..<self.instruments.count {
                        self.instrumentsAndCmp[self.instruments[i]] = priceArray[i]
                    }
                    
                    print("► instrumentsAndCmp = \(self.instrumentsAndCmp)")
                    
                    // FIXME: CHANGE CMP IN LEGS
                    
                    // render legs
                    if let legs = self.managerDetailLegs {
                        for leg in legs {
                            if let symbolName = self.analyzerSummary?.symbol, let instrument = leg.instrument, let expiry = leg.expiry, let optType = leg.optType, let strike = leg.strike {
                                var fullInstrument = "\(symbolName)_\(expiry)_1"
                                
                                if instrument == "Opt" {
                                    fullInstrument.append("_\(optType)_\(strike)")
                                }
                                
                                if let cmp = self.instrumentsAndCmp[fullInstrument] {
                                    leg.cmp = "\(cmp)"
                                }
                            }
                        }
                        
                        self.renderLegs(legs: legs)
                    }
                } else {
                    if let legs = self.managerDetailLegs {
                        self.renderLegs(legs: legs)
                    }
                }
            }
        } else {
            if let legs = self.managerDetailLegs {
                self.renderLegs(legs: legs)
            }
        }
    }
    
    private func addNewLeg() {
        let vc = AddNewLegViewController()
        vc.analyzerSummary = analyzerSummary
        vc.toolViewController = self
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    private func renderLegs(legs: [ManagerDetailLegs], selection: [Bool]? = nil, isLegRemoved: Bool = false) {
        
        guard let toolType = selectedTool else { return }
        
        let verticalGap: CGFloat = 10.0
        let containerHeightForAdd: CGFloat = 128.0 // 86.0
        let containerHeightForUpdate: CGFloat = 128.0
        var totalHeight: CGFloat = 0.0
        var prevLegContainer: LegBookFullProfit?
        
        formView.removeSubviews()
        
        if toolType != .Modify {
            // lblCurrentSpreadValue
            formView.addSubview(lblCurrentSpreadValue)
            lblCurrentSpreadValue.text = currentSpread.roundToDecimal(2).toString()
            lblCurrentSpreadValue.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 16).isActive = true
            lblCurrentSpreadValue.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -16).isActive = true
            lblCurrentSpreadValue.topAnchor.constraint(equalTo: formView.topAnchor, constant: 10).isActive = true
            lblCurrentSpreadValue.heightAnchor.constraint(equalToConstant: (lblCurrentSpreadValue.text?.height(constraintedWidth: screenWidth - 32, font: lblCurrentSpreadValue.font))!).isActive = true
            
            totalHeight += (lblCurrentSpreadValue.text?.height(constraintedWidth: screenWidth - 32, font: lblCurrentSpreadValue.font))!
            
            // lblCurrentSpread
            formView.addSubview(lblCurrentSpread)
            lblCurrentSpread.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 16).isActive = true
            lblCurrentSpread.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -16).isActive = true
            lblCurrentSpread.topAnchor.constraint(equalTo: lblCurrentSpreadValue.bottomAnchor, constant: 0).isActive = true
            lblCurrentSpread.heightAnchor.constraint(equalToConstant: (lblCurrentSpread.text?.height(constraintedWidth: screenWidth - 32, font: lblCurrentSpread.font))!).isActive = true
            
            totalHeight += ((lblCurrentSpread.text?.height(constraintedWidth: screenWidth - 32, font: lblCurrentSpread.font))! + 10)
        }
        
        tempManagerDetailLegsSelection.removeAll()
        tempManagerDetailLegs.removeAll()
        legContainers.removeAll()
        
        for i in 0..<legs.count {
            let isLegAddedInModify = isLegRemoved ? legExists(leg: legs[i], lookInTemp: false) == -1 : false
            
            let legContainer = LegBookFullProfit(id: i, managerDetailLeg: legs[i], showCheckBox: (toolType == .Modify), showDeleteButton: isLegAddedInModify, selectedToolType: toolType, lot: lot, legAction: isLegAddedInModify ? .add : .update)
            formView.addSubview(legContainer)
            legContainer.delegate = self
            legContainer.translatesAutoresizingMaskIntoConstraints = false
            legContainer.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
            legContainer.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
            legContainer.heightAnchor.constraint(equalToConstant: toolType == .Modify ? isLegAddedInModify ? containerHeightForAdd : containerHeightForUpdate : containerHeightForAdd).isActive = true
            
            if let prevLeg = prevLegContainer {
                legContainer.topAnchor.constraint(equalTo: prevLeg.bottomAnchor, constant: verticalGap).isActive = true
            } else {
                if toolType == .Modify {
                    legContainer.topAnchor.constraint(equalTo: formView.topAnchor, constant: 10).isActive = true
                    totalHeight += 10
                } else {
                    legContainer.topAnchor.constraint(equalTo: lblCurrentSpread.bottomAnchor, constant: (CGFloat(i) * verticalGap) + 16).isActive = true
                    totalHeight += 16
                }
                
            }
            
            prevLegContainer = legContainer
            totalHeight += (verticalGap + (toolType == .Modify ? isLegAddedInModify ? containerHeightForAdd : containerHeightForUpdate : containerHeightForAdd))
            
            if let initialSelection = selection {
                if initialSelection.indices.contains(i) {
                    tempManagerDetailLegsSelection.append(initialSelection[i])
                    if initialSelection[i] {
                        legContainer.markCheckBoxSelected()
                    }
                } else {
                    tempManagerDetailLegsSelection.append(false)
                }
            } else {
                tempManagerDetailLegsSelection.append(false)
            }
            
            tempManagerDetailLegs.append(legs[i].copy() as! ManagerDetailLegs)
            legContainers.append(legContainer)
        }
        
        if toolType == .PartProfit || toolType == .Modify {
            // lblTargetSpread
            formView.addSubview(lblTargetSpread)
            lblTargetSpread.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 16).isActive = true
            lblTargetSpread.heightAnchor.constraint(equalToConstant: 20).isActive = true
            lblTargetSpread.rightAnchor.constraint(equalTo: formView.centerXAnchor, constant: -8).isActive = true
            
            if let prevLeg = prevLegContainer {
                lblTargetSpreadTopConstraint = NSLayoutConstraint(item: lblTargetSpread, attribute: .top, relatedBy: .equal, toItem: prevLeg, attribute: .bottom, multiplier: 1.0, constant: 16)
                lblTargetSpreadTopConstraint.isActive = true
            } else {
                lblTargetSpreadTopConstraint = NSLayoutConstraint(item: lblTargetSpread, attribute: .top, relatedBy: .equal, toItem: formView, attribute: .bottom, multiplier: 1.0, constant: 16)
                lblTargetSpreadTopConstraint.isActive = true
            }
            
            // lblStopLossSpread
            formView.addSubview(lblStopLossSpread)
            lblStopLossSpread.leftAnchor.constraint(equalTo: formView.centerXAnchor, constant: 14).isActive = true
            lblStopLossSpread.heightAnchor.constraint(equalToConstant: 20).isActive = true
            lblStopLossSpread.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -16).isActive = true
            lblStopLossSpread.topAnchor.constraint(equalTo: lblTargetSpread.topAnchor, constant: 0).isActive = true
            
            totalHeight += 36
            
            // tfTargetSpread
            formView.addSubview(tfTargetSpread)
            tfTargetSpread.delegate = self
            tfTargetSpread.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
            tfTargetSpread.heightAnchor.constraint(equalToConstant: 40).isActive = true
            tfTargetSpread.rightAnchor.constraint(equalTo: formView.centerXAnchor, constant: -8).isActive = true
            tfTargetSpread.topAnchor.constraint(equalTo: lblTargetSpread.bottomAnchor, constant: 4).isActive = true
            
            if let spread = targetSpread {
                tfTargetSpread.text = "\(spread.toString())"
            }
            
            // tfStopLossSpread
            formView.addSubview(tfStopLossSpread)
            tfStopLossSpread.delegate = self
            tfStopLossSpread.leftAnchor.constraint(equalTo: formView.centerXAnchor, constant: 8).isActive = true
            tfStopLossSpread.heightAnchor.constraint(equalToConstant: 40).isActive = true
            tfStopLossSpread.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
            tfStopLossSpread.topAnchor.constraint(equalTo: lblStopLossSpread.bottomAnchor, constant: 4).isActive = true
            
            if let spread = stopLossSpread {
                tfStopLossSpread.text = "\(spread.toString())"
            }
            
            totalHeight += 44
            
            // lblComment
            formView.addSubview(lblComment)
            lblComment.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 16).isActive = true
            lblComment.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -16).isActive = true
            lblComment.heightAnchor.constraint(equalToConstant: 20).isActive = true
            lblComment.topAnchor.constraint(equalTo: tfTargetSpread.bottomAnchor, constant: 16).isActive = true
            
            /*if let prevLeg = prevLegContainer {
                lblCommentTopConstraint = NSLayoutConstraint(item: lblComment, attribute: .top, relatedBy: .equal, toItem: prevLeg, attribute: .bottom, multiplier: 1.0, constant: 16)
                lblCommentTopConstraint.isActive = true
            } else {
                lblCommentTopConstraint = NSLayoutConstraint(item: lblComment, attribute: .top, relatedBy: .equal, toItem: formView, attribute: .bottom, multiplier: 1.0, constant: 16)
                lblCommentTopConstraint.isActive = true
            }*/
            
            totalHeight += 36
            
            
            // tvComment
            formView.addSubview(tvComment)
            tvComment.topAnchor.constraint(equalTo: lblComment.bottomAnchor, constant: 4).isActive = true
            tvComment.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
            tvComment.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
            tvComment.heightAnchor.constraint(equalToConstant: 100).isActive = true
            
            totalHeight += 104
            
            // keyboard event handler
            NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
        
        formHeightConstraint.isActive = false
        formHeightConstraint.constant = totalHeight + 10
        formHeightConstraint.isActive = true
        
        formView.layoutSubviews()
        formView.layoutIfNeeded()
        view.layoutIfNeeded()
        
        scrollViewContainer.contentSize = formView.size
    }
    
    private func addLeg(leg: ManagerDetailLegs) {
        let verticalGap: CGFloat = 10.0
        //let containerHeightForUpdate: CGFloat = 128.0 //86.0
        let containerHeightForAdd: CGFloat = 128.0 // 86.0
        var currentFormHeight = formHeightConstraint.constant
        var prevLegContainer: LegBookFullProfit?
        print("currentFormHeight = \(currentFormHeight)")
        
        tempManagerDetailLegs.append(leg)
        tempManagerDetailLegsSelection.append(true)
        
        if legContainers.count > 0 {
            prevLegContainer = legContainers[legContainers.count - 1]
        }
        
        let legContainer = LegBookFullProfit(id: getNewIdForLegContainer(), managerDetailLeg: leg, showCheckBox: true, showDeleteButton: true, selectedToolType: OpenCallToolType.Modify, lot: lot, legAction: .add)
        formView.addSubview(legContainer)
        legContainer.markCheckBoxSelected()
        legContainers.append(legContainer)
        legContainer.delegate = self
        legContainer.translatesAutoresizingMaskIntoConstraints = false
        legContainer.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        legContainer.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        legContainer.heightAnchor.constraint(equalToConstant: containerHeightForAdd).isActive = true
        
        if let prevLeg = prevLegContainer {
            legContainer.topAnchor.constraint(equalTo: prevLeg.bottomAnchor, constant: verticalGap).isActive = true
        } else {
            legContainer.topAnchor.constraint(equalTo: formView.topAnchor, constant: 10).isActive = true
        }
        
        currentFormHeight += (containerHeightForAdd + verticalGap)
        
        lblTargetSpreadTopConstraint.isActive = false
        lblTargetSpreadTopConstraint = NSLayoutConstraint(item: lblTargetSpread, attribute: .top, relatedBy: .equal, toItem: legContainer, attribute: .bottom, multiplier: 1.0, constant: 16)
        lblTargetSpreadTopConstraint.isActive = true
        
        /*lblCommentTopConstraint.isActive = false
        lblCommentTopConstraint = NSLayoutConstraint(item: lblComment, attribute: .top, relatedBy: .equal, toItem: legContainer, attribute: .bottom, multiplier: 1.0, constant: 16)
        lblCommentTopConstraint.isActive = true*/
        
        formHeightConstraint.isActive = false
        formHeightConstraint.constant = currentFormHeight
        formHeightConstraint.isActive = true
        scrollViewContainer.contentSize = formView.size
        
        formView.layoutSubviews()
        formView.layoutIfNeeded()
        view.layoutIfNeeded()
    }
    
    private func removeLeg(atIndex index: Int) {
        
        let verticalGap: CGFloat = 10.0
        let containerHeightForUpdate: CGFloat = 128.0 //86.0
        let containerHeightForAdd: CGFloat = 128.0 // 86.0
        var currentFormHeight = formHeightConstraint.constant
        
        let removedLegContainer = legContainers[index]
        if removedLegContainer.actionOfLeg == .update {
            currentFormHeight -= containerHeightForUpdate
        } else {
            currentFormHeight -= containerHeightForAdd
        }
        
        tempManagerDetailLegs.remove(at: index)
        tempManagerDetailLegsSelection.remove(at: index)
        
        legContainers[index].removeFromSuperview()
        legContainers.remove(at: index)
        
        for i in 0..<legContainers.count {
            let legContainer = legContainers[i]
            
            if legContainers.indices.contains(i-1) {
                let prevLegContainer = legContainers[i-1]
                legContainer.topAnchor.constraint(equalTo: prevLegContainer.bottomAnchor, constant: verticalGap).isActive = true
            } else {
                legContainer.topAnchor.constraint(equalTo: formView.topAnchor, constant: verticalGap).isActive = true
            }
        }
        
        if legContainers.indices.contains(legContainers.count - 1) {
            let lastContainer = legContainers[legContainers.count - 1]
            
            lblTargetSpreadTopConstraint.isActive = false
            lblTargetSpreadTopConstraint = NSLayoutConstraint(item: lblTargetSpread, attribute: .top, relatedBy: .equal, toItem: lastContainer, attribute: .bottom, multiplier: 1.0, constant: 16)
            lblTargetSpreadTopConstraint.isActive = true
            
            /*lblCommentTopConstraint.isActive = false
            lblCommentTopConstraint = NSLayoutConstraint(item: lblComment, attribute: .top, relatedBy: .equal, toItem: lastContainer, attribute: .bottom, multiplier: 1.0, constant: 16)
            lblCommentTopConstraint.isActive = true*/
        } else {
            lblTargetSpreadTopConstraint.isActive = false
            lblTargetSpreadTopConstraint = NSLayoutConstraint(item: lblTargetSpread, attribute: .top, relatedBy: .equal, toItem: formView, attribute: .top, multiplier: 1.0, constant: 16)
            lblTargetSpreadTopConstraint.isActive = true
            
            /*lblCommentTopConstraint.isActive = false
            lblCommentTopConstraint = NSLayoutConstraint(item: lblComment, attribute: .top, relatedBy: .equal, toItem: formView, attribute: .top, multiplier: 1.0, constant: 16)
            lblCommentTopConstraint.isActive = true*/
        }
        
        formHeightConstraint.isActive = false
        formHeightConstraint.constant = currentFormHeight
        formHeightConstraint.isActive = true
        
        scrollViewContainer.contentSize = formView.size
        
        UIView.animate(withDuration: 0.3) {
            self.formView.layoutSubviews()
            self.formView.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }
    }
    
    
    private func getNewIdForLegContainer() -> Int {
        var maxId: Int = -1
        
        for legContainer in legContainers {
            if let id = legContainer.containerId {
                if id > maxId {
                    maxId = id
                }
            }
        }
        
        return maxId + 1
    }
    
    private func confirm(title: String, message: String, completion: @escaping ((_ confirmed: Bool) -> Void)) {
        
        let dialog = ZAlertView(title: title,
                                message: message,
                                isOkButtonLeft: false,
                                okButtonText: "YES",
                                cancelButtonText: "NO",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
                                    completion(true)
        }) { (alertView) in
            alertView.dismissAlertView()
            completion(false)
        }
        dialog.show()
    }
    
    private func confirm(title: String, message: NSAttributedString, completion: @escaping ((_ confirmed: Bool) -> Void)) {
        let dialog = ZAlertView()
        dialog.alertTitle = title
        dialog.messageAttributedString = message
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        
        dialog.addButton("OK", color: Constants.ZAlertViewUI.successButtonColor, titleColor: UIColor.white) { (alertView) in
            alertView.dismissWithDuration(0.3)
            //completion(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                completion(true)
            })
        }
        
        dialog.addButton("CANCEL", color: UIColor(white: 0.0, alpha: 0.3), titleColor: UIColor.white) { (alertView) in
            alertView.dismissAlertView()
        }
        
        dialog.show()
    }
    
    @objc private func actionConfirm(_ sender: UIButton) {
        print("► actionConfirm()")
        guard let toolType = selectedTool else { return }
        guard let bookType = selectedBook?.bookType else { return }
        
        let comment = tvComment.text.trimmed.stripUnicodeCharacters()
        
        if let symbol = self.analyzerSummary?.symbol, let positionNo = self.analyzerSummary?.positionNo {
            if toolType == .BookFullProfit {
                self.bookFullProfit(symbol: symbol, positionNo: positionNo, legs: self.tempManagerDetailLegs, bookType: bookType)
            } else if toolType == .PartProfit {
                self.bookPartProfit(symbol: symbol, positionNo: positionNo, comment: comment.isEmpty ? "0" : comment, legs: self.tempManagerDetailLegs, bookType: bookType)
            } else if toolType == .Exit {
                self.exitCall(symbol: symbol, positionNo: positionNo, legs: self.tempManagerDetailLegs, bookType: bookType)
            } else if toolType == .Modify {
                self.modifyCall(symbol: symbol, positionNo: positionNo, comment: comment.isEmpty ? "0" : comment, legs: self.tempManagerDetailLegs, selection: self.tempManagerDetailLegsSelection, bookType: bookType)
            }
        }
    }
    
    private func bookFullProfit(symbol: String, positionNo: String, legs: [ManagerDetailLegs], bookType: String) {
        
        var api = [String]()
        
        for leg in legs {
            if let instrument = leg.instrument, let expiry = leg.expiry, let optType = leg.optType, let strike = leg.strike, let qty = leg.qty?.int, let price = leg.price {
                var fullInstrument = "\(instrument) \(expiry)"
                
                if instrument == "Opt" {
                    fullInstrument.append(" \(strike) \(optType)")
                }
                
                fullInstrument.append(",\(price)")
                fullInstrument.append(",\(qty * -1)")
                
                api.append(fullInstrument)
            }
        }
        
        if api.count > 0 {
            
            // create message preview
            let attributedString = createLegSummary(api: api, toolType: .BookFullProfit, target: nil, stopLoss: nil)
            
            confirm(title: "CONFIRM", message: attributedString) { (confirmed) in
                if confirmed {
                    SVProgressHUD.show()
                    
                    OSOWebService.sharedInstance.bookFullProfit(symbol: symbol, position: positionNo, bookType: bookType, legs: api) { (success, error, message, bookName, notificationTitle, notificationMessage, dataValues) in
                        if let err = error {
                            SVProgressHUD.dismiss()
                            if err.code == OSOError.InvalidSession.rawValue {
                                self.showZAlertView(withTitle: "", andMessage: err.localizedDescription, cancelTitle: "LOGIN", isError: true, completion: {
                                    // LOGOUT CODE
                                    self.logout()
                                })
                            } else {
                                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                            }
                        } else {
                            if success {
                                self.deletePosition(bookType: bookType, positionNo: positionNo, message: message, bookName: [bookName!], notificationTitle: notificationTitle, notificationMessage: notificationMessage, dataValues: dataValues)
                            } else {
                                SVProgressHUD.dismiss()
                                self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {})
                            }
                        }
                    }
                }
            }
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "No legs found.", cancelTitle: "OK", isError: true, completion: {})
        }
        
    }
    
    private func bookPartProfit(symbol: String, positionNo: String, comment: String, legs: [ManagerDetailLegs], bookType: String) {
        
        guard let target = targetSpread else { return }
        guard let stopLoss = stopLossSpread else { return }
        guard let targetText = tfTargetSpread.text else { return }
        guard let stopLossText = tfStopLossSpread.text else { return }
        
        if targetText.isEmpty {
            self.showZAlertView(withTitle: "Error", andMessage: "Please enter target spread.", cancelTitle: "OK", isError: true) {
                self.tfTargetSpread.becomeFirstResponder()
            }
            
            return
        } else if stopLossText.isEmpty {
            self.showZAlertView(withTitle: "Error", andMessage: "Please enter stop loss spread.", cancelTitle: "OK", isError: true) {
                self.tfStopLossSpread.becomeFirstResponder()
            }
            
            return
        }
        
        guard let targetValue = targetText.double() else { return }
        guard let stopLossValue = stopLossText.double() else { return }
        
        if stopLossValue >= targetValue {
            self.showZAlertView(withTitle: "Error", andMessage: "Target spread should be greater than stop loss spread.", cancelTitle: "OK", isError: true) {
                self.tfStopLossSpread.becomeFirstResponder()
            }
            
            return
        }
        
        var newTarget: String = "-"
        var newStopLoss: String = "-"
        
        if target != targetValue {
            newTarget = targetValue.toString()
        }
        
        if stopLoss != stopLossValue {
            newStopLoss = stopLossValue.toString()
        }
        
        
        var api = [String]()
        
        for leg in legs {
            if let instrument = leg.instrument, let expiry = leg.expiry, let optType = leg.optType, let strike = leg.strike, let qty = leg.qty?.int, let price = leg.price {
                var fullInstrument = "\(instrument) \(expiry)"
                
                if instrument == "Opt" {
                    fullInstrument.append(" \(strike) \(optType)")
                }
                
                fullInstrument.append(",\(price)")
                fullInstrument.append(",\((qty/2) * -1)") //fullInstrument.append(",\(qty + ((qty/2) * -1))")
                
                api.append(fullInstrument)
            }
        }
        
        if api.count > 0 {
            
            // create message preview
            let attributedString = createLegSummary(api: api, toolType: .PartProfit, target: newTarget == "-" ? nil : newTarget, stopLoss: newStopLoss == "-" ? nil : newStopLoss)
            
            confirm(title: "CONFIRM", message: attributedString) { (confirmed) in
                if confirmed {
                    SVProgressHUD.show()
                    
                    OSOWebService.sharedInstance.bookPartProfit(symbol: symbol, position: positionNo, bookType: bookType, comment: comment, legs: api, target: newTarget, stopLoss: newStopLoss) { (success, error, message, bookName, notificationTitle, notificationMessage, dataValues) in
                        if let err = error {
                            SVProgressHUD.dismiss()
                            
                            if err.code == OSOError.InvalidSession.rawValue {
                                self.showZAlertView(withTitle: "", andMessage: err.localizedDescription, cancelTitle: "LOGIN", isError: true, completion: {
                                    // LOGOUT CODE
                                    self.logout()
                                })
                            } else {
                                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                            }
                        } else {
                            if success {
                                // FIXME: UPDATE POSITION
                                /*DatabaseManager.sharedInstance.deletePositions(positions: [positionNo], bookType: bookType) { (success) in
                                    if success {
                                        //self.updatePosition(bookType: bookType, positionNo: positionNo, legs: api, notificationMessage: notificationMessage)
                                        SVProgressHUD.dismiss()
                                        if let notfMsg = notificationMessage, let notfTitle = notificationTitle, let book = bookName, let values = dataValues {
                                            self.showNotificationViewController(positionNos: [positionNo], bookName: [book], notificationTitle: notfTitle, notificationMessage: notfMsg, dataValues: values)
                                        }
                                    } else {
                                        SVProgressHUD.dismiss()
                                        self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {})
                                    }
                                }*/
                                
                                SVProgressHUD.dismiss()
                                if let notfMsg = notificationMessage, let notfTitle = notificationTitle, let book = bookName, let values = dataValues {
                                    self.showNotificationViewController(positionNos: [positionNo], bookName: [book], notificationTitle: notfTitle, notificationMessage: notfMsg, dataValues: values)
                                }
                                
                            } else {
                                SVProgressHUD.dismiss()
                                self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {})
                            }
                        }
                    }
                }
            }
            
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "No legs found.", cancelTitle: "OK", isError: true, completion: {})
        }
    }
    
    private func modifyCall(symbol: String, positionNo: String, comment: String, legs: [ManagerDetailLegs], selection: [Bool], bookType: String) {
        
        guard let target = targetSpread else { return }
        guard let stopLoss = stopLossSpread else { return }
        guard let targetText = tfTargetSpread.text else { return }
        guard let stopLossText = tfStopLossSpread.text else { return }
        
        if targetText.isEmpty {
            self.showZAlertView(withTitle: "Error", andMessage: "Please enter target spread.", cancelTitle: "OK", isError: true) {
                self.tfTargetSpread.becomeFirstResponder()
            }
            
            return
        } else if stopLossText.isEmpty {
            self.showZAlertView(withTitle: "Error", andMessage: "Please enter stop loss spread.", cancelTitle: "OK", isError: true) {
                self.tfStopLossSpread.becomeFirstResponder()
            }
            
            return
        }
        
        guard let targetValue = targetText.double() else { return }
        guard let stopLossValue = stopLossText.double() else { return }
        
        if stopLossValue >= targetValue {
            self.showZAlertView(withTitle: "Error", andMessage: "Target spread should be greater than stop loss spread.", cancelTitle: "OK", isError: true) {
                self.tfStopLossSpread.becomeFirstResponder()
            }
            
            return
        }
        
        var newTarget: String = "-"
        var newStopLoss: String = "-"
        
        if target != targetValue {
            newTarget = targetValue.toString()
        }
        
        if stopLoss != stopLossValue {
            newStopLoss = stopLossValue.toString()
        }
        
        
        
        var api = [String]()
        var ctr: Int = 0
        var numberOfLegsWithZeroQty: Int = 0
        
        for leg in legs {
            
            if selection[ctr] {
                if let instrument = leg.instrument, let expiry = leg.expiry, let optType = leg.optType, let strike = leg.strike, let qty = leg.qty?.int, let price = leg.price {
                    var fullInstrument = "\(instrument) \(expiry)"
                    
                    if instrument == "Opt" {
                        fullInstrument.append(" \(strike) \(optType)")
                    }
                    
                    fullInstrument.append(",\(price)")
                    fullInstrument.append(",\(qty)")
                    
                    api.append(fullInstrument)
                    
                    let itemIndex = legExists(leg: leg, lookInTemp: false)
                    
                    if itemIndex > -1 {
                        if let originalLegs = managerDetailLegs {
                            if originalLegs.indices.contains(itemIndex) {
                                if let originalQty = originalLegs[itemIndex].qty?.double(), let newQty = leg.qty?.double() {
                                    if originalQty + newQty == 0 {
                                        numberOfLegsWithZeroQty += 1
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            ctr += 1
        }
        
        ctr = 0
        
        if numberOfLegsWithZeroQty == api.count && api.count > 0 {
            if let originalLegs = managerDetailLegs {
                if numberOfLegsWithZeroQty == originalLegs.count {
                    let msg = "Action is not allowed. Either use Book Full Profit or Exit Call action."
                    showZAlertView(withTitle: "Error", andMessage: msg, cancelTitle: "OK", completion: {})
                    return
                }
            }
            
        }
        
        /*if legs.count == api.count {
            // check if qty is 0
            print("Only one leg left")
            
            if let originalLegs = managerDetailLegs {
                for i in 0..<originalLegs.count {
                    let originalLeg = originalLegs[i]
                    let newLeg = legs[i]
                    
                    if let originalQty = originalLeg.qty?.double(), let newQty = newLeg.qty?.double() {
                        if originalQty + newQty == 0 {
                            ctr += 1
                        }
                    }
                }
                
                if ctr == legs.count {
                    let msg = "Action is not allowed. Either use Book Full Profit or Exit Call action."
                    showZAlertView(withTitle: "Error", andMessage: msg, cancelTitle: "OK", completion: {})
                    return
                }
            }
        }*/
        
        if api.count > 0 {
            
            // create message preview
            let attributedString = createLegSummary(api: api, toolType: .Modify, target: newTarget == "-" ? nil : newTarget, stopLoss: newStopLoss == "-" ? nil : newStopLoss)
            
            confirm(title: "CONFIRM", message: attributedString) { (confirmed) in
                
                if confirmed {
                    
                    // call webserver to update call
                    SVProgressHUD.show()
                    
                    OSOWebService.sharedInstance.modifyCall(symbol: symbol, position: positionNo, bookType: bookType, comment: comment, legs: api, target: newTarget, stopLoss: newStopLoss) { (success, error, message, bookName, notificationTitle, notificationMessage, dataValues) in
                        if let err = error {
                            SVProgressHUD.dismiss()
                            
                            if err.code == OSOError.InvalidSession.rawValue {
                                self.showZAlertView(withTitle: "", andMessage: err.localizedDescription, cancelTitle: "LOGIN", isError: true, completion: {
                                    // LOGOUT CODE
                                    self.logout()
                                })
                            } else {
                                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                            }
                        } else {
                            if success {
                                // FIXME: UPDATE POSITION
                                /*DatabaseManager.sharedInstance.deletePositions(positions: [positionNo], bookType: bookType) { (success) in
                                    if success {
                                        //self.updatePosition(bookType: bookType, positionNo: positionNo, legs: api, notificationMessage: notificationMessage)
                                        SVProgressHUD.dismiss()
                                        if let notfTitle = notificationTitle, let notfMsg = notificationMessage, let book = bookName, let values = dataValues {
                                            self.showNotificationViewController(positionNos: [positionNo], bookName: [book], notificationTitle: notfTitle, notificationMessage: notfMsg, dataValues: values)
                                        }
                                    } else {
                                        SVProgressHUD.dismiss()
                                        self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {})
                                    }
                                }*/
                                
                                SVProgressHUD.dismiss()
                                if let notfTitle = notificationTitle, let notfMsg = notificationMessage, let book = bookName, let values = dataValues {
                                    self.showNotificationViewController(positionNos: [positionNo], bookName: [book], notificationTitle: notfTitle, notificationMessage: notfMsg, dataValues: values)
                                }
                            } else {
                                SVProgressHUD.dismiss()
                                self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {})
                            }
                        }
                    }
                }
            }
            
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "Select atleast one leg to modify.", cancelTitle: "OK", isError: true, completion: {})
        }
    }
    
    private func exitCall(symbol: String, positionNo: String, legs: [ManagerDetailLegs], bookType: String) {
        var api = [String]()
        
        for leg in legs {
            if let instrument = leg.instrument, let expiry = leg.expiry, let optType = leg.optType, let strike = leg.strike, let qty = leg.qty?.int, let price = leg.price {
                var fullInstrument = "\(instrument) \(expiry)"
                
                if instrument == "Opt" {
                    fullInstrument.append(" \(strike) \(optType)")
                }
                
                fullInstrument.append(",\(price)")
                fullInstrument.append(",\(qty * -1)")
                
                api.append(fullInstrument)
            }
        }
        
        if api.count > 0 {
            
            // create message preview
            let attributedString = createLegSummary(api: api, toolType: .Exit, target: nil, stopLoss: nil)
            
            // confirm
            confirm(title: "CONFIRM", message: attributedString) { (confirmed) in
                if confirmed {
                    SVProgressHUD.show()
                    
                    OSOWebService.sharedInstance.exitCall(symbol: symbol, position: positionNo, bookType: bookType, legs: api) { (success, error, message, bookName, notificationTitle, notificationMessage, dataValues) in
                        if let err = error {
                            SVProgressHUD.dismiss()
                            
                            if err.code == OSOError.InvalidSession.rawValue {
                                self.showZAlertView(withTitle: "", andMessage: err.localizedDescription, cancelTitle: "LOGIN", isError: true, completion: {
                                    // LOGOUT CODE
                                    self.logout()
                                })
                            } else {
                                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                            }
                        } else {
                            if success {
                                self.deletePosition(bookType: bookType, positionNo: positionNo, message: message, bookName: [bookName!], notificationTitle: notificationTitle, notificationMessage: notificationMessage, dataValues: dataValues)
                            } else {
                                SVProgressHUD.dismiss()
                                self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {})
                            }
                        }
                    }
                }
            }
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "No legs found.", cancelTitle: "OK", isError: true, completion: {})
        }
    }
    
    private func deletePosition(bookType: String, positionNo: String, message: String?, bookName: [String]?, notificationTitle: String?, notificationMessage: String?, dataValues: String?) {
        DatabaseManager.sharedInstance.deletePositions(positions: [positionNo], bookType: bookType) { (success) in
            SVProgressHUD.dismiss()
            if let notfMessage = notificationMessage, let notfTitle = notificationTitle, let book = bookName, let values = dataValues {
                self.showNotificationViewController(positionNos: [positionNo], bookName: book, notificationTitle: notfTitle, notificationMessage: notfMessage, dataValues: values)
            } else {
                if let msg = message {
                    self.showZAlertView(withTitle: "", andMessage: msg, cancelTitle: "OK", isError: false, completion: {
                        self.showHomeScreen()
                    })
                } else {
                    self.showZAlertView(withTitle: "", andMessage: "Call closed successfully", cancelTitle: "OK", isError: false, completion: {
                        self.showHomeScreen()
                    })
                }
            }
            
        }
    }
    
    private func updatePosition(bookType: String, positionNo: String, legs: [String], bookName: String, notificationTitle: String, notificationMessage: String?, dataValues: String?) {
        
        guard let analyzerSummary = analyzerSummary else { return }
        
        var tempInstrument = [String]()
        let alerts: String = "0"
        
        var analyzerSummaries = [AnalyzerSummary]()
        
        for i in 0..<legs.count {
            let summary = AnalyzerSummary()
            summary.bookType = bookType
            summary.positionNo = positionNo
            summary.symbol = analyzerSummary.symbol
            summary.positionName = analyzerSummary.positionName
            summary.margin = analyzerSummary.margin
            summary.dateCreated = analyzerSummary.dateCreated
            summary.alertOne = alerts
            summary.alertTwo = alerts
            summary.tradeStatus = analyzerSummary.tradeStatus
            summary.Papi = legs[i]
            
            tempInstrument = legs[i].components(separatedBy: ",")
            
            summary.price = tempInstrument[1]
            summary.qty = tempInstrument[2]
            
            tempInstrument = tempInstrument[0].components(separatedBy: " ")
            
            summary.instrument = tempInstrument[0]
            summary.expiry = tempInstrument[1]
            summary.srno = "\(i+1)"
            summary.syncNo = "0"
            
            if tempInstrument[0] == "Fut" {
                summary.strike = "-1"
                summary.optType = "XX"
            } else {
                summary.strike = tempInstrument[2]
                summary.optType = tempInstrument[3]
            }
            
            summary.expired = "0"
            
            analyzerSummaries.append(summary)
        }
        
        DatabaseManager.sharedInstance.addPosition(analyzerSummaries: analyzerSummaries) { (success) in
            SVProgressHUD.dismiss()
            if success {
                if let notfMsg = notificationMessage, let values = dataValues {
                    self.showNotificationViewController(positionNos: [positionNo], bookName: [bookName], notificationTitle: notificationTitle, notificationMessage: notfMsg, dataValues: values)
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: "Notification message was not received.", cancelTitle: "OK", isError: false, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            } else {
                self.showZAlertView(withTitle: "Error", andMessage: "Could not save the summary", cancelTitle: "OK", isError: true, completion: {
                    //
                })
            }
        }
    }
    
    private func showNotificationViewController(positionNos: [String], bookName: [String], notificationTitle: String, notificationMessage: String, dataValues: String) {
        let vc = NotificationMessageViewController()
        vc.positionNos = positionNos
        vc.bookName = bookName
        vc.dataValues = dataValues
        vc.notificationTitle = notificationTitle
        vc.notificationMessage = notificationMessage
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    public func legExists(leg: ManagerDetailLegs, lookInTemp: Bool = true) -> Int {
        var itemIndex: Int = -1
        var fullInstrument = ""
        
        if let instrument = leg.instrument, let expiry = leg.expiry {
            fullInstrument.append("\(instrument) \(expiry)")
            
            if instrument == "Opt" {
                if let optType = leg.optType, let strike = leg.strike {
                    fullInstrument.append(" \(strike) \(optType)")
                }
            }
        }
        
        if lookInTemp {
            for i in 0..<tempManagerDetailLegs.count {
                let tempLeg = tempManagerDetailLegs[i]
                var tempFullInstrument : String = ""
                
                if let instrument = tempLeg.instrument, let expiry = tempLeg.expiry {
                    tempFullInstrument.append("\(instrument) \(expiry)")
                    
                    if instrument == "Opt" {
                        if let optType = tempLeg.optType, let strike = tempLeg.strike {
                            tempFullInstrument.append(" \(strike) \(optType)")
                        }
                    }
                }
                
                if fullInstrument == tempFullInstrument {
                    itemIndex = i
                    return itemIndex
                }
            }
        } else {
            if let originalManagerDetailLegs = managerDetailLegs {
                
                for i in 0..<originalManagerDetailLegs.count {
                    let tempLeg = originalManagerDetailLegs[i]
                    var tempFullInstrument : String = ""
                    
                    if let instrument = tempLeg.instrument, let expiry = tempLeg.expiry {
                        tempFullInstrument.append("\(instrument) \(expiry)")
                        
                        if instrument == "Opt" {
                            if let optType = tempLeg.optType, let strike = tempLeg.strike {
                                tempFullInstrument.append(" \(strike) \(optType)")
                            }
                        }
                    }
                    
                    if fullInstrument == tempFullInstrument {
                        itemIndex = i
                        return itemIndex
                    }
                }
            }
            
        }
        
        
        
        return itemIndex
    }
    
    private func showHomeScreen() {
        let homeVC = self.navigationController!.viewControllers.filter { $0 is HomeViewController }.first!
        navigationController!.popToViewController(homeVC, animated: true)
    }
    
    private func createLegSummary(api: [String], toolType: OpenCallToolType, target: String?, stopLoss: String?) -> NSAttributedString {
        // create message preview
        
        var ctr: Int = 0
        let attributedString = NSMutableAttributedString()
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.left
        
        guard let symbolName = analyzerSummary?.symbol else { return attributedString }
        
        for leg in api {
            let splitLeg = leg.components(separatedBy: ",")
            
            attributedString.append(NSAttributedString(string: "•", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            
            if let qty = splitLeg[2].double() {
                if qty < 0 {
                    attributedString.append(NSAttributedString(string: " Sell", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
                } else if qty > 0 {
                    attributedString.append(NSAttributedString(string: " Buy", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
                }
            }
            
            let temp = splitLeg[0].components(separatedBy: " ")
            
            if temp.indices.contains(2) {
                attributedString.append(NSAttributedString(string: " \(temp[2])", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            if temp.indices.contains(3) {
                attributedString.append(NSAttributedString(string: " \(temp[3])", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            if temp.indices.contains(1) {
                attributedString.append(NSAttributedString(string: " \(temp[1])", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            if let qty = splitLeg[2].double() {
                attributedString.append(NSAttributedString(string: " Qty \(qty.roundToDecimal(2).abs.toString())", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            // CMP
            var instrumentString = "\(symbolName)"
            
            if temp.indices.contains(1) {
                instrumentString.append("_\(temp[1])_1")
            }
            
            if temp.indices.contains(3) {
                instrumentString.append("_\(temp[3])")
            }
            
            if temp.indices.contains(2) {
                instrumentString.append("_\(temp[2])")
            }
            
            if let cmp = instrumentsAndCmp[instrumentString] {
                attributedString.append(NSAttributedString(string: " @ \(cmp.roundToDecimal(2).toString())\(ctr < api.count - 1 ? "\n" : "")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            } else {
                attributedString.append(NSAttributedString(string: "\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            /*if let price = splitLeg[1].double() {
                attributedString.append(NSAttributedString(string: " @ \(price.roundToDecimal(2).toString())\(ctr < api.count - 1 ? "\n" : "")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }*/
            
            ctr += 1
        }
        
        if toolType == .PartProfit || toolType == .Modify {
            if let spread = target {
                attributedString.append(NSAttributedString(string: "\n\nTarget Spread revised to \(spread)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            if let spread = stopLoss {
                if target == nil {
                    attributedString.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
                } else {
                    attributedString.append(NSAttributedString(string: "\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
                }
                
                attributedString.append(NSAttributedString(string: "Stop Loss Spread revised to \(spread)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
        }
        
        return attributedString
    }
    

    private func addOSONavigationBar() {
        
        var title = "Open Calls"
        var subTitle: String?
        
        var rightButtonImage: UIImage?
        var rightButtonTintColor: UIColor?
        
        if let selectedToolType = selectedTool {
            title = selectedToolType.title()
            
            if selectedToolType == .Modify {
                rightButtonImage = UIImage(named: "icon_plus")
                rightButtonTintColor = Constants.UIConfig.themeColor
            }
        }
        
        if let bookTitle = selectedBook?.title {
            subTitle = bookTitle
        }
        
        
        osoNavBar = OSONavigationBar(frame: CGRect.zero, title: title, subTitle: subTitle, leftbuttonImage: UIImage(named: "icon_left"), rightButtonImage: rightButtonImage, leftButtonTintColor: nil, rightButtonTintColor: rightButtonTintColor)
        osoNavBar.translatesAutoresizingMaskIntoConstraints = false
        osoNavBar.delegate = self
        view.addSubview(osoNavBar)
        
        osoNavBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        osoNavBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        osoNavBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        osoNavBar.heightAnchor.constraint(equalToConstant: Constants.OSONavigationBarConstants.barHeight).isActive = true
    }
    
    private func updateOSONavigationBar() {
        var trueCtr: Int = 0
        
        for flag in tempManagerDetailLegsSelection {
            if flag {
                trueCtr += 1
            }
        }
        
        osoNavBar.rightBarButtonEnabled(isEnabled: trueCtr > 0)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // MARK:- Keyboard Events
    
    @objc func keyBoardDidShow(notification: NSNotification) {
        //handle appearing of keyboard here
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            scrollViewContainerBottomAnchorConstraint.isActive = false
            
            scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -(keyboardSize.height))
            
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollViewContainerBottomAnchorConstraint.isActive = true
                self.view.layoutIfNeeded()
            }) { (finished) in
                if let firstResponder = self.view.firstResponder() {
                    self.scrollViewContainer.scrollRectToVisible(firstResponder.frame, animated: true)
                }
            }
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
        guard selectedTool != nil else { return }
        
        scrollViewContainerBottomAnchorConstraint.isActive = false
        
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnConfirm, attribute: .top, multiplier: 1.0, constant: -16)
        
        UIView.animate(withDuration: 0.3) {
            self.scrollViewContainerBottomAnchorConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }
}

// MARK:- AddNewLegDelegate

extension OpenCallToolViewController {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfTargetSpread || textField == tfStopLossSpread {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789-.").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            
            if string == filtered {
                if string == "-" {
                    let count = (textField.text?.components(separatedBy: "-").count)! - 1
                    
                    if count == 0 {
                        if textField.text?.count == 0 {
                            return true
                        } else {
                            return false
                        }
                    } else {
                        return false
                    }
                } else if string == "." {
                    let count = (textField.text?.components(separatedBy: ".").count)! - 1
                    if count > 0 {
                        return false
                    } else {
                        return true
                    }
                } else {
                    return true
                }
            } else {
                return false
            }
        } else {
            return true
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}

// MARK:- AddNewLegDelegate

extension OpenCallToolViewController: AddNewLegDelegate {
    
    func newLegAdded(managerDetailLeg: ManagerDetailLegs) {
        
        guard let symbol = analyzerSummary?.symbol else { return }
        
        print("newLegAdded()")
        if let qty = managerDetailLeg.qty {
            print("Qty : \(qty)")
        }
        
        var instrumentString = "\(symbol)"
        
        if let expiry = managerDetailLeg.expiry {
            instrumentString.append("_\(expiry)_1")
        }
        
        if let instrument = managerDetailLeg.instrument {
            if instrument.uppercased() == "OPT" {
                if let optType = managerDetailLeg.optType, let strike = managerDetailLeg.strike {
                    instrumentString.append("_\(optType)_\(strike)")
                }
            }
        }
        
        if !instruments.contains(instrumentString) {
            instruments.append(instrumentString)
        }
        
        // get CMP for new instrument
        SVProgressHUD.show()
        
        OSOWebService.sharedInstance.getPrice(forInstrument: instrumentString) { (error, cmp) in
            SVProgressHUD.dismiss()
            
            if let err = error {
                print("Error: \(err.localizedDescription)")
            } else if let cmpValue = cmp {
                self.instrumentsAndCmp[instrumentString] = cmpValue
                managerDetailLeg.cmp = cmpValue.toString()
                self.addLeg(leg: managerDetailLeg)
            }
        }
    }
}


// MARK:- LegBookFullProfitDelegate

extension OpenCallToolViewController: LegBookFullProfitDelegate {
    func quantityUpdated(qty: Int, id: Int) {
        var index = -1
        var ctr: Int = 0
        for container in legContainers {
            if let containerId = container.containerId {
                if containerId == id {
                    index = ctr
                    break
                }
            }
            ctr += 1
        }
        
        if index != -1 {
            if tempManagerDetailLegs.indices.contains(index) {
                let leg = tempManagerDetailLegs[index]
                leg.qty = "\(qty)"
            }
        }
    }
    
    func legSelected(leg: ManagerDetailLegs, id: Int) {
        var index = -1
        var ctr: Int = 0
        for container in legContainers {
            if let containerId = container.containerId {
                if containerId == id {
                    index = ctr
                    break
                }
            }
            ctr += 1
        }
        
        if index != -1 {
            if tempManagerDetailLegs.indices.contains(index) {
                tempManagerDetailLegsSelection[index] = true
            }
        }
    }
    
    func legDeselected(leg: ManagerDetailLegs, id: Int) {
        var index = -1
        var ctr: Int = 0
        for container in legContainers {
            if let containerId = container.containerId {
                if containerId == id {
                    index = ctr
                    break
                }
            }
            ctr += 1
        }
        
        if index != -1 {
            if tempManagerDetailLegs.indices.contains(index) {
                tempManagerDetailLegsSelection[index] = false
            }
        }
    }
    
    func legRemoved(leg: ManagerDetailLegs, id: Int) {
        var index = -1
        var ctr: Int = 0
        for container in legContainers {
            if let containerId = container.containerId {
                if containerId == id {
                    index = ctr
                    break
                }
            }
            ctr += 1
        }
        
        if index != -1 {
            if tempManagerDetailLegs.indices.contains(index) {
                
                /*var newLegs = [ManagerDetailLegs]()
                var oldSelection = [Bool]()
                
                for i in 0..<tempManagerDetailLegsSelection.count {
                    if i != index {
                        newLegs.append(tempManagerDetailLegs[i])
                        oldSelection.append(tempManagerDetailLegsSelection[i])
                    }
                }
                
                renderLegs(legs: newLegs, selection: oldSelection, isLegRemoved: true)*/
                
                removeLeg(atIndex: index)
            }
        }
    }
}

// MARK:- OSONavigationBarDelegate

extension OpenCallToolViewController: OSONavigationBarDelegate {
    func rightButtonTapped(sender: OSONavigationBar) {
        print("Add Leg")
        
        if tempManagerDetailLegs.count < 4 {
            addNewLeg()
        } else {
            showZAlertView(withTitle: "Error", andMessage: "Maximum allowed legs is 4.", cancelTitle: "OK", isError: true, completion: {})
        }
        
        
        /*var newLegs = [ManagerDetailLegs]()
        
        for i in 0..<tempManagerDetailLegsSelection.count {
            let delete = tempManagerDetailLegsSelection[i]
            
            if !delete {
                newLegs.append(tempManagerDetailLegs[i])
            }
        }
        
        renderLegs(legs: newLegs)*/
        
    }
    
    func leftButtonTapped(sender: OSONavigationBar) {
        tempManagerDetailLegs.removeAll()
        legContainers.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
}
