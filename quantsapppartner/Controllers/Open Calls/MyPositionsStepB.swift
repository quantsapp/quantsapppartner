//
//  MyPositionsStepB.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 18/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import SVProgressHUD
import ZAlertView
import Sheeeeeeeeet
import SwifterSwift

protocol MyPositionsStepBDelegate: class {
    func stepBErrorOccurred()
    func stepBAnalyze(positionDetails: PositionDetails, analyzerSummary: AnalyzerSummaryList, xAxes: String, yAxes: String, maxDays: Int, upperBound: Double, lowerBound: Double)
}

extension MyPositionsStepBDelegate {
    func stepBErrorOccurred() {}
    func stepBAnalyze(positionDetails: PositionDetails, analyzerSummary: AnalyzerSummaryList, xAxes: String, yAxes: String, maxDays: Int, upperBound: Double, lowerBound: Double) {}
}

class MyPositionsStepB: UIViewController {
    
    weak var delegate: MyPositionsStepBDelegate?
    
    // navigation bar
    public var osoNavBar: OSONavigationBar?

    var viewAppeared: Bool = false
    private var screenWidth = SwifterSwift.screenWidth
    
    public var type: String?
    private var pushedFromOpenAlerts: Bool = false
    
    public var analyzerSummary: AnalyzerSummaryList?
    public var selectedBook: OSOBook?
    private var positionDetails: PositionDetails?
    private var managerDetailLegs = [ManagerDetailLegs]()
    private var initiationSpread: Double = 0
    private var currentSpread: Double = 0
    private var lot: Int = -1
    
    private var expired: Int = 0
    private var notExpired: Int = 0
    
    private var refreshTimer: Timer?
    
    private var instrumentsList = [String]()
    
    // constraints
    private var buttonsContainerBottomAnchorConstraint: NSLayoutConstraint!
    
    let buttonsContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let btnShowMoreTools: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_more")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor(white: 1.0, alpha: 0.7) // Constants.UIConfig.bottomColor
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.2) // Constants.UIConfig.themeColor
        button.layer.cornerRadius = Constants.UIConfig.buttonCornerRadius
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let detailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // setup views
        setupViews()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !viewAppeared {
            viewAppeared = true
            
            // get position details
            initPosition()
        } else {
            if instrumentsList.count > 0 {
                startTimer()
            }
        }
    }
    
    private func setupViews() {
        
        // buttonsContainer
        view.addSubview(buttonsContainer)
        buttonsContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        buttonsContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        buttonsContainer.heightAnchor.constraint(equalToConstant: Constants.APPConfig.isDeviceIphone ? 64 : 76).isActive = true
        if #available(iOS 11.0, *) {
            buttonsContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            buttonsContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        
        // btnShowMoreTools
        buttonsContainer.addSubview(btnShowMoreTools)
        btnShowMoreTools.addTarget(self, action: #selector(actionShowMoreTools(_:)), for: .touchUpInside)
        btnShowMoreTools.rightAnchor.constraint(equalTo: buttonsContainer.rightAnchor, constant: -10).isActive = true
        btnShowMoreTools.centerYAnchor.constraint(equalTo: buttonsContainer.centerYAnchor).isActive = true
        btnShowMoreTools.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnShowMoreTools.widthAnchor.constraint(equalToConstant: Constants.APPConfig.isDeviceIphone ? Constants.UIConfig.buttonHeight : 100).isActive = true
        
        // detailsTableView
        view.addSubview(detailsTableView)
        detailsTableView.isHidden = true
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        detailsTableView.backgroundColor = UIColor.clear
        detailsTableView.separatorStyle = .none
        detailsTableView.scrollsToTop = true
        detailsTableView.register(MyPositionStepBCell1.self, forCellReuseIdentifier: MyPositionStepBCell1.cellIdentifier)
        detailsTableView.register(MyPositionStepBCell2.self, forCellReuseIdentifier: MyPositionStepBCell2.cellIdentifier)
        detailsTableView.register(MyPositionStepBCell3.self, forCellReuseIdentifier: MyPositionStepBCell3.cellIdentifier)
        detailsTableView.tableFooterView = UIView()
        detailsTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        detailsTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        detailsTableView.topAnchor.constraint(equalTo: view.topAnchor , constant: 0).isActive = true
        detailsTableView.bottomAnchor.constraint(equalTo: buttonsContainer.topAnchor, constant: 0).isActive = true
    }
    
    private func initPosition() {
        if let summary = analyzerSummary {
            if let symbol = summary.symbol {
                
                // get lot for symbol
                lot = DatabaseManager.sharedInstance.getLot(forSymbol: symbol, inTable: Constants.DBTables.OptMaster)
                print("❖ Lot for \"\(symbol)\" is \(lot)")
                
                SVProgressHUD.show()
                getUpdatedCMP(symbol: symbol)
            }
        }
    }
    
    private func getUpdatedCMP(symbol: String) {
        if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbol) {
            let instrument = "\(symbol.uppercased())_\(expiry)_1"
            
            var counter: Int = 0
            
            let dispatchGroup = DispatchGroup()
            let dispatchQueue = DispatchQueue(label: "oso_cmp")
            let dispatchSemaphore = DispatchSemaphore(value: 0)
            
            dispatchQueue.async {
                dispatchGroup.enter()
                
                OSOWebService.sharedInstance.getPrice(forInstrument: instrument) { (error, price) in
                    if let priceValue = price {
                        if let summary = self.analyzerSummary {
                            summary.cmp = priceValue.roundToDecimal(2).toString()
                        }
                    }
                    
                    counter += 1
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                }
                
                dispatchSemaphore.wait()
            }
            
            dispatchGroup.notify(queue: dispatchQueue) {
                print("Finished for \(dispatchQueue.label)")
                if counter == 1 && dispatchQueue.label == "oso_cmp" {
                    if let summary = self.analyzerSummary {
                        self.getInstruments(summary: summary)
                    }
                }
            }
            
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    private func getInstruments(summary: AnalyzerSummaryList) {
        var instruments = [String]()
        
        if let symbol = summary.symbol {
            if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbol) {
                instruments.append("\(symbol.uppercased())_\(expiry)_1")
            }
        }
        
        if let positionNo = summary.positionNo {
            DatabaseManager.sharedInstance.getPositionDetails(positionNo: positionNo, completion: { (error, positions) in
                if let err = error {
                    print("Error: \(err.localizedDescription)")
                } else {
                    if let positionsList = positions {
                        for pos in positionsList {
                            if let symbol = summary.symbol, let expiry = pos.expiry, let instrument = pos.instrument, let strike = pos.strike, let optType = pos.optType {
                                var instrumentString = "\(symbol)_\(expiry)_1"
                                if instrument == "Opt" {
                                    instrumentString.append("_\(optType)_\(strike)")
                                }
                                
                                if !instruments.contains(instrumentString) {
                                    instruments.append(instrumentString)
                                }
                            }
                        }
                        
                        self.instrumentsList.removeAll()
                        self.instrumentsList = instruments
                        
                        self.getInstrumentPrice(instruments: instruments, summary: summary)
                    }
                }
            })
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    private func getInstrumentPrice(instruments: [String], summary: AnalyzerSummaryList) {
        var counter: Int = 0
        var instrumentsAndCmp: [String: Double] = [:]
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "oso_cmp")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async {
            
            dispatchGroup.enter()
            
            OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, priceList, oiList, volumeList) in
                if let priceArray = priceList {
                    print("priceArray[\(priceArray.count)] = \(priceArray)")
                    for i in 0..<instruments.count {
                        instrumentsAndCmp[instruments[i]] = priceArray[i]
                    }
                }
                
                counter += 1
                dispatchSemaphore.signal()
                dispatchGroup.leave()
            }
            
            dispatchSemaphore.wait()
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == 1 && dispatchQueue.label == "oso_cmp" {
                self.calculateMTM(summary: summary, instrumentsAndCmp: instrumentsAndCmp)
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    private func calculateMTM(summary: AnalyzerSummaryList, instrumentsAndCmp: [String: Double]) {
        var counter: Int = 0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "oso_mtm")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        if let symbol = summary.symbol {
            if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbol) {
                let instrument = "\(symbol.uppercased())_\(expiry)_1"
                
                if let cmp = instrumentsAndCmp[instrument] {
                    
                    summary.cmp = cmp.roundToDecimal(2).toString()
                }
            }
        }
        
        dispatchQueue.async {
            if let positionNo = summary.positionNo {
                
                dispatchGroup.enter()
                
                DatabaseManager.sharedInstance.getPositionDetails(positionNo: positionNo, completion: { (error, positions) in
                    if let err = error {
                        print("Error: \(err.localizedDescription)")
                    } else {
                        if let positionsList = positions {
                            
                            var totalMtm: Double = 0
                            
                            self.managerDetailLegs.removeAll()
                            
                            for pos in positionsList {
                                
                                let leg = ManagerDetailLegs()
                                
                                if let instrument = pos.instrument {
                                    leg.instrument = instrument
                                }
                                
                                if let expiry = pos.expiry {
                                    leg.expiry = expiry
                                }
                                
                                if let strike = pos.strike {
                                    leg.strike = strike
                                }
                                
                                if let optType = pos.optType {
                                    leg.optType = optType
                                }
                                
                                if let expired = pos.expired {
                                    leg.expired = expired
                                    
                                    if expired == "1" {
                                        self.expired += 1
                                    } else {
                                        self.notExpired += 1
                                    }
                                }
                                
                                if let qty = pos.qty {
                                    leg.qty = qty
                                }
                                
                                if let price = pos.price {
                                    leg.price = price
                                }
                                
                                var dateLegClosed = "-"
                                var closingPrice = "-"
                                var fixedProfit = "-"
                                
                                if let dateString = pos.dateLegClosed {
                                    dateLegClosed = dateString
                                    
                                    if let date = dateString.date(withFormat: "yyyy-MM-dd") {
                                        leg.closingDate = date
                                    }
                                }
                                
                                if let price = pos.closingPrice {
                                    closingPrice = price
                                }
                                
                                if let profit = pos.fixedProfit {
                                    fixedProfit = profit
                                }
                                
                                if dateLegClosed == "-" {
                                    if let papi = pos.Papi, let symbol = pos.symbol {
                                        print("\(positionNo): \(papi)")
                                        
                                        let splitPapi = papi.components(separatedBy: ",")
                                        
                                        if let qty = splitPapi[2].double(), let price = splitPapi[1].double() {
                                            let fullInstrument = splitPapi[0].components(separatedBy: " ")
                                            var instrumentString = "\(symbol)_\(fullInstrument[1])_1"
                                            if fullInstrument[0] == "Opt" {
                                                instrumentString.append("_\(fullInstrument[3])_\(fullInstrument[2])")
                                            }
                                            
                                            if let cmp = instrumentsAndCmp[instrumentString] {
                                                
                                                leg.cmp = cmp.roundToDecimal(2).toString()
                                                print("\(instrumentString) (CMP): \(cmp)")
                                                
                                                var mtm: Double = 0
                                                
                                                mtm = (cmp * qty) - (price * qty)
                                                
                                                leg.mtm = mtm.roundToDecimal(2).toString()
                                                
                                                totalMtm += mtm
                                            }
                                        }
                                    }
                                } else {
                                    if closingPrice != "-" {
                                        leg.cmp = closingPrice
                                    }
                                    
                                    if fixedProfit != "-" {
                                        leg.mtm = fixedProfit
                                        
                                        if let dblVal = fixedProfit.double() {
                                            totalMtm += dblVal
                                        }
                                    }
                                }
                                
                                self.managerDetailLegs.append(leg)
                            }
                            
                            print("total MTM = \(totalMtm)")
                            summary.mtm = totalMtm.roundToDecimal(0).toString()
                        }
                    }
                    
                    counter += 1
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                })
                
                dispatchSemaphore.wait()
            } else {
                SVProgressHUD.dismiss()
            }
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == 1 && dispatchQueue.label == "oso_mtm" {
                self.calculateSpread()
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.detailsTableView.isHidden = false
                    //self.updateButtonsContainerConstraints()
                    self.detailsTableView.reloadData()
                    
                    // start refresh timer
                    self.startTimer()
                }
            }
        }
    }
    
    private func calculateSpread() {
        
        var totalCurrentPremium: Double = 0
        var totalInitiationPremium: Double = 0
        var minQty: Double = Double.greatestFiniteMagnitude
        
        for i in 0..<managerDetailLegs.count {
            let leg = managerDetailLegs[i]
            
            if let priceValue = leg.price?.double(), let qtyValue = leg.qty?.double(), let cmpValue = leg.cmp?.double() {
                if qtyValue.abs < minQty {
                    minQty = qtyValue.abs
                }
                
                totalInitiationPremium += priceValue * qtyValue
                totalCurrentPremium += cmpValue * qtyValue
            }
        }
        
        if minQty > 0 {
            if totalInitiationPremium != 0 {
                initiationSpread = totalInitiationPremium/minQty
            }
            
            if totalCurrentPremium != 0 {
                currentSpread = totalCurrentPremium/minQty
            }
        }
    }
    
    private func getPositionDetails(positionNo: String) {
        
        DatabaseManager.sharedInstance.getPositionDetails(positionNo: positionNo, completion: { (error, summaries) in
            if let err = error {
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    if self.pushedFromOpenAlerts {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        self.delegate?.stepBErrorOccurred()
                    }
                })
            } else {
                
                var mtmTemp: Double = 0
                
                self.managerDetailLegs.removeAll()
                
                if let summaryList = summaries {
                    
                    for i in 0..<summaryList.count {
                        let leg = ManagerDetailLegs()
                        let summary = summaryList[i]
                        
                        if let instrument = summary.instrument {
                            leg.instrument = instrument
                        }
                        
                        if let expiry = summary.expiry {
                            leg.expiry = expiry
                        }
                        
                        if let strike = summary.strike {
                            leg.strike = strike
                        }
                        
                        if let optType = summary.optType {
                            leg.optType = optType
                        }
                        
                        if let expired = summary.expired {
                            leg.expired = expired
                            
                            if expired == "1" {
                                self.expired += 1
                            } else {
                                self.notExpired += 1
                            }
                        }
                        
                        if let qty = summary.qty {
                            leg.qty = qty
                        }
                        
                        if let price = summary.price {
                            leg.price = price
                        }
                        
                        self.managerDetailLegs.append(leg)
                    }
                    
                    DispatchQueue.main.async {
                        self.detailsTableView.isHidden = false
                        //self.updateButtonsContainerConstraints()
                        self.detailsTableView.reloadData()
                    }
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: "No positions found.", cancelTitle: "OK", isError: true, completion: {
                        if self.pushedFromOpenAlerts {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.delegate?.stepBErrorOccurred()
                        }
                    })
                }
            }
        })
    }
    
    
    
    func createGradientLayer(topColor:UIColor, bottomColor:UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        view.layer.addSublayer(gradientLayer)
    }
    
    /*func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: Constants.OSONavigationBarConstants.barHeight), title: "Position Details", subTitle: nil, leftbuttonImage: UIImage(named: "icon_left"), rightButtonImage: nil)
        view.addSubview(osoNavBar)
        osoNavBar.delegate = self
        
    }*/
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    public func actionBack() {
        stopTimer()
    }
    
    public func refresh() {
        guard let summary = analyzerSummary else { return }
        
        refreshPrices(summary: summary, showProgressIndicator: true)
    }
    
    private func refreshPrices(summary: AnalyzerSummaryList, showProgressIndicator: Bool = false) {
        
        if showProgressIndicator {
            SVProgressHUD.show()
        }
        
        var instrumentsAndCmp: [String: Double] = [:]
        
        OSOWebService.sharedInstance.getPrice(forInstruments: instrumentsList) { (error, priceList, oiList, volumeList) in
            
            if showProgressIndicator {
                SVProgressHUD.dismiss()
            }
            
            if let err = error {
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    self.startTimer()
                })
            } else {
                if let priceArray = priceList {
                    print("priceArray[\(priceArray.count)] = \(priceArray)")
                    for i in 0..<self.instrumentsList.count {
                        instrumentsAndCmp[self.instrumentsList[i]] = priceArray[i]
                    }
                    
                    self.calculateMTM(summary: summary, instrumentsAndCmp: instrumentsAndCmp)
                } else {
                    self.startTimer()
                }
            }
        }
    }
    
    // MARK:- Refresh Timer
    
    private func startTimer() {
        print("► startTimer()")
        if let timer = refreshTimer {
            timer.invalidate()
            refreshTimer = nil
        }
        
        if Constants.APIConfig.AutomaticUpdateEnabled {
            refreshTimer = Timer.scheduledTimer(timeInterval: Constants.APIConfig.PriceUpdateRefreshTime, target: self, selector: #selector(timerFired(_:)), userInfo: nil, repeats: false)
        }
    }
    
    @objc private func timerFired(_ sender: Timer) {
        print("► timerFired()")
        sender.invalidate()
        
        if instrumentsList.count == 0 {
            startTimer()
            return
        }
        
        guard let summary = analyzerSummary else {
            startTimer()
            return
        }
        
        refreshPrices(summary: summary)
    }
    
    private func stopTimer() {
        print("► stopTimer()")
        OSOWebService.sharedInstance.cancelWebRequest()
        
        if let timer = refreshTimer {
            timer.invalidate()
            refreshTimer = nil
        }
    }
    
    
    // MARK:- Show More
    
    @objc private func actionShowMoreTools(_ sender: UIButton) {
        showMoreTools(sender: sender)
    }
    
    public func showMoreTools(sender: UIButton? = nil) {
        
        guard let osoNavigationBar = self.osoNavBar  else { return }
        
        if lot == -1 {
            return
        }
        
        var isEligibleForPartProfit: Bool = true
        
        for leg in managerDetailLegs {
            if let absQty = leg.qty?.int?.abs {
                if absQty / lot == 1 {
                    isEligibleForPartProfit = false
                } else if (absQty/2) % lot != 0 {
                    isEligibleForPartProfit = false
                }
            }
        }
        
        var items = [ActionSheetItem]()
        
        if isEligibleForPartProfit {
            let itemPartProfit = ActionSheetItem(title: OpenCallToolType.PartProfit.title(), value: OpenCallToolType.PartProfit.rawValue, image: nil)
            items.append(itemPartProfit)
        }
        
        let itemModify = ActionSheetItem(title: OpenCallToolType.Modify.title(), value: OpenCallToolType.Modify.rawValue, image: nil)
        items.append(itemModify)
        
        let itemBookFullProfit = ActionSheetItem(title: OpenCallToolType.BookFullProfit.title(), value: OpenCallToolType.BookFullProfit.rawValue, image: nil)
        items.append(itemBookFullProfit)
        
        let itemExit = ActionSheetItem(title: OpenCallToolType.Exit.title(), value: OpenCallToolType.Exit.rawValue, image: nil)
        items.append(itemExit)
        
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            
            if let selectedType = OpenCallToolType(rawValue: value) {
                self.performActionOnCall(toolSelected: selectedType)
            }
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        
        if let button = sender {
            actionSheet.present(in: self, from: button)
        } else {
            actionSheet.present(in: self, from: osoNavigationBar.rightBarButton)
        }
        
    }
    
    private func performActionOnCall(toolSelected: OpenCallToolType) {
        print(toolSelected.title())
        
        // stop timer
        stopTimer()
        
        var nonClosedLegs = [ManagerDetailLegs]()
        
        for leg in managerDetailLegs {
            if leg.closingDate == nil {
                nonClosedLegs.append(leg)
            }
        }
        
        let vc = OpenCallToolViewController()
        vc.lot = lot
        vc.selectedTool = toolSelected
        vc.selectedBook = selectedBook
        vc.managerDetailLegs = nonClosedLegs // managerDetailLegs
        vc.analyzerSummary = analyzerSummary
        vc.currentSpread = currentSpread
        
        if let summary = analyzerSummary {
            if let spread = summary.spreadTarget?.double() {
                vc.targetSpread = spread
            }
            
            if let spread = summary.spreadStopLoss?.double() {
                vc.stopLossSpread = spread
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    // MARK:- UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let parentView = textField.superview {
            parentView.endEditing(true)
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let inverseSet = NSCharacterSet(charactersIn:"0123456789-").inverted
        let components = string.components(separatedBy: inverseSet)
        let filtered = components.joined(separator: "")
        
        if string == filtered {
            if string == "-" {
                let countMinus = (textField.text?.components(separatedBy: "-").count)! - 1
                
                if countMinus == 0 {
                    if textField.text?.count == 0 {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            } else {
                return true
            }
        } else {
            return false
        }
    }
    
    deinit {
        print("► deinit()")
        stopTimer()
    }

}

// MARK:-  OSONavigationBarDelegate

extension MyPositionsStepB: OSONavigationBarDelegate {
    
    func leftButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func rightButtonTapped() {
        print("Show More...")
    }
}



// MARK:- UITableViewDataSource

extension MyPositionsStepB: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return managerDetailLegs.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: MyPositionStepBCell1.cellIdentifier, for: indexPath) as! MyPositionStepBCell1
            
            if let summary = analyzerSummary {
                // date created
                if let dateCreated = summary.dateCreated?.string(withFormat: "dd-MMM-yy") {
                    cell.lblDateCreatedValue.text = dateCreated
                } else {
                    cell.lblDateCreatedValue.text = "-"
                }
                
                // CMP
                if let cmp = summary.cmp {
                    if let dblVal = cmp.double()?.roundToDecimal(2) {
                        cell.lblCMPValue.text = dblVal.toString()
                    } else {
                        cell.lblCMPValue.text = cmp
                    }
                } else {
                    cell.lblCMPValue.text = "-"
                }
                
                // MTM
                if let mtm = summary.mtm {
                    if let dblVal = mtm.double() {
                        if dblVal > 0 {
                            cell.lblMTMValue.textColor = Constants.Charts.chartGreenColor
                        } else if dblVal < 0 {
                            cell.lblMTMValue.textColor = Constants.Charts.chartRedColor
                        } else {
                            cell.lblMTMValue.textColor = .white
                        }
                        
                        cell.lblMTMValue.text = dblVal.roundToDecimal(2).toString()
                    } else {
                        cell.lblMTMValue.text = mtm
                        cell.lblMTMValue.textColor = .white
                    }
                } else {
                    cell.lblMTMValue.text = "-"
                    cell.lblMTMValue.textColor = .white
                }
                
                // initiation spread
                if initiationSpread == 0 {
                    cell.lblInitiationSpreadValue.text = initiationSpread.roundToDecimal(2).toString()
                    cell.lblInitiationSpreadValue.textColor = .white
                } else {
                    let style = NSMutableParagraphStyle()
                    style.alignment = NSTextAlignment.right
                    
                    let attributedString = NSMutableAttributedString()
                    attributedString.append(NSAttributedString(string: "\(initiationSpread.abs.roundToDecimal(2).toString())", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.paragraphStyle: style]))
                    attributedString.append(NSAttributedString(string: " \(initiationSpread == 0 ? "" : initiationSpread < 0 ? "Cr" : "")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: initiationSpread == 0 ? UIColor(white: 1.0, alpha: 0.3) : initiationSpread < 0 ? Constants.Charts.chartGreenColor : Constants.Charts.chartRedColor, NSAttributedString.Key.paragraphStyle: style]))
                    cell.lblInitiationSpreadValue.attributedText = attributedString
                }
                
                
                // current spread
                if currentSpread == 0 {
                    cell.lblCurrentSpreadValue.text = currentSpread.roundToDecimal(2).toString()
                    cell.lblCurrentSpreadValue.textColor = .white
                } else {
                    let style = NSMutableParagraphStyle()
                    style.alignment = NSTextAlignment.right
                    
                    let attributedString = NSMutableAttributedString()
                    attributedString.append(NSAttributedString(string: "\(currentSpread.abs.roundToDecimal(2).toString())", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.paragraphStyle: style]))
                    attributedString.append(NSAttributedString(string: " \(currentSpread == 0 ? "" : currentSpread < 0 ? "Cr" : "")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: currentSpread == 0 ? UIColor(white: 1.0, alpha: 0.3) : currentSpread < 0 ? Constants.Charts.chartGreenColor : Constants.Charts.chartRedColor, NSAttributedString.Key.paragraphStyle: style]))
                    cell.lblCurrentSpreadValue.attributedText = attributedString
                }
                
                // target spread
                if let spread = summary.spreadTarget {
                    if let dblVal = spread.double() {
                        cell.lblSpreadTargetValue.text = dblVal.roundToDecimal(2).toString()
                    } else {
                        cell.lblSpreadTargetValue.text = spread
                    }
                } else {
                    cell.lblSpreadTargetValue.text = "-"
                }
                
                // stop loss spread
                if let spread = summary.spreadStopLoss {
                    if let dblVal = spread.double() {
                        cell.lblSpreadStopLossValue.text = dblVal.roundToDecimal(2).toString()
                    } else {
                        cell.lblSpreadStopLossValue.text = spread
                    }
                } else {
                    cell.lblSpreadStopLossValue.text = "-"
                }
            } else {
                cell.lblDateCreatedValue.text = "-"
                cell.lblCMPValue.text = "-"
                cell.lblMTMValue.text = "-"
                cell.lblInitiationSpreadValue.text = "-"
                cell.lblCurrentSpreadValue.text = "-"
                cell.lblSpreadTargetValue.text = "-"
                cell.lblSpreadStopLossValue.text = "-"
            }
            
            if let details = self.positionDetails {
                if let iv = details.iv {
                    cell.lblAtmVolValue.text = (iv == "0") ? "-" : iv
                } else {
                    cell.lblAtmVolValue.text = "-"
                }
                
                if let summary = self.analyzerSummary {
                    if let mtm = summary.mtm {
                        if let mtmDouble = mtm.double()?.roundToDecimal(0) {
                            cell.lblMTMValue.text = mtmDouble.toString()
                            
                            if mtmDouble > 0 {
                                cell.lblMTMValue.textColor = Constants.Charts.chartGreenColor
                            } else if mtmDouble < 0 {
                                cell.lblMTMValue.textColor = Constants.Charts.chartRedColor
                            } else {
                                cell.lblMTMValue.textColor = UIColor.white
                            }
                        } else {
                            cell.lblMTMValue.text = "-"
                            cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                        }
                    } else {
                        cell.lblMTMValue.text = "-"
                        cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                    }
                    
                    if let cmp = summary.cmp {
                        cell.lblCMPValue.text = cmp
                    } else {
                        cell.lblCMPValue.text = "-"
                    }
                    
                    /*if let margin = summary.margin {
                        cell.lblMarginValue.text = margin == "NaN" ? "-" : margin
                    } else {
                        cell.lblMarginValue.text = "-"
                    }
                    
                    if let roi = summary.roi {
                        cell.lblROIValue.text = roi
                    } else {
                        cell.lblROIValue.text = "-"
                    }*/
                    
                    if let dateCreated = summary.dateCreated {
                        let formatter = DateFormatter()
                        formatter.dateStyle = .short
                        
                        let date = formatter.string(from: dateCreated)
                        cell.lblDateCreatedValue.text = date //"\(dateCreated.string(withFormat: "dd-MMM-yy"))"
                    } else {
                        cell.lblDateCreatedValue.text = "-"
                    }
                    
                    /*if let alertOne = summary.alertOne {
                        cell.lblAlertAboveValue.text = (alertOne == "0") ? "-" : alertOne
                    } else {
                        cell.lblAlertAboveValue.text = "-"
                    }
                    
                    if let alertTwo = summary.alertTwo {
                        cell.lblAlertBelowValue.text = (alertTwo == "0") ? "-" : alertTwo
                    } else {
                        cell.lblAlertBelowValue.text = "-"
                    }*/
                }
            }
            
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: MyPositionStepBCell2.cellIdentifier, for: indexPath) as! MyPositionStepBCell2
            
            if let details = self.positionDetails {
                if let acType = details.acType {
                    if acType == "1" {
                        // pro user
                        if let delta = details.delta {
                            if let doubleValue = delta.double() {
                                if doubleValue == 0 {
                                    //cell.lblDeltaValue.text = "0"
                                    cell.deltaValue = "0"
                                } else {
                                    //cell.lblDeltaValue.text = delta
                                    cell.deltaValue = doubleValue.toString()
                                }
                            } else {
                                //cell.lblDeltaValue.text = delta
                                cell.deltaValue = delta
                            }
                        } else {
                            cell.deltaValue = "-"
                        }
                        
                        if let theta = details.theta {
                            if let doubleValue = theta.double() {
                                if doubleValue == 0 {
                                    //cell.lblThetaValue.text = "0"
                                    cell.thetaValue = "0"
                                } else {
                                    //cell.lblThetaValue.text = theta
                                    cell.thetaValue = doubleValue.toString()
                                }
                            } else {
                                //cell.lblThetaValue.text = theta
                                cell.thetaValue = theta
                            }
                        } else {
                            cell.thetaValue = "-"
                        }
                        
                        if let vega = details.vega {
                            if let doubleValue = vega.double() {
                                if doubleValue == 0 {
                                    //cell.lblVegaValue.text = "0"
                                    cell.vegaValue = "0"
                                } else {
                                    //cell.lblVegaValue.text = vega
                                    cell.vegaValue = doubleValue.toString()
                                }
                            } else {
                                //cell.lblVegaValue.text = vega
                                cell.vegaValue = vega
                            }
                        } else {
                            cell.vegaValue = "-"
                        }
                        
                        if let gamma = details.gamma {
                            if let doubleValue = gamma.double() {
                                if doubleValue == 0 {
                                    //cell.lblGammaValue.text = "0"
                                    cell.gammaValue = "0"
                                } else {
                                    //cell.lblGammaValue.text = gamma
                                    cell.gammaValue = doubleValue.toString()
                                }
                            } else {
                                //cell.lblGammaValue.text = gamma
                                cell.gammaValue = gamma
                            }
                        } else {
                            cell.gammaValue = "-"
                        }
                        
                    } else {
                        // lite user
                        
                        /*cell.lblDeltaValue.text = "GO PRO"
                        cell.lblThetaValue.text = "GO PRO"
                        cell.lblVegaValue.text = "GO PRO"
                        cell.lblGammaValue.text = "GO PRO"*/
                        
                        cell.deltaValue = "GO PRO"
                        cell.thetaValue = "GO PRO"
                        cell.vegaValue = "GO PRO"
                        cell.gammaValue = "GO PRO"
                    }
                }
            }
            
            return cell
            
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: MyPositionStepBCell3.cellIdentifier, for: indexPath) as! MyPositionStepBCell3
            
            let leg = self.managerDetailLegs[indexPath.row]
            var legClosedDate = "-"
            
            if let date = leg.closingDate {
                legClosedDate = date.string(withFormat: "dd-MMM-yy")
            }
            
            if let expired = leg.expired {
                cell.expired = expired
            }
            
            if legClosedDate == "-" {
                cell.lblMTM.textColor = UIColor(white: 1.0, alpha: 0.5)
                cell.lblMtmText = "MTM"
                
                cell.lblCMPText = "CMP"
                
                cell.lblPriceText = "Price"
                
                cell.backgroundColor = UIColor.clear
            } else {
                cell.lblMTM.textColor = UIColor(white: 1.0, alpha: 0.3)
                cell.lblMtmText = "Close Price"
                
                cell.lblCMPText = "PnL"
                
                cell.lblPriceText = "Init. Price"
                
                cell.backgroundColor = UIColor(white: 1.0, alpha: 0.05)
            }
            
            if legClosedDate == "-" {
                if let mtm = leg.mtm {
                    if let mtmDouble = mtm.double()?.roundToDecimal(0) {
                        cell.lblMTMValue.text = mtmDouble.toString()
                        
                        if mtmDouble > 0 {
                            cell.lblMTMValue.textColor = Constants.Charts.chartGreenColor
                        } else if mtmDouble < 0 {
                            cell.lblMTMValue.textColor = Constants.Charts.chartRedColor
                        } else {
                            cell.lblMTMValue.textColor = UIColor.white
                        }
                    } else {
                        cell.lblMTMValue.text = mtm
                        cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                    }
                } else {
                    cell.lblMTMValue.text = "-"
                    cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                }
                
                if let cmp = leg.cmp {
                    if let dblVal = cmp.double()?.roundToDecimal(2) {
                        cell.lblCMPValue.text = dblVal.toString()
                    } else {
                        cell.lblCMPValue.text = cmp
                    }
                } else {
                    cell.lblCMPValue.text = "-"
                }
                
                cell.lblDateClosed.text = ""
                cell.iconInformation.image = nil
            } else {
                if let cmp = leg.cmp {
                    if let dblVal = cmp.double()?.roundToDecimal(2) {
                        cell.lblMTMValue.text = dblVal.toString()
                    } else {
                        cell.lblMTMValue.text = cmp
                    }
                    cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                } else {
                    cell.lblMTMValue.text = "-"
                    cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                }
                
                if let pnl = leg.mtm {
                    if let dblVal = pnl.double()?.roundToDecimal(0) {
                        cell.lblCMPValue.text = dblVal.toString()
                        
                        if dblVal > 0 {
                            cell.lblCMPValue.textColor = Constants.Charts.chartGreenColor
                        } else if dblVal < 0 {
                            cell.lblCMPValue.textColor = Constants.Charts.chartRedColor
                        } else {
                            cell.lblCMPValue.textColor = UIColor.white
                        }
                    } else {
                        cell.lblCMPValue.text = pnl
                        cell.lblCMPValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                    }
                } else {
                    cell.lblCMPValue.text = "-"
                    cell.lblCMPValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                }
                
                let attrString = NSMutableAttributedString()
                attrString.append(NSAttributedString(string: "Closed On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                attrString.append(NSAttributedString(string: "\(legClosedDate)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.8)]))
                cell.lblDateClosed.attributedText = attrString
                
                cell.iconInformation.image = UIImage(named: "icon_info")?.withRenderingMode(.alwaysTemplate)
                cell.iconInformation.tintColor = Constants.UIConfig.themeColor
            }
            
            
            
            
            
            if let instrument = leg.instrument {
                cell.lblInstrumentValue.text = instrument
            } else {
                cell.lblInstrumentValue.text = "-"
            }
            
            if let strike = leg.strike {
                if strike == "-1" {
                    cell.lblStrikeValue.text = "-"
                } else {
                    if let dblVal = strike.double() {
                        cell.lblStrikeValue.text = dblVal.toString()
                    } else {
                        cell.lblStrikeValue.text = strike
                    }
                }
            } else {
                cell.lblStrikeValue.text = "-"
            }
            
            if let expiry = leg.expiry {
                cell.lblExpiryValue.text = expiry
            } else {
                cell.lblExpiryValue.text = "-"
            }
            
            if let optType = leg.optType {
                cell.lblOptionTypeValue.text = optType
            } else {
                cell.lblOptionTypeValue.text = "-"
            }
            
            if let price = leg.price {
                if let dblVal = price.double()?.roundToDecimal(2) {
                    cell.lblPriceValue.text = dblVal.toString()
                } else {
                    cell.lblPriceValue.text = price
                }
            } else {
                cell.lblPriceValue.text = "-"
            }
            
            if let qty = leg.qty {
                if let dblVal = qty.double() {
                    cell.lblQuantityValue.text = dblVal.toString()
                } else {
                    cell.lblQuantityValue.text = qty
                }
            } else {
                cell.lblQuantityValue.text = "-"
            }
            
            /*if let delta = leg.delta {
                cell.deltaValue = delta
            } else {
                cell.deltaValue = "-"
            }
            
            if let theta = leg.theta {
                cell.thetaValue = theta
            } else {
                cell.thetaValue = "-"
            }
            
            if let vega = leg.vega {
                cell.vegaValue = vega
            } else {
                cell.vegaValue = "-"
            }
            
            if let gamma = leg.gamma {
                cell.gammaValue = gamma
            } else {
                cell.gammaValue = "-"
            }*/
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
    }
}

// MARK:- UITableViewDelegate

extension MyPositionsStepB: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 10, height: 30))
        header.backgroundColor = UIColor(white: 0.0, alpha: 0.9)
        
        if section == 0 {
            if let summary = self.analyzerSummary {
                /*let lblTradeStatus = UILabel()
                lblTradeStatus.textColor = UIColor(white: 1.0, alpha: 0.5)
                lblTradeStatus.font = UIFont.systemFont(ofSize: 13)
                lblTradeStatus.textAlignment = .right
                lblTradeStatus.translatesAutoresizingMaskIntoConstraints = false
                
                var tradeStatus: String = ""
                
                if let status = summary.tradeStatus {
                    tradeStatus = status
                }
                
                if self.notExpired == 0 {
                    lblTradeStatus.text = "Expired"
                    lblTradeStatus.textColor = Constants.Charts.chartRedColor
                } else if self.notExpired > 0 && self.expired > 0 {
                    let tempStatus = tradeStatus == "1" ? "Traded" : "Tracking"
                    lblTradeStatus.text = "\(tempStatus): Partially Expired"
                    lblTradeStatus.textColor = UIColor.red
                } else if tradeStatus == "1" {
                    lblTradeStatus.text = "Traded"
                    lblTradeStatus.textColor = Constants.Charts.chartGreenColor
                } else {
                    lblTradeStatus.text = "Tracking"
                    lblTradeStatus.textColor = UIColor.yellow
                }
                
                header.addSubview(lblTradeStatus)
                lblTradeStatus.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
                lblTradeStatus.topAnchor.constraint(equalTo: header.topAnchor, constant: 0).isActive = true
                lblTradeStatus.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: 0).isActive = true
                lblTradeStatus.widthAnchor.constraint(equalToConstant: (lblTradeStatus.text?.sizeOfString(usingFont: lblTradeStatus.font).width)! + 4).isActive = true*/
                
                let lblSymbolAndPosition = UILabel()
                lblSymbolAndPosition.adjustsFontSizeToFitWidth = true
                lblSymbolAndPosition.minimumScaleFactor = 0.8
                lblSymbolAndPosition.translatesAutoresizingMaskIntoConstraints = false
                header.addSubview(lblSymbolAndPosition)
                lblSymbolAndPosition.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 10).isActive = true
                lblSymbolAndPosition.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
                //lblSymbolAndPosition.rightAnchor.constraint(equalTo: lblTradeStatus.leftAnchor, constant: -8).isActive = true
                lblSymbolAndPosition.topAnchor.constraint(equalTo: header.topAnchor, constant: 0).isActive = true
                lblSymbolAndPosition.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: 0).isActive = true
                
                if let symbol = summary.symbol, let positionName = summary.positionName {
                    let attrString = NSMutableAttributedString()
                    attrString.append(NSAttributedString(string: "\(symbol) ", attributes: [NSAttributedString.Key.foregroundColor: Constants.UIConfig.themeColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13)]))
                    attrString.append(NSAttributedString(string: " \(positionName)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.75)]))
                    
                    lblSymbolAndPosition.attributedText = attrString
                }
            }
            
        } else if section == 1 {
            let lblGreeks = UILabel()
            lblGreeks.text = "GREEKS"
            lblGreeks.font = UIFont.boldSystemFont(ofSize: 12)
            lblGreeks.textColor = UIColor(white: 1.0, alpha: 0.7)
            lblGreeks.translatesAutoresizingMaskIntoConstraints = false
            header.addSubview(lblGreeks)
            lblGreeks.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 10).isActive = true
            lblGreeks.heightAnchor.constraint(equalToConstant: 20).isActive = true
            lblGreeks.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
            lblGreeks.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
        } else if section == 2 {
            let lblPositions = UILabel()
            lblPositions.text = "POSITIONS"
            lblPositions.font = UIFont.boldSystemFont(ofSize: 12)
            lblPositions.textColor = UIColor(white: 1.0, alpha: 0.7)
            lblPositions.translatesAutoresizingMaskIntoConstraints = false
            header.addSubview(lblPositions)
            lblPositions.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 10).isActive = true
            lblPositions.heightAnchor.constraint(equalToConstant: 20).isActive = true
            lblPositions.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
            lblPositions.widthAnchor.constraint(equalToConstant: (lblPositions.text?.sizeOfString(usingFont: lblPositions.font).width)! + 4).isActive = true
            
            let lblDataTime = UILabel()
            lblDataTime.textAlignment = .right
            lblDataTime.adjustsFontSizeToFitWidth = true
            lblDataTime.minimumScaleFactor = 0.8
            lblDataTime.translatesAutoresizingMaskIntoConstraints = false
            header.addSubview(lblDataTime)
            lblDataTime.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
            lblDataTime.leftAnchor.constraint(equalTo: lblPositions.rightAnchor, constant: 8).isActive = true
            lblDataTime.topAnchor.constraint(equalTo: header.topAnchor, constant: 0).isActive = true
            lblDataTime.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: 0).isActive = true
            
            if let details = self.positionDetails {
                if let dataTime = details.dataTime {
                    let attrString = NSMutableAttributedString()
                    attrString.append(NSAttributedString(string: "Price Updated At ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.5), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                    attrString.append(NSAttributedString(string: " \(dataTime)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: Constants.UIConfig.themeColor]))
                    
                    lblDataTime.attributedText = attrString
                }
            }
        }
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 128 //88
        case 1:
            return 0 //48
        case 2:
            let leg = managerDetailLegs[indexPath.row]
            var legClosedDate = "-"
            
            if let date = leg.closingDate {
                legClosedDate = date.string(withFormat: "dd-MMM-yy")
            }
            
            if legClosedDate == "-" {
                return 72 //104
            } else {
                return 72 + 24
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 30
        case 1:
            return 0
        case 2:
            return 30
        default:
            return 0
        }
    }
}
