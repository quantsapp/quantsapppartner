//
//  PredefinedStrategiesStepC.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 13/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SVProgressHUD
import ZAlertView
import DLRadioButton

protocol PredefinedStrategiesStepCDelegate: class {
    func stepCErrorOccured()
    func stepCEnded()
    func stepCCompleted(symbol: String, direction: Int, risk: Int, positionNo: String, strategyName: String)
    func stepCFinished(positionName: String, symbol: String, margin: String, legs: [String], tradeStatus: String, direction: Int, risk: Int, strategyName: String)
}

extension PredefinedStrategiesStepCDelegate {
    func stepCErrorOccured() {}
    func stepCEnded() {}
    func stepCCompleted(symbol: String, direction: Int, risk: Int, positionNo: String, strategyName: String) {}
    func stepCFinished(positionName: String, symbol: String, margin: String, legs: [String], tradeStatus: String, direction: Int, risk: Int, strategyName: String) {}
}

class PredefinedStrategiesStepC: UIViewController {
    
    weak var delegate: PredefinedStrategiesStepCDelegate?
    
    var viewAppeared: Bool = false
    private var activeTextField: UITextField?
    
    var fieldVerticalGap: CGFloat = 4
    var fieldHeight: CGFloat = 30
    var instrumentContainerHeight: CGFloat = 66
    
    // constraints
    var scrollViewContainerBottomAnchorConstraint: NSLayoutConstraint!
    var formHeightConstraint: NSLayoutConstraint!
    var instrumentAContainerHeightConstraint: NSLayoutConstraint!
    var instrumentBContainerHeightConstraint: NSLayoutConstraint!
    var instrumentCContainerHeightConstraint: NSLayoutConstraint!
    var instrumentDContainerHeightConstraint: NSLayoutConstraint!
    private var btnLotsWidthConstraint: NSLayoutConstraint!
    
    public var symbol: String?
    public var direction: Int?
    public var risk: Int?
    public var strategy: MyLibrary?
    
    private var defaultLotValue: Int = 2
    private var legs: Int = 0
    private var buySell = [Int]()
    private var callPut = [Int]()
    private var strikePosition = [Int]()
    private var lots = [Double]()
    private var expiry = [Int]()
    
    // strikes
    var strikes: [String]?
    
    // expiries
    private var selectedExpiry: Int = 0
    var expiries: [String]?
    
    // optMaster
    var optMasters: [OptMaster]?
    
    // constraint
    var formBottomAnchorConstraint: NSLayoutConstraint!
    
    // ZAlertView TextField
    private var tfLots: UITextField!
    
    var radioButtons = [DLRadioButton]()
    var selectedRadioButton: Int = 0
    
    let scrollViewContainer: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    let formView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let btnGetStarted: UIButton = {
        let button = UIButton()
        button.setTitle("NEXT", for: .normal)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.9)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageViewDirection: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let imageViewRisk: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.tintColor = UIColor(white: 1.0, alpha: 0.6)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let lblRisk: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor(white: 1.0, alpha: 0.8)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblSymbol: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = Constants.UIConfig.themeColor
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "CMP"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMPValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblBaseExpiry: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Base Expiry"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownBaseExpiry: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblLots: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Lots"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let btnLots: UIButton = {
        let button = UIButton()
        button.titleLabel?.textAlignment = .center
        button.contentHorizontalAlignment = .center
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnAddQty: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.setImage(UIImage(named: "icon_increment")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDelete(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnDeleteQty: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.setImage(UIImage(named: "icon_decrement")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDelete(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    
    private let instrumentHeaderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let positionHeaderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblInstrumentHeader: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.text = "Instruments"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPositionHeader: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.text = "Position Details"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let btnFetchCMP: FetchCMPButton = {
        let button = FetchCMPButton()
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.7), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.setImage(UIImage(named: "icon_import")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.layer.cornerRadius = 3
        button.layer.masksToBounds = true
        button.clipsToBounds = true
        button.setTitle("Fetch CMP", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblPositionName: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Position Name"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tfPositionName: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.default
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderPositionName: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK:- Instrument A
    
    private let instrumentAContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblInstrumentA: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyA: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "QTY"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyAValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrikeA: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Strike"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownStrikeA: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblPriceA: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Price"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tfPriceA: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderPriceA: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK:- Instrument B
    
    private let instrumentBContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblInstrumentB: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyB: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "QTY"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyBValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrikeB: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Strike"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownStrikeB: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblPriceB: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Price"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tfPriceB: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderPriceB: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK:- Instrument C
    
    private let instrumentCContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblInstrumentC: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyC: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "QTY"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyCValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrikeC: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Strike"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownStrikeC: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblPriceC: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Price"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tfPriceC: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderPriceC: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    // MARK:- Instrument D
    
    private let instrumentDContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblInstrumentD: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyD: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "QTY"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyDValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrikeD: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Strike"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownStrikeD: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblPriceD: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Price"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tfPriceD: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderPriceD: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    

    // MARK:- VIEW

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // keyboard event handler
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // setup views
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !viewAppeared {
            viewAppeared = true
            
            if let strategy = self.strategy {
                if let symbolName = self.symbol {
                    getStrategyBuilderDetails(strategy: strategy, symbolName: symbolName)
                } else {
                    delegate?.stepCErrorOccured()
                }
            } else {
                delegate?.stepCErrorOccured()
            }
        }
        
    }

    private func setupViews() {
        // btnGetStarted
        view.addSubview(btnGetStarted)
        btnGetStarted.addTarget(self, action: #selector(actionGetStarted(_:)), for: .touchUpInside)
        btnGetStarted.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        btnGetStarted.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnGetStarted.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        btnGetStarted.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        
        
        // headerView
        view.addSubview(headerView)
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        headerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        headerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        // imageViewDirection
        var directionImageName = ""
        
        if let direction = self.direction {
            switch direction {
            case 0:
                directionImageName = "icon_bullish"
            case 1:
                directionImageName = "icon_bearish"
            case 2:
                directionImageName = "icon_eitherways"
            case 3:
                directionImageName = "icon_oscillate"
            default:
                directionImageName = ""
            }
        }
        
        imageViewDirection.image = UIImage(named: directionImageName)?.withRenderingMode(.alwaysOriginal)
        headerView.addSubview(imageViewDirection)
        imageViewDirection.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
        imageViewDirection.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewDirection.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewDirection.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 8).isActive = true
        
        // lblSymbol
        headerView.addSubview(lblSymbol)
        if let symbolName = self.symbol {
            lblSymbol.text = "\(symbolName)"
        } else {
            lblSymbol.text = "-"
        }
        
        lblSymbol.leftAnchor.constraint(equalTo: imageViewDirection.rightAnchor, constant: 8).isActive = true
        //lblSymbol.rightAnchor.constraint(equalTo: lblRisk.leftAnchor, constant: -8).isActive = true
        lblSymbol.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblSymbol.centerYAnchor.constraint(equalTo: imageViewDirection.centerYAnchor, constant: 0).isActive = true
        lblSymbol.widthAnchor.constraint(equalToConstant: (lblSymbol.text?.sizeOfString(usingFont: lblSymbol.font).width)! + 2).isActive = true
        
        
        // lblRisk + imageViewRisk
        var riskImageName = ""
        var riskText = ""
        
        if let strategy = self.strategy {
            if let strategyName = strategy.name {
                riskText = strategyName.uppercased()
            }
        }
        
        if let risk = self.risk {
            switch risk {
            case 1:
                riskImageName = "icon_limited_risk_strategies"
                //riskText = "Limited Risk Strategies"
            case 3:
                riskImageName = "icon_all_strategies"
                //riskText = "All Strategies"
            default:
                riskImageName = ""
            }
        }
        
        imageViewRisk.image = UIImage(named: riskImageName)?.withRenderingMode(.alwaysTemplate)
        headerView.addSubview(imageViewRisk)
        imageViewRisk.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -10).isActive = true
        imageViewRisk.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewRisk.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewRisk.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 8).isActive = true
        
        lblRisk.text = riskText
        headerView.addSubview(lblRisk)
        lblRisk.rightAnchor.constraint(equalTo: imageViewRisk.leftAnchor, constant: -4).isActive = true
        lblRisk.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblRisk.centerYAnchor.constraint(equalTo: imageViewRisk.centerYAnchor, constant: 0).isActive = true
        lblRisk.leftAnchor.constraint(equalTo: lblSymbol.rightAnchor, constant: 4).isActive = true
        //lblRisk.widthAnchor.constraint(equalToConstant: (lblRisk.text?.sizeOfString(usingFont: lblRisk.font).width)! + 4).isActive = true
        
        // scrollview container
        view.addSubview(scrollViewContainer)
        scrollViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollViewContainer.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 0).isActive = true
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnGetStarted, attribute: .top, multiplier: 1.0, constant: -16)
        scrollViewContainerBottomAnchorConstraint.isActive = true
        
        // form view
        scrollViewContainer.addSubview(formView)
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .top, relatedBy: .equal, toItem: scrollViewContainer, attribute: .top, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .left, relatedBy: .equal, toItem: scrollViewContainer, attribute: .left, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .right, relatedBy: .equal, toItem: scrollViewContainer, attribute: .right, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: scrollViewContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
        formHeightConstraint = NSLayoutConstraint(item: formView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 280)
        formHeightConstraint.isActive = true
        
        scrollViewContainer.contentSize = formView.size
        
        
        // MARK:- lblCMP
        formView.addSubview(lblCMP)
        lblCMP.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblCMP.topAnchor.constraint(equalTo: formView.topAnchor, constant: fieldVerticalGap).isActive = true
        lblCMP.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCMP.widthAnchor.constraint(equalToConstant: (lblCMP.text?.sizeOfString(usingFont: lblCMP.font).width)! + 8).isActive = true
        
        // lblCMPValue
        formView.addSubview(lblCMPValue)
        lblCMPValue.leftAnchor.constraint(equalTo: lblCMP.rightAnchor, constant: 8).isActive = true
        lblCMPValue.topAnchor.constraint(equalTo: lblCMP.topAnchor, constant: 0).isActive = true
        lblCMPValue.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        lblCMPValue.heightAnchor.constraint(equalTo: lblCMP.heightAnchor, multiplier: 1.0).isActive = true
        
        // MARK:- lblBaseExpiry
        formView.addSubview(lblBaseExpiry)
        lblBaseExpiry.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblBaseExpiry.topAnchor.constraint(equalTo: lblCMP.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblBaseExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblBaseExpiry.widthAnchor.constraint(equalToConstant: (lblBaseExpiry.text?.sizeOfString(usingFont: lblBaseExpiry.font).width)! + 8).isActive = true
        
        // dropdownBaseExpiry
        formView.addSubview(dropdownBaseExpiry)
        dropdownBaseExpiry.addTarget(self, action: #selector(showBaseExpiry(_:)), for: .touchUpInside)
        dropdownBaseExpiry.leftAnchor.constraint(equalTo: lblBaseExpiry.rightAnchor, constant: 8).isActive = true
        dropdownBaseExpiry.centerYAnchor.constraint(equalTo: lblBaseExpiry.centerYAnchor, constant: 0).isActive = true
        dropdownBaseExpiry.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownBaseExpiry.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        // MARK:- lblLots
        formView.addSubview(lblLots)
        lblLots.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblLots.topAnchor.constraint(equalTo: lblBaseExpiry.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblLots.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblLots.widthAnchor.constraint(equalToConstant: (lblLots.text?.sizeOfString(usingFont: lblLots.font).width)! + 8).isActive = true
        
        // btnAddQty
        formView.addSubview(btnAddQty)
        btnAddQty.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        btnAddQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btnAddQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btnAddQty.centerYAnchor.constraint(equalTo: lblLots.centerYAnchor, constant: 0).isActive = true
        
        // btnLots
        formView.addSubview(btnLots)
        btnLots.setTitle("\(defaultLotValue)", for: .normal)
        btnLots.addTarget(self, action: #selector(getLotsInput(_:)), for: .touchUpInside)
        btnLots.topAnchor.constraint(equalTo: lblLots.topAnchor, constant: 0).isActive = true
        btnLots.heightAnchor.constraint(equalTo: lblLots.heightAnchor, multiplier: 1.0).isActive = true
        //btnLots.leftAnchor.constraint(equalTo: lblLots.rightAnchor, constant: 10).isActive = true
        btnLots.rightAnchor.constraint(equalTo: btnAddQty.leftAnchor, constant: -10).isActive = true
        btnLotsWidthConstraint = NSLayoutConstraint(item: btnLots, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: "\(defaultLotValue)".sizeOfString(usingFont: (btnLots.titleLabel?.font)!).width + 25)
        btnLotsWidthConstraint.isActive = true
        
        formView.addSubview(btnDeleteQty)
        btnDeleteQty.rightAnchor.constraint(equalTo: btnLots.leftAnchor, constant: -10).isActive = true
        btnDeleteQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btnDeleteQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btnDeleteQty.centerYAnchor.constraint(equalTo: lblLots.centerYAnchor, constant: 0).isActive = true
        
        // MARK:- instrumentHeaderView
        formView.addSubview(instrumentHeaderView)
        instrumentHeaderView.topAnchor.constraint(equalTo: btnLots.bottomAnchor, constant: fieldVerticalGap).isActive = true
        instrumentHeaderView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        instrumentHeaderView.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 0).isActive = true
        instrumentHeaderView.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: 0).isActive = true
        
        instrumentHeaderView.addSubview(btnFetchCMP)
        btnFetchCMP.addTarget(self, action: #selector(fetchCMP(_:)), for: .touchUpInside)
        btnFetchCMP.topAnchor.constraint(equalTo: instrumentHeaderView.topAnchor, constant: 4).isActive = true
        btnFetchCMP.bottomAnchor.constraint(equalTo: instrumentHeaderView.bottomAnchor, constant: -4).isActive = true
        btnFetchCMP.rightAnchor.constraint(equalTo: instrumentHeaderView.rightAnchor, constant: -10).isActive = true
        //btnFetchCMP.leftAnchor.constraint(equalTo: lblInstrumentHeader.rightAnchor, constant: 10).isActive = true
        btnFetchCMP.widthAnchor.constraint(equalToConstant: (btnFetchCMP.titleLabel?.text?.sizeOfString(usingFont: (btnFetchCMP.titleLabel?.font)!).width)! + 44).isActive = true
        
        instrumentHeaderView.addSubview(lblInstrumentHeader)
        lblInstrumentHeader.topAnchor.constraint(equalTo: instrumentHeaderView.topAnchor, constant: 0).isActive = true
        lblInstrumentHeader.bottomAnchor.constraint(equalTo: instrumentHeaderView.bottomAnchor, constant: 0).isActive = true
        lblInstrumentHeader.leftAnchor.constraint(equalTo: instrumentHeaderView.leftAnchor, constant: 10).isActive = true
        //lblInstrumentHeader.widthAnchor.constraint(equalToConstant: (lblInstrumentHeader.text?.sizeOfString(usingFont: lblInstrumentHeader.font).width)! + 4).isActive = true
        lblInstrumentHeader.rightAnchor.constraint(equalTo: btnFetchCMP.leftAnchor, constant: -10).isActive = true
        
        
        
        // MARK: instrument A Container
        formView.addSubview(instrumentAContainer)
        instrumentAContainer.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 0).isActive = true
        instrumentAContainer.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: 0).isActive = true
        instrumentAContainer.topAnchor.constraint(equalTo: instrumentHeaderView.bottomAnchor, constant: 0).isActive = true
        instrumentAContainerHeightConstraint = NSLayoutConstraint(item: instrumentAContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        instrumentAContainerHeightConstraint.isActive = true
        
        instrumentAContainer.addSubview(lblInstrumentA)
        lblInstrumentA.leftAnchor.constraint(equalTo: instrumentAContainer.leftAnchor, constant: 10).isActive = true
        lblInstrumentA.topAnchor.constraint(equalTo: instrumentAContainer.topAnchor, constant: 4).isActive = true
        lblInstrumentA.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblInstrumentA.rightAnchor.constraint(equalTo: instrumentAContainer.centerXAnchor, constant: -5).isActive = true
        
        instrumentAContainer.addSubview(lblQtyA)
        lblQtyA.leftAnchor.constraint(equalTo: instrumentAContainer.centerXAnchor, constant: 5).isActive = true
        lblQtyA.topAnchor.constraint(equalTo: lblInstrumentA.topAnchor, constant: 0).isActive = true
        lblQtyA.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyA.widthAnchor.constraint(equalToConstant: (lblQtyA.text?.sizeOfString(usingFont: lblQtyA.font).width)! + 4).isActive = true
        
        instrumentAContainer.addSubview(lblQtyAValue)
        lblQtyAValue.leftAnchor.constraint(equalTo: lblQtyA.rightAnchor, constant: 8).isActive = true
        lblQtyAValue.topAnchor.constraint(equalTo: lblInstrumentA.topAnchor, constant: 0).isActive = true
        lblQtyAValue.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyAValue.rightAnchor.constraint(equalTo: instrumentAContainer.rightAnchor, constant: -10).isActive = true
        
        instrumentAContainer.addSubview(lblStrikeA)
        lblStrikeA.leftAnchor.constraint(equalTo: instrumentAContainer.leftAnchor, constant: 10).isActive = true
        lblStrikeA.topAnchor.constraint(equalTo: lblInstrumentA.bottomAnchor, constant: 2).isActive = true
        lblStrikeA.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblStrikeA.widthAnchor.constraint(equalToConstant: (lblStrikeA.text?.sizeOfString(usingFont: lblStrikeA.font).width)! + 4).isActive = true
        
        // dropdownStrikeA
        instrumentAContainer.addSubview(dropdownStrikeA)
        dropdownStrikeA.addTarget(self, action: #selector(showStrikeForInstrument(_:)), for: .touchUpInside)
        dropdownStrikeA.leftAnchor.constraint(equalTo: lblStrikeA.rightAnchor, constant: 8).isActive = true
        dropdownStrikeA.centerYAnchor.constraint(equalTo: lblStrikeA.centerYAnchor, constant: 0).isActive = true
        dropdownStrikeA.rightAnchor.constraint(equalTo: instrumentAContainer.centerXAnchor, constant: -5).isActive = true
        dropdownStrikeA.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        instrumentAContainer.addSubview(lblPriceA)
        lblPriceA.leftAnchor.constraint(equalTo: instrumentAContainer.centerXAnchor, constant: 5).isActive = true
        lblPriceA.topAnchor.constraint(equalTo: lblStrikeA.topAnchor, constant: 0).isActive = true
        lblPriceA.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblPriceA.widthAnchor.constraint(equalToConstant: (lblPriceA.text?.sizeOfString(usingFont: lblPriceA.font).width)! + 4).isActive = true
        
        instrumentAContainer.addSubview(tfPriceA)
        addToolBar(textField: tfPriceA)
        tfPriceA.rightAnchor.constraint(equalTo: instrumentAContainer.rightAnchor, constant: -10).isActive = true
        tfPriceA.leftAnchor.constraint(equalTo: lblPriceA.rightAnchor, constant: 8).isActive = true
        tfPriceA.topAnchor.constraint(equalTo: lblPriceA.topAnchor, constant: 0).isActive = true
        tfPriceA.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        instrumentAContainer.addSubview(borderPriceA)
        borderPriceA.topAnchor.constraint(equalTo: tfPriceA.bottomAnchor, constant: 0).isActive = true
        borderPriceA.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderPriceA.leftAnchor.constraint(equalTo: tfPriceA.leftAnchor, constant: -4).isActive = true
        borderPriceA.rightAnchor.constraint(equalTo: tfPriceA.rightAnchor, constant: 0).isActive = true
        
        // MARK: instrument B Container
        formView.addSubview(instrumentBContainer)
        instrumentBContainer.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 0).isActive = true
        instrumentBContainer.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: 0).isActive = true
        instrumentBContainer.topAnchor.constraint(equalTo: instrumentAContainer.bottomAnchor, constant: 0).isActive = true
        instrumentBContainerHeightConstraint = NSLayoutConstraint(item: instrumentBContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        instrumentBContainerHeightConstraint.isActive = true
        
        instrumentBContainer.addSubview(lblInstrumentB)
        lblInstrumentB.leftAnchor.constraint(equalTo: instrumentBContainer.leftAnchor, constant: 10).isActive = true
        lblInstrumentB.topAnchor.constraint(equalTo: instrumentBContainer.topAnchor, constant: 8).isActive = true
        lblInstrumentB.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblInstrumentB.rightAnchor.constraint(equalTo: instrumentBContainer.centerXAnchor, constant: -5).isActive = true
        
        instrumentBContainer.addSubview(lblQtyB)
        lblQtyB.leftAnchor.constraint(equalTo: instrumentBContainer.centerXAnchor, constant: 5).isActive = true
        lblQtyB.topAnchor.constraint(equalTo: lblInstrumentB.topAnchor, constant: 0).isActive = true
        lblQtyB.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyB.widthAnchor.constraint(equalToConstant: (lblQtyB.text?.sizeOfString(usingFont: lblQtyB.font).width)! + 4).isActive = true
        
        instrumentBContainer.addSubview(lblQtyBValue)
        lblQtyBValue.leftAnchor.constraint(equalTo: lblQtyB.rightAnchor, constant: 8).isActive = true
        lblQtyBValue.topAnchor.constraint(equalTo: lblInstrumentB.topAnchor, constant: 0).isActive = true
        lblQtyBValue.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyBValue.rightAnchor.constraint(equalTo: instrumentBContainer.rightAnchor, constant: -10).isActive = true
        
        instrumentBContainer.addSubview(lblStrikeB)
        lblStrikeB.leftAnchor.constraint(equalTo: instrumentBContainer.leftAnchor, constant: 10).isActive = true
        lblStrikeB.topAnchor.constraint(equalTo: lblInstrumentB.bottomAnchor, constant: 4).isActive = true
        lblStrikeB.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblStrikeB.widthAnchor.constraint(equalToConstant: (lblStrikeB.text?.sizeOfString(usingFont: lblStrikeB.font).width)! + 4).isActive = true
        
        // dropdownStrikeB
        instrumentBContainer.addSubview(dropdownStrikeB)
        dropdownStrikeB.addTarget(self, action: #selector(showStrikeForInstrument(_:)), for: .touchUpInside)
        dropdownStrikeB.leftAnchor.constraint(equalTo: lblStrikeB.rightAnchor, constant: 8).isActive = true
        dropdownStrikeB.centerYAnchor.constraint(equalTo: lblStrikeB.centerYAnchor, constant: 0).isActive = true
        dropdownStrikeB.rightAnchor.constraint(equalTo: instrumentBContainer.centerXAnchor, constant: -5).isActive = true
        dropdownStrikeB.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        instrumentBContainer.addSubview(lblPriceB)
        lblPriceB.leftAnchor.constraint(equalTo: instrumentBContainer.centerXAnchor, constant: 5).isActive = true
        lblPriceB.topAnchor.constraint(equalTo: lblStrikeB.topAnchor, constant: 0).isActive = true
        lblPriceB.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblPriceB.widthAnchor.constraint(equalToConstant: (lblPriceB.text?.sizeOfString(usingFont: lblPriceB.font).width)! + 4).isActive = true
        
        instrumentBContainer.addSubview(tfPriceB)
        addToolBar(textField: tfPriceB)
        tfPriceB.rightAnchor.constraint(equalTo: instrumentBContainer.rightAnchor, constant: -10).isActive = true
        tfPriceB.leftAnchor.constraint(equalTo: lblPriceB.rightAnchor, constant: 8).isActive = true
        tfPriceB.topAnchor.constraint(equalTo: lblPriceB.topAnchor, constant: 0).isActive = true
        tfPriceB.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        instrumentBContainer.addSubview(borderPriceB)
        borderPriceB.topAnchor.constraint(equalTo: tfPriceB.bottomAnchor, constant: 0).isActive = true
        borderPriceB.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderPriceB.leftAnchor.constraint(equalTo: tfPriceB.leftAnchor, constant: -4).isActive = true
        borderPriceB.rightAnchor.constraint(equalTo: tfPriceB.rightAnchor, constant: 0).isActive = true
        
        // MARK: instrument C Container
        formView.addSubview(instrumentCContainer)
        instrumentCContainer.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 0).isActive = true
        instrumentCContainer.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: 0).isActive = true
        instrumentCContainer.topAnchor.constraint(equalTo: instrumentBContainer.bottomAnchor, constant: 0).isActive = true
        instrumentCContainerHeightConstraint = NSLayoutConstraint(item: instrumentCContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        instrumentCContainerHeightConstraint.isActive = true
        
        instrumentCContainer.addSubview(lblInstrumentC)
        lblInstrumentC.leftAnchor.constraint(equalTo: instrumentCContainer.leftAnchor, constant: 10).isActive = true
        lblInstrumentC.topAnchor.constraint(equalTo: instrumentCContainer.topAnchor, constant: 8).isActive = true
        lblInstrumentC.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblInstrumentC.rightAnchor.constraint(equalTo: instrumentCContainer.centerXAnchor, constant: -5).isActive = true
        
        instrumentCContainer.addSubview(lblQtyC)
        lblQtyC.leftAnchor.constraint(equalTo: instrumentCContainer.centerXAnchor, constant: 5).isActive = true
        lblQtyC.topAnchor.constraint(equalTo: lblInstrumentC.topAnchor, constant: 0).isActive = true
        lblQtyC.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyC.widthAnchor.constraint(equalToConstant: (lblQtyC.text?.sizeOfString(usingFont: lblQtyC.font).width)! + 4).isActive = true
        
        instrumentCContainer.addSubview(lblQtyCValue)
        lblQtyCValue.leftAnchor.constraint(equalTo: lblQtyC.rightAnchor, constant: 8).isActive = true
        lblQtyCValue.topAnchor.constraint(equalTo: lblInstrumentC.topAnchor, constant: 0).isActive = true
        lblQtyCValue.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyCValue.rightAnchor.constraint(equalTo: instrumentCContainer.rightAnchor, constant: -10).isActive = true
        
        instrumentCContainer.addSubview(lblStrikeC)
        lblStrikeC.leftAnchor.constraint(equalTo: instrumentCContainer.leftAnchor, constant: 10).isActive = true
        lblStrikeC.topAnchor.constraint(equalTo: lblInstrumentC.bottomAnchor, constant: 4).isActive = true
        lblStrikeC.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblStrikeC.widthAnchor.constraint(equalToConstant: (lblStrikeC.text?.sizeOfString(usingFont: lblStrikeC.font).width)! + 4).isActive = true
        
        // dropdownStrikeC
        instrumentCContainer.addSubview(dropdownStrikeC)
        dropdownStrikeC.addTarget(self, action: #selector(showStrikeForInstrument(_:)), for: .touchUpInside)
        dropdownStrikeC.leftAnchor.constraint(equalTo: lblStrikeC.rightAnchor, constant: 8).isActive = true
        dropdownStrikeC.centerYAnchor.constraint(equalTo: lblStrikeC.centerYAnchor, constant: 0).isActive = true
        dropdownStrikeC.rightAnchor.constraint(equalTo: instrumentCContainer.centerXAnchor, constant: -5).isActive = true
        dropdownStrikeC.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        instrumentCContainer.addSubview(lblPriceC)
        lblPriceC.leftAnchor.constraint(equalTo: instrumentBContainer.centerXAnchor, constant: 5).isActive = true
        lblPriceC.topAnchor.constraint(equalTo: lblStrikeC.topAnchor, constant: 0).isActive = true
        lblPriceC.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblPriceC.widthAnchor.constraint(equalToConstant: (lblPriceC.text?.sizeOfString(usingFont: lblPriceC.font).width)! + 4).isActive = true
        
        instrumentCContainer.addSubview(tfPriceC)
        addToolBar(textField: tfPriceC)
        tfPriceC.rightAnchor.constraint(equalTo: instrumentCContainer.rightAnchor, constant: -10).isActive = true
        tfPriceC.leftAnchor.constraint(equalTo: lblPriceC.rightAnchor, constant: 8).isActive = true
        tfPriceC.topAnchor.constraint(equalTo: lblPriceC.topAnchor, constant: 0).isActive = true
        tfPriceC.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        instrumentCContainer.addSubview(borderPriceC)
        borderPriceC.topAnchor.constraint(equalTo: tfPriceC.bottomAnchor, constant: 0).isActive = true
        borderPriceC.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderPriceC.leftAnchor.constraint(equalTo: tfPriceC.leftAnchor, constant: -4).isActive = true
        borderPriceC.rightAnchor.constraint(equalTo: tfPriceC.rightAnchor, constant: 0).isActive = true
        
        // MARK: instrument D Container
        formView.addSubview(instrumentDContainer)
        instrumentDContainer.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 0).isActive = true
        instrumentDContainer.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: 0).isActive = true
        instrumentDContainer.topAnchor.constraint(equalTo: instrumentCContainer.bottomAnchor, constant: 0).isActive = true
        instrumentDContainerHeightConstraint = NSLayoutConstraint(item: instrumentDContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        instrumentDContainerHeightConstraint.isActive = true
        
        instrumentDContainer.addSubview(lblInstrumentD)
        lblInstrumentD.leftAnchor.constraint(equalTo: instrumentDContainer.leftAnchor, constant: 10).isActive = true
        lblInstrumentD.topAnchor.constraint(equalTo: instrumentDContainer.topAnchor, constant: 8).isActive = true
        lblInstrumentD.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblInstrumentD.rightAnchor.constraint(equalTo: instrumentDContainer.centerXAnchor, constant: -5).isActive = true
        
        instrumentDContainer.addSubview(lblQtyD)
        lblQtyD.leftAnchor.constraint(equalTo: instrumentDContainer.centerXAnchor, constant: 5).isActive = true
        lblQtyD.topAnchor.constraint(equalTo: lblInstrumentD.topAnchor, constant: 0).isActive = true
        lblQtyD.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyD.widthAnchor.constraint(equalToConstant: (lblQtyD.text?.sizeOfString(usingFont: lblQtyD.font).width)! + 4).isActive = true
        
        instrumentDContainer.addSubview(lblQtyDValue)
        lblQtyDValue.leftAnchor.constraint(equalTo: lblQtyD.rightAnchor, constant: 8).isActive = true
        lblQtyDValue.topAnchor.constraint(equalTo: lblInstrumentD.topAnchor, constant: 0).isActive = true
        lblQtyDValue.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblQtyDValue.rightAnchor.constraint(equalTo: instrumentDContainer.rightAnchor, constant: -10).isActive = true
        
        instrumentDContainer.addSubview(lblStrikeD)
        lblStrikeD.leftAnchor.constraint(equalTo: instrumentDContainer.leftAnchor, constant: 10).isActive = true
        lblStrikeD.topAnchor.constraint(equalTo: lblInstrumentD.bottomAnchor, constant: 4).isActive = true
        lblStrikeD.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblStrikeD.widthAnchor.constraint(equalToConstant: (lblStrikeD.text?.sizeOfString(usingFont: lblStrikeD.font).width)! + 4).isActive = true
        
        // dropdownStrikeD
        instrumentDContainer.addSubview(dropdownStrikeD)
        dropdownStrikeD.addTarget(self, action: #selector(showStrikeForInstrument(_:)), for: .touchUpInside)
        dropdownStrikeD.leftAnchor.constraint(equalTo: lblStrikeD.rightAnchor, constant: 8).isActive = true
        dropdownStrikeD.centerYAnchor.constraint(equalTo: lblStrikeD.centerYAnchor, constant: 0).isActive = true
        dropdownStrikeD.rightAnchor.constraint(equalTo: instrumentDContainer.centerXAnchor, constant: -5).isActive = true
        dropdownStrikeD.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        instrumentDContainer.addSubview(lblPriceD)
        lblPriceD.leftAnchor.constraint(equalTo: instrumentCContainer.centerXAnchor, constant: 5).isActive = true
        lblPriceD.topAnchor.constraint(equalTo: lblStrikeD.topAnchor, constant: 0).isActive = true
        lblPriceD.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblPriceD.widthAnchor.constraint(equalToConstant: (lblPriceD.text?.sizeOfString(usingFont: lblPriceD.font).width)! + 4).isActive = true
        
        instrumentDContainer.addSubview(tfPriceD)
        addToolBar(textField: tfPriceD)
        tfPriceD.rightAnchor.constraint(equalTo: instrumentDContainer.rightAnchor, constant: -10).isActive = true
        tfPriceD.leftAnchor.constraint(equalTo: lblPriceD.rightAnchor, constant: 8).isActive = true
        tfPriceD.topAnchor.constraint(equalTo: lblPriceD.topAnchor, constant: 0).isActive = true
        tfPriceD.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        instrumentDContainer.addSubview(borderPriceD)
        borderPriceD.topAnchor.constraint(equalTo: tfPriceD.bottomAnchor, constant: 0).isActive = true
        borderPriceD.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderPriceD.leftAnchor.constraint(equalTo: tfPriceD.leftAnchor, constant: -4).isActive = true
        borderPriceD.rightAnchor.constraint(equalTo: tfPriceD.rightAnchor, constant: 0).isActive = true
        
        
        // MARK:- positionHeaderView
        formView.addSubview(positionHeaderView)
        positionHeaderView.topAnchor.constraint(equalTo: instrumentDContainer.bottomAnchor, constant: 0).isActive = true
        positionHeaderView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        positionHeaderView.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 0).isActive = true
        positionHeaderView.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: 0).isActive = true
        
        positionHeaderView.addSubview(lblPositionHeader)
        lblPositionHeader.topAnchor.constraint(equalTo: positionHeaderView.topAnchor, constant: 0).isActive = true
        lblPositionHeader.bottomAnchor.constraint(equalTo: positionHeaderView.bottomAnchor, constant: 0).isActive = true
        lblPositionHeader.leftAnchor.constraint(equalTo: positionHeaderView.leftAnchor, constant: 10).isActive = true
        lblPositionHeader.rightAnchor.constraint(equalTo: positionHeaderView.rightAnchor, constant: -10).isActive = true
        
        // MARK:- lblPositionName
        formView.addSubview(lblPositionName)
        lblPositionName.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblPositionName.topAnchor.constraint(equalTo: positionHeaderView.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblPositionName.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPositionName.widthAnchor.constraint(equalToConstant: (lblPositionName.text?.sizeOfString(usingFont: lblPositionName.font).width)! + 4).isActive = true
        
        formView.addSubview(tfPositionName)
        addToolBar(textField: tfPositionName)
        tfPositionName.leftAnchor.constraint(equalTo: lblPositionName.rightAnchor, constant: 8).isActive = true
        tfPositionName.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfPositionName.topAnchor.constraint(equalTo: lblPositionName.topAnchor, constant: 0).isActive = true
        tfPositionName.heightAnchor.constraint(equalTo: lblPositionName.heightAnchor, multiplier: 1).isActive = true
        
        
        formView.addSubview(borderPositionName)
        borderPositionName.leftAnchor.constraint(equalTo: tfPositionName.leftAnchor, constant: 0).isActive = true
        borderPositionName.rightAnchor.constraint(equalTo: tfPositionName.rightAnchor, constant: 0).isActive = true
        borderPositionName.bottomAnchor.constraint(equalTo: tfPositionName.bottomAnchor, constant: 0).isActive = true
        borderPositionName.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // update lots button
        updateAddDeleteLotButtons()
    }
    
    private func updateInstruments() {
        
        if let masters = self.optMasters {
            
            SVProgressHUD.dismiss()
            
            var formHeight: CGFloat = 314
            
            if legs > 0 {
                if (expiry[0] + selectedExpiry) < masters.count {
                    if let expiryDate = masters[expiry[0] + selectedExpiry].expiry {
                        instrumentAContainerHeightConstraint.constant = instrumentContainerHeight
                        formHeight += instrumentContainerHeight
                        //formView.layoutIfNeeded()
                        lblInstrumentA.text = "\(expiryDate.string(withFormat: "dd-MMM-yy")) \(callPut[0] == 1 ? "CE" : "PE")"
                        tfPriceA.clear()
                        
                        let lot: Double = (masters[expiry[0] + selectedExpiry].lot?.double())!
                        let qty = "\((lot * lots[0] * defaultLotValue.double).toString())"
                        
                        if buySell[0] == 1 {
                            lblQtyAValue.text = "\(qty)"
                        } else {
                            lblQtyAValue.text = "-\(qty)"
                        }
                    }
                }
            }
            
            if legs > 1 {
                if (expiry[1] + selectedExpiry) < masters.count {
                    if let expiryDate = masters[expiry[1] + selectedExpiry].expiry {
                        instrumentBContainerHeightConstraint.constant = instrumentContainerHeight
                        formHeight += instrumentContainerHeight
                        //formView.layoutIfNeeded()
                        lblInstrumentB.text = "\(expiryDate.string(withFormat: "dd-MMM-yy")) \(callPut[1] == 1 ? "CE" : "PE")"
                        tfPriceB.clear()
                        
                        let lot: Double = (masters[expiry[1] + selectedExpiry].lot?.double())!
                        let qty = "\((lot * lots[1] * defaultLotValue.double).toString())"
                        
                        if buySell[1] == 1 {
                            lblQtyBValue.text = "\(qty)"
                        } else {
                            lblQtyBValue.text = "-\(qty)"
                        }
                    }
                }
            }
            
            if legs > 2 {
                if (expiry[2] + selectedExpiry) < masters.count {
                    if let expiryDate = masters[expiry[2] + selectedExpiry].expiry {
                        instrumentCContainerHeightConstraint.constant = instrumentContainerHeight
                        formHeight += instrumentContainerHeight
                        //formView.layoutIfNeeded()
                        lblInstrumentC.text = "\(expiryDate.string(withFormat: "dd-MMM-yy")) \(callPut[2] == 1 ? "CE" : "PE")"
                        tfPriceC.clear()
                        
                        let lot: Double = (masters[expiry[2] + selectedExpiry].lot?.double())!
                        let qty = "\((lot * lots[2] * defaultLotValue.double).toString())"
                        
                        if buySell[2] == 1 {
                            lblQtyCValue.text = "\(qty)"
                        } else {
                            lblQtyCValue.text = "-\(qty)"
                        }
                    }
                }
            }
            
            if legs > 3 {
                if (expiry[3] + selectedExpiry) < masters.count {
                    if let expiryDate = masters[expiry[3] + selectedExpiry].expiry {
                        instrumentDContainerHeightConstraint.constant = instrumentContainerHeight
                        formHeight += instrumentContainerHeight
                        //formView.layoutIfNeeded()
                        lblInstrumentD.text = "\(expiryDate.string(withFormat: "dd-MMM-yy")) \(callPut[3] == 1 ? "CE" : "PE")"
                        tfPriceD.clear()
                        
                        let lot: Double = (masters[expiry[3] + selectedExpiry].lot?.double())!
                        let qty = "\((lot * lots[3] * defaultLotValue.double).toString())"
                        
                        if buySell[3] == 1 {
                            lblQtyDValue.text = "\(qty)"
                        } else {
                            lblQtyDValue.text = "-\(qty)"
                        }
                    }
                }
            }
            
            formHeightConstraint.constant = formHeight
            self.formView.layoutIfNeeded()
            
            guard let cmpValue = lblCMPValue.text?.double() else {
                self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {
                    self.delegate?.stepCErrorOccured()
                })
                return
            }
            
            var closestIndex = closestIndexInStrikes(strikes: self.strikes!, CMP: cmpValue)
            print("closestIndex = \(closestIndex)")
            
            if closestIndex < 0 {
                self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong", cancelTitle: "OK", isError: true, completion: {
                    self.delegate?.stepCErrorOccured()
                })
                return
            }
            
            if strikePosition.count > 1 {
                
                if let minRequired = strikePosition.min(), let maxRequired = strikePosition.max() {
                    var requiredStrikes: Int = 0
                    
                    if minRequired <= 0 && maxRequired >= 0 {
                        requiredStrikes = abs(minRequired) + maxRequired + 1
                    } else if minRequired > 0 {
                        requiredStrikes = maxRequired + 1
                    } else if minRequired <= 0 && maxRequired < 0 {
                        requiredStrikes = abs(minRequired) + 1
                    }
                    
                    let lowerBound = closestIndex + minRequired
                    
                    if lowerBound < 0 {
                        closestIndex = closestIndex + abs(lowerBound)
                    }
                    
                    let upperBound = closestIndex + maxRequired
                    
                    if upperBound > (strikes?.count)! {
                        closestIndex = closestIndex - (upperBound - (strikes?.count)!)
                    }
                }
                
                
            }
            
            dropdownStrikeA.setTitle(self.strikes?[closestIndex + strikePosition[0]], for: .normal)
            
            if legs > 1 {
                dropdownStrikeB.setTitle(self.strikes?[closestIndex + strikePosition[1]], for: .normal)
            }
            
            if legs > 2 {
                dropdownStrikeC.setTitle(self.strikes?[closestIndex + strikePosition[2]], for: .normal)
            }
            
            if legs > 3 {
                dropdownStrikeD.setTitle(self.strikes?[closestIndex + strikePosition[3]], for: .normal)
            }
        } else {
            SVProgressHUD.dismiss()
            self.showZAlertView(withTitle: "Error", andMessage: "No master details found.", cancelTitle: "OK", isError: true, completion: {
                self.delegate?.stepCErrorOccured()
            })
        }
    }
    
    
    
    @objc private func actionGetStarted(_ sender: UIButton) {
        resignAllResponders()
        
        if legs > 0 {
            if tfPriceA.isEmpty {
                showZAlertView(withTitle: "Error", andMessage: "Please enter the trade price of fetch CMP", cancelTitle: "OK", isError: true) {
                    self.tfPriceA.becomeFirstResponder()
                }
                return
            }
        }
        
        if legs > 1 {
            if tfPriceB.isEmpty {
                showZAlertView(withTitle: "Error", andMessage: "Please enter the trade price of fetch CMP", cancelTitle: "OK", isError: true) {
                    self.tfPriceB.becomeFirstResponder()
                }
                return
            }
        }
        
        if legs > 2 {
            if tfPriceC.isEmpty {
                showZAlertView(withTitle: "Error", andMessage: "Please enter the trade price of fetch CMP", cancelTitle: "OK", isError: true) {
                    self.tfPriceC.becomeFirstResponder()
                }
                return
            }
        }
        
        if legs > 3 {
            if tfPriceD.isEmpty {
                showZAlertView(withTitle: "Error", andMessage: "Please enter the trade price of fetch CMP", cancelTitle: "OK", isError: true) {
                    self.tfPriceD.becomeFirstResponder()
                }
                return
            }
        }
        
        tfPositionName.text = tfPositionName.text?.trimmed
        
        if let positionName = tfPositionName.text {
            if positionName.isEmpty {
                showZAlertView(withTitle: "Error", andMessage: "Please enter a position name", cancelTitle: "OK", isError: true) {
                    self.tfPositionName.becomeFirstResponder()
                }
                return
            } else if DatabaseManager.sharedInstance.checkIfPositionNameAlreadyExists(position: positionName) {
                showZAlertView(withTitle: "Error", andMessage: "Strategy name already exists.", cancelTitle: "OK", isError: true) {
                    self.tfPositionName.becomeFirstResponder()
                }
                return
            }
        }
        
        // save position
        /*confirmSave { (save) in
            if save {
                self.savePosition()
            }
        }*/
        
        self.savePosition()
    }
    
    private func confirmSave(completion: @escaping ((_ save: Bool) -> Void)) {
        let dialog = ZAlertView(title: "Confirm",
                                message: "Do you want to save the call?",
                                isOkButtonLeft: false,
                                okButtonText: "Yes",
                                cancelButtonText: "No",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
                                    completion(true)
        }) { (alertView) in
            alertView.dismissAlertView()
            completion(false)
        }
        dialog.show()
    }
    
    private func savePosition() {
        print("Save Position")
        
        let tradeStatus: String = selectedRadioButton.string
        
        var papi = [String]()
        
        if let masters = self.optMasters {
            if legs > 0 {
                if let strike = dropdownStrikeA.titleLabel?.text, let price = tfPriceA.text?.trimmed, let qty = lblQtyAValue.text, let expiryString = masters[expiry[0] + selectedExpiry].expiry?.string(withFormat: "dd-MMM-yy") {
                    papi.append("Opt \(expiryString) \(strike) \(callPut[0] == 1 ? "CE" : "PE"),\(price),\(qty)")
                }
            }
            
            if legs > 1 {
                if let strike = dropdownStrikeB.titleLabel?.text, let price = tfPriceB.text?.trimmed, let qty = lblQtyBValue.text, let expiryString = masters[expiry[1] + selectedExpiry].expiry?.string(withFormat: "dd-MMM-yy") {
                    papi.append("Opt \(expiryString) \(strike) \(callPut[1] == 1 ? "CE" : "PE"),\(price),\(qty)")
                }
            }
            
            if legs > 2 {
                if let strike = dropdownStrikeC.titleLabel?.text, let price = tfPriceC.text?.trimmed, let qty = lblQtyCValue.text, let expiryString = masters[expiry[2] + selectedExpiry].expiry?.string(withFormat: "dd-MMM-yy") {
                    papi.append("Opt \(expiryString) \(strike) \(callPut[2] == 1 ? "CE" : "PE"),\(price),\(qty)")
                }
            }
            
            if legs > 3 {
                if let strike = dropdownStrikeD.titleLabel?.text, let price = tfPriceD.text?.trimmed, let qty = lblQtyDValue.text, let expiryString = masters[expiry[3] + selectedExpiry].expiry?.string(withFormat: "dd-MMM-yy") {
                    papi.append("Opt \(expiryString) \(strike) \(callPut[3] == 1 ? "CE" : "PE"),\(price),\(qty)")
                }
            }
            
            //print("papi[\(papi.count)] = \(papi)")

            if let positionName = tfPositionName.text, let symbol = self.symbol, let directionVal = direction, let strategyName = self.strategy?.name, let riskVal = risk {
                self.delegate?.stepCFinished(positionName: positionName, symbol: symbol, margin: "0", legs: papi, tradeStatus: tradeStatus, direction: directionVal, risk: riskVal, strategyName: strategyName)
            }
            
            
            /*if let positionName = tfPositionName.text, let symbol = self.symbol {
                SVProgressHUD.show()
                OSOWebService.sharedInstance.uploadPosition(positionName: positionName, symbol: symbol, margin: "0", legs: papi, tradeStatus: tradeStatus) { (error, positionNo, margin) in
                    if let err = error {
                        SVProgressHUD.dismiss()
                        self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                    } else {
                        if let posNo = positionNo, let marginValue = margin {
                            self.addPosition(positionNo: posNo, symbol: symbol, positionName: positionName, margin: marginValue, legs: papi, tradeStatus: tradeStatus)
                        } else {
                            SVProgressHUD.dismiss()
                            self.showZAlertView(withTitle: "Error", andMessage: "Position Number not found.", cancelTitle: "OK", isError: true, completion: {})
                        }
                    }
                }
            }*/
        }
        
    }
    
    private func addPosition(positionNo: String, symbol: String, positionName: String, margin: String, legs: [String], tradeStatus: String) {
        
        var tempInstrument = [String]()
        let alerts: String = "0"
        let dateCreated: Date = Date()
        var analyzerSummaries = [AnalyzerSummary]()
        
        for i in 0..<legs.count {
            let summary = AnalyzerSummary()
            summary.positionNo = positionNo
            summary.symbol = symbol
            summary.positionName = positionName
            summary.margin = margin
            summary.dateCreated = dateCreated
            summary.alertOne = alerts
            summary.alertTwo = alerts
            summary.tradeStatus = tradeStatus
            summary.Papi = legs[i]
            
            tempInstrument = legs[i].components(separatedBy: ",")
            
            summary.price = tempInstrument[1]
            summary.qty = tempInstrument[2]
            
            tempInstrument = tempInstrument[0].components(separatedBy: " ")
            
            summary.instrument = tempInstrument[0]
            summary.expiry = tempInstrument[1]
            summary.srno = "\(i+1)"
            summary.syncNo = "0"
            
            if tempInstrument[0] == "Fut" {
                summary.strike = "-1"
                summary.optType = "XX"
            } else {
                summary.strike = tempInstrument[2]
                summary.optType = tempInstrument[3]
            }
            
            summary.expired = "0"
            
            analyzerSummaries.append(summary)
        }
        
        DatabaseManager.sharedInstance.addPosition(analyzerSummaries: analyzerSummaries) { (success) in
            SVProgressHUD.dismiss()
            if success {
                var strategyName = ""
                
                if let strategy = self.strategy {
                    if let name = strategy.name {
                        strategyName = name
                    }
                }
                self.showSetAlert(symbol: symbol, direction: self.direction!, risk: self.risk!, positionNo: positionNo, strategyName: strategyName)
            } else {
                self.showZAlertView(withTitle: "Error", andMessage: "Could not save the summary", cancelTitle: "OK", isError: true, completion: {
                    //
                })
            }
        }
        
    }
    
    private func showSetAlert(symbol: String, direction: Int, risk: Int, positionNo: String, strategyName: String) {
        let dialog = ZAlertView()
        dialog.message = "Position added successfully."
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        dialog.addButton("SET ALERT", hexColor: Constants.UIConfig.themeColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
            alertView.dismissAlertView()
            self.delegate?.stepCCompleted(symbol: symbol, direction: self.direction!, risk: self.risk!, positionNo: positionNo, strategyName: strategyName)
        }
        dialog.addButton("VIEW POSITIONS", hexColor: UIColor.gray.hexString, hexTitleColor: "#ffffff") { (alertView) in
            alertView.dismissAlertView()
            self.delegate?.stepCEnded()
        }
        
        dialog.show()
    }
    
    private func getStrategyBuilderDetails(strategy: MyLibrary, symbolName: String) {
        
        if let strategyName = strategy.name {
            
            if let trade = strategy.trade {
                
                var formHeight: CGFloat = 282
                
                let splittedTrade = trade.components(separatedBy: ";")
                legs = splittedTrade.count
                
                for i in 0..<legs {
                    let temp = splittedTrade[i].components(separatedBy: "*")
                    buySell.append(temp[0].int!)
                    callPut.append(temp[1].int!)
                    strikePosition.append(temp[2].int!)
                    lots.append(temp[3].double()!)
                    
                    if temp.count == 5 {
                        expiry.append(temp[4].int! - 1)
                    } else {
                        expiry.append(0)
                    }
                }
            } else {
                delegate?.stepCErrorOccured()
            }
            
            SVProgressHUD.show()
            
            DatabaseManager.sharedInstance.getNameStrategyBuilder(strategyName: strategyName) { (success, positionName, legsAllowed) in
                
                if success {
                    if let posName = positionName {
                        self.tfPositionName.text = posName
                    }
                    
                    self.getStrikes(symbolName: symbolName)
                } else {
                    
                    SVProgressHUD.dismiss()
                    
                    if let posName = positionName {
                        self.showZAlertView(withTitle: "Error", andMessage: posName, cancelTitle: "OK", isError: true, completion: {
                            self.delegate?.stepCErrorOccured()
                        })
                    } else {
                        self.delegate?.stepCErrorOccured()
                    }
                }
            }
        } else {
            showZAlertView(withTitle: "Error", andMessage: "Stragegy Name not found.", cancelTitle: "OK", isError: true) {
                self.delegate?.stepCErrorOccured()
            }
        }
        
    }
    
    private func getCMP(forSymbol symbol: String, expiry: String) {
        OSOWebService.sharedInstance.getPrice(forInstrument: "\(symbol)_\(expiry)_1") { (error, price) in
            if let err = error {
                
                print("Error: \(err.localizedDescription)")
                SVProgressHUD.dismiss(completion: {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription == "0" ? "Invalid Session. Please login again." : err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                        //self.delegate?.stepCErrorOccured()
                    })
                })
                
            } else {
                if let cmpValue = price {
                    
                    print("CMP = \(cmpValue)")
                    
                    self.lblCMPValue.text = cmpValue.toString()
                    
                    // update instrument
                    self.updateInstruments()
                } else {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    private func getCMP(forSymbol symbol: String) {
        
        resignAllResponders()
        
        OSOWebService.sharedInstance.getCMP(forSymbol: symbol) { (success, error, CMP) in
            if let err = error {
                
                print("Error: \(err.localizedDescription)")
                SVProgressHUD.dismiss(completion: {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription == "0" ? "Invalid Session. Please login again." : err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                        //self.delegate?.stepCErrorOccured()
                    })
                })
                
            } else {
                if let cmpValue = CMP {
                    
                    print("CMP = \(cmpValue)")
                    
                    self.lblCMPValue.text = cmpValue.toString()
                    
                    // get strikes
                    self.getStrikes(symbolName: symbol)
                } else {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    private func getStrikes(symbolName: String) {
        DatabaseManager.sharedInstance.getAllStrikesForSymbol(symbolName: symbolName) { (success, error, strikesArray) in
            if let err = error {
                print("Error: \(err.localizedDescription)")
                SVProgressHUD.dismiss()
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    self.delegate?.stepCErrorOccured()
                })
            } else {
                if let strikes = strikesArray {
                    print("strikes.count = \(strikes.count)")
                    self.strikes = strikes
                    self.getOptMaster(symbolName: symbolName)
                } else {
                    SVProgressHUD.dismiss()
                    self.showZAlertView(withTitle: "Error", andMessage: "No strikes found.", cancelTitle: "OK", isError: true, completion: {
                        self.delegate?.stepCErrorOccured()
                    })
                }
            }
        }
    }
    
    private func getOptMaster(symbolName: String) {
        DatabaseManager.sharedInstance.getOptMasterForSymbol(symbolName: symbolName) { (success, error, optMasterArray) in
            if let err = error {
                print("Error: \(err.localizedDescription)")
                SVProgressHUD.dismiss()
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    self.delegate?.stepCErrorOccured()
                })
            } else {
                if let optMasters = optMasterArray {
                    self.optMasters = optMasters
                    
                    var tmpExpiries = [String]()
                    for optMaster in optMasters {
                        if let expiryDate = optMaster.expiry {
                            tmpExpiries.append(expiryDate.string(withFormat: "dd-MMM-yy"))
                        }
                    }
                    
                    self.expiries = tmpExpiries
                    self.strikes = optMasters[self.selectedExpiry].strikes
                    //self.selectedExpiry = 0
                    self.dropdownBaseExpiry.setTitle("\(tmpExpiries[self.selectedExpiry])", for: .normal)
                    
                    if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbolName) {
                        self.getCMP(forSymbol: symbolName, expiry: expiry)
                    }
                    
                    //self.updateInstruments()
                    //self.getCMP(forSymbol: symbolName, expiry: tmpExpiries[self.selectedExpiry])
                } else {
                    SVProgressHUD.dismiss()
                    self.showZAlertView(withTitle: "Error", andMessage: "No master details found.", cancelTitle: "OK", isError: true, completion: {
                        self.delegate?.stepCErrorOccured()
                    })
                }
            }
        }
    }
    
    @objc private func showBaseExpiry(_ sender: UIButton) {
        
        if let expiryDates = self.expiries {
            let picker = ActionSheetStringPicker(title: "Select Expiry", rows: expiryDates, initialSelection: selectedExpiry, doneBlock: { (picker, value, index) in
                if self.selectedExpiry != value {
                    self.selectedExpiry = value
                    sender.setTitle(index as? String, for: .normal)
                    
                    if let symbolName = self.symbol {
                        self.getOptMaster(symbolName: symbolName)
                    }
                    //self.updateInstruments()
                }
            }, cancel: { (picker) in
                //
            }, origin: sender)
            
            picker?.show()
        }
    }
    
    @objc private func actionAddOrDelete(_ sender: UIButton) {
        if sender == btnAddQty {
            if defaultLotValue == 1 {
                defaultLotValue += 1
            } else {
                defaultLotValue += 2
            }
        } else if sender == btnDeleteQty {
            if defaultLotValue == 2 {
                defaultLotValue -= 1
            } else {
                defaultLotValue -= 2
            }
        }
        
        updateAddDeleteLotButtons()
        updateInstruments()
    }
    
    @objc private func getLotsInput(_ sender: UIButton) {
        let dialog = ZAlertView()
        dialog.alertTitle = "Enter Lot Multiple"
        dialog.message = ""
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        dialog.addTextField("lot", placeHolder: (btnLots.titleLabel?.text)!, keyboardType: .decimalPad)
        
        if let inputTf = dialog.getTextFieldWithIdentifier("lot") {
            self.tfLots = inputTf
            inputTf.delegate = self
        }
        
        dialog.addButton("Update", color: Constants.UIConfig.themeColor, titleColor: UIColor.white) { (alertView) in
            
            alertView.message = ""
            
            if let inputTf = dialog.getTextFieldWithIdentifier("lot") {
                if let text = inputTf.text {
                    
                    self.validateLotsInput(input: text, sender: sender, zAlertView: alertView)
                    
                    self.updateInstruments()
                    
                    alertView.dismissAlertView()
                    
                    if let textIntValue = text.int {
                        sender.setTitle("\(text)", for: .normal)
                        self.defaultLotValue = textIntValue
                    }
                }
            }
        }
        dialog.addButton("Cancel", color: UIColor(white: 0.0, alpha: 0.3), titleColor: UIColor.white) { (alertView) in
            alertView.dismissAlertView()
        }
        
        dialog.show()
    }
    
    private func validateLotsInput(input: String, sender: UIButton, zAlertView: ZAlertView) {
        if let intValue = input.int {
            
            if intValue == 0 {
                zAlertView.dismissAlertView()
                
                let dialog = ZAlertView()
                dialog.alertTitle = "Error"
                dialog.message = "Multiple cannot be zero."
                dialog.alertType = .multipleChoice
                dialog.allowTouchOutsideToDismiss = false
                dialog.addButton("OK", hexColor: Constants.ZAlertViewUI.errorButtonColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
                    alertView.dismissAlertView()
                    self.getLotsInput(sender)
                }
                dialog.show()
            } else if intValue != 1 && intValue % 2 == 1 {
                zAlertView.dismissAlertView()
                
                let dialog = ZAlertView()
                dialog.alertTitle = "Error"
                dialog.message = "Please enter even number for lot multiple."
                dialog.alertType = .multipleChoice
                dialog.allowTouchOutsideToDismiss = false
                dialog.addButton("OK", hexColor: Constants.ZAlertViewUI.errorButtonColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
                    alertView.dismissAlertView()
                    self.getLotsInput(sender)
                }
                dialog.show()
            } else {
                sender.setTitle("\(input)", for: .normal)
                self.defaultLotValue = intValue
                self.updateAddDeleteLotButtons()
                self.updateInstruments()
                zAlertView.dismissAlertView()
            }
        } else {
            zAlertView.dismissAlertView()
            
            let dialog = ZAlertView()
            dialog.alertTitle = "Error"
            dialog.message = "Please enter numeric value only."
            dialog.alertType = .multipleChoice
            dialog.allowTouchOutsideToDismiss = false
            dialog.addButton("OK", hexColor: Constants.ZAlertViewUI.errorButtonColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
                alertView.dismissAlertView()
                self.getLotsInput(sender)
            }
            
            dialog.show()
        }
    }
    
    private func updateAddDeleteLotButtons() {
        if defaultLotValue == 1 {
            btnDeleteQty.isEnabled = false
            btnAddQty.isEnabled = true
        } else if defaultLotValue > 1 {
            btnDeleteQty.isEnabled = true
            btnAddQty.isEnabled = true
        }
        
        DispatchQueue.main.async {
            self.btnLots.setTitle("\(self.defaultLotValue)", for: .normal)
        }
        
        btnLotsWidthConstraint.isActive = false
        btnLotsWidthConstraint.constant = "\(defaultLotValue)".sizeOfString(usingFont: (btnLots.titleLabel?.font)!).width + 25
        btnLotsWidthConstraint.isActive = true
        self.view.layoutIfNeeded()
        
    }
    
    @objc private func fetchCMP(_ sender: UIButton) {
        print("fetchCMP")
        resignAllResponders()
        
        if let symbolName = self.symbol {
            
            //----------------------------- NEW CODE: START --------------------------//
            var instruments = [String]()
            
            if legs > 0 && (lblInstrumentA.text != nil) {
                guard let leg = lblInstrumentA.text else { return }
                guard let strike = dropdownStrikeA.titleLabel?.text?.double() else { return }
                var legSplit = leg.components(separatedBy: " ")
                instruments.append("\(symbolName.uppercased())_\(legSplit[0])_1_\(legSplit[1])_\(strike.toString())")
            }
            
            if legs > 1 && (lblInstrumentB.text != nil) {
                guard let leg = lblInstrumentB.text else { return }
                guard let strike = dropdownStrikeB.titleLabel?.text?.double() else { return }
                var legSplit = leg.components(separatedBy: " ")
                instruments.append("\(symbolName.uppercased())_\(legSplit[0])_1_\(legSplit[1])_\(strike.toString())")
            }
            
            if legs > 2 && (lblInstrumentC.text != nil) {
                guard let leg = lblInstrumentC.text else { return }
                guard let strike = dropdownStrikeC.titleLabel?.text?.double() else { return }
                var legSplit = leg.components(separatedBy: " ")
                instruments.append("\(symbolName.uppercased())_\(legSplit[0])_1_\(legSplit[1])_\(strike.toString())")
            }
            
            if legs > 3 && (lblInstrumentD.text != nil) {
                guard let leg = lblInstrumentD.text else { return }
                guard let strike = dropdownStrikeD.titleLabel?.text?.double() else { return }
                var legSplit = leg.components(separatedBy: " ")
                instruments.append("\(symbolName.uppercased())_\(legSplit[0])_1_\(legSplit[1])_\(strike.toString())")
            }
            
            SVProgressHUD.show()
            
            OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, price, oi, volume) in
                SVProgressHUD.dismiss()
                
                if let err = error {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                        if self.legs > 0 {
                            self.tfPriceA.text = "-"
                        }
                        
                        if self.legs > 1 {
                            self.tfPriceB.text = "-"
                        }
                        
                        if self.legs > 2 {
                            self.tfPriceC.text = "-"
                        }
                        
                        if self.legs > 3 {
                            self.tfPriceD.text = "-"
                        }
                    })
                } else {
                    if let priceArray = price {
                        
                        let roundOffUptoDecimal: Int = 2
                        
                        if self.legs > 0 {
                            if priceArray.indices.contains(0) {
                                self.tfPriceA.text = priceArray[0].roundToDecimal(roundOffUptoDecimal).toString()
                            }
                        }
                        
                        if self.legs > 1 {
                            if priceArray.indices.contains(1) {
                                self.tfPriceB.text = priceArray[1].roundToDecimal(roundOffUptoDecimal).toString()
                            }
                        }
                        
                        if self.legs > 2 {
                            if priceArray.indices.contains(2) {
                                self.tfPriceC.text = priceArray[2].roundToDecimal(roundOffUptoDecimal).toString()
                            }
                        }
                        
                        if self.legs > 3 {
                            if priceArray.indices.contains(3) {
                                self.tfPriceD.text = priceArray[3].roundToDecimal(roundOffUptoDecimal).toString()
                            }
                        }
                    }
                }
            }
            
            //------------------------------ NEW CODE: END ---------------------------//
            
            //----------------------------- OLD CODE: START --------------------------//
            /*var instruments: String = ""
            
            if legs > 0 && (lblInstrumentA.text != nil) {
                guard let leg1 = lblInstrumentA.text else { return }
                guard let strike1 = dropdownStrikeA.titleLabel?.text else { return }
                instruments.append("\(leg1) \(strike1)")
            }
            
            if legs > 1 && (lblInstrumentB.text != nil) {
                guard let leg2 = lblInstrumentB.text else { return }
                guard let strike2 = dropdownStrikeB.titleLabel?.text else { return }
                instruments.append(",\(leg2) \(strike2)")
            }
            
            if legs > 2 && (lblInstrumentC.text != nil) {
                guard let leg3 = lblInstrumentC.text else { return }
                guard let strike3 = dropdownStrikeC.titleLabel?.text else { return }
                instruments.append(",\(leg3) \(strike3)")
            }
            
            if legs > 3 && (lblInstrumentD.text != nil) {
                guard let leg4 = lblInstrumentD.text else { return }
                guard let strike4 = dropdownStrikeD.titleLabel?.text else { return }
                instruments.append(",\(leg4) \(strike4)")
            }
            
            SVProgressHUD.show()
            
            OSOWebService.sharedInstance.getMultipleOptionCmps(symbol: symbolName, instruments: instruments) { (success, error, cmps) in
                SVProgressHUD.dismiss()
                if success {
                    if let cmpArray = cmps {
                        if self.legs > 0 {
                            self.tfPriceA.text = (cmpArray[0] == "0") ? "-" : cmpArray[0]
                        }
                        
                        if self.legs > 1 {
                            self.tfPriceB.text = (cmpArray[1] == "0") ? "-" : cmpArray[1]
                        }
                        
                        if self.legs > 2 {
                            self.tfPriceC.text = (cmpArray[2] == "0") ? "-" : cmpArray[2]
                        }
                        
                        if self.legs > 3 {
                            self.tfPriceD.text = (cmpArray[3] == "0") ? "-" : cmpArray[3]
                        }
                    } else {
                        if self.legs > 0 {
                            self.tfPriceA.text = "-"
                        }
                        
                        if self.legs > 1 {
                            self.tfPriceB.text = "-"
                        }
                        
                        if self.legs > 2 {
                            self.tfPriceC.text = "-"
                        }
                        
                        if self.legs > 3 {
                            self.tfPriceD.text = "-"
                        }
                    }
                    
                } else {
                    if let err = error {
                        self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                            if self.legs > 0 {
                                self.tfPriceA.text = "-"
                            }
                            
                            if self.legs > 1 {
                                self.tfPriceB.text = "-"
                            }
                            
                            if self.legs > 2 {
                                self.tfPriceC.text = "-"
                            }
                            
                            if self.legs > 3 {
                                self.tfPriceD.text = "-"
                            }
                        })
                    }
                }
            }*/
            //----------------------------- OLD CODE: END --------------------------//
        }
    }
    
    @objc private func showStrikeForInstrument(_ sender: UIButton) {
        
        resignAllResponders()
        
        if let selectedOption = sender.titleLabel?.text, let strikesArray = self.strikes {
            
            if let initialSelection = strikesArray.index(of: selectedOption) {
                
                let picker = ActionSheetStringPicker(title: "Select Strike", rows: strikesArray, initialSelection: initialSelection, doneBlock: { (picker, value, index) in
                    sender.setTitle(index as? String, for: .normal)
                    
                    switch sender {
                    case self.dropdownStrikeA:
                        self.tfPriceA.clear()
                    case self.dropdownStrikeB:
                        self.tfPriceB.clear()
                    case self.dropdownStrikeC:
                        self.tfPriceC.clear()
                    case self.dropdownStrikeD:
                        self.tfPriceD.clear()
                    default:
                        ()
                    }
                }, cancel: { (picker) in
                    //
                }, origin: sender)
                
                picker?.show()
            }
        }
    }
    
    func closestIndexInStrikes(strikes: [String], CMP: Double) -> Int {
        
        guard let firstStrike = strikes[0].double() else {
            return -1
        }
        
        var smallestDifference = (CMP - firstStrike).abs
        var closest = 0
        
        for i in 0..<strikes.count {
            guard let strikeValue = strikes[i].double() else {
                return -1
            }
            
            let currentDifference = (CMP - strikeValue).abs
            
            if currentDifference < smallestDifference {
                smallestDifference = currentDifference
                closest = i
            }
        }
        
        return closest
    }
    
    @objc func radioButtonSelected(_ radioButton: DLRadioButton) {
        resignAllResponders()
        
        guard let tag = radioButton.selected()?.tag else { return }
        
        selectedRadioButton = tag
    }
    
    private func resignAllResponders() {
        view.endEditing(true)
    }
    
    // MARK:- Keyboard Events
    
    @objc func keyBoardDidShow(notification: NSNotification) {
        //handle appearing of keyboard here
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            /*if let textField = activeTextField {
                UIView.animate(withDuration: 0.3) {
                    self.scrollViewContainer.contentOffset = CGPoint(x: 0, y: textField.frame.origin.y)
                }
            }*/
            
            scrollViewContainerBottomAnchorConstraint.isActive = false
            
            scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -(keyboardSize.height))
            
            UIView.animate(withDuration: 0.3) {
                self.scrollViewContainerBottomAnchorConstraint.isActive = true
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
        
        scrollViewContainerBottomAnchorConstraint.isActive = false
        
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnGetStarted, attribute: .top, multiplier: 1.0, constant: -16)
        
        UIView.animate(withDuration: 0.3) {
            self.scrollViewContainerBottomAnchorConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK:- UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.tfPriceA || textField == self.tfPriceB || textField == self.tfPriceC || textField == self.tfPriceD {
            /*let countdots =  (textField.text!).components(separatedBy: ".").count - 1
            
            if countdots > 0 && string == "." {
                return false
            } else {
                return true
            }*/
            
            if textField.text != "" || string != "" {
                let res = (textField.text ?? "") + string
                return Double(res) != nil
            }
            
            return true
        } else if textField == self.tfLots {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return string == filtered
        } else {
            return true
        }
    }

}

extension PredefinedStrategiesStepC: OSOFormRowPickerDelegate {
    
    func optionChanged(row: OSOFormRowPicker, option: String, optionIndex: Int) {
        if row.rowIdentifier == "row_base_expiry" {
            self.selectedExpiry = optionIndex
        }
    }
    
}
