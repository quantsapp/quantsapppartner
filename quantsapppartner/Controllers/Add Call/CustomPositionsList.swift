//
//  CustomPositionsList.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 26/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import ZAlertView

protocol CustomPositionsListDelegate: class {
    func closeList(positions: [AddEditPositions])
}

class CustomPositionsList: UIViewController {
    
    weak var delegate: CustomPositionsListDelegate?
    
    // navigation bar
    private var osoNavBar: OSONavigationBar!
    
    // postions
    public var positions: [AddEditPositions]?
    
    // TextFields
    private var tfPrice: UITextField!
    private var tfQuantity: UITextField!
    
    private let positionsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private let lblMessage: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // background gradient
        createGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        
        // setup views
        self.setupViews()
    }
    
    private func setupViews() {
        
        // lblMessage
        view.addSubview(lblMessage)
        lblMessage.isHidden = true
        lblMessage.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        lblMessage.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        lblMessage.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 20).isActive = true
        lblMessage.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "NO POSITIONS", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.8)]))
        attributedString.append(NSAttributedString(string: "\n\nLooks like you have deleted all the positions.", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.5)]))
        lblMessage.attributedText = attributedString
        
        
        // positionsTableView
        view.addSubview(positionsTableView)
        positionsTableView.isHidden = true
        positionsTableView.dataSource = self
        positionsTableView.delegate = self
        positionsTableView.backgroundColor = UIColor.clear
        positionsTableView.separatorStyle = .none
        positionsTableView.scrollsToTop = true
        positionsTableView.register(CustomPositionsListCell.self, forCellReuseIdentifier: CustomPositionsListCell.cellIdentifier)
        positionsTableView.tableFooterView = UIView()
        positionsTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        positionsTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        positionsTableView.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 0).isActive = true
        if #available(iOS 11.0, *) {
            positionsTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            positionsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        
        if self.positions != nil {
            positionsTableView.isHidden = false
            positionsTableView.reloadData()
        }
    }

    
    private func createGradientLayer(topColor:UIColor, bottomColor:UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        view.layer.addSublayer(gradientLayer)
    }
    
    private func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: Constants.OSONavigationBarConstants.barHeight), title: "Positions", subTitle: nil, leftbuttonImage: nil, rightButtonImage: UIImage(named: "icon_close"))
        view.addSubview(osoNavBar)
        osoNavBar.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func editPosition(index: Int) {
        if let positions = self.positions {
            let position = positions[index]
            
            if let price = position.price, let quantity = position.quantity {
                showEditLegAlert(title: "Edit Leg", price: price, quantity: quantity) { (priceValue, quantityValue) in
                    if let price = priceValue, let quantity = quantityValue {
                        
                        if let priceDbl = price.double(), let quantityDbl = quantity.double() {
                            if priceDbl <= 0 {
                                self.showZAlertView(withTitle: "Error", andMessage: "Price cannot be zero or left blank.", cancelTitle: "OK", isError: true, completion: {
                                    self.editPosition(index: index)
                                })
                            } else {
                                self.positions![index].price = price
                            }
                            
                            if quantityDbl == 0 {
                                self.showZAlertView(withTitle: "Error", andMessage: "Quantity cannot be zero or left blank.", cancelTitle: "OK", isError: true, completion: {
                                    self.editPosition(index: index)
                                })
                            } else {
                                self.positions![index].quantity = quantity
                            }
                            
                            self.positionsTableView.reloadData()
                        } else {
                            self.positions![index].price = price
                            self.positions![index].quantity = quantity
                            
                            self.positionsTableView.reloadData()
                        }
                        
                    }
                }
            }
        }
    }
    
    // MARK:- Edit Leg
    
    private func showEditLegAlert(title: String, price: String, quantity: String, completion: @escaping((_ price: String?, _ quantity: String?) -> Void)) {
        let dialog = ZAlertView()
        dialog.alertTitle = title
        dialog.message = "Enter Price and Quantity"
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        dialog.addTextField("field_price", placeHolder: "Price", keyboardType: .decimalPad)
        if let inputTf = dialog.getTextFieldWithIdentifier("field_price") {
            tfPrice = inputTf
            tfPrice.delegate = self
            inputTf.text = price == "0" ? "" : price
            inputTf.delegate = self
        }
        
        dialog.addTextField("field_quantity", placeHolder: "Quantity", keyboardType: .decimalPad)
        if let inputTf = dialog.getTextFieldWithIdentifier("field_quantity") {
            tfQuantity = inputTf
            tfQuantity.delegate = self
            
            if let intQuantity = quantity.int {
                inputTf.text = quantity == "0" ? "" : "\(intQuantity.abs)"
                inputTf.delegate = self
            }
        }
        
        dialog.addButton("Update", color: Constants.UIConfig.themeColor, titleColor: UIColor.white) { (alertView) in
            var newPrice = "0"
            var newQuantity = "0"
            
            if let inputTf = dialog.getTextFieldWithIdentifier("field_price") {
                if let text = inputTf.text {
                    newPrice = text.isEmpty ? "0" : text
                }
            }
            
            if let inputTf = dialog.getTextFieldWithIdentifier("field_quantity") {
                if let text = inputTf.text {
                    var factor = 1
                    
                    if let qtyValue = quantity.int {
                        factor = qtyValue < 0 ? -1 : 1
                    }
                    
                    newQuantity = text.isEmpty ? "0" : "\(text.int! * factor)"
                }
            }
            
            alertView.dismissAlertView()
            completion(newPrice, newQuantity)
        }
        dialog.addButton("Cancel", color: UIColor(white: 0.0, alpha: 0.3), titleColor: UIColor.white) { (alertView) in
            alertView.dismissAlertView()
            completion(nil, nil)
        }
        
        dialog.show()
    }
    
    // MARK:- UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfPrice {
            /*let countdots =  (textField.text!).components(separatedBy: ".").count - 1 //textField.text.componentsSeparatedByString(".").count - 1
            
            if countdots > 0 && string == "." {
                return false
            } else {
                return true
            }*/
            
            if textField.text != "" || string != "" {
                let res = (textField.text ?? "") + string
                return Double(res) != nil
            }
            
            return true
        } else if textField == tfQuantity {
            /*let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return string == filtered*/
            
            if textField.text != "" || string != "" {
                let res = (textField.text ?? "") + string
                return Int(res) != nil
            }
            
            return true
        } else {
            return true
        }
    }
}

// MARK:- UITableViewDataSource

extension CustomPositionsList: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let positionsArray = self.positions {
            return positionsArray.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomPositionsListCell.cellIdentifier, for: indexPath) as! CustomPositionsListCell
        
        if let positionsArray = self.positions {
            let position = positionsArray[indexPath.row]
            
            if let instrument = position.instrument {
                cell.lblInstrumentValue.text = instrument
            } else {
                cell.lblInstrumentValue.text = "-"
            }
            
            if let expiry = position.expiry {
                cell.lblExpiryValue.text = expiry
            } else {
                cell.lblExpiryValue.text = "-"
            }
            
            if let strike = position.strike {
                cell.lblStrikeValue.text = (strike == "-1") ? "-" : strike
            } else {
                cell.lblStrikeValue.text = "-"
            }
            
            if let optType = position.optType {
                cell.lblOptionTypeValue.text = optType
            } else {
                cell.lblOptionTypeValue.text = "-"
            }
            
            if let quantity = position.quantity {
                cell.lblQuantityValue.text = quantity
            } else {
                cell.lblQuantityValue.text = "-"
            }
            
            if let price = position.price {
                cell.lblPriceValue.text = price
            } else {
                cell.lblPriceValue.text = "-"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            print("Delete")
            self.positions?.remove(at: indexPath.row)
            if self.positions?.count == 0 {
                self.positionsTableView.isHidden = true
                self.lblMessage.isHidden = false
            } else {
                tableView.reloadData()
                /*let range = NSMakeRange(0, tableView.numberOfSections)
                let sections = NSIndexSet(indexesIn: range)
                tableView.reloadSections(sections as IndexSet, with: .automatic)*/
            }
        }
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            print("Edit")
            self.editPosition(index: indexPath.row)
        }
        
        return [deleteAction, editAction]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: "Delete") { (action, view, completionHandler) in
            print("Delete")
            self.positions?.remove(at: indexPath.row)
            if self.positions?.count == 0 {
                self.positionsTableView.isHidden = true
                self.lblMessage.isHidden = false
            } else {
                tableView.reloadData()
                /*let range = NSMakeRange(0, tableView.numberOfSections)
                let sections = NSIndexSet(indexesIn: range)
                tableView.reloadSections(sections as IndexSet, with: .automatic)*/
            }
            
            completionHandler(true)
        }
        
        deleteAction.image = UIImage(named: "icon_trash")
        deleteAction.backgroundColor = Constants.Charts.chartRedColor
        
        let editAction = UIContextualAction(style: .normal, title: "Edit") { (action, view, completionHandler) in
            print("Edit")
            self.editPosition(index: indexPath.row)
            completionHandler(true)
        }
        
        editAction.image = UIImage(named: "icon_swipe_edit")
        editAction.backgroundColor = Constants.UIConfig.themeColor
        
        
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
}

// MARK:- UITableViewDelegate

extension CustomPositionsList: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.positions != nil {
            return 76
        } else {
            return 0
        }
    }
    
}

// MARK:-  OSONavigationBarDelegate

extension CustomPositionsList: OSONavigationBarDelegate {
    
    func rightButtonTapped(sender: OSONavigationBar) {
        delegate?.closeList(positions: self.positions!)
    }
    
}
