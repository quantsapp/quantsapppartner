//
//  FindSpecificStepA.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 09/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import SearchTextField
import SVProgressHUD
import ZAlertView
import Sheeeeeeeeet

protocol FindSpecificStepADelegate: class {
    func stepACompleted(symbol: String, direction: Int, risk: Int)
}

class FindSpecificStepA: UIViewController {
    
    weak var delegate: FindSpecificStepADelegate?
    
    var viewAppeared: Bool = false
    
    let directions = [["name": "Bullish", "icon": "icon_bullish"],
                      ["name": "Bearish", "icon": "icon_bearish"],
                      ["name": "Eitherways", "icon": "icon_eitherways"],
                      ["name": "Oscillate", "icon": "icon_oscillate"]]
    
    let risks = [["name": "Limited Risk Strategies", "icon": "icon_limited_risk_strategies"],
                 ["name": "All Strategies", "icon": "icon_all_strategies"]]
    
    // constraints
    var scrollViewContainerBottomAnchorConstraint: NSLayoutConstraint!
    var formHeightConstraint: NSLayoutConstraint!
    
    var symbols = [String]()
    var currentDirection: String?
    
    let scrollViewContainer: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    let formView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let btnGetStarted: UIButton = {
        let button = UIButton()
        button.setTitle("NEXT", for: .normal)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let lblSymbol: UILabel = {
        let label = UILabel()
        label.text = "Symbol"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let borderSymbol: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let borderDirection: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let borderRisk: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let lblDirection: UILabel = {
        let label = UILabel()
        label.text = "Direction"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let btnDirection: DirectionButton = {
        let button = DirectionButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let lblRisk: UILabel = {
        let label = UILabel()
        label.text = "Risk"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let btnRisk: RiskButton = {
        let button = RiskButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.75)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let stfSymbol: SearchTextField = {
        let stf = SearchTextField()
        stf.font = UIFont.boldSystemFont(ofSize: 14)
        stf.textColor = Constants.UIConfig.themeColor
        stf.textAlignment = .left
        stf.autocorrectionType = .no
        stf.clearButtonMode = .whileEditing
        stf.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        stf.autocapitalizationType = .allCharacters
        stf.theme.font = UIFont.systemFont(ofSize: 16)
        stf.theme.fontColor = UIColor(white: 1.0, alpha: 0.5)
        stf.theme.bgColor = UIColor.black
        stf.theme.borderColor = UIColor(white: 1.0, alpha: 0.1)
        stf.theme.separatorColor = UIColor(white: 1.0, alpha: 0.25)
        stf.theme.cellHeight = 40
        stf.highlightAttributes = [NSAttributedString.Key.foregroundColor: Constants.UIConfig.themeColor,
                                   NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)]
        stf.translatesAutoresizingMaskIntoConstraints = false
        return stf
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // keyboard event handler
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !viewAppeared {
            viewAppeared = true
            
            // get Symbols
            getSymbols()
        }
        
    }

    private func setupViews() {
        
        view.backgroundColor = UIColor.clear
        
        // btnGetStarted
        view.addSubview(btnGetStarted)
        btnGetStarted.addTarget(self, action: #selector(actionNext(_:)), for: .touchUpInside)
        btnGetStarted.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        btnGetStarted.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnGetStarted.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        btnGetStarted.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        
        // scrollview container
        view.addSubview(scrollViewContainer)
        
        
        scrollViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollViewContainer.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnGetStarted, attribute: .top, multiplier: 1.0, constant: -16)
        scrollViewContainerBottomAnchorConstraint.isActive = true
        
        // form view
        scrollViewContainer.addSubview(formView)
        
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .top, relatedBy: .equal, toItem: scrollViewContainer, attribute: .top, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .left, relatedBy: .equal, toItem: scrollViewContainer, attribute: .left, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .right, relatedBy: .equal, toItem: scrollViewContainer, attribute: .right, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: scrollViewContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
        formHeightConstraint = NSLayoutConstraint(item: formView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 122)
        formHeightConstraint.isActive = true
        //formView.heightAnchor.constraint(equalToConstant: 236).isActive = true
        
        scrollViewContainer.contentSize = formView.size
        
        // MARK: Symbol
        // lblSymbol
        formView.addSubview(lblSymbol)
        lblSymbol.topAnchor.constraint(equalTo: formView.topAnchor, constant: 16).isActive = true
        lblSymbol.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblSymbol.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        //lblSymbol.widthAnchor.constraint(equalToConstant: Constants.UIConfig.fieldLabelWidth).isActive = true
        lblSymbol.widthAnchor.constraint(equalToConstant: (lblSymbol.text?.sizeOfString(usingFont: lblSymbol.font).width)! + 4).isActive = true
        
        // stfSymbol
        formView.addSubview(stfSymbol)
        addToolBar(textField: stfSymbol)
        stfSymbol.topAnchor.constraint(equalTo: lblSymbol.topAnchor, constant: 0).isActive = true
        stfSymbol.heightAnchor.constraint(equalToConstant: 30).isActive = true
        stfSymbol.leftAnchor.constraint(equalTo: lblSymbol.rightAnchor, constant: 10).isActive = true
        stfSymbol.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        stfSymbol.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            //print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            self.stfSymbol.text = item.title
            
            // save to Instrument settings
            InstrumentSettings.sharedInstance.symbol = item.title
            
            self.stfSymbol.resignFirstResponder()
        }
        
        // (borderSymbol)
        formView.addSubview(borderSymbol)
        borderSymbol.topAnchor.constraint(equalTo: stfSymbol.bottomAnchor, constant: 0).isActive = true
        borderSymbol.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderSymbol.leftAnchor.constraint(equalTo: stfSymbol.leftAnchor, constant: -4).isActive = true
        borderSymbol.rightAnchor.constraint(equalTo: stfSymbol.rightAnchor, constant: 0).isActive = true
        
        // MARK: Direction
        // lblDirection
        formView.addSubview(lblDirection)
        lblDirection.topAnchor.constraint(equalTo: lblSymbol.bottomAnchor, constant: 8).isActive = true
        lblDirection.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblDirection.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        //lblDirection.widthAnchor.constraint(equalToConstant: Constants.UIConfig.fieldLabelWidth).isActive = true
        lblDirection.widthAnchor.constraint(equalToConstant: (lblDirection.text?.sizeOfString(usingFont: lblDirection.font).width)! + 4).isActive = true
        
        formView.addSubview(btnDirection)
        btnDirection.setTitle("\(directions.first!["name"]!)", for: .normal)
        btnDirection.setImage(UIImage(named: "\(directions.first!["icon"]!)"), for: .normal)
        btnDirection.addTarget(self, action: #selector(showDirectionPicker(_:)), for: .touchUpInside)
        btnDirection.topAnchor.constraint(equalTo: lblDirection.topAnchor, constant: 0).isActive = true
        btnDirection.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btnDirection.leftAnchor.constraint(equalTo: lblDirection.rightAnchor, constant: 10).isActive = true
        btnDirection.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        // (borderDirection)
        formView.addSubview(borderDirection)
        borderDirection.topAnchor.constraint(equalTo: btnDirection.bottomAnchor, constant: 0).isActive = true
        borderDirection.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderDirection.leftAnchor.constraint(equalTo: btnDirection.leftAnchor, constant: -4).isActive = true
        borderDirection.rightAnchor.constraint(equalTo: btnDirection.rightAnchor, constant: 0).isActive = true
        
        // MARK: Risk
        // lblRisk
        formView.addSubview(lblRisk)
        lblRisk.topAnchor.constraint(equalTo: lblDirection.bottomAnchor, constant: 8).isActive = true
        lblRisk.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblRisk.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        //lblRisk.widthAnchor.constraint(equalToConstant: Constants.UIConfig.fieldLabelWidth).isActive = true
        lblRisk.widthAnchor.constraint(equalToConstant: (lblRisk.text?.sizeOfString(usingFont: lblRisk.font).width)! + 4).isActive = true
        
        formView.addSubview(btnRisk)
        btnRisk.setTitle("\(risks[1]["name"]!)", for: .normal)
        btnRisk.setImage(UIImage(named: "\(risks[1]["icon"]!)")?.withRenderingMode(.alwaysTemplate), for: .normal)
        //btnRisk.setTitle("\(risks.first!["name"]!)", for: .normal)
        //btnRisk.setImage(UIImage(named: "\(risks.first!["icon"]!)")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnRisk.addTarget(self, action: #selector(showRiskPicker(_:)), for: .touchUpInside)
        btnRisk.topAnchor.constraint(equalTo: lblRisk.topAnchor, constant: 0).isActive = true
        btnRisk.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btnRisk.leftAnchor.constraint(equalTo: lblRisk.rightAnchor, constant: 10).isActive = true
        btnRisk.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        // (borderRisk)
        formView.addSubview(borderRisk)
        borderRisk.bottomAnchor.constraint(equalTo: btnRisk.bottomAnchor, constant: 0).isActive = true
        borderRisk.heightAnchor.constraint(equalToConstant: 1).isActive = true
        borderRisk.leftAnchor.constraint(equalTo: btnRisk.leftAnchor, constant: -4).isActive = true
        borderRisk.rightAnchor.constraint(equalTo: btnRisk.rightAnchor, constant: 0).isActive = true
        
        
        // set direction
        currentDirection = directions.first!["name"]!
        
    }
    
    @objc private func actionNext(_ sender: UIButton) {
        if !isSymbolValid(symbolName: (stfSymbol.text?.trimmed)!) {
            return
        } else {
            var index: Int = 0
            let riskIndex = getRiskIndexFor(risk: (btnRisk.titleLabel?.text)!)
            
            switch riskIndex {
            case 1:
                index = 1
            case 2:
                index = 3
            default:
                ()
            }
            
            delegate?.stepACompleted(symbol: (stfSymbol.text?.trimmed)!, direction: getDirectionIndexFor(direction: (btnDirection.titleLabel?.text)!), risk: index)
        }
    }
    
    @objc private func showDirectionPicker(_ sender: UIButton) {
        
        view.endEditing(true)
        
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Select Direction"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        for direction in directions {
            let item = ActionSheetItem(title: direction["name"]!, value: direction["name"], image: UIImage(named: "\(direction["icon"]!)"))
            items.append(item)
        }
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            print("selected = \(value)")
            
            if self.currentDirection != value {
                
                for direction in self.directions {
                    if let name = direction["name"] {
                        if name == value {
                            let image = UIImage(named: "\(direction["icon"]!)")
                            self.btnDirection.setTitle(name, for: .normal)
                            self.btnDirection.setImage(image, for: .normal)
                            self.currentDirection = value
                        }
                    }
                }
            }
            
            
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
        
    }
    
    @objc private func showRiskPicker(_ sender: UIButton) {
        
        view.endEditing(true)
        
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Select Risk"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        for risk in risks {
            let item = ActionSheetItem(title: risk["name"]!, value: risk["name"], image: UIImage(named: "\(risk["icon"]!)"))
            items.append(item)
        }
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            print("selected = \(value)")
            
            for risk in self.risks {
                if let name = risk["name"] {
                    if name == value {
                        let image = UIImage(named: "\(risk["icon"]!)")
                        self.btnRisk.setTitle(name, for: .normal)
                        self.btnRisk.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
                    }
                }
            }
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
        
    }
    
    private func getSymbols() {
        
        SVProgressHUD.show()
        
        DatabaseManager.sharedInstance.getOptionSymbols { (success, error, symbols) in
            
            SVProgressHUD.dismiss()
            
            if let error = error {
                print("Error: \(error.localizedDescription)")
            } else {
                if let sym = symbols {
                    self.symbols = sym
                    self.stfSymbol.filterStrings(sym)
                    
                    self.initWithDefaultSymbol()
                }
            }
        }
    }
    
    func initWithDefaultSymbol() {
        
        var lastSymbol = "NIFTY"
        
        if let sym = InstrumentSettings.sharedInstance.symbol {
            lastSymbol = sym
        }
        
        self.stfSymbol.text = lastSymbol
    }
    
    private func isSymbolValid(symbolName: String) -> Bool {
        if symbolName.isEmpty {
            showZAlertView(withTitle: "Blank Symbol", andMessage: "Please enter a symbol name.", cancelTitle: "OK", isError: true, completion: {
                self.stfSymbol.text = ""
                self.stfSymbol.becomeFirstResponder()
            })
            return false
        } else if !symbols.contains(symbolName) {
            showZAlertView(withTitle: "Invalid Symbol", andMessage: "Symbol does not exists. Please enter valid symbol name.", cancelTitle: "OK", isError: true, completion: {
                self.stfSymbol.text = ""
                self.stfSymbol.becomeFirstResponder()
            })
            return false
        } else {
            return true
        }
    }
    
    private func getDirectionIndexFor(direction: String) -> Int {
        let directionIndex = directions.index {
            if let dic = $0 as? Dictionary<String, String> {
                if let value = dic["name"] {
                    if value == direction {
                        return true
                    }
                }
            }
            return false
        }
        
        return directionIndex!
    }
    
    private func getRiskIndexFor(risk: String) -> Int {
        let riskIndex = risks.index {
            if let dic = $0 as? Dictionary<String, String> {
                if let value = dic["name"] {
                    if value == risk {
                        return true
                    }
                }
            }
            return false
        }
        
        return riskIndex! + 1
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollViewContainer.scrollRectToVisible(textField.frame, animated: true)
    }
    

    // MARK:- Keyboard Events
    
    @objc func keyBoardDidShow(notification: NSNotification) {
        //handle appearing of keyboard here
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            scrollViewContainerBottomAnchorConstraint.isActive = false
            
            scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -(keyboardSize.height))
            
            UIView.animate(withDuration: 0.3) {
                self.scrollViewContainerBottomAnchorConstraint.isActive = true
                self.view.layoutIfNeeded()
            }
            
            
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
        
        scrollViewContainerBottomAnchorConstraint.isActive = false
        
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnGetStarted, attribute: .top, multiplier: 1.0, constant: -16)
        
        UIView.animate(withDuration: 0.3) {
            self.scrollViewContainerBottomAnchorConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }

}
