//
//  PredefinedStrategiesStepsPages.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 13/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class PredefinedStrategiesStepsPages: UIPageViewController {
    
    public var osoNavBar: OSONavigationBar?
    
    public var osoStepProgress: OSOStepProgress? {
        didSet {
            if let stepProgress = osoStepProgress {
                stepProgress.delegate = self
            }
        }
    }
    
    fileprivate lazy var pages: [UIViewController] = {
        return [
            self.getViewController(forStep: "step_a")
        ]
    }()

    var pageControl = UIPageControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        //self.dataSource = self
        self.delegate = self
        
        if let firstVC = pages.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
        //configurePageControl()
    }
    
    fileprivate func getViewController(forStep step: String) -> UIViewController
    {
        switch step {
        case "step_a":
            let vc = FindSpecificStepA()
            vc.delegate = self
            return vc
        
        default:
            return UIViewController()
        }
    }
    
    
    
    func configurePageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 150, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = pages.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor(white: 1.0, alpha: 0.2)
        self.view.addSubview(pageControl)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

// MARK:- FindSpecificStepADelegate

extension PredefinedStrategiesStepsPages: FindSpecificStepADelegate {
    
    func stepACompleted(symbol: String, direction: Int, risk: Int) {
        for i in (0..<pages.count).reversed() {
            if self.pages[i].isKind(of: PredefinedStrategiesStepD.self) {
                self.pages[i].removeFromParent()
                self.pages.remove(at: i)
            } else if pages[i].isKind(of: PredefinedStrategiesStepC.self) {
                pages[i].removeFromParent()
                pages.remove(at: i)
            } else if pages[i].isKind(of: FindSpecificStepB.self) {
                pages[i].removeFromParent()
                pages.remove(at: i)
            }
        }
        
        let vc = FindSpecificStepB()
        vc.symbol = symbol
        vc.direction = direction
        vc.risk = risk
        vc.delegate = self
        pages.append(vc)
        pageControl.numberOfPages = pages.count
        
        setViewControllers([pages[1]], direction: .forward, animated: true) { (finished) in
            if let stepProgress = self.osoStepProgress {
                stepProgress.setCurrentStepAsCompleted()
            }
        }
    }
}

// MARK:- FindSpecificStepBDelegate

extension PredefinedStrategiesStepsPages: FindSpecificStepBDelegate {
    
    func stepBCompleted(symbol: String, direction: Int, risk: Int, strategy: MyLibrary) {
        
        print("stepBCompleted")
        
        for i in (0..<pages.count).reversed() {
            if self.pages[i].isKind(of: PredefinedStrategiesStepD.self) {
                self.pages[i].removeFromParent()
                self.pages.remove(at: i)
            } else if pages[i].isKind(of: PredefinedStrategiesStepC.self) {
                pages[i].removeFromParent()
                pages.remove(at: i)
            }
        }
        
        let vc = PredefinedStrategiesStepC()
        vc.symbol = symbol
        vc.direction = direction
        vc.risk = risk
        vc.strategy = strategy
        vc.delegate = self
        pages.append(vc)
        pageControl.numberOfPages = pages.count
        
        setViewControllers([pages[2]], direction: .forward, animated: true) { (finished) in
            if let stepProgress = self.osoStepProgress {
                stepProgress.setCurrentStepAsCompleted()
            }
        }
    }
    
}

// MARK:- PredefinedStrategiesStepCDelegate

extension PredefinedStrategiesStepsPages: PredefinedStrategiesStepCDelegate {
    
    func stepCErrorOccured() {
        if let stepProgress = self.osoStepProgress {
            stepProgress.setCurrentStepAsIncomplete()
        }
    }
    
    func stepCEnded() {
        if let vc = getParentViewController() as? AddCallViewController {
            //vc.positionAdded()
        }
    }
    
    func stepCFinished(positionName: String, symbol: String, margin: String, legs: [String], tradeStatus: String, direction: Int, risk: Int, strategyName: String) {
        
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: PredefinedStrategiesStepD.self) {
                pages[i].removeFromParent()
                pages.remove(at: i)
            }
        }
        
        let vc = PredefinedStrategiesStepD()
        vc.symbol = symbol
        vc.positionName = positionName
        vc.symbol = symbol
        vc.margin = margin
        vc.legs = legs
        vc.tradeStatus = tradeStatus
        vc.direction = direction
        vc.risk = risk
        vc.strategyName = strategyName
        vc.delegate = self
        pages.append(vc)
        pageControl.numberOfPages = pages.count
        
        setViewControllers([pages[3]], direction: .forward, animated: true) { (finished) in
            if let stepProgress = self.osoStepProgress {
                stepProgress.setCurrentStepAsCompleted()
            }
        }
    }
    
    func stepCCompleted(symbol: String, direction: Int, risk: Int, positionNo: String, strategyName: String) {
        print("stepCCompleted")
        
        for i in (0..<pages.count).reversed() {
            if pages[i].isKind(of: PredefinedStrategiesStepD.self) {
                pages[i].removeFromParent()
                pages.remove(at: i)
            }
        }
        
        let vc = PredefinedStrategiesStepD()
        vc.symbol = symbol
        vc.direction = direction
        vc.risk = risk
        vc.positionNo = positionNo
        vc.strategyName = strategyName
        vc.delegate = self
        pages.append(vc)
        pageControl.numberOfPages = pages.count
        
        setViewControllers([pages[3]], direction: .forward, animated: true) { (finished) in
            if let stepProgress = self.osoStepProgress {
                stepProgress.setCurrentStepAsCompleted()
            }
        }
    }
}

// MARK:- PredefinedStrategiesStepDDelegate

extension PredefinedStrategiesStepsPages: PredefinedStrategiesStepDDelegate {
    
    func stepDCompleted() {
        print("stepDCompleted")
        
        if let vc = getParentViewController() as? AddCallViewController {
            //vc.positionAdded()
        }
    }
    
}

// MARK:- OSOStepProgressDelegate

extension PredefinedStrategiesStepsPages: OSOStepProgressDelegate {
    
    func stepSelected(stepIndex: Int, isPreviousStep: Bool) {
        
        setViewControllers([pages[stepIndex]], direction: isPreviousStep ? .reverse : .forward, animated: true) { (finished) in
            
            for i in (0..<self.pages.count).reversed() {
                if i > stepIndex {
                    if self.pages[i].isKind(of: PredefinedStrategiesStepD.self) {
                        self.pages[i].removeFromParent()
                        self.pages.remove(at: i)
                    } else if self.pages[i].isKind(of: PredefinedStrategiesStepC.self) {
                        self.pages[i].removeFromParent()
                        self.pages.remove(at: i)
                    } else if self.pages[i].isKind(of: FindSpecificStepB.self) {
                        self.pages[i].removeFromParent()
                        self.pages.remove(at: i)
                    }
                }
            }
            
            self.pageControl.numberOfPages = self.pages.count
            //self.dataSource = nil
            
        }
    }
}

// MARK:- UIPageViewControllerDelegate

extension PredefinedStrategiesStepsPages: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let pageContentViewController = pageViewController.viewControllers![0]
        
        self.pageControl.currentPage = pages.index(of: pageContentViewController)!
    }
    
    
}

// MARK:- UIPageViewControllerDataSource

extension PredefinedStrategiesStepsPages: UIPageViewControllerDataSource {
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0          else { return nil /*pages.last*/ }
        guard pages.count > previousIndex else { return nil }
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < pages.count else { return nil /*pages.first*/ }
        guard pages.count > nextIndex else { return nil }
        return pages[nextIndex]
    }
    
    
}
