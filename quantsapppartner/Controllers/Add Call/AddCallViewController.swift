//
//  AddCallViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import ZAlertView

class AddCallViewController: UIViewController {
    
    private var osoNavBar: OSONavigationBar!
    
    public var callForType: CallForType = .PreDefinedStrategies
    
    private var customStrategy = CustomStrategy()
    
    // step progress
    var osoStepProgress: OSOStepProgress!
    
    private let stepperContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        // initial setup of all views
        setupViews()
    }
    

    private func setupViews() {
        
        if callForType == .PreDefinedStrategies {
            
            // stepperContainerView
            view.addSubview(stepperContainerView)
            stepperContainerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
            stepperContainerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
            stepperContainerView.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 0).isActive = true
            stepperContainerView.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            // osoStepProgress
            osoStepProgress = OSOStepProgress(steps: ["Select", "Strategies", "Add", "Save"])
            osoStepProgress.translatesAutoresizingMaskIntoConstraints = false
            stepperContainerView.addSubview(osoStepProgress)
            osoStepProgress.leftAnchor.constraint(equalTo: stepperContainerView.leftAnchor, constant: 0).isActive = true
            osoStepProgress.rightAnchor.constraint(equalTo: stepperContainerView.rightAnchor, constant: 0).isActive = true
            osoStepProgress.topAnchor.constraint(equalTo: stepperContainerView.topAnchor, constant: 0).isActive = true
            osoStepProgress.bottomAnchor.constraint(equalTo: stepperContainerView.bottomAnchor, constant: 0).isActive = true
            
            // stepsPageVC
            let stepsPageVC = PredefinedStrategiesStepsPages(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
            stepsPageVC.osoNavBar = osoNavBar
            stepsPageVC.osoStepProgress = osoStepProgress
            addChild(stepsPageVC)
            view.addSubview(stepsPageVC.view)
            
            stepsPageVC.view.translatesAutoresizingMaskIntoConstraints = false
            stepsPageVC.view.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
            stepsPageVC.view.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
            stepsPageVC.view.topAnchor.constraint(equalTo: stepperContainerView.bottomAnchor, constant: 0).isActive = true
            if #available(iOS 11.0, *) {
                stepsPageVC.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
            } else {
                stepsPageVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -49).isActive = true
            }
            
            stepsPageVC.didMove(toParent: self)
            
        } else if callForType == .CustomStrategy {
            /*customStrategy.type = "add"
            customStrategy.delegate = self
            customStrategy.view.translatesAutoresizingMaskIntoConstraints = false
            addChild(customStrategy)
            view.addSubview(customStrategy.view)
            
            customStrategy.view.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
            customStrategy.view.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
            customStrategy.view.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 0).isActive = true
            if #available(iOS 11.0, *) {
                customStrategy.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
            } else {
                customStrategy.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
            }
            
            customStrategy.didMove(toParent: self)*/
            
            // stepperContainerView
            view.addSubview(stepperContainerView)
            stepperContainerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
            stepperContainerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
            stepperContainerView.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 0).isActive = true
            stepperContainerView.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            // osoStepProgress
            osoStepProgress = OSOStepProgress(steps: ["Add", "Save"])
            osoStepProgress.translatesAutoresizingMaskIntoConstraints = false
            stepperContainerView.addSubview(osoStepProgress)
            osoStepProgress.leftAnchor.constraint(equalTo: stepperContainerView.leftAnchor, constant: 0).isActive = true
            osoStepProgress.rightAnchor.constraint(equalTo: stepperContainerView.rightAnchor, constant: 0).isActive = true
            osoStepProgress.topAnchor.constraint(equalTo: stepperContainerView.topAnchor, constant: 0).isActive = true
            osoStepProgress.bottomAnchor.constraint(equalTo: stepperContainerView.bottomAnchor, constant: 0).isActive = true
            
            // stepsPageVC
            let stepsPageVC = CustomStrategyStepsPages(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
            stepsPageVC.osoNavBar = osoNavBar
            stepsPageVC.osoStepProgress = osoStepProgress
            addChild(stepsPageVC)
            view.addSubview(stepsPageVC.view)
            
            stepsPageVC.view.translatesAutoresizingMaskIntoConstraints = false
            stepsPageVC.view.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
            stepsPageVC.view.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
            stepsPageVC.view.topAnchor.constraint(equalTo: stepperContainerView.bottomAnchor, constant: 0).isActive = true
            if #available(iOS 11.0, *) {
                stepsPageVC.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
            } else {
                stepsPageVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -49).isActive = true
            }
            
            stepsPageVC.didMove(toParent: self)
        }
    }

    private func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect.zero, title: "Add Call", subTitle: "For \(callForType.title())", leftbuttonImage: UIImage(named: "icon_left"), rightButtonImage: nil)
        osoNavBar.translatesAutoresizingMaskIntoConstraints = false
        osoNavBar.delegate = self
        view.addSubview(osoNavBar)
        
        osoNavBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        osoNavBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        osoNavBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        osoNavBar.heightAnchor.constraint(equalToConstant: Constants.OSONavigationBarConstants.barHeight).isActive = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK:- CustomStrategyDelegate

extension AddCallViewController: CustomStrategyDelegate {
    
    func positionForCustomStrategyCancelled() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK:- OSONavigationBarDelegate

extension AddCallViewController: OSONavigationBarDelegate {
    func leftButtonTapped(sender: OSONavigationBar) {
        
        if callForType == .CustomStrategy {
            /*if customStrategy.positionsList.count > 0 {
                customStrategy.positionsNotSaved()
            } else {
                self.navigationController?.popViewController(animated: true)
            }*/
            
            // ask for confirmation
            let dialog = ZAlertView(title: "Confirm",
                                    message: "Do you want to exit the Add Position tool?",
                                    isOkButtonLeft: false,
                                    okButtonText: "No",
                                    cancelButtonText: "Yes",
                                    okButtonHandler: { (alertView) in
                                        alertView.dismissAlertView()
            }) { (alertView) in
                alertView.dismissAlertView()
                self.navigationController?.popViewController(animated: true)
            }
            dialog.show()
        } else {
            
            // ask for confirmation
            let dialog = ZAlertView(title: "Confirm",
                                    message: "Do you want to exit the Add Position tool?",
                                    isOkButtonLeft: false,
                                    okButtonText: "No",
                                    cancelButtonText: "Yes",
                                    okButtonHandler: { (alertView) in
                                        alertView.dismissAlertView()
            }) { (alertView) in
                alertView.dismissAlertView()
                self.navigationController?.popViewController(animated: true)
            }
            dialog.show()
        }
    }
    
    func rightButtonTapped(sender: OSONavigationBar) {
        //
    }
}
