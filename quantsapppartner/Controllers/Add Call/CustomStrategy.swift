//
//  CustomStrategy.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 18/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import SearchTextField
import SVProgressHUD
import ZAlertView
import Sheeeeeeeeet
import ActionSheetPicker_3_0
import DLRadioButton

protocol CustomStrategyDelegate: class {
    func positionForCustomStrategyAdded()
    func positionForCustomStrategyEdited(tradeStatus: String)
    func positionForCustomStrategyCancelled()
    func positionForCustomStrategyDelete()
    
    func CustomStrategyStepCompleted(positionName: String, symbol: String, positions: [AddEditPositions])
}

extension CustomStrategyDelegate {
    func positionForCustomStrategyAdded() {}
    func positionForCustomStrategyEdited(tradeStatus: String) {}
    func positionForCustomStrategyCancelled() {}
    func positionForCustomStrategyDelete() {}
    
    func CustomStrategyStepCompleted(positionName: String, symbol: String, positions: [AddEditPositions]){}
}

class CustomStrategy: UIViewController, CustomPositionsListDelegate {
    
    weak var delegate: CustomStrategyDelegate?
    
    // navigation bar
    var osoNavBar: OSONavigationBar!
    
    private var viewAppeared: Bool = false
    private var buttonsShown: Bool = false
    
    private var currentSymbol: String = ""
    
    // AddEditPositions
    private var positions = [AddEditPositions]()
    
    public var positionsList: [AddEditPositions] {
        get {
            return positions
        }
    }
    
    private var selectedBook: OSOBook?
    
    private var strikes: [String]?
    public var type = "add"
    public var positionNo: String?
    public var cmp: String?
    public var tradeStatus: String?
    
    private var radioButtons = [DLRadioButton]()
    private var selectedRadioButton: Int = 0
    
    // expiries
    private var selectedExpiry: Int = 0
    private var expiries: [String]?
    
    // lot
    private var lot: Int = -1
    private var lots: [Int]?
    private var selectedLot = 0
    
    // optMaster
    private var optMasters: [OptMaster]?
    
    private var fieldVerticalGap: CGFloat = 4
    private var fieldHeight: CGFloat = 30
    
    // TextField
    private var tfAlertAbove: UITextField!
    private var tfAlertBelow: UITextField!
    
    // constraints
    private var scrollViewContainerBottomAnchorConstraint: NSLayoutConstraint!
    private var formHeightConstraint: NSLayoutConstraint!
    private var btnAddRightAnchorConstraint: NSLayoutConstraint!
    private var btnDoneRightAnchorConstraint: NSLayoutConstraint!
    private var btnPositionsWidthAnchorConstraint: NSLayoutConstraint!
    private var lblStrikeTopAnchorConstraint: NSLayoutConstraint!
    private var lblStrikeHeightAnchorConstraint: NSLayoutConstraint!
    private var borderStrikeHeightAnchorConstraint: NSLayoutConstraint!
    private var lblCallPutOptionTopAnchorConstraint: NSLayoutConstraint!
    private var lblCallPutOptionHeightAnchorConstraint: NSLayoutConstraint!
    private var borderCallPutOptionHeightAnchorConstraint: NSLayoutConstraint!
    private var lblQuantityValueWidthAnchorConstraint: NSLayoutConstraint!
    
    private let scrollViewContainer: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let formView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let btnAdd: UIButton = {
        let button = UIButton()
        button.setTitle("ADD", for: .normal)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnPositions: OSOBadgeButton = {
        let button = OSOBadgeButton()
        button.setImage(UIImage(named: "icon_positions")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.badgeEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 4)
        button.tintColor = UIColor.white
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnDone: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_done")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.bottomColor
        button.backgroundColor = Constants.UIConfig.themeColor //UIColor(white: 1.0, alpha: 0.1)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblPositionName: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Position Name"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    private let tfPositionName: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .left
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.default
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let lblSymbol: UILabel = {
        let label = UILabel()
        label.text = "Symbol"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let borderSymbol: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let stfSymbol: SearchTextField = {
        let stf = SearchTextField()
        stf.font = UIFont.boldSystemFont(ofSize: 14)
        stf.textColor = Constants.UIConfig.themeColor
        stf.textAlignment = .left
        stf.autocorrectionType = .no
        stf.clearButtonMode = .whileEditing
        stf.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        stf.autocapitalizationType = .allCharacters
        stf.theme.font = UIFont.systemFont(ofSize: 16)
        stf.theme.fontColor = UIColor(white: 1.0, alpha: 0.5)
        stf.theme.bgColor = UIColor.black
        stf.theme.borderColor = UIColor(white: 1.0, alpha: 0.1)
        stf.theme.separatorColor = UIColor(white: 1.0, alpha: 0.25)
        stf.theme.cellHeight = 40
        stf.highlightAttributes = [NSAttributedString.Key.foregroundColor: Constants.UIConfig.themeColor,
                                   NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)]
        stf.translatesAutoresizingMaskIntoConstraints = false
        return stf
    }()
    
    private let borderPositionName: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.text = "CMP"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMPValue: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.text = "-"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblMargin: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Margin"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfMargin: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.attributedPlaceholder = NSAttributedString(string: "(Optional)", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.3), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderMargin: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblPositionType: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Type"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let borderPositionType: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let dropdownPositionType: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblBaseExpiry: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Expiry"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownBaseExpiry: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderExpiry: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblStrike: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Strike"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dropdownStrike: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.clipsToBounds = true
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderStrike: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblCallPutOption: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Option"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dropdownCallPutOption: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.clipsToBounds = true
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderCallPutOption: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblPrice: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Price"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfPrice: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let btnFetchCMP: FetchCMPButton = {
        let button = FetchCMPButton()
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.7), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.setImage(UIImage(named: "icon_import")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        button.layer.cornerRadius = 3
        button.layer.masksToBounds = true
        button.clipsToBounds = true
        button.setTitle("Get CMP", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderPrice: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblQuantity: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Quantity"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQuantityValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = Constants.UIConfig.themeColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tfQuantity: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let btnAddQty: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.setImage(UIImage(named: "icon_increment")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDelete(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnDeleteQty: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.setImage(UIImage(named: "icon_decrement")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDelete(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderQuantity: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let radioButtonBuy: DLRadioButton = {
        let radioButton = DLRadioButton()
        radioButton.isMultipleSelectionEnabled = false
        radioButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 14)
        radioButton.setTitle("Buy", for: [])
        radioButton.setTitleColor(UIColor(white: 1.0, alpha: 0.6), for: .normal)
        radioButton.setTitleColor(Constants.UIConfig.themeColor, for: UIControl.State.selected)
        radioButton.iconSize = 20
        radioButton.indicatorSize = 10
        radioButton.marginWidth = 10
        radioButton.iconColor = UIColor(white: 1.0, alpha: 0.6)
        radioButton.indicatorColor = Constants.UIConfig.themeColor
        radioButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        radioButton.addTarget(self, action: #selector(radioButtonSelected(_:)), for: UIControl.Event.touchUpInside)
        radioButton.tag = 1
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        return radioButton
    }()
    
    let radioButtonSell: DLRadioButton = {
        let radioButton = DLRadioButton()
        radioButton.isMultipleSelectionEnabled = false
        radioButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 14)
        radioButton.setTitle("Sell", for: [])
        radioButton.setTitleColor(UIColor(white: 1.0, alpha: 0.6), for: .normal)
        radioButton.setTitleColor(Constants.UIConfig.themeColor, for: UIControl.State.selected)
        radioButton.iconSize = 20
        radioButton.indicatorSize = 10
        radioButton.marginWidth = 10
        radioButton.iconColor = UIColor(white: 1.0, alpha: 0.6)
        radioButton.indicatorColor = Constants.UIConfig.themeColor
        radioButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        radioButton.addTarget(self, action: #selector(radioButtonSelected(_:)), for: UIControl.Event.touchUpInside)
        radioButton.tag = 2
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        return radioButton
    }()
    
    private let lblBookType: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Book Type"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dropdownBookType: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.5), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        /*button.layer.cornerRadius = 4
         button.layer.borderColor = UIColor(white: 1.0, alpha: 0.12).cgColor
         button.layer.borderWidth = 0.5
         button.layer.masksToBounds = true
         button.backgroundColor = UIColor(white: 1.0, alpha: 0.08)*/
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderBookType: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // keyboard event handler
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        if type == "edit" {
            // background gradient
            createGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
            
            // navigation bar
            addOSONavigationBar()
        }
        
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !viewAppeared {
            viewAppeared = true
            
            // get position name
            if type == "add" {
                self.getPositionName()
            } else {
                self.getPositionDetailsForEdit()
            }
        }
        
    }
    
    // MARK:- Setup View
    
    private func setupViews() {
        
        // btnPositions
        /*view.addSubview(btnPositions)
        btnPositions.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        btnPositions.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        btnPositions.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnPositionsWidthAnchorConstraint = NSLayoutConstraint(item: btnPositions, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        btnPositionsWidthAnchorConstraint.isActive = true*/
        
        // btnAdd
        view.addSubview(btnAdd)
        btnAdd.addTarget(self, action: #selector(actionAdd(_:)), for: .touchUpInside)
        btnAdd.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        if #available(iOS 11.0, *) {
            btnAdd.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        } else {
            btnAdd.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        }
        btnAdd.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnAddRightAnchorConstraint = NSLayoutConstraint(item: btnAdd, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: -16)
        btnAddRightAnchorConstraint.isActive = true
        
        // btnDone
        /*view.addSubview(btnDone)
        btnDone.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        btnDone.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        btnDone.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        btnDone.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnDoneLeftAnchorConstraint = NSLayoutConstraint(item: btnDone, attribute: .left, relatedBy: .equal, toItem: btnAdd, attribute: .right, multiplier: 1, constant: 0)
        btnDoneLeftAnchorConstraint.isActive = true*/
        
        
        // MARK: scrollview container
        view.addSubview(scrollViewContainer)
        scrollViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollViewContainer.topAnchor.constraint(equalTo: (type == "add") ? view.topAnchor : osoNavBar.bottomAnchor, constant: 0).isActive = true
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnAdd, attribute: .top, multiplier: 1.0, constant: -10)
        scrollViewContainerBottomAnchorConstraint.isActive = true
        
        // MARK: form view
        scrollViewContainer.addSubview(formView)
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .top, relatedBy: .equal, toItem: scrollViewContainer, attribute: .top, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .left, relatedBy: .equal, toItem: scrollViewContainer, attribute: .left, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .right, relatedBy: .equal, toItem: scrollViewContainer, attribute: .right, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: scrollViewContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
        formHeightConstraint = NSLayoutConstraint(item: formView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 284)
        formHeightConstraint.isActive = true
        
        scrollViewContainer.contentSize = formView.size
        
        // MARK: Position Name
        // lblPositionName
        formView.addSubview(lblPositionName)
        lblPositionName.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblPositionName.topAnchor.constraint(equalTo: formView.topAnchor, constant: fieldVerticalGap + 4).isActive = true
        lblPositionName.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPositionName.widthAnchor.constraint(equalToConstant: (lblPositionName.text?.sizeOfString(usingFont: lblPositionName.font).width)! + 4).isActive = true
        
        // tfPositionName
        formView.addSubview(tfPositionName)
        addToolBar(textField: tfPositionName)
        tfPositionName.leftAnchor.constraint(equalTo: lblPositionName.rightAnchor, constant: 8).isActive = true
        tfPositionName.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfPositionName.topAnchor.constraint(equalTo: lblPositionName.topAnchor, constant: 0).isActive = true
        tfPositionName.heightAnchor.constraint(equalTo: lblPositionName.heightAnchor, multiplier: 1.0).isActive = true
        
        // borderPositionName
        formView.addSubview(borderPositionName)
        borderPositionName.leftAnchor.constraint(equalTo: tfPositionName.leftAnchor, constant: 0).isActive = true
        borderPositionName.rightAnchor.constraint(equalTo: tfPositionName.rightAnchor, constant: 0).isActive = true
        borderPositionName.bottomAnchor.constraint(equalTo: tfPositionName.bottomAnchor, constant: 0).isActive = true
        borderPositionName.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // MARK: Symbol
        formView.addSubview(lblSymbol)
        lblSymbol.topAnchor.constraint(equalTo: lblPositionName.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblSymbol.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSymbol.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblSymbol.widthAnchor.constraint(equalToConstant: (lblSymbol.text?.sizeOfString(usingFont: lblSymbol.font).width)! + 4).isActive = true
        
        // stfSymbol
        formView.addSubview(stfSymbol)
        
        if type == "add" {
            addToolBar(textField: stfSymbol)
        } else {
            stfSymbol.isEnabled = false
            stfSymbol.textAlignment = .right
            stfSymbol.textColor = UIColor.white
        }
        
        stfSymbol.topAnchor.constraint(equalTo: lblSymbol.topAnchor, constant: 0).isActive = true
        stfSymbol.heightAnchor.constraint(equalTo: lblSymbol.heightAnchor, multiplier: 1.0).isActive = true
        stfSymbol.leftAnchor.constraint(equalTo: lblSymbol.rightAnchor, constant: 8).isActive = true
        stfSymbol.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        stfSymbol.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            //print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            if self.positions.count > 0 && self.currentSymbol != item.title {
                
                let dialog = ZAlertView(title: "Choose Action",
                                        message: "Changing the security will delete all existing positions. Do you want to continue?",
                                        isOkButtonLeft: true,
                                        okButtonText: "Yes",
                                        cancelButtonText: "No",
                                        okButtonHandler: { (alertView) in
                                            alertView.dismissAlertView()
                                            self.positions.removeAll()
                                            self.updatePositionBadge()
                                            self.stfSymbol.text = item.title
                                            self.currentSymbol = item.title
                                            
                                            self.tfPrice.clear()
                                            
                                            // save to Instrument settings
                                            InstrumentSettings.sharedInstance.symbol = item.title
                                            
                                            //self.getCMP(forSymbol: item.title)
                                            self.getStrikesExpiriesAndLot(symbolName: item.title)
                }) { (alertView) in
                    alertView.dismissAlertView()
                    self.stfSymbol.text = self.currentSymbol
                }
                dialog.show()
                
            } else {
                if self.currentSymbol != item.title {
                    self.tfPrice.clear()
                }
                
                self.stfSymbol.text = item.title
                self.currentSymbol = item.title
                
                // save to Instrument settings
                InstrumentSettings.sharedInstance.symbol = item.title
                
                //self.getCMP(forSymbol: item.title)
                self.getStrikesExpiriesAndLot(symbolName: item.title)
            }
            
            self.stfSymbol.resignFirstResponder()
        }
        
        // bottomBorder
        if type == "add" {
            formView.addSubview(borderSymbol)
            borderSymbol.topAnchor.constraint(equalTo: stfSymbol.bottomAnchor, constant: 0).isActive = true
            borderSymbol.heightAnchor.constraint(equalToConstant: 1).isActive = true
            borderSymbol.leftAnchor.constraint(equalTo: stfSymbol.leftAnchor, constant: -4).isActive = true
            borderSymbol.rightAnchor.constraint(equalTo: stfSymbol.rightAnchor, constant: 0).isActive = true
        }
        
        
        // MARK: CMP
        // lblCMP
        formView.addSubview(lblCMP)
        lblCMP.topAnchor.constraint(equalTo: lblSymbol.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblCMP.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCMP.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblCMP.widthAnchor.constraint(equalToConstant: (lblCMP.text?.sizeOfString(usingFont: lblCMP.font).width)! + 4).isActive = true
        
        // lblCMPValue
        formView.addSubview(lblCMPValue)
        lblCMPValue.topAnchor.constraint(equalTo: lblCMP.topAnchor, constant: 0).isActive = true
        lblCMPValue.heightAnchor.constraint(equalTo: lblCMP.heightAnchor, multiplier: 1.0).isActive = true
        lblCMPValue.leftAnchor.constraint(equalTo: lblCMP.rightAnchor, constant: 8).isActive = true
        lblCMPValue.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        // MARK: lblMargin
        /*formView.addSubview(lblMargin)
        lblMargin.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblMargin.topAnchor.constraint(equalTo: lblCMP.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblMargin.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblMargin.widthAnchor.constraint(equalToConstant: (lblMargin.text?.sizeOfString(usingFont: lblMargin.font).width)! + 4).isActive = true
        
        formView.addSubview(tfMargin)
        addToolBar(textField: tfMargin)
        tfMargin.leftAnchor.constraint(equalTo: lblMargin.rightAnchor, constant: 8).isActive = true
        tfMargin.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfMargin.topAnchor.constraint(equalTo: lblMargin.topAnchor, constant: 0).isActive = true
        tfMargin.heightAnchor.constraint(equalTo: lblMargin.heightAnchor, multiplier: 1).isActive = true
        
        formView.addSubview(borderMargin)
        borderMargin.leftAnchor.constraint(equalTo: tfMargin.leftAnchor, constant: 0).isActive = true
        borderMargin.rightAnchor.constraint(equalTo: tfMargin.rightAnchor, constant: 0).isActive = true
        borderMargin.bottomAnchor.constraint(equalTo: tfMargin.bottomAnchor, constant: 0).isActive = true
        borderMargin.heightAnchor.constraint(equalToConstant: 1).isActive = true*/
        
        // MARK: lblPositionType
        formView.addSubview(lblPositionType)
        lblPositionType.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblPositionType.topAnchor.constraint(equalTo: lblCMP.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblPositionType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPositionType.widthAnchor.constraint(equalToConstant: (lblPositionType.text?.sizeOfString(usingFont: lblPositionType.font).width)! + 8).isActive = true
        
        // dropdownPositionType
        formView.addSubview(dropdownPositionType)
        dropdownPositionType.setTitle("Fut", for: .normal)
        dropdownPositionType.addTarget(self, action: #selector(showPositionType(_:)), for: .touchUpInside)
        dropdownPositionType.leftAnchor.constraint(equalTo: lblPositionType.rightAnchor, constant: 8).isActive = true
        dropdownPositionType.centerYAnchor.constraint(equalTo: lblPositionType.centerYAnchor, constant: 0).isActive = true
        dropdownPositionType.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownPositionType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // borderPositionType
        formView.addSubview(borderPositionType)
        borderPositionType.leftAnchor.constraint(equalTo: dropdownPositionType.leftAnchor, constant: 0).isActive = true
        borderPositionType.rightAnchor.constraint(equalTo: dropdownPositionType.rightAnchor, constant: 0).isActive = true
        borderPositionType.bottomAnchor.constraint(equalTo: dropdownPositionType.bottomAnchor, constant: 0).isActive = true
        borderPositionType.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // MARK: lblBaseExpiry
        formView.addSubview(lblBaseExpiry)
        lblBaseExpiry.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblBaseExpiry.topAnchor.constraint(equalTo: lblPositionType.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblBaseExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblBaseExpiry.widthAnchor.constraint(equalToConstant: (lblBaseExpiry.text?.sizeOfString(usingFont: lblBaseExpiry.font).width)! + 8).isActive = true
        
        // dropdownBaseExpiry
        formView.addSubview(dropdownBaseExpiry)
        dropdownBaseExpiry.addTarget(self, action: #selector(showBaseExpiry(_:)), for: .touchUpInside)
        dropdownBaseExpiry.leftAnchor.constraint(equalTo: lblBaseExpiry.rightAnchor, constant: 8).isActive = true
        dropdownBaseExpiry.centerYAnchor.constraint(equalTo: lblBaseExpiry.centerYAnchor, constant: 0).isActive = true
        dropdownBaseExpiry.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownBaseExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // borderExpiry
        formView.addSubview(borderExpiry)
        borderExpiry.leftAnchor.constraint(equalTo: dropdownBaseExpiry.leftAnchor, constant: 0).isActive = true
        borderExpiry.rightAnchor.constraint(equalTo: dropdownBaseExpiry.rightAnchor, constant: 0).isActive = true
        borderExpiry.bottomAnchor.constraint(equalTo: dropdownBaseExpiry.bottomAnchor, constant: 0).isActive = true
        borderExpiry.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // MARK: Strike
        formView.addSubview(lblStrike)
        lblStrike.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        //lblStrike.topAnchor.constraint(equalTo: lblBaseExpiry.bottomAnchor, constant: fieldVerticalGap).isActive = true
        //lblStrike.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblStrike.widthAnchor.constraint(equalToConstant: (lblStrike.text?.sizeOfString(usingFont: lblStrike.font).width)! + 8).isActive = true
        lblStrikeTopAnchorConstraint = NSLayoutConstraint(item: lblStrike, attribute: .top, relatedBy: .equal, toItem: lblBaseExpiry, attribute: .bottom, multiplier: 1.0, constant: 0)
        lblStrikeTopAnchorConstraint.isActive = true
        lblStrikeHeightAnchorConstraint = NSLayoutConstraint(item: lblStrike, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        lblStrikeHeightAnchorConstraint.isActive = true
        
        
        // dropdownStrike
        formView.addSubview(dropdownStrike)
        dropdownStrike.addTarget(self, action: #selector(showStrikeForInstrument(_:)), for: .touchUpInside)
        dropdownStrike.leftAnchor.constraint(equalTo: lblStrike.rightAnchor, constant: 8).isActive = true
        dropdownStrike.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        dropdownStrike.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownStrike.heightAnchor.constraint(equalTo: lblStrike.heightAnchor, constant: 0).isActive = true
        
        // borderStrike
        formView.addSubview(borderStrike)
        borderStrike.leftAnchor.constraint(equalTo: dropdownStrike.leftAnchor, constant: 0).isActive = true
        borderStrike.rightAnchor.constraint(equalTo: dropdownStrike.rightAnchor, constant: 0).isActive = true
        borderStrike.bottomAnchor.constraint(equalTo: dropdownStrike.bottomAnchor, constant: 0).isActive = true
        borderStrikeHeightAnchorConstraint = NSLayoutConstraint(item: borderStrike, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        borderStrikeHeightAnchorConstraint.isActive = true
        
        // MARK: CE/PE Option
        formView.addSubview(lblCallPutOption)
        lblCallPutOption.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        //lblCallPutOption.topAnchor.constraint(equalTo: lblStrike.bottomAnchor, constant: fieldVerticalGap).isActive = true
        //lblCallPutOption.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCallPutOption.widthAnchor.constraint(equalToConstant: (lblCallPutOption.text?.sizeOfString(usingFont: lblCallPutOption.font).width)! + 8).isActive = true
        lblCallPutOptionTopAnchorConstraint = NSLayoutConstraint(item: lblCallPutOption, attribute: .top, relatedBy: .equal, toItem: lblStrike, attribute: .bottom, multiplier: 1.0, constant: 0)
        lblCallPutOptionTopAnchorConstraint.isActive = true
        lblCallPutOptionHeightAnchorConstraint = NSLayoutConstraint(item: lblCallPutOption, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        lblCallPutOptionHeightAnchorConstraint.isActive = true
        
        // dropdownCallPutOption
        formView.addSubview(dropdownCallPutOption)
        dropdownCallPutOption.setTitle("CE", for: .normal)
        dropdownCallPutOption.addTarget(self, action: #selector(showCallPutOption(_:)), for: .touchUpInside)
        dropdownCallPutOption.leftAnchor.constraint(equalTo: lblCallPutOption.rightAnchor, constant: 8).isActive = true
        dropdownCallPutOption.topAnchor.constraint(equalTo: lblCallPutOption.topAnchor, constant: 0).isActive = true
        dropdownCallPutOption.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        dropdownCallPutOption.heightAnchor.constraint(equalTo: lblCallPutOption.heightAnchor, constant: 0).isActive = true
        
        // borderCallPutOption
        formView.addSubview(borderCallPutOption)
        borderCallPutOption.leftAnchor.constraint(equalTo: dropdownCallPutOption.leftAnchor, constant: 0).isActive = true
        borderCallPutOption.rightAnchor.constraint(equalTo: dropdownCallPutOption.rightAnchor, constant: 0).isActive = true
        borderCallPutOption.bottomAnchor.constraint(equalTo: dropdownCallPutOption.bottomAnchor, constant: 0).isActive = true
        borderCallPutOptionHeightAnchorConstraint = NSLayoutConstraint(item: borderCallPutOption, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        borderCallPutOptionHeightAnchorConstraint.isActive = true
        
        // MARK: Price
        formView.addSubview(lblPrice)
        lblPrice.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblPrice.topAnchor.constraint(equalTo: lblCallPutOption.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblPrice.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPrice.widthAnchor.constraint(equalToConstant: (lblPrice.text?.sizeOfString(usingFont: lblPrice.font).width)! + 4).isActive = true
        
        formView.addSubview(btnFetchCMP)
        btnFetchCMP.addTarget(self, action: #selector(fetchCMP(_:)), for: .touchUpInside)
        btnFetchCMP.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        btnFetchCMP.bottomAnchor.constraint(equalTo: lblPrice.bottomAnchor, constant: 0).isActive = true
        btnFetchCMP.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        btnFetchCMP.widthAnchor.constraint(equalToConstant: (btnFetchCMP.titleLabel?.text?.sizeOfString(usingFont: (btnFetchCMP.titleLabel?.font)!).width)! + 44).isActive = true
        
        formView.addSubview(tfPrice)
        addToolBar(textField: tfPrice)
        tfPrice.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 8).isActive = true
        tfPrice.rightAnchor.constraint(equalTo: btnFetchCMP.leftAnchor, constant: -8).isActive = true
        tfPrice.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        tfPrice.heightAnchor.constraint(equalTo: lblPrice.heightAnchor, multiplier: 1).isActive = true
        
        
        
        formView.addSubview(borderPrice)
        borderPrice.leftAnchor.constraint(equalTo: tfPrice.leftAnchor, constant: 0).isActive = true
        borderPrice.rightAnchor.constraint(equalTo: tfPrice.rightAnchor, constant: 0).isActive = true
        borderPrice.bottomAnchor.constraint(equalTo: tfPrice.bottomAnchor, constant: 0).isActive = true
        borderPrice.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // MARK: Quantity
        formView.addSubview(lblQuantity)
        lblQuantity.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblQuantity.topAnchor.constraint(equalTo: lblPrice.bottomAnchor, constant: fieldVerticalGap).isActive = true
        lblQuantity.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblQuantity.widthAnchor.constraint(equalToConstant: (lblQuantity.text?.sizeOfString(usingFont: lblQuantity.font).width)! + 4).isActive = true
        
        formView.addSubview(btnAddQty)
        btnAddQty.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        btnAddQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btnAddQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btnAddQty.centerYAnchor.constraint(equalTo: lblQuantity.centerYAnchor, constant: 0).isActive = true
        
        formView.addSubview(lblQuantityValue)
        lblQuantityValue.text = "0"
        lblQuantityValue.rightAnchor.constraint(equalTo: btnAddQty.leftAnchor, constant: -10).isActive = true
        lblQuantityValue.bottomAnchor.constraint(equalTo: lblQuantity.bottomAnchor, constant: 0).isActive = true
        lblQuantityValue.heightAnchor.constraint(equalTo: lblQuantity.heightAnchor, constant: 0).isActive = true
        lblQuantityValueWidthAnchorConstraint = NSLayoutConstraint(item: lblQuantityValue, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (lblQuantityValue.text?.sizeOfString(usingFont: lblQuantityValue.font).width)! + 2)
        lblQuantityValueWidthAnchorConstraint.isActive = true
        
        formView.addSubview(btnDeleteQty)
        btnDeleteQty.rightAnchor.constraint(equalTo: lblQuantityValue.leftAnchor, constant: -10).isActive = true
        btnDeleteQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btnDeleteQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btnDeleteQty.centerYAnchor.constraint(equalTo: lblQuantity.centerYAnchor, constant: 0).isActive = true
        
        /*formView.addSubview(tfQuantity)
        addToolBar(textField: tfQuantity)
        tfQuantity.leftAnchor.constraint(equalTo: lblQuantity.rightAnchor, constant: 8).isActive = true
        tfQuantity.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfQuantity.topAnchor.constraint(equalTo: lblQuantity.topAnchor, constant: 0).isActive = true
        tfQuantity.heightAnchor.constraint(equalTo: lblQuantity.heightAnchor, multiplier: 1).isActive = true
        
        formView.addSubview(borderQuantity)
        borderQuantity.leftAnchor.constraint(equalTo: tfQuantity.leftAnchor, constant: 0).isActive = true
        borderQuantity.rightAnchor.constraint(equalTo: tfQuantity.rightAnchor, constant: 0).isActive = true
        borderQuantity.bottomAnchor.constraint(equalTo: tfQuantity.bottomAnchor, constant: 0).isActive = true
        borderQuantity.heightAnchor.constraint(equalToConstant: 1).isActive = true*/
        
        // MARK: Radio Button (Buy/Sell)
        formView.addSubview(radioButtonBuy)
        radioButtonBuy.isSelected = true
        selectedRadioButton = radioButtonBuy.tag
        radioButtonBuy.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        radioButtonBuy.topAnchor.constraint(equalTo: lblQuantity.bottomAnchor, constant: 8).isActive = true
        radioButtonBuy.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        radioButtonBuy.rightAnchor.constraint(equalTo: formView.centerXAnchor, constant: -5).isActive = true
        
        formView.addSubview(radioButtonSell)
        radioButtonSell.isSelected = false
        radioButtons.append(radioButtonSell)
        radioButtonBuy.otherButtons = radioButtons
        radioButtonSell.leftAnchor.constraint(equalTo: formView.centerXAnchor, constant: 5).isActive = true
        radioButtonSell.topAnchor.constraint(equalTo: radioButtonBuy.topAnchor, constant: 0).isActive = true
        radioButtonSell.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        radioButtonSell.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        
        view.layoutSubviews()
        view.layoutIfNeeded()
    }
    
    private func getPositionName() {
        SVProgressHUD.show()
        
        DatabaseManager.sharedInstance.getNameForPosition { (error, positionName, legsAllowed) in
            if let err = error {
                SVProgressHUD.dismiss()
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    //self.navigationController?.popViewController(animated: true)
                })
            } else {
                
                if let name = positionName {
                    self.tfPositionName.text = name
                }
                
                // get Symbols
                self.getSymbols()
            }
        }
    }
    
    private func getPositionDetailsForEdit() {
        if let pNo = self.positionNo {
            SVProgressHUD.show()
            
            DatabaseManager.sharedInstance.getPositionDetailsForEdit(positionNo: pNo) { (error, summaries, legsAllowedValue) in
                SVProgressHUD.dismiss()
                if let err = error {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                } else {
                    if let summaryArray = summaries, let legsAllowed = legsAllowedValue {
                        
                        // AddEditPositions
                        self.positions.removeAll()
                        for summary in summaryArray {
                            
                            let position = AddEditPositions()
                            
                            if let instrument = summary.instrument, let strike = summary.strike, let optType = summary.optType, let expiry = summary.expiry, let price = summary.price, let quantity = summary.qty, let expired = summary.expired {
                                position.instrument = instrument
                                position.strike = strike
                                position.optType = optType
                                position.expiry = expiry
                                position.price = price
                                position.quantity = quantity
                                position.expired = expired
                                position.fullInstrument = (instrument == "Opt") ? "\(instrument) \(expiry) \(strike) \(optType)" : "\(instrument) \(expiry)"
                                
                                self.positions.append(position)
                            }
                        }
                        
                        // show Done and Position Buttons
                        if self.positions.count > 0 {
                            self.addDoneAndPositionButtons()
                        }
                        
                        
                        if let summary = summaryArray.first {
                            
                            // position name
                            if let positionName = summary.positionName {
                                self.tfPositionName.text = positionName
                            }
                            
                            // cmp
                            if let cmpValue = self.cmp {
                                self.lblCMPValue.text = cmpValue
                            }
                            
                            // margin
                            if let margin = summary.margin {
                                self.tfMargin.text = margin
                            }
                            
                            
                            
                            // symbol
                            if let symbol = summary.symbol {
                                self.stfSymbol.text = symbol
                                self.getStrikesExpiriesAndLot(symbolName: symbol)
                            }
                            
                        }
                    } else {
                        self.showZAlertView(withTitle: "Error", andMessage: "Position details not found.", cancelTitle: "OK", isError: true, completion: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "Position details not found.", cancelTitle: "OK", isError: true, completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }
        
    }
    
    @objc private func actionAdd(_ sender: UIButton) {
        
        resignAllResponders()
        
        // FIXME:- CHECK MAXIMUM ALLOWED LEGS
        if positions.count == 4 {
            showZAlertView(withTitle: "Error", andMessage: "Maximum allowed legs is 4.", cancelTitle: "OK", isError: true, completion: {})
            return
        }
        
        guard let symbol = stfSymbol.text?.trimmed else { return }
        guard let positionName = tfPositionName.text?.trimmed else { return }
        guard let price = tfPrice.text?.trimmed else { return }
        //guard let quantity = tfQuantity.text?.trimmed else { return }
        guard let quantity = lblQuantityValue.text?.trimmed else { return }
        guard let instrument = dropdownPositionType.titleLabel?.text else { return }
        
        if positionName.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter position name.", cancelTitle: "OK", isError: true, completion: {
                self.tfPositionName.becomeFirstResponder()
            })
        } else if symbol.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter symbol name.", cancelTitle: "OK", isError: true, completion: {
                self.stfSymbol.becomeFirstResponder()
            })
        } else if price.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter price.", cancelTitle: "OK", isError: true, completion: {
                self.tfPrice.becomeFirstResponder()
            })
        } else if price == "0" {
            showZAlertView(withTitle: "Error", andMessage: "Price cannot be zero.", cancelTitle: "OK", isError: true, completion: {
                self.tfPrice.becomeFirstResponder()
            })
        } else if quantity.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter quantity.", cancelTitle: "OK", isError: true, completion: {
                self.tfQuantity.becomeFirstResponder()
            })
        } else if quantity == "0" {
            showZAlertView(withTitle: "Error", andMessage: "Quantity cannot be zero.", cancelTitle: "OK", isError: true, completion: {
                self.tfQuantity.becomeFirstResponder()
            })
        } else {
            let position = AddEditPositions()
            
            guard let expiry = dropdownBaseExpiry.titleLabel?.text else { return }
            
            position.expiry = expiry
            position.expired = "0"
            position.instrument = instrument
            
            if instrument == "Fut" {
                position.strike = "-1"
                position.optType = "XX"
                position.fullInstrument = "\(instrument) \(expiry)"
            } else if instrument == "Opt" {
                guard let strike = dropdownStrike.titleLabel?.text else { return }
                guard let optType = dropdownCallPutOption.titleLabel?.text else { return }
                
                position.strike = strike
                position.optType = optType
                position.fullInstrument = "Opt \(expiry) \(strike) \(optType)"
            }
            
            let itemIndex = positionExists(pos: position)
            
            if itemIndex != -1 {
                let dialog = ZAlertView(title: "Position Exists",
                                        message: "Position already exists, do you want to update it?",
                                        isOkButtonLeft: true,
                                        okButtonText: "Yes",
                                        cancelButtonText: "No",
                                        okButtonHandler: { (alertView) in
                                            alertView.dismissAlertView()
                                            if let price = self.tfPrice.text {
                                                self.positions[itemIndex].price = price
                                            }
                                            
                                            if let quantity = self.lblQuantityValue.text {
                                                if self.selectedRadioButton == 1 {
                                                    self.positions[itemIndex].quantity = quantity
                                                } else {
                                                    self.positions[itemIndex].quantity = "-\(quantity)"
                                                }
                                            }
                                            
                                            self.tfPrice.clear()
                }) { (alertView) in
                    alertView.dismissAlertView()
                }
                dialog.show()
            } else {
                if let price = self.tfPrice.text {
                    position.price = price
                }
                
                if let quantity = self.lblQuantityValue.text {
                    position.quantity = selectedRadioButton == 1 ? quantity : "-\(quantity)"
                }
                
                positions.append(position)
                
                
                
                self.tfPrice.clear()
            }
            
            if positions.count > 0 {
                
                if !buttonsShown {
                    buttonsShown = true
                    
                    addDoneAndPositionButtons()
                } else {
                    showDoneAndPositionButtons()
                    //self.perform(#selector(self.updatePositionBadge), with: nil, afterDelay: 0.5)
                }
            }
        }
    }
    
    private func confirmSave(completion: @escaping ((_ save: Bool) -> Void)) {
        let dialog = ZAlertView(title: "Confirm",
                                message: "Do you want to save the call?",
                                isOkButtonLeft: false,
                                okButtonText: "Yes",
                                cancelButtonText: "No",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
                                    completion(true)
        }) { (alertView) in
            alertView.dismissAlertView()
            completion(false)
        }
        dialog.show()
    }
    
    private func addComment(completion: @escaping ((_ comment: String) -> Void)) {
        /*if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbol) {
            let instrument = "\(symbol.uppercased())_\(expiry)_1"
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "Could not get CMP.", cancelTitle: "OK", isError: true, completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }*/
    }
    
    @objc private func actionDone(_ sender: UIButton) {
        
        resignAllResponders()
        
        guard let positionName = self.tfPositionName.text?.trimmed else { return }
        guard let symbol = self.stfSymbol.text?.trimmed else { return }
        //guard let margin = self.tfMargin.text?.trimmed else { return }
        
        if positionName.isEmpty {
            self.showZAlertView(withTitle: "Error", andMessage: "Please give your position a name.", cancelTitle: "OK", isError: true, completion: {
                self.tfPositionName.becomeFirstResponder()
            })
        } else {
            var papi = [String]()
            
            for position in self.positions {
                if let fullInstrument = position.fullInstrument, let price = position.price, let quantity = position.quantity {
                    papi.append("\(fullInstrument),\(price),\(quantity)")
                }
            }
            
            if papi.count > 0 {
                
                self.delegate?.CustomStrategyStepCompleted(positionName: positionName, symbol: symbol, positions: self.positions)
                
                /*self.confirmSave(completion: { (save) in
                    if save {
                        
                        SVProgressHUD.show()
                        
                        OSOWebService.sharedInstance.uploadCall(positionName: positionName, strategyName: positionName, symbol: symbol, margin: "0", legs: papi, tradeStatus: "1", bookType: "\(bookType)", spread: "0", target: "0", stopLoss: "0", comment: "Test", completion: { (error, positionNo, margin, notificationMessage) in
                            
                            if let err = error {
                                SVProgressHUD.dismiss()
                                print("Error: \(err.localizedDescription)")
                            } else {
                                if let posNo = positionNo, let marginValue = margin {
                                    print("Position No.: \(posNo)")
                                    print("Margin: \(marginValue)")
                                    self.addPosition(positionNo: posNo, symbol: symbol, positionName: positionName, margin: marginValue, legs: papi, tradeStatus: "1")
                                } else {
                                    SVProgressHUD.dismiss()
                                    self.showZAlertView(withTitle: "Error", andMessage: "Data not received.", cancelTitle: "OK", isError: true, completion: {})
                                }
                            }
                        })
                    }
                })*/
            }
        }
    }
    
    /*private func editPosition(positionNo: String, symbol: String, positionName: String, margin: String, legs: [String], tradeStatus: String) {
        
        var tempInstrument = [String]()
        let alerts: String = "0"
        let dateCreated: Date = Date()
        var analyzerSummaries = [AnalyzerSummary]()
        
        for i in 0..<legs.count {
            let summary = AnalyzerSummary()
            summary.positionNo = positionNo
            summary.symbol = symbol
            summary.positionName = positionName
            summary.margin = margin
            summary.dateCreated = dateCreated
            summary.alertOne = alerts
            summary.alertTwo = alerts
            summary.tradeStatus = tradeStatus
            summary.Papi = legs[i]
            
            tempInstrument = legs[i].components(separatedBy: ",")
            
            summary.price = tempInstrument[1]
            summary.qty = tempInstrument[2]
            
            tempInstrument = tempInstrument[0].components(separatedBy: " ")
            
            summary.instrument = tempInstrument[0]
            summary.expiry = tempInstrument[1]
            summary.srno = "\(i+1)"
            summary.syncNo = "0"
            
            if tempInstrument[0] == "Fut" {
                summary.strike = "-1"
                summary.optType = "XX"
            } else {
                summary.strike = tempInstrument[2]
                summary.optType = tempInstrument[3]
            }
            
            summary.expired = "0"
            
            analyzerSummaries.append(summary)
        }
        
        DatabaseManager.sharedInstance.getPositionDetails(positionNo: positionNo) { (error, summaries) in
            SVProgressHUD.dismiss()
            if let err = error {
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
            } else {
                if let summary = summaries?.first {
                    if let alertOne = summary.alertOne, let alertTwo = summary.alertTwo {
                        DatabaseManager.sharedInstance.deletePositions(positions: [positionNo], completion: { (deleted) in
                            if deleted {
                                DatabaseManager.sharedInstance.addPosition(analyzerSummaries: analyzerSummaries, completion: { (added) in
                                    if added {
                                        DatabaseManager.sharedInstance.addAlert(positionNo: positionNo, alertOne: alertOne, alertTwo: alertTwo, completion: { (updated) in
                                            if updated {
                                                self.showZAlertView(withTitle: "Status", andMessage: "Position edited successfully.", cancelTitle: "OK", isError: false, completion: {
                                                    //self.navigationController?.popViewController(animated: true)
                                                    self.delegate?.positionForCustomStrategyEdited(tradeStatus: tradeStatus)
                                                })
                                            } else {
                                                self.showZAlertView(withTitle: "Error", andMessage: "Could not update alert for position.", cancelTitle: "OK", isError: true, completion: {})
                                            }
                                        })
                                    } else {
                                        self.showZAlertView(withTitle: "Error", andMessage: "Could not add position.", cancelTitle: "OK", isError: true, completion: {})
                                    }
                                })
                            } else {
                                self.showZAlertView(withTitle: "Error", andMessage: "Could not delete position.", cancelTitle: "OK", isError: true, completion: {})
                            }
                        })
                    } else {
                        self.showZAlertView(withTitle: "Error", andMessage: "No alerts found for position", cancelTitle: "OK", isError: true, completion: {})
                    }
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: "No positions found to edit.", cancelTitle: "OK", isError: true, completion: {})
                }
            }
        }
        
    }*/
    
    private func addPosition(bookType: String, positionNo: String, symbol: String, positionName: String, margin: String, legs: [String], tradeStatus: String) {
        
        var tempInstrument = [String]()
        let alerts: String = "0"
        let dateCreated: Date = Date()
        var analyzerSummaries = [AnalyzerSummary]()
        
        for i in 0..<legs.count {
            let summary = AnalyzerSummary()
            summary.bookType = bookType
            summary.positionNo = positionNo
            summary.symbol = symbol
            summary.positionName = positionName
            summary.margin = margin
            summary.dateCreated = dateCreated
            summary.alertOne = alerts
            summary.alertTwo = alerts
            summary.tradeStatus = tradeStatus
            summary.Papi = legs[i]
            
            tempInstrument = legs[i].components(separatedBy: ",")
            
            summary.price = tempInstrument[1]
            summary.qty = tempInstrument[2]
            
            tempInstrument = tempInstrument[0].components(separatedBy: " ")
            
            summary.instrument = tempInstrument[0]
            summary.expiry = tempInstrument[1]
            summary.srno = "\(i+1)"
            summary.syncNo = "0"
            
            if tempInstrument[0] == "Fut" {
                summary.strike = "-1"
                summary.optType = "XX"
            } else {
                summary.strike = tempInstrument[2]
                summary.optType = tempInstrument[3]
            }
            
            summary.expired = "0"
            
            analyzerSummaries.append(summary)
        }
        
        DatabaseManager.sharedInstance.addPosition(analyzerSummaries: analyzerSummaries) { (success) in
            SVProgressHUD.dismiss()
            if success {
                self.showZAlertView(withTitle: "Call Added", andMessage: "Call has been added successfully.", cancelTitle: "OK", isError: false, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.showZAlertView(withTitle: "Error", andMessage: "Could not save the summary", cancelTitle: "OK", isError: true, completion: {
                    //
                })
            }
        }
    }
    
    private func showSetAlert(positionNo: String) {
        let dialog = ZAlertView()
        dialog.title = "Status"
        dialog.message = "Position added successfully."
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        dialog.addButton("SET ALERT", hexColor: Constants.UIConfig.themeColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
            alertView.dismissAlertView()
            
            self.showSetAlert(title: "Set Alert", completion: { (alertAbove, alertBelow) in
                if let alertOne = alertAbove, let alertTwo = alertBelow {
                    self.saveAlert(positionNo: positionNo, alertAbove: alertOne, alertBelow: alertTwo)
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: "Alert not found.", cancelTitle: "OK", isError: true, completion: {})
                }
            })
        }
        dialog.addButton("SKIP", hexColor: UIColor.gray.hexString, hexTitleColor: "#ffffff") { (alertView) in
            alertView.dismissAlertView()
            self.positionAdded()
        }
        
        dialog.show()
    }
    
    private func saveAlert(positionNo: String, alertAbove: String, alertBelow: String) {
        SVProgressHUD.show()
        
        OSOWebService.sharedInstance.uploadAlert(positionNo: positionNo, alertOne: alertAbove, alertTwo: alertBelow) { (success, message, error) in
            SVProgressHUD.dismiss()
            if success {
                DatabaseManager.sharedInstance.addAlert(positionNo: positionNo, alertOne: alertAbove, alertTwo: alertBelow, completion: { (added) in
                    if added {
                        self.showZAlertView(withTitle: "Alert Set", andMessage: "Alert has been saved successfully.", cancelTitle: "OK", isError: false, completion: {
                            self.positionAdded()
                        })
                    } else {
                        self.showZAlertView(withTitle: "Error", andMessage: "Could not save alert on device.", cancelTitle: "OK", isError: true, completion: {
                            self.positionAdded()
                        })
                    }
                })
            } else {
                if let msg = message {
                    self.showZAlertView(withTitle: "Error", andMessage: msg, cancelTitle: "OK", isError: true, completion: {})
                } else {
                    if let err = error {
                        self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                    }
                }
            }
        }
    }
    
    private func addDoneAndPositionButtons() {
        // btnDone
        view.addSubview(btnDone)
        btnDone.addTarget(self, action: #selector(actionDone(_:)), for: .touchUpInside)
        btnDone.widthAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnDone.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnDone.bottomAnchor.constraint(equalTo: btnAdd.bottomAnchor, constant: 0).isActive = true
        btnDoneRightAnchorConstraint = NSLayoutConstraint(item: btnDone, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1.0, constant: 118)
        btnDoneRightAnchorConstraint.isActive = true
        
        // btnPositions
        view.addSubview(btnPositions)
        btnPositions.addTarget(self, action: #selector(showPositionsList(_:)), for: .touchUpInside)
        btnPositions.widthAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnPositions.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnPositions.bottomAnchor.constraint(equalTo: btnAdd.bottomAnchor, constant: 0).isActive = true
        btnPositions.rightAnchor.constraint(equalTo: btnDone.leftAnchor, constant: -10).isActive = true
        
        perform(#selector(showDoneAndPositionButtons), with: nil, afterDelay: 0.3)
    }
    
    @objc private func showDoneAndPositionButtons() {
        
        btnAddRightAnchorConstraint.isActive = false
        btnAddRightAnchorConstraint = NSLayoutConstraint(item: btnAdd, attribute: .right, relatedBy: .equal, toItem: btnPositions, attribute: .left, multiplier: 1.0, constant: -10)
        btnAddRightAnchorConstraint.isActive = true
        
        btnDoneRightAnchorConstraint.constant = -10
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
            self.perform(#selector(self.updatePositionBadge), with: nil, afterDelay: 0.5)
        }
    }
    
    private func hideDoneAndPositionButtons() {
        
        btnAddRightAnchorConstraint.isActive = false
        btnAddRightAnchorConstraint = NSLayoutConstraint(item: btnAdd, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1.0, constant: -10)
        btnAddRightAnchorConstraint.isActive = true
        
        btnDoneRightAnchorConstraint.constant = 118
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc private func showPositionsList(_ sender: UIButton) {
        let vc = CustomPositionsList()
        vc.positions = self.positions
        vc.delegate = self
        present(vc, animated: true) {
            //
        }
    }
    
    
    
    @objc private func updatePositionBadge() {
        
        // get snapshot
        /*if let snapshot = view.getSnapshot(of: scrollViewContainer.frame, afterScreenUpdates: true) {
            let tempView = UIView(frame: scrollViewContainer.frame)
            view.addSubview(tempView)
            
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = scrollViewContainer.frame
            gradientLayer.colors = [Constants.UIConfig.topColor.cgColor, Constants.UIConfig.bottomColor.cgColor]
            tempView.layer.addSublayer(gradientLayer)
            
            let imageView = UIImageView(frame: tempView.frame)
            imageView.image = snapshot
            tempView.addSubview(imageView)
            
            tempView.genieInTransition(duration: 2.0, destinationRect: btnPositions.frame, destinationEdge: .bottom) {
                tempView.removeFromSuperview()
            }
            
        }*/
        
        self.btnPositions.badge = self.positions.count == 0 ? nil : "\(self.positions.count)"
        
        if self.positions.count == 0 {
            hideDoneAndPositionButtons()
        }
    }
    
    private func positionExists(pos: AddEditPositions) -> Int {
        var itemIndex: Int = -1
        
        for i in 0..<positions.count {
            if let fullInstrument = pos.fullInstrument, let fi = positions[i].fullInstrument {
                if fullInstrument == fi {
                    itemIndex = i
                    return itemIndex
                }
            }
        }
        
        return itemIndex
    }
    
    private func getBookType(sender: UIButton, completion: @escaping((_ book: OSOBook?) -> Void)) {
        
        let books = DatabaseManager.sharedInstance.getBooks()
        
        if books.count > 0 {
            
            var bookTitles = [String]()
            var initialSelection: Int = 0
            var i: Int = 0
            
            for book in books {
                if let title = book.title {
                    bookTitles.append(title)
                    
                    if let currentBookTitle = selectedBook?.title {
                        if title == currentBookTitle {
                            initialSelection = i
                        }
                    }
                }
                
                i += 1
            }
            
            if bookTitles.count > 0 {
                
                let picker = ActionSheetStringPicker(title: "Select Book Type", rows: bookTitles, initialSelection: initialSelection, doneBlock: { (picker, value, index) in
                    sender.setTitleColor(Constants.UIConfig.themeColor, for: [])
                    //completion(index as? String)
                    completion(books[value])
                }, cancel: { (picker) in
                    //
                }, origin: sender)
                
                picker?.show()
                
            }
        }
    }
    
    private func getTradeStatus(completion: @escaping((_ tradeStatus: String) -> Void)) {
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Set Trade Status"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        let itemA = ActionSheetItem(title: "Traded", value: "1", image: nil)
        items.append(itemA)
        
        let itemB = ActionSheetItem(title: "Tracking", value: "2", image: nil)
        items.append(itemB)
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            completion(value)
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: btnDone)
    }
    
    @objc private func showPositionType(_ sender: UIButton) {
        
        resignAllResponders()
        
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Select Type"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        let itemFut = ActionSheetItem(title: "Fut", value: "Fut", image: nil)
        items.append(itemFut)
        
        let itemOpt = ActionSheetItem(title: "Opt", value: "Opt", image: nil)
        items.append(itemOpt)
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            print("selected = \(value)")
            sender.setTitle(value, for: .normal)
            
            if let symbol = self.stfSymbol.text?.trimmed {
                if !symbol.isEmpty {
                    SVProgressHUD.show()
                    self.getStrikesExpiriesAndLot(symbolName: symbol)
                }
            }
            
            self.tfPrice.clear()
            
            
            self.updateFieldConstraints(positionType: value)
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
    }
    
    @objc private func showCallPutOption(_ sender: UIButton) {
        
        resignAllResponders()
        
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Select Option"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        let itemCE = ActionSheetItem(title: "CE", value: "CE", image: nil)
        items.append(itemCE)
        
        let itemPE = ActionSheetItem(title: "PE", value: "PE", image: nil)
        items.append(itemPE)
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            guard let previousValue = sender.titleLabel?.text else { return }
            if previousValue != value {
                print("selected = \(value)")
                sender.setTitle(value, for: .normal)
                self.tfPrice.clear()
            }
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
    }
    
    @objc private func showBaseExpiry(_ sender: UIButton) {
        
        resignAllResponders()
        
        if let expiryDates = self.expiries {
            let picker = ActionSheetStringPicker(title: "Select Expiry", rows: expiryDates, initialSelection: selectedExpiry, doneBlock: { (picker, value, index) in
                if self.selectedExpiry != value {
                    self.selectedExpiry = value
                    sender.setTitle(index as? String, for: .normal)
                    self.tfPrice.clear()
                    
                    self.selectedLot = value
                    
                    if let lotsArray = self.lots {
                        self.lot = lotsArray[self.selectedLot]
                        
                        DispatchQueue.main.async {
                            self.lblQuantityValue.text = "\(self.lot * 2)"
                        }
                        
                        self.lblQuantityValueWidthAnchorConstraint.constant = "\(self.lot * 2)".sizeOfString(usingFont: self.lblQuantityValue.font).width + 2
                        self.view.layoutIfNeeded()
                        
                        self.btnAddQty.isEnabled = self.lot != 1
                        self.btnDeleteQty.isEnabled = self.lot == -1 ? false : true
                    }
                    
                    if let instrument = self.dropdownPositionType.titleLabel?.text {
                        if instrument.uppercased() == "OPT" && !self.currentSymbol.isEmpty {
                            self.getOptMaster(symbolName: self.currentSymbol)
                        }
                    }
                }
            }, cancel: { (picker) in
                //
            }, origin: sender)
            
            picker?.show()
        }
    }
    
    @objc private func showStrikeForInstrument(_ sender: UIButton) {
        
        resignAllResponders()
        
        if let selectedOption = sender.titleLabel?.text, let strikesArray = self.strikes {
            
            if let initialSelection = strikesArray.index(of: selectedOption) {
                
                let picker = ActionSheetStringPicker(title: "Select Strike", rows: strikesArray, initialSelection: initialSelection, doneBlock: { (picker, value, index) in
                    guard let previousStrike = sender.titleLabel?.text else { return }
                    guard let selectedStrike = index as? String else { return }
                    if previousStrike != selectedStrike {
                        sender.setTitle(index as? String, for: .normal)
                        self.tfPrice.clear()
                    }
                    
                }, cancel: { (picker) in
                    //
                }, origin: sender)
                
                picker?.show()
            }
        }
    }
    
    @objc private func radioButtonSelected(_ radioButton: DLRadioButton) {
        
        resignAllResponders()
        
        guard let tag = radioButton.selected()?.tag else { return }
        selectedRadioButton = tag
    }
    
    
    private func initWithDefaultSymbol() {
        
        var lastSymbol = "NIFTY"
        
        if let sym = InstrumentSettings.sharedInstance.symbol {
            lastSymbol = sym
        }
        
        currentSymbol = lastSymbol
        
        self.stfSymbol.text = lastSymbol
        //self.getCMP(forSymbol: lastSymbol)
        
        resignAllResponders()
        SVProgressHUD.show()
        getStrikesExpiriesAndLot(symbolName: lastSymbol)
    }
    
    private func getCMP(forSymbol symbol: String, expiry: String) {
        OSOWebService.sharedInstance.getPrice(forInstrument: "\(symbol)_\(expiry)_1") { (error, price) in
            SVProgressHUD.dismiss()
            if let err = error {
                
                print("Error: \(err.localizedDescription)")
                SVProgressHUD.dismiss(completion: {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription == "0" ? "Invalid Session. Please login again." : err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                        //self.delegate?.stepCErrorOccured()
                    })
                })
                
            } else {
                if let cmpValue = price {
                    print("CMP = \(cmpValue)")
                    self.lblCMPValue.text = cmpValue.toString()
                } else {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    /*private func getCMP(forSymbol symbol: String) {
        
        resignAllResponders()
        SVProgressHUD.show()
        
        OSOWebService.sharedInstance.getCMP(forSymbol: symbol) { (success, error, CMP) in
            if let err = error {
                
                print("Error: \(err.localizedDescription)")
                SVProgressHUD.dismiss(completion: {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                        //self.navigationController?.popViewController(animated: true)
                    })
                })
                
            } else {
                if let cmpValue = CMP {
                    
                    print("CMP = \(cmpValue)")
                    
                    self.lblCMPValue.text = cmpValue.toString()
                    
                    // get strikes
                    //self.getStrikes(symbolName: symbol)
                    self.getStrikesExpiriesAndLot(symbolName: symbol)
                } else {
                    SVProgressHUD.dismiss()
                    self.showZAlertView(withTitle: "Error", andMessage: "Could not get CMP.", cancelTitle: "OK", isError: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }*/
    
    private func getSymbols() {
        
        DatabaseManager.sharedInstance.getOptionSymbols { (success, error, symbols) in
            
            SVProgressHUD.dismiss()
            
            if let error = error {
                print("Error: \(error.localizedDescription)")
            } else {
                if let sym = symbols {
                    self.stfSymbol.filterStrings(sym)
                    
                    self.initWithDefaultSymbol()
                }
            }
        }
        
    }
    
    private func getStrikesExpiriesAndLot(symbolName: String) {
        if let instrument = dropdownPositionType.titleLabel?.text {
            if instrument.isEmpty {
                SVProgressHUD.dismiss()
                self.showZAlertView(withTitle: "Error", andMessage: "Instrument not found.", cancelTitle: "OK", isError: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                //SVProgressHUD.dismiss()
                DatabaseManager.sharedInstance.getExpiriesStrikesAndLot(symbol: symbolName, instrument: instrument) { (expiries, strikes, lots) in
                    if let expiriesArray = expiries {
                        self.expiries = expiriesArray
                        self.selectedExpiry = 0
                        self.dropdownBaseExpiry.setTitle("\(expiriesArray[self.selectedExpiry])", for: .normal)
                    }
                    
                    if let strikesArray = strikes {
                        self.strikes = strikesArray
                        
                        if let cmpValue = self.lblCMPValue.text?.double() {
                            let closestIndex = self.closestIndexInStrikes(strikes: strikesArray, CMP: cmpValue)
                            self.dropdownStrike.setTitle(strikesArray[closestIndex], for: .normal)
                        }
                    }
                    
                    if let lotsArray = lots {
                        self.lots = lotsArray
                        self.selectedLot = 0
                        
                        self.lot = lotsArray[self.selectedLot]
                        
                        DispatchQueue.main.async {
                            self.lblQuantityValue.text = "\(self.lot * 2)"
                        }
                        
                        self.lblQuantityValueWidthAnchorConstraint.constant = "\(self.lot * 2)".sizeOfString(usingFont: self.lblQuantityValue.font).width + 2
                        self.view.layoutIfNeeded()
                        
                        self.btnAddQty.isEnabled = self.lot != 1
                        self.btnDeleteQty.isEnabled = self.lot == -1 ? false : true
                    }
                    
                    /*if let lotValue = lot?.int {
                        self.lot = lotValue
                        //self.tfQuantity.text = "\(lotValue)"
                        DispatchQueue.main.async {
                            self.lblQuantityValue.text = "\(lotValue * 2)"
                        }
                        
                        self.lblQuantityValueWidthAnchorConstraint.constant = "\(lotValue * 2)".sizeOfString(usingFont: self.lblQuantityValue.font).width + 2
                        self.view.layoutIfNeeded()
                        
                        self.btnAddQty.isEnabled = self.lot != 1
                        self.btnDeleteQty.isEnabled = self.lot == -1 ? false : true
                    }*/
                    
                    if let firstExpiry = self.expiries?.first {
                        if instrument.uppercased() == "FUT" {
                            self.getCMP(forSymbol: symbolName, expiry: firstExpiry)
                        } else {
                            SVProgressHUD.dismiss()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.showZAlertView(withTitle: "Error", andMessage: "Expiry not found", cancelTitle: "OK", isError: true, completion: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
        } else {
            SVProgressHUD.dismiss()
            self.showZAlertView(withTitle: "Error", andMessage: "Instrument not found.", cancelTitle: "OK", isError: true, completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    private func getStrikes(symbolName: String) {
        DatabaseManager.sharedInstance.getAllStrikesForSymbol(symbolName: symbolName) { (success, error, strikesArray) in
            
            if let err = error {
                SVProgressHUD.dismiss()
                print("Error: \(err.localizedDescription)")
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                if let strikes = strikesArray {
                    print("strikes[\(strikes.count)] = \(strikes)")
                    self.strikes = strikes
                    
                    self.getOptMaster(symbolName: symbolName)
                } else {
                    SVProgressHUD.dismiss()
                    self.showZAlertView(withTitle: "Error", andMessage: "No strikes found.", cancelTitle: "OK", isError: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }
    
    private func getOptMaster(symbolName: String) {
        DatabaseManager.sharedInstance.getOptMasterForSymbol(symbolName: symbolName) { (success, error, optMasterArray) in
            SVProgressHUD.dismiss()
            if let err = error {
                print("Error: \(err.localizedDescription)")
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                if let optMasters = optMasterArray {
                    self.optMasters = optMasters
                    
                    var tmpExpiries = [String]()
                    for optMaster in optMasters {
                        if let expiryDate = optMaster.expiry {
                            tmpExpiries.append(expiryDate.string(withFormat: "dd-MMM-yy"))
                        }
                    }
                    
                    self.expiries = tmpExpiries
                    self.strikes = optMasters[self.selectedExpiry].strikes
                    //self.selectedExpiry = 0
                    self.dropdownBaseExpiry.setTitle("\(tmpExpiries[self.selectedExpiry])", for: .normal)
                    
                    if let strikesArray = self.strikes, let cmpValue = self.lblCMPValue.text?.double() {
                        let closestIndex = self.closestIndexInStrikes(strikes: strikesArray, CMP: cmpValue)
                        self.dropdownStrike.setTitle(strikesArray[closestIndex], for: .normal)
                    }
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: "No master details found.", cancelTitle: "OK", isError: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }
    
    
    @objc private func fetchCMP(_ sender: UIButton) {
        print("fetchCMP")
        resignAllResponders()
        
        if let symbol = stfSymbol.text?.trimmed {
            if symbol.isEmpty {
                showZAlertView(withTitle: "Error", andMessage: "Please enter a symbol", cancelTitle: "OK", isError: true, completion: {
                    self.stfSymbol.becomeFirstResponder()
                })
            } else {
                if let instrumentType = dropdownPositionType.titleLabel?.text {
                    
                    if instrumentType.isEmpty {
                        showZAlertView(withTitle: "Error", andMessage: "Please select an Instrument.", cancelTitle: "OK", isError: true, completion: {})
                    } else {
                        if let expiry = dropdownBaseExpiry.titleLabel?.text {
                            if expiry.isEmpty {
                                showZAlertView(withTitle: "Error", andMessage: "Please select Expiry.", cancelTitle: "OK", isError: true, completion: {})
                            } else {
                                if instrumentType.uppercased() == "FUT" {
                                    
                                    //----------------------------- NEW CODE: START --------------------------//
                                    var instruments = [String]()
                                    instruments.append("\(symbol.uppercased())_\(expiry)_1")
                                    
                                    SVProgressHUD.show()
                                    OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, price, oi, volume) in
                                        SVProgressHUD.dismiss()
                                        if let err = error {
                                            self.tfPrice.text = "-"
                                            self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                                        } else {
                                            if let priceArray = price {
                                                if priceArray.indices.contains(0) {
                                                    self.tfPrice.text = priceArray[0].roundToDecimal(2).toString()
                                                } else {
                                                    self.tfPrice.text = "-"
                                                }
                                            } else {
                                                self.tfPrice.text = "-"
                                                self.showZAlertView(withTitle: "Error", andMessage: "Could not get price. Please try again.", cancelTitle: "OK", isError: true, completion: {})
                                            }
                                        }
                                    }
                                    //------------------------------ NEW CODE: END ---------------------------//
                                    
                                    //----------------------------- OLD CODE: START --------------------------//
                                    /*SVProgressHUD.show()
                                    OSOWebService.sharedInstance.getCMPForInstrument(instrumentType: instrumentType, symbol: symbol, expiry: expiry, strike: "", optType: "") { (cmpValue) in
                                        SVProgressHUD.dismiss()
                                        self.tfPrice.text = cmpValue
                                    }*/
                                    //----------------------------- OLD CODE: END --------------------------//
                                } else if instrumentType.uppercased() == "OPT" {
                                    guard let strike = dropdownStrike.titleLabel?.text else {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select strike.", cancelTitle: "OK", isError: true, completion: {})
                                        return
                                    }
                                    
                                    guard let strikeDblVal = strike.double() else  {
                                        showZAlertView(withTitle: "Error", andMessage: "Strike value is not valid.", cancelTitle: "OK", isError: true, completion: {})
                                        return
                                    }
                                    
                                    guard let optType = dropdownCallPutOption.titleLabel?.text else {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select Option Type.", cancelTitle: "OK", isError: true, completion: {})
                                        return
                                    }
                                    
                                    if strike.isEmpty {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select strike.", cancelTitle: "OK", isError: true, completion: {})
                                    } else if optType.isEmpty {
                                        showZAlertView(withTitle: "Error", andMessage: "Please select Option Type.", cancelTitle: "OK", isError: true, completion: {})
                                    } else {
                                        
                                        //----------------------------- NEW CODE: START --------------------------//
                                        var instruments = [String]()
                                        instruments.append("\(symbol.uppercased())_\(expiry)_1_\(optType.uppercased())_\(strikeDblVal.toString())")
                                        
                                        SVProgressHUD.show()
                                        OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, price, oi, volume) in
                                            SVProgressHUD.dismiss()
                                            if let err = error {
                                                self.tfPrice.text = "-"
                                                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                                            } else {
                                                if let priceArray = price {
                                                    if priceArray.indices.contains(0) {
                                                        self.tfPrice.text = priceArray[0].roundToDecimal(2).toString()
                                                    } else {
                                                        self.tfPrice.text = "-"
                                                    }
                                                } else {
                                                    self.tfPrice.text = "-"
                                                    self.showZAlertView(withTitle: "Error", andMessage: "Could not get price. Please try again.", cancelTitle: "OK", isError: true, completion: {})
                                                }
                                            }
                                        }
                                        //------------------------------ NEW CODE: END ---------------------------//
                                        
                                        //----------------------------- OLD CODE: START --------------------------//
                                        /*SVProgressHUD.show()
                                        OSOWebService.sharedInstance.getCMPForInstrument(instrumentType: instrumentType, symbol: symbol, expiry: expiry, strike: strike, optType: optType) { (cmpValue) in
                                            SVProgressHUD.dismiss()
                                            self.tfPrice.text = cmpValue
                                        }*/
                                        //----------------------------- OLD CODE: END --------------------------//
                                    }
                                }
                            }
                        } else {
                            showZAlertView(withTitle: "Error", andMessage: "Please select Expiry.", cancelTitle: "OK", isError: true, completion: {})
                        }
                    }
                }
            }
        }
    }
    
    func updateFieldConstraints(positionType: String) {
        if positionType == "Fut" {
            lblStrikeTopAnchorConstraint.constant = 0
            lblStrikeHeightAnchorConstraint.constant = 0
            lblCallPutOptionTopAnchorConstraint.constant = 0
            lblCallPutOptionHeightAnchorConstraint.constant = 0
            borderStrikeHeightAnchorConstraint.constant = 0
            borderCallPutOptionHeightAnchorConstraint.constant = 0
            formHeightConstraint.constant = 318
        } else if positionType == "Opt" {
            lblStrikeTopAnchorConstraint.constant = fieldVerticalGap
            lblStrikeHeightAnchorConstraint.constant = fieldHeight
            lblCallPutOptionTopAnchorConstraint.constant = fieldVerticalGap
            lblCallPutOptionHeightAnchorConstraint.constant = fieldHeight
            borderStrikeHeightAnchorConstraint.constant = 1
            borderCallPutOptionHeightAnchorConstraint.constant = 1
            formHeightConstraint.constant = 386
        }
        
        /*UIView.animate(withDuration: 0.3, animations: {
            self.formView.layoutIfNeeded()
        }) { (completed) in
            self.scrollViewContainer.contentSize = self.formView.size
        }*/
        
        self.formView.layoutIfNeeded()
        self.scrollViewContainer.contentSize = self.formView.size
    }
    
    func closestIndexInStrikes(strikes: [String], CMP: Double) -> Int {
        
        var smallestDifference = (CMP - strikes[0].double()!).abs
        var closest = 0
        
        for i in 0..<strikes.count {
            let currentDifference = (CMP - strikes[i].double()!).abs
            
            if currentDifference < smallestDifference {
                smallestDifference = currentDifference
                closest = i
            }
        }
        
        return closest
    }
    
    @objc private func actionAddOrDelete(_ sender: UIButton) {
        
        guard let currentQty = lblQuantityValue.text?.int else { return }
        
        var tempQty = currentQty
        
        if lot == -1 { return }
        
        if sender == btnAddQty {
            if tempQty == 0 || tempQty/lot == -2 || tempQty.abs/lot == 1 {
                tempQty += lot
            } else if (tempQty.abs/lot) % 2 == 0 {
                tempQty += (lot * 2)
            }
        } else if sender == btnDeleteQty {
            if tempQty == 0 || tempQty/lot == 2 || tempQty.abs/lot == 1 {
                tempQty -= lot
            } else if (tempQty.abs/lot) % 2 == 0 {
                tempQty -= (lot * 2)
            }
        }
        
        
        lblQuantityValue.text = "\(tempQty)"
        lblQuantityValueWidthAnchorConstraint.constant = "\(tempQty)".sizeOfString(usingFont: lblQuantityValue.font).width + 2
        self.view.layoutIfNeeded()
        
        if tempQty == lot {
            btnDeleteQty.isEnabled = false
        } else {
            btnDeleteQty.isEnabled = true
        }
    }
    
    private func resignAllResponders() {
        view.endEditing(true)
    }
    
    private func positionAdded() {
        self.delegate?.positionForCustomStrategyAdded()
    }
    
    public func positionsNotSaved() {
        let dialog = ZAlertView(title: "Save Position",
                                message: "You have added \(self.positions.count) position\(self.positions.count > 1 ? "s" : ""). Would you like to save \(self.positions.count > 1 ? "them" : "it")?",
                                isOkButtonLeft: true,
                                okButtonText: "Yes",
                                cancelButtonText: "No",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
                                    self.actionDone(self.btnDone)
        }) { (alertView) in
            alertView.dismissAlertView()
            self.delegate?.positionForCustomStrategyCancelled()
        }
        dialog.show()
    }
    
    func createGradientLayer(topColor:UIColor, bottomColor:UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        view.layer.addSublayer(gradientLayer)
    }
    
    func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: Constants.OSONavigationBarConstants.barHeight), title: "Edit Position", subTitle: nil, leftbuttonImage: UIImage(named: "icon_left"), rightButtonImage: nil)
        view.addSubview(osoNavBar)
        osoNavBar.delegate = self
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:- Set Alert
    
    private func showSetAlert(title: String, completion: @escaping((_ alertAbove: String?, _ alertBelow: String?) -> Void)) {
        let dialog = ZAlertView()
        dialog.alertTitle = title
        dialog.message = "Enter Alert Above and Alert Below"
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        dialog.addTextField("fieldAlertAbove", placeHolder: "Alert Above", keyboardType: .numbersAndPunctuation)
        if let inputTf = dialog.getTextFieldWithIdentifier("fieldAlertAbove") {
            tfAlertAbove = inputTf
            inputTf.autocorrectionType = .no
            inputTf.returnKeyType = .done
            inputTf.delegate = self
        }
        
        dialog.addTextField("fieldAlertBelow", placeHolder: "Alert Below", keyboardType: .numbersAndPunctuation)
        if let inputTf = dialog.getTextFieldWithIdentifier("fieldAlertBelow") {
            tfAlertBelow = inputTf
            inputTf.autocorrectionType = .no
            inputTf.returnKeyType = .done
            inputTf.delegate = self
        }
        
        dialog.addButton("Update", color: Constants.UIConfig.themeColor, titleColor: UIColor.white) { (alertView) in
            var alertAboveText = "0"
            var alertBelowText = "0"
            if let inputTf = dialog.getTextFieldWithIdentifier("fieldAlertAbove") {
                if let text = inputTf.text {
                    alertAboveText = text.isEmpty ? "0" : text
                }
            }
            
            if let inputTf = dialog.getTextFieldWithIdentifier("fieldAlertBelow") {
                if let text = inputTf.text {
                    alertBelowText = text.isEmpty ? "0" : text
                }
            }
            
            completion(alertAboveText, alertBelowText)
            alertView.dismissAlertView()
        }
        dialog.addButton("Cancel", color: UIColor(white: 0.0, alpha: 0.3), titleColor: UIColor.white) { (alertView) in
            alertView.dismissAlertView()
            completion(nil, nil)
        }
        
        dialog.show()
    }
    
    // MARK:- CustomPositionsListDelegate
    
    func closeList(positions: [AddEditPositions]) {
        self.positions = positions
        dismiss(animated: true) {
            self.updatePositionBadge()
        }
    }
    
    // MARK:- Keyboard Events
    
    @objc private func keyBoardDidShow(notification: NSNotification) {
        //handle appearing of keyboard here
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            scrollViewContainerBottomAnchorConstraint.isActive = false
            
            scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -(keyboardSize.height))
            
            UIView.animate(withDuration: 0.3) {
                self.scrollViewContainerBottomAnchorConstraint.isActive = true
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc private func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
        
        scrollViewContainerBottomAnchorConstraint.isActive = false
        
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnAdd, attribute: .top, multiplier: 1.0, constant: -16)
        
        UIView.animate(withDuration: 0.3) {
            self.scrollViewContainerBottomAnchorConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK:- UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let parentView = textField.superview {
            parentView.endEditing(true)
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.tfMargin || textField == self.tfQuantity {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return string == filtered
        } else if textField == self.tfPrice {
            if textField.text != "" || string != "" {
                let res = (textField.text ?? "") + string
                return Double(res) != nil
            }
            
            return true
        } else if textField == tfAlertAbove || textField == tfAlertBelow {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789-").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            
            if string == filtered {
                if string == "-" {
                    let countMinus = (textField.text?.components(separatedBy: "-").count)! - 1
                    
                    if countMinus == 0 {
                        if textField.text?.count == 0 {
                            return true
                        } else {
                            return false
                        }
                    } else {
                        return false
                    }
                } else {
                    return true
                }
            } else {
                return false
            }
        } else if textField == self.tfPositionName {
            let inverseSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_()&[]{}#+ ").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return string == filtered
        } else {
            return true
        }
    }

}

// MARK:- OSONavigationBarDelegate

extension CustomStrategy: OSONavigationBarDelegate {
    
    func leftButtonTapped(sender: OSONavigationBar) {
        
        
        if type == "edit" {
            if self.positions.count == 0 {
                
                let dialog = ZAlertView(title: "No Positions",
                                        message: "You don't have any trade added to the position. Do you want to delete the position or add a new Trade?",
                    isOkButtonLeft: false,
                    okButtonText: "Add Trade",
                    cancelButtonText: "Delete Position",
                    okButtonHandler: { (alertView) in
                        alertView.dismissAlertView()
                }) { (alertView) in
                    alertView.dismissAlertView()
                    self.delegate?.positionForCustomStrategyDelete()
                }
                dialog.show()
                
                
            } else {
                
                let dialog = ZAlertView(title: "Save Position",
                                        message: "You have added \(self.positions.count) trade\(self.positions.count > 1 ? "s" : ""). Would you like to save \(self.positions.count > 1 ? "them" : "it")?",
                    isOkButtonLeft: true,
                    okButtonText: "Yes",
                    cancelButtonText: "No",
                    okButtonHandler: { (alertView) in
                        alertView.dismissAlertView()
                        self.actionDone(self.btnDone)
                }) { (alertView) in
                    alertView.dismissAlertView()
                    self.navigationController?.popViewController(animated: true)
                }
                dialog.show()
                
            }
            
        }
    }
    
    func rightButtonTapped(sender: OSONavigationBar) {
        print("rightButtonTapped()")
    }
    
}

