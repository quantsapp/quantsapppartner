//
//  FindSpecificStepB.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 09/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import ZAlertView

protocol FindSpecificStepBDelegate: class {
    func stepBCompleted(symbol: String, direction: Int, risk: Int, strategy: MyLibrary)
}

class FindSpecificStepB: UIViewController {
    
    weak var delegate: FindSpecificStepBDelegate?
    
    public var symbol: String?
    public var direction: Int?
    public var risk: Int?
    
    private var strategies = [MyLibrary]()
    
    let strategiesTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupViews()
        
        // get strategies
        if let directionIndex = direction, let riskIndex = risk {
            getStrategies(directionIndex: directionIndex + 1, riskIndex: riskIndex)
        }
    }
    
    private func setupViews() {
        
        view.backgroundColor = UIColor.clear
        
        
        // detailsTableView
        view.addSubview(strategiesTableView)
        strategiesTableView.dataSource = self
        strategiesTableView.delegate = self
        strategiesTableView.backgroundColor = UIColor.clear
        strategiesTableView.separatorStyle = .none
        strategiesTableView.clipsToBounds = true
        strategiesTableView.alwaysBounceVertical = false
        //strategiesTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        strategiesTableView.register(SpecificStrategyTableViewCell.self, forCellReuseIdentifier: SpecificStrategyTableViewCell.cellIdentifier)
        strategiesTableView.tableFooterView = UIView()
        strategiesTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        strategiesTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        strategiesTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        strategiesTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
    
    private func getStrategies(directionIndex: Int, riskIndex: Int) {
        
        DatabaseManager.sharedInstance.getStrategiesFor(directionIndex: directionIndex, riskIndex: riskIndex) { (success, error, strategyList) in
            if let err = error {
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {
                    //
                })
            } else {
                if let data = strategyList {
                    self.strategies = data
                    self.strategiesTableView.reloadData()
                }
            }
        }
        
    }

}

extension FindSpecificStepB: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strategies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SpecificStrategyTableViewCell.cellIdentifier, for: indexPath) as! SpecificStrategyTableViewCell
        
        let strategy = strategies[indexPath.row]
        
        if let name = strategy.name {
            cell.lblStrategy.text = "\(name)"
            cell.chartImageView.image = UIImage(named: "chart_\(name.lowercased().removingWhitespaces())")
        } else {
            cell.lblStrategy.text = "-"
            cell.chartImageView.image = nil
        }
        
        return cell
    }
}

extension FindSpecificStepB: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 10, height: 40))
        header.backgroundColor = UIColor(white: 0.0, alpha: 0.9)
        
        var directionImageName = ""
        
        if let direction = self.direction {
            switch direction {
            case 0:
                directionImageName = "icon_bullish"
            case 1:
                directionImageName = "icon_bearish"
            case 2:
                directionImageName = "icon_eitherways"
            case 3:
                directionImageName = "icon_oscillate"
            default:
                directionImageName = ""
            }
        }
        
        let imageViewDirection = UIImageView()
        imageViewDirection.image = UIImage(named: directionImageName)?.withRenderingMode(.alwaysOriginal)
        imageViewDirection.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(imageViewDirection)
        
        imageViewDirection.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 10).isActive = true
        imageViewDirection.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewDirection.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewDirection.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
        
        
        var riskImageName = ""
        var riskText = ""
        
        if let risk = self.risk {
            switch risk {
            case 1:
                riskImageName = "icon_limited_risk_strategies"
                riskText = "Limited Risk Strategies"
            case 3:
                riskImageName = "icon_all_strategies"
                riskText = "All Strategies"
            default:
                riskImageName = ""
            }
        }
        
        let imageViewRisk = UIImageView()
        imageViewRisk.image = UIImage(named: riskImageName)?.withRenderingMode(.alwaysTemplate)
        imageViewRisk.tintColor = UIColor(white: 1.0, alpha: 0.6)
        imageViewRisk.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(imageViewRisk)
        
        imageViewRisk.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
        imageViewRisk.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewRisk.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewRisk.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
        
        let lblRisk = UILabel()
        lblRisk.text = riskText
        lblRisk.textAlignment = .right
        lblRisk.font = UIFont.systemFont(ofSize: 12)
        lblRisk.textColor = UIColor(white: 1.0, alpha: 0.6)
        lblRisk.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(lblRisk)
        
        lblRisk.rightAnchor.constraint(equalTo: imageViewRisk.leftAnchor, constant: -4).isActive = true
        lblRisk.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblRisk.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
        lblRisk.widthAnchor.constraint(equalToConstant: (lblRisk.text?.sizeOfString(usingFont: lblRisk.font).width)! + 4).isActive = true
        
        
        let lblSymbol = UILabel()
        
        if let symbolName = self.symbol {
            lblSymbol.text = "\(symbolName)"
        }
        
        lblSymbol.textAlignment = .left
        lblSymbol.textColor = Constants.UIConfig.themeColor
        lblSymbol.font = UIFont.boldSystemFont(ofSize: 14)
        lblSymbol.adjustsFontSizeToFitWidth = true
        lblSymbol.minimumScaleFactor = 0.6
        lblSymbol.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(lblSymbol)
        
        lblSymbol.leftAnchor.constraint(equalTo: imageViewDirection.rightAnchor, constant: 8).isActive = true
        lblSymbol.rightAnchor.constraint(equalTo: lblRisk.leftAnchor, constant: -8).isActive = true
        lblSymbol.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblSymbol.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
        
        
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let strategy = strategies[indexPath.row]
        
        if let symbolName = self.symbol, let directionIndex = self.direction, let riskIndex = self.risk {
            delegate?.stepBCompleted(symbol: symbolName, direction: directionIndex, risk: riskIndex, strategy: strategy)
        }
    }
}
