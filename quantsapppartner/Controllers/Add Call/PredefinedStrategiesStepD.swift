//
//  PredefinedStrategiesStepD.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 17/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import ZAlertView
import SVProgressHUD
import ActionSheetPicker_3_0
import Sheeeeeeeeet
import SwifterSwift

protocol PredefinedStrategiesStepDDelegate: class {
    func stepDCompleted()
}

class PredefinedStrategiesStepD: UIViewController {
    
    weak var delegate: PredefinedStrategiesStepDDelegate?
    
    public var positionName: String?
    public var legs: [String]?
    public var tradeStatus: String?
    public var margin: String?
    
    public var symbol: String?
    public var direction: Int?
    public var risk: Int?
    public var positionNo: String?
    public var strategyName: String?
    
    private var initiationSpread: Double?
    
    private var allBooks = DatabaseManager.sharedInstance.getBooks()
    private var selectedBooks: [OSOBook]?
    
    private var fieldVerticalGap: CGFloat = 4
    private var fieldHeight: CGFloat = 30
    private var defaultFormHeight: CGFloat = 282.0
    private var screenWidth: CGFloat = SwifterSwift.screenWidth
    private var dropdownBookTypeButtonWidth: CGFloat = 0.0
    
    // constraint
    var scrollViewContainerBottomAnchorConstraint: NSLayoutConstraint!
    var formHeightConstraint: NSLayoutConstraint!
    private var dropdownBookTypeHeightConstraint: NSLayoutConstraint!
    
    let btnSave: UIButton = {
        let button = UIButton()
        button.setTitle("SAVE", for: .normal)
        button.backgroundColor = Constants.UIConfig.themeColor//UIColor(white: 1.0, alpha: 0.1)
        button.setTitleColor(Constants.UIConfig.bottomColor, for: .normal) //
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let scrollViewContainer: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    let formView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.9)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageViewDirection: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let imageViewRisk: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.tintColor = UIColor(white: 1.0, alpha: 0.6)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let lblRisk: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor(white: 1.0, alpha: 0.8)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblSymbol: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = Constants.UIConfig.themeColor
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSpread: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Initiation Spread"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTarget: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Target Spread"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStopLoss: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Stop Loss Spread"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tfSpread: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.isEnabled = false
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.attributedPlaceholder = NSAttributedString(string: "Spread", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.3), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let tfTarget: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.attributedPlaceholder = NSAttributedString(string: "Target Spread", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.3), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let tfStopLoss: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = Constants.UIConfig.themeColor
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.attributedPlaceholder = NSAttributedString(string: "Stop Loss Spread", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.3), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let borderSpread: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let borderTarget: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let borderStopLoss: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblBookType: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Book Type"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dropdownBookType: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.3), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.titleLabel?.sizeToFit()
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderBookType: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblComment: UILabel = {
        let label = UILabel()
        label.text = "Comment"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tvComment: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textColor = UIColor.white
        textView.layer.borderColor = Constants.UIConfig.borderColor.cgColor
        textView.layer.cornerRadius = 4
        textView.layer.borderWidth = 0.5
        textView.backgroundColor = UIColor(white: 1.0, alpha: 0.05)
        textView.returnKeyType = .done
        textView.keyboardType = .asciiCapable
        textView.placeholder = "Add comment..."
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // calculate initiation spread
        initiationSpread = calculateSpread()
        
        // keyboard event handler
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // setup views
        setupViews()
    }
    
    private func setupViews() {
        
        // btnSave
        view.addSubview(btnSave)
        btnSave.addTarget(self, action: #selector(actionSave(_:)), for: .touchUpInside)
        btnSave.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        btnSave.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnSave.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        btnSave.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        
        // headerView
        view.addSubview(headerView)
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        headerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        headerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        // imageViewDirection
        var directionImageName = ""
        
        if let direction = self.direction {
            switch direction {
            case 0:
                directionImageName = "icon_bullish"
            case 1:
                directionImageName = "icon_bearish"
            case 2:
                directionImageName = "icon_eitherways"
            case 3:
                directionImageName = "icon_oscillate"
            default:
                directionImageName = ""
            }
        }
        
        imageViewDirection.image = UIImage(named: directionImageName)?.withRenderingMode(.alwaysOriginal)
        headerView.addSubview(imageViewDirection)
        imageViewDirection.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
        imageViewDirection.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewDirection.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewDirection.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 8).isActive = true
        
        // lblSymbol
        headerView.addSubview(lblSymbol)
        if let symbolName = self.symbol {
            lblSymbol.text = "\(symbolName)"
        } else {
            lblSymbol.text = "-"
        }
        lblSymbol.leftAnchor.constraint(equalTo: imageViewDirection.rightAnchor, constant: 8).isActive = true
        lblSymbol.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblSymbol.centerYAnchor.constraint(equalTo: imageViewDirection.centerYAnchor, constant: 0).isActive = true
        lblSymbol.widthAnchor.constraint(equalToConstant: (lblSymbol.text?.sizeOfString(usingFont: lblSymbol.font).width)! + 2).isActive = true
        
        // lblRisk + imageViewRisk
        var riskImageName = ""
        var riskText = ""
        
        if let name = strategyName {
            riskText = name.uppercased()
        }
        
        if let risk = self.risk {
            switch risk {
            case 1:
                riskImageName = "icon_limited_risk_strategies"
                //riskText = "Limited Risk Strategies"
            case 3:
                riskImageName = "icon_all_strategies"
                //riskText = "All Strategies"
            default:
                riskImageName = ""
            }
        }
        
        imageViewRisk.image = UIImage(named: riskImageName)?.withRenderingMode(.alwaysTemplate)
        headerView.addSubview(imageViewRisk)
        imageViewRisk.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -10).isActive = true
        imageViewRisk.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewRisk.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageViewRisk.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 8).isActive = true
        
        lblRisk.text = riskText
        headerView.addSubview(lblRisk)
        lblRisk.rightAnchor.constraint(equalTo: imageViewRisk.leftAnchor, constant: -4).isActive = true
        lblRisk.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblRisk.centerYAnchor.constraint(equalTo: imageViewRisk.centerYAnchor, constant: 0).isActive = true
        lblRisk.leftAnchor.constraint(equalTo: lblSymbol.rightAnchor, constant: 4).isActive = true
        //lblRisk.widthAnchor.constraint(equalToConstant: (lblRisk.text?.sizeOfString(usingFont: lblRisk.font).width)! + 4).isActive = true
        
        // scrollview container
        view.addSubview(scrollViewContainer)
        scrollViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollViewContainer.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 0).isActive = true
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnSave, attribute: .top, multiplier: 1.0, constant: -16)
        scrollViewContainerBottomAnchorConstraint.isActive = true
        
        // form view
        scrollViewContainer.addSubview(formView)
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .top, relatedBy: .equal, toItem: scrollViewContainer, attribute: .top, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .left, relatedBy: .equal, toItem: scrollViewContainer, attribute: .left, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .right, relatedBy: .equal, toItem: scrollViewContainer, attribute: .right, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: scrollViewContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
        formHeightConstraint = NSLayoutConstraint(item: formView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: defaultFormHeight) //80
        formHeightConstraint.isActive = true
        
        scrollViewContainer.contentSize = formView.size
        
        // lblSpread
        formView.addSubview(lblSpread)
        lblSpread.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblSpread.topAnchor.constraint(equalTo: formView.topAnchor, constant: 8).isActive = true
        lblSpread.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblSpread.widthAnchor.constraint(equalToConstant: (lblSpread.text?.sizeOfString(usingFont: lblSpread.font).width)! + 8).isActive = true
        
        // tfSpread
        formView.addSubview(tfSpread)
        addToolBar(textField: tfSpread)
        tfSpread.leftAnchor.constraint(equalTo: lblSpread.rightAnchor, constant: 8).isActive = true
        tfSpread.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfSpread.topAnchor.constraint(equalTo: lblSpread.topAnchor, constant: 0).isActive = true
        tfSpread.heightAnchor.constraint(equalTo: lblSpread.heightAnchor, multiplier: 1.0, constant: 0).isActive = true
        
        // calculate spread
        if let spread = initiationSpread {
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.right
            
            let attributedString = NSMutableAttributedString()
            attributedString.append(NSAttributedString(string: "\(spread.abs.roundToDecimal(2).toString())", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: Constants.UIConfig.themeColor, NSAttributedString.Key.paragraphStyle: style]))
            attributedString.append(NSAttributedString(string: " \(spread == 0 ? "" : spread < 0 ? "Cr" : "")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: spread == 0 ? UIColor(white: 1.0, alpha: 0.3) : spread < 0 ? Constants.Charts.chartGreenColor : Constants.Charts.chartRedColor, NSAttributedString.Key.paragraphStyle: style]))
            
            //tfSpread.text = "\(spread.roundToDecimal(2).toString())"
            tfSpread.attributedText = attributedString
        } else {
            tfSpread.text = "-"
        }
        
        // borderSpread
        formView.addSubview(borderSpread)
        borderSpread.leftAnchor.constraint(equalTo: tfSpread.leftAnchor, constant: 0).isActive = true
        borderSpread.rightAnchor.constraint(equalTo: tfSpread.rightAnchor, constant: 0).isActive = true
        borderSpread.bottomAnchor.constraint(equalTo: tfSpread.bottomAnchor, constant: 0).isActive = true
        borderSpread.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // lblTarget
        formView.addSubview(lblTarget)
        lblTarget.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblTarget.topAnchor.constraint(equalTo: lblSpread.bottomAnchor, constant: 4).isActive = true
        lblTarget.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblTarget.widthAnchor.constraint(equalToConstant: (lblTarget.text?.sizeOfString(usingFont: lblTarget.font).width)! + 8).isActive = true
        
        // tfTarget
        formView.addSubview(tfTarget)
        addToolBar(textField: tfTarget)
        tfTarget.leftAnchor.constraint(equalTo: lblTarget.rightAnchor, constant: 8).isActive = true
        tfTarget.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfTarget.topAnchor.constraint(equalTo: lblTarget.topAnchor, constant: 0).isActive = true
        tfTarget.heightAnchor.constraint(equalTo: lblTarget.heightAnchor, multiplier: 1.0, constant: 0).isActive = true
        
        // borderTarget
        formView.addSubview(borderTarget)
        borderTarget.leftAnchor.constraint(equalTo: tfTarget.leftAnchor, constant: 0).isActive = true
        borderTarget.rightAnchor.constraint(equalTo: tfTarget.rightAnchor, constant: 0).isActive = true
        borderTarget.bottomAnchor.constraint(equalTo: tfTarget.bottomAnchor, constant: 0).isActive = true
        borderTarget.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // lblStopLoss
        formView.addSubview(lblStopLoss)
        lblStopLoss.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblStopLoss.topAnchor.constraint(equalTo: lblTarget.bottomAnchor, constant: 4).isActive = true
        lblStopLoss.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblStopLoss.widthAnchor.constraint(equalToConstant: (lblStopLoss.text?.sizeOfString(usingFont: lblStopLoss.font).width)! + 8).isActive = true
        
        // tfStopLoss
        formView.addSubview(tfStopLoss)
        addToolBar(textField: tfStopLoss)
        tfStopLoss.leftAnchor.constraint(equalTo: lblStopLoss.rightAnchor, constant: 8).isActive = true
        tfStopLoss.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        tfStopLoss.topAnchor.constraint(equalTo: lblStopLoss.topAnchor, constant: 0).isActive = true
        tfStopLoss.heightAnchor.constraint(equalTo: lblStopLoss.heightAnchor, multiplier: 1.0, constant: 0).isActive = true
        
        // borderStopLoss
        formView.addSubview(borderStopLoss)
        borderStopLoss.leftAnchor.constraint(equalTo: tfStopLoss.leftAnchor, constant: 0).isActive = true
        borderStopLoss.rightAnchor.constraint(equalTo: tfStopLoss.rightAnchor, constant: 0).isActive = true
        borderStopLoss.bottomAnchor.constraint(equalTo: tfStopLoss.bottomAnchor, constant: 0).isActive = true
        borderStopLoss.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // lblBookType
        formView.addSubview(lblBookType)
        lblBookType.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblBookType.topAnchor.constraint(equalTo: lblStopLoss.bottomAnchor, constant: 4).isActive = true
        lblBookType.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblBookType.widthAnchor.constraint(equalToConstant: (lblBookType.text?.sizeOfString(usingFont: lblBookType.font).width)! + 8).isActive = true
        
        // dropdownBookType
        formView.addSubview(dropdownBookType)
        dropdownBookType.setTitle("Select Book Type", for: .normal)
        //dropdownBookType.addTarget(self, action: #selector(showBookTypePicker(_:)), for: .touchUpInside)
        dropdownBookType.addTarget(self, action: #selector(showMultipleBookTypePicker(_:)), for: .touchUpInside)
        dropdownBookType.leftAnchor.constraint(equalTo: lblBookType.rightAnchor, constant: 8).isActive = true
        dropdownBookType.topAnchor.constraint(equalTo: lblBookType.topAnchor, constant: 0).isActive = true
        dropdownBookType.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        //dropdownBookType.heightAnchor.constraint(equalToConstant: 30).isActive = true
        dropdownBookTypeHeightConstraint = NSLayoutConstraint(item: dropdownBookType, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: fieldHeight)
        dropdownBookTypeHeightConstraint.isActive = true
        
        // borderBookType
        formView.addSubview(borderBookType)
        borderBookType.leftAnchor.constraint(equalTo: dropdownBookType.leftAnchor, constant: 0).isActive = true
        borderBookType.rightAnchor.constraint(equalTo: dropdownBookType.rightAnchor, constant: 0).isActive = true
        borderBookType.bottomAnchor.constraint(equalTo: dropdownBookType.bottomAnchor, constant: 0).isActive = true
        borderBookType.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // lblComment
        formView.addSubview(lblComment)
        lblComment.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblComment.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        lblComment.topAnchor.constraint(equalTo: dropdownBookType.bottomAnchor, constant: 10).isActive = true
        lblComment.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // tvComment
        formView.addSubview(tvComment)
        //tvComment.delegate = self
        tvComment.topAnchor.constraint(equalTo: lblComment.bottomAnchor, constant: 4).isActive = true
        tvComment.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 5).isActive = true
        tvComment.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -5).isActive = true
        tvComment.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        // dropdownBookTypeButtonWidth
        dropdownBookTypeButtonWidth = screenWidth - (lblBookType.text?.sizeOfString(usingFont: lblBookType.font).width)!
        dropdownBookTypeButtonWidth -= (10 + 10 + 8 + 40 + 10)
    }
    
    private func createLegSummary(symbol: String, api: [String]) -> NSAttributedString {
        // create message preview
        
        var ctr: Int = 0
        let attributedString = NSMutableAttributedString()
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.left
        
        for leg in api {
            let splitLeg = leg.components(separatedBy: ",")
            
            attributedString.append(NSAttributedString(string: "•", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            
            if let qty = splitLeg[2].double() {
                if qty < 0 {
                    attributedString.append(NSAttributedString(string: " Sell", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
                } else if qty > 0 {
                    attributedString.append(NSAttributedString(string: " Buy", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
                }
            }
            
            let temp = splitLeg[0].components(separatedBy: " ")
            
            if temp.indices.contains(2) {
                attributedString.append(NSAttributedString(string: " \(temp[2])", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            if temp.indices.contains(3) {
                attributedString.append(NSAttributedString(string: " \(temp[3])", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            if temp.indices.contains(1) {
                attributedString.append(NSAttributedString(string: " \(temp[1])", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            if let qty = splitLeg[2].double() {
                attributedString.append(NSAttributedString(string: " Qty \(qty.roundToDecimal(2).abs.toString())", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            // Price
            if let price = splitLeg[1].double() {
                attributedString.append(NSAttributedString(string: " @ \(price.roundToDecimal(2).toString())\(ctr < api.count - 1 ? "\n" : "")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6), NSAttributedString.Key.paragraphStyle: style]))
            }
            
            ctr += 1
        }
        
        return attributedString
    }
    
    private func confirmSave(completion: @escaping ((_ save: Bool) -> Void)) {
        
        guard let symbolName = symbol else { return }
        guard let legsArray = legs else { return }
        
        let attributedString = createLegSummary(symbol: symbolName, api: legsArray)
        
        confirm(title: "Confirm", message: attributedString) { (confirmed) in
            completion(confirmed)
        }
        
        /*let dialog = ZAlertView(title: "Confirm",
                                message: "Do you want to save the call?",
                                isOkButtonLeft: false,
                                okButtonText: "Yes",
                                cancelButtonText: "No",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissWithDuration(0.3)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                                        completion(true)
                                    })
        }) { (alertView) in
            alertView.dismissAlertView()
            completion(false)
        }
        dialog.show()*/
    }
    
    private func confirm(title: String, message: NSAttributedString, completion: @escaping ((_ confirmed: Bool) -> Void)) {
        let dialog = ZAlertView()
        dialog.alertTitle = title
        dialog.messageAttributedString = message
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        
        dialog.addButton("OK", color: Constants.ZAlertViewUI.successButtonColor, titleColor: UIColor.white) { (alertView) in
            alertView.dismissWithDuration(0.3)
            //completion(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                completion(true)
            })
        }
        
        dialog.addButton("CANCEL", color: UIColor(white: 0.0, alpha: 0.3), titleColor: UIColor.white) { (alertView) in
            alertView.dismissAlertView()
        }
        
        dialog.show()
    }
    
    @objc private func saveCall() {
        print("► saveCall()")
        
        guard let position = positionName else { return }
        guard let strategy = strategyName else { return }
        guard let symbolName = symbol else { return }
        guard let marginValue = margin else { return }
        guard let legsArray = legs else { return }
        guard let tradeStatusValue = tradeStatus else { return }
        guard let books = selectedBooks else { return }
        guard let spreadValue = initiationSpread?.roundToDecimal(2).toString() else { return }
        guard let targetValue = tfTarget.text else { return }
        guard let stopLossValue = tfStopLoss.text else { return }
        guard let commentValue = tvComment.text else { return }
        
        var bookTypes = [String]()
        
        for book in books {
            if let type = book.bookType {
                bookTypes.append(type)
            }
        }
        
        SVProgressHUD.show()
        
        OSOWebService.sharedInstance.uploadCall(positionName: position, strategyName: strategy, symbol: symbolName, margin: marginValue, legs: legsArray, tradeStatus: tradeStatusValue, bookTypes: bookTypes, spread: spreadValue, target: targetValue, stopLoss: stopLossValue, comment: commentValue.trimmed.stripUnicodeCharacters()) { (error, positions, margin, bookName, notificationTitle, notificationMessage, dataValues) in
            if let err = error {
                SVProgressHUD.dismiss()
                print("Error: \(err.localizedDescription)")
                if err.code == OSOError.InvalidSession.rawValue {
                    self.showZAlertView(withTitle: "", andMessage: err.localizedDescription, cancelTitle: "LOGIN", isError: true, completion: {
                        // LOGOUT CODE
                        self.logout()
                    })
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                }
            } else {
                if let positionsList = positions, let marginValue = margin, let notfMsg = notificationMessage, let legsArray = self.legs, let book = bookName, let notfTitle = notificationTitle, let values = dataValues {
                    print("Positions: \(positionsList)")
                    print("Margin: \(marginValue)")
                    //self.addPosition(bookTypes: bookTypes, positions: positionsList, symbol: symbolName, positionName: position, margin: marginValue, legs: legsArray, tradeStatus: tradeStatusValue, notificationMessage: notfMsg)
                    SVProgressHUD.dismiss()
                    self.showNotificationViewController(positionNos: positionsList, bookName: book, notificationTitle: notfTitle, notificationMessage: notfMsg, dataValues: values)
                } else {
                    SVProgressHUD.dismiss()
                    self.showZAlertView(withTitle: "Error", andMessage: "Data not received.", cancelTitle: "OK", isError: true, completion: {})
                }
            }
        }
    }
    
    private func addPosition(bookTypes: [String], positions: [String], symbol: String, positionName: String, margin: String, legs: [String], tradeStatus: String, bookName: [String], notificationTitle: String, notificationMessage: String?, dataValues: String) {
        print("► addPosition()")
        
        var tempInstrument = [String]()
        let alerts: String = "0"
        let dateCreated: Date = Date()
        var analyzerSummaries = [AnalyzerSummary]()
        
        for i in 0..<positions.count {
            for j in 0..<legs.count {
                let summary = AnalyzerSummary()
                summary.bookType = bookTypes[i]
                summary.positionNo = positions[i]
                summary.symbol = symbol
                summary.positionName = positionName
                summary.margin = margin
                summary.dateCreated = dateCreated
                summary.alertOne = alerts
                summary.alertTwo = alerts
                summary.tradeStatus = tradeStatus
                summary.Papi = legs[j]
                
                tempInstrument = legs[j].components(separatedBy: ",")
                
                summary.price = tempInstrument[1]
                summary.qty = tempInstrument[2]
                
                tempInstrument = tempInstrument[0].components(separatedBy: " ")
                
                summary.instrument = tempInstrument[0]
                summary.expiry = tempInstrument[1]
                summary.srno = "\(j+1)"
                summary.syncNo = "0"
                
                if tempInstrument[0] == "Fut" {
                    summary.strike = "-1"
                    summary.optType = "XX"
                } else {
                    summary.strike = tempInstrument[2]
                    summary.optType = tempInstrument[3]
                }
                
                summary.expired = "0"
                
                analyzerSummaries.append(summary)
            }
        }
        
        DatabaseManager.sharedInstance.addPosition(analyzerSummaries: analyzerSummaries) { (success) in
            SVProgressHUD.dismiss()
            if success {
                if let notfMsg = notificationMessage {
                    self.showNotificationViewController(positionNos: positions, bookName: bookName, notificationTitle: notificationTitle, notificationMessage: notfMsg, dataValues: dataValues)
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: "Notification message was not received.", cancelTitle: "OK", isError: false, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            } else {
                self.showZAlertView(withTitle: "Error", andMessage: "Could not save the summary", cancelTitle: "OK", isError: true, completion: {
                    //
                })
            }
        }
        
    }
    
    private func showNotificationViewController(positionNos: [String], bookName: [String], notificationTitle: String, notificationMessage: String, dataValues: String) {
        let vc = NotificationMessageViewController()
        vc.positionNos = positionNos
        vc.bookName = bookName
        vc.dataValues = dataValues
        vc.notificationTitle = notificationTitle
        vc.notificationMessage = notificationMessage
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func actionSave(_ sender: UIButton) {
        
        resignAllResponders()
        
        guard let targetSpread = tfTarget.text?.trimmed else { return }
        guard let stopLossSpread = tfStopLoss.text?.trimmed else { return }
        let comment = tvComment.text.trimmed
        
        if targetSpread.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter target spread.", cancelTitle: "OK", isError: true) {
                self.tfTarget.becomeFirstResponder()
            }
        } else if stopLossSpread.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter stop loss spread", cancelTitle: "OK", isError: true) {
                self.tfStopLoss.becomeFirstResponder()
            }
        } else if selectedBooks == nil {
            showZAlertView(withTitle: "Error", andMessage: "Please select Book Type.", cancelTitle: "OK", isError: true) {
                //self.showBookTypePicker(self.dropdownBookType)
                self.showMultipleBookTypePicker(self.dropdownBookType)
            }
        } else if comment.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter comment for the call.", cancelTitle: "OK", isError: true) {
                self.tvComment.becomeFirstResponder()
            }
        } else {
            if let targetSpreadDblVal = targetSpread.double(), let stopLossSpreadDblVal = stopLossSpread.double(), let spread = initiationSpread {
                
                if targetSpreadDblVal < spread {
                    showZAlertView(withTitle: "Error", andMessage: "Target spread should be greater than the Initiation spread.", cancelTitle: "OK", isError: true) {
                        self.tfTarget.becomeFirstResponder()
                    }
                } else if stopLossSpreadDblVal > spread {
                    showZAlertView(withTitle: "Error", andMessage: "Stop loss spread should be less than the Initiation spread.", cancelTitle: "OK", isError: true) {
                        self.tfStopLoss.becomeFirstResponder()
                    }
                } else {
                    confirmSave { (save) in
                        if save {
                            //self.perform(#selector(self.saveCall), with: nil, afterDelay: 0.6)
                            self.saveCall()
                        }
                    }
                }
            } else {
                showZAlertView(withTitle: "Error", andMessage: "Please enter target and stop loss spread.", cancelTitle: "OK", isError: true) {}
            }
        }
    }
    
    private func addAlert(positionNo: String, alertOne: String, alertTwo: String) {
        //let maxAttempts: Int = 5
        //var attemptCounter: Int = 0
        
        DatabaseManager.sharedInstance.addAlert(positionNo: positionNo, alertOne: alertOne, alertTwo: alertTwo) { (success) in
            if success {
                self.showZAlertView(withTitle: "Alert Set", andMessage: "Alert has been set successfully.", cancelTitle: "CLOSE", isError: false, completion: {
                    self.delegate?.stepDCompleted()
                })
                /*let dialog = ZAlertView()
                dialog.message = "Alert has been set successfully. Would you like to see your positions?"
                dialog.alertType = .multipleChoice
                dialog.allowTouchOutsideToDismiss = false
                dialog.addButton("YES", hexColor: Constants.UIConfig.themeColor.hexString, hexTitleColor: "#ffffff") { (alertView) in
                    alertView.dismissAlertView()
                    self.delegate?.stepDCompleted()
                }
                dialog.addButton("NO", hexColor: UIColor.gray.hexString, hexTitleColor: "#ffffff") { (alertView) in
                    alertView.dismissAlertView()
                    self.navigationController?.popViewController(animated: true)
                }
                
                dialog.show()*/
            } else {
                self.showZAlertView(withTitle: "Error", andMessage: "Failed to save alert into database", cancelTitle: "OK", isError: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    private func saveAlert(positionNo: String, alertOne: String, alertTwo: String) {
        OSOWebService.sharedInstance.uploadAlert(positionNo: positionNo, alertOne: alertOne, alertTwo: alertTwo) { (success, message, error) in
            
            SVProgressHUD.dismiss()
            
            if success {
                self.addAlert(positionNo: positionNo, alertOne: alertOne, alertTwo: alertTwo)
            } else {
                if let msg = message {
                    self.showZAlertView(withTitle: "Error", andMessage: msg, cancelTitle: "OK", isError: true, completion: {})
                } else {
                    if let err = error {
                        self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
                    }
                }
            }
        }
    }
    
    @objc private func showBookTypePicker(_ sender: DropdownButton) {
        print("► showBookTypePicker()")
        
        resignAllResponders()
        
        /*getBookType(sender: sender) { (osoBook) in
            if let book = osoBook {
                self.selectedBook = book
                
                if let bookTitle = book.title {
                    print("selectedBook = \(bookTitle)")
                    sender.setTitle(bookTitle, for: [])
                    sender.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                }
            }
        }*/
    }
    
    @objc private func showMultipleBookTypePicker(_ sender: DropdownButton) {
        print("► showMultipleBookTypePicker()")
        resignAllResponders()
        
        getBookTypes(sender: sender) { (osoBooks) in
            if let books = osoBooks {
                self.selectedBooks?.removeAll()
                self.selectedBooks = books
                
                var titleArray = [String]()
                
                for book in books {
                    if let title = book.title {
                        titleArray.append(title)
                    }
                }
                
                if titleArray.count > 0 {
                    let bookString = titleArray.joined(separator: " + ")
                    let bookStringHeight = bookString.height(constraintedWidth: self.dropdownBookTypeButtonWidth, font: UIFont.boldSystemFont(ofSize: 14)) + 14
                    var extraHeight: CGFloat = 0
                    
                    if bookStringHeight > self.fieldHeight {
                        extraHeight = bookStringHeight - self.fieldHeight
                    }
                    
                    self.dropdownBookTypeHeightConstraint.constant = self.fieldHeight + extraHeight
                    self.formHeightConstraint.constant = self.defaultFormHeight + extraHeight
                    self.view.layoutIfNeeded()
                    
                    DispatchQueue.main.async {
                        sender.setTitle(titleArray.joined(separator: " + "), for: .normal)
                        sender.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
                        sender.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                    }
                } else {
                    self.dropdownBookTypeHeightConstraint.constant = self.fieldHeight
                    self.formHeightConstraint.constant = self.defaultFormHeight
                    self.view.layoutIfNeeded()
                    
                    DispatchQueue.main.async {
                        sender.setTitle("Select Book", for: .normal)
                        sender.setTitleColor(UIColor(white: 1.0, alpha: 0.3), for: .normal)
                        sender.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                    }
                }
            } else {
                self.selectedBooks = nil
                
                self.dropdownBookTypeHeightConstraint.constant = self.fieldHeight
                self.formHeightConstraint.constant = self.defaultFormHeight
                self.view.layoutIfNeeded()
                
                DispatchQueue.main.async {
                    sender.setTitle("Select Book", for: .normal)
                    sender.setTitleColor(UIColor(white: 1.0, alpha: 0.3), for: .normal)
                    sender.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                }
            }
        }
    }
    
    private func getBookType(sender: UIButton, completion: @escaping((_ book: OSOBook?) -> Void)) {
        
        if allBooks.count > 0 {
            
            var bookTitles = [String]()
            var initialSelection: Int = 0
            var i: Int = 0
            
            for book in allBooks {
                if let title = book.title {
                    bookTitles.append(title)
                    
                    /*if let currentBookTitle = selectedBook?.title {
                        if title == currentBookTitle {
                            initialSelection = i
                        }
                    }*/
                }
                
                i += 1
            }
            
            if bookTitles.count > 0 {
                
                let picker = ActionSheetStringPicker(title: "Select Book Type", rows: bookTitles, initialSelection: initialSelection, doneBlock: { (picker, value, index) in
                    sender.setTitleColor(Constants.UIConfig.themeColor, for: [])
                    //completion(index as? String)
                    completion(self.allBooks[value])
                }, cancel: { (picker) in
                    //
                }, origin: sender)
                
                picker?.show()
                
            }
        }
    }
    
    private func getBookTypes(sender: UIButton, completion: @escaping((_ book: [OSOBook]?) -> Void)) {
        if allBooks.count > 0 {
            
            var items = [ActionSheetItem]()
            var trueFlagCounter: Int = 0
            
            let toggler = ActionSheetMultiSelectToggleItem(title: "Select Books", state: .selectAll, group: "books", selectAllTitle: "Select All", deselectAllTitle: "Deselect All")
            items.append(toggler)
            
            ActionSheetSelectItemCell.appearance().selectedIcon = UIImage(named: "ic_checkmark")
            ActionSheetSelectItemCell.appearance().unselectedIcon = UIImage(named: "ic_empty")
            ActionSheetCancelButtonCell.appearance().titleColor = .lightGray
            ActionSheetTitleCell.appearance().titleFont = UIFont.boldSystemFont(ofSize: 13)
            ActionSheetMultiSelectToggleItemCell.appearance().titleFont = UIFont.systemFont(ofSize: 12)
            ActionSheetMultiSelectToggleItemCell.appearance().selectAllSubtitleColor = .lightGray
            ActionSheetMultiSelectToggleItemCell.appearance().deselectAllSubtitleColor = .red
            
            for book in allBooks {
                if let title = book.title, let type = book.bookType {
                    
                    var flagSelected = false
                    
                    if let selected = selectedBooks {
                        for b in selected {
                            if let bTitle = b.title, let bType = b.bookType {
                                if bTitle == title && bType == type {
                                    flagSelected = true
                                    trueFlagCounter += 1
                                }
                            }
                        }
                    }
                    
                    let item = ActionSheetMultiSelectItem(title: title, subtitle: "", isSelected: flagSelected, group: "books", value: type, image: nil)
                    items.append(item)
                }
            }
            
            toggler.state = (trueFlagCounter == allBooks.count) ? .deselectAll : .selectAll
            
            let ok = ActionSheetOkButton(title: "Done")
            items.append(ok)
            
            let cancel = ActionSheetCancelButton(title: "Cancel")
            items.append(cancel)
            
            let sheet = ActionSheet(items: items) { (sheet, item) in
                guard item.isOkButton else { return }
                let books = sheet.items.compactMap { $0 as? ActionSheetMultiSelectItem }
                let selectedBooks = books.filter { $0.isSelected }
                print("Selected Books: \(selectedBooks.count)")
                
                var tempArray = [OSOBook]()
                
                for book in selectedBooks {
                    if let title = book.title as? String {
                        
                        for b in self.allBooks {
                            if let bTitle = b.title {
                                if bTitle == title {
                                    print("- \(bTitle)")
                                    tempArray.append(b)
                                }
                            }
                        }
                    }
                }
                
                completion(tempArray.count > 0 ? tempArray : nil)
            }
            
            sheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
            sheet.present(in: self, from: sender)
        }
    }
    
    
    private func actionSkip() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func resignAllResponders() {
        view.endEditing(true)
    }
    
    // MARK:- Spread Calculation
    
    private func calculateSpread() -> Double? {
        
        print("► calculateSpread()")
        guard let legsArray = legs else { return nil }
        
        var totalPremium: Double = 0
        var minQty: Double = Double.greatestFiniteMagnitude
        
        if legsArray.count > 0 {
            
            for i in 0..<legsArray.count {
                //print("\(i+1) > \(legsArray[i])")
                var splitLeg = legsArray[i].components(separatedBy: ",")
                
                if let priceValue = splitLeg[1].double(), let qtyValue = splitLeg[2].double() {
                    if qtyValue.abs < minQty {
                        minQty = qtyValue.abs
                    }
                    
                    totalPremium += priceValue * qtyValue
                }
            }
        } else {
            return nil
        }
        
        if minQty > 0 {
            return totalPremium/minQty
        } else {
            return nil
        }
    }

    // MARK:- Keyboard Events
    
    @objc func keyBoardDidShow(notification: NSNotification) {
        //handle appearing of keyboard here
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            scrollViewContainerBottomAnchorConstraint.isActive = false
            
            scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -(keyboardSize.height))
            
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollViewContainerBottomAnchorConstraint.isActive = true
                self.view.layoutIfNeeded()
            }) { (finished) in
                if let firstResponder = self.view.firstResponder() {
                    self.scrollViewContainer.scrollRectToVisible(firstResponder.frame, animated: true)
                }
            }
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
        
        scrollViewContainerBottomAnchorConstraint.isActive = false
        
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnSave, attribute: .top, multiplier: 1.0, constant: -16)
        
        UIView.animate(withDuration: 0.3) {
            self.scrollViewContainerBottomAnchorConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK:- UITextFieldDelegate
    
    /*func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollViewContainer.scrollRectToVisible(textField.frame, animated: true)
    }*/
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.tfSpread || textField == self.tfTarget || textField == self.tfStopLoss {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789-.").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            
            if string == filtered {
                if string == "-" {
                    let count = (textField.text?.components(separatedBy: "-").count)! - 1
                    
                    if count == 0 {
                        if textField.text?.count == 0 {
                            return true
                        } else {
                            return false
                        }
                    } else {
                        return false
                    }
                } else if string == "." {
                    let count = (textField.text?.components(separatedBy: ".").count)! - 1
                    if count > 0 {
                        return false
                    } else {
                        return true
                    }
                } else {
                    return true
                }
            } else {
                return false
            }
        } else {
            return true
        }
    }

}

// MARK:- UITextViewDelegate

/*extension PredefinedStrategiesStepD: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        scrollViewContainer.scrollRectToVisible(textView.frame, animated: true)
    }
}*/
