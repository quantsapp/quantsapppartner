//
//  MarketUpdateViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 25/04/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import ZAlertView
import SwifterSwift
import Sheeeeeeeeet
import SVProgressHUD

class MarketUpdateViewController: UIViewController {
    
    private var osoNavBar: OSONavigationBar!
    
    private var allBooks = DatabaseManager.sharedInstance.getBooks()
    private var selectedBooks: [OSOBook]?
    
    private var fieldVerticalGap: CGFloat = 4
    private var fieldHeight: CGFloat = 30
    private var defaultFormHeight: CGFloat = 324.0
    private var screenWidth: CGFloat = SwifterSwift.screenWidth
    private var dropdownBookTypeButtonWidth: CGFloat = 0.0
    
    // constraint
    var scrollViewContainerBottomAnchorConstraint: NSLayoutConstraint!
    var formHeightConstraint: NSLayoutConstraint!
    private var dropdownBookTypeHeightConstraint: NSLayoutConstraint!
    
    private let btnSend: UIButton = {
        let button = UIButton()
        button.setTitle("SEND", for: .normal)
        button.backgroundColor = Constants.UIConfig.themeColor//UIColor(white: 1.0, alpha: 0.1)
        button.setTitleColor(Constants.UIConfig.bottomColor, for: .normal) //
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let scrollViewContainer: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let formView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblBookType: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Book Type"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dropdownBookType: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.3), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.titleLabel?.sizeToFit()
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderBookType: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblTitle: UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTitleCount: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tvTitle: OSOTextView = {
        let textView = OSOTextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textColor = UIColor.white
        textView.maxCharacterLimit = 60
        textView.layer.borderColor = Constants.UIConfig.borderColor.cgColor
        textView.layer.cornerRadius = 4
        textView.layer.borderWidth = 0.5
        textView.backgroundColor = UIColor(white: 1.0, alpha: 0.05)
        textView.returnKeyType = .done
        textView.keyboardType = .asciiCapable
        textView.placeholder = "Add title..."
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    private let lblMessage: UILabel = {
        let label = UILabel()
        label.text = "Message"
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblMessageCount: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let tvMessage: OSOMultilineTextView = {
        let textView = OSOMultilineTextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textColor = UIColor.white
        textView.maxCharacterLimit = 1000
        textView.layer.borderColor = Constants.UIConfig.borderColor.cgColor
        textView.layer.cornerRadius = 4
        textView.layer.borderWidth = 0.5
        textView.backgroundColor = UIColor(white: 1.0, alpha: 0.05)
        textView.returnKeyType = .default
        textView.keyboardType = .asciiCapable
        textView.placeholder = "Add message..."
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        // keyboard event handler
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // initial setup of all views
        setupViews()
    }
    

    private func setupViews() {
        // btnSend
        view.addSubview(btnSend)
        btnSend.addTarget(self, action: #selector(actionSend(_:)), for: .touchUpInside)
        btnSend.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        btnSend.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnSend.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        btnSend.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        
        // scrollview container
        view.addSubview(scrollViewContainer)
        scrollViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollViewContainer.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 0).isActive = true
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnSend, attribute: .top, multiplier: 1.0, constant: -16)
        scrollViewContainerBottomAnchorConstraint.isActive = true
        
        // form view
        scrollViewContainer.addSubview(formView)
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .top, relatedBy: .equal, toItem: scrollViewContainer, attribute: .top, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .left, relatedBy: .equal, toItem: scrollViewContainer, attribute: .left, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .right, relatedBy: .equal, toItem: scrollViewContainer, attribute: .right, multiplier: 1.0, constant: 0))
        scrollViewContainer.addConstraint(NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: scrollViewContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
        formHeightConstraint = NSLayoutConstraint(item: formView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: defaultFormHeight)
        formHeightConstraint.isActive = true
        
        scrollViewContainer.contentSize = formView.size
        
        // lblBookType
        formView.addSubview(lblBookType)
        lblBookType.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblBookType.topAnchor.constraint(equalTo: formView.topAnchor, constant: 8).isActive = true
        lblBookType.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblBookType.widthAnchor.constraint(equalToConstant: (lblBookType.text?.sizeOfString(usingFont: lblBookType.font).width)! + 8).isActive = true
        
        // dropdownBookType
        formView.addSubview(dropdownBookType)
        dropdownBookType.setTitle("Select Book Type", for: .normal)
        //dropdownBookType.addTarget(self, action: #selector(showBookTypePicker(_:)), for: .touchUpInside)
        dropdownBookType.addTarget(self, action: #selector(showMultipleBookTypePicker(_:)), for: .touchUpInside)
        dropdownBookType.leftAnchor.constraint(equalTo: lblBookType.rightAnchor, constant: 8).isActive = true
        dropdownBookType.topAnchor.constraint(equalTo: lblBookType.topAnchor, constant: 0).isActive = true
        dropdownBookType.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        //dropdownBookType.heightAnchor.constraint(equalToConstant: 30).isActive = true
        dropdownBookTypeHeightConstraint = NSLayoutConstraint(item: dropdownBookType, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: fieldHeight)
        dropdownBookTypeHeightConstraint.isActive = true
        
        // borderBookType
        formView.addSubview(borderBookType)
        borderBookType.leftAnchor.constraint(equalTo: dropdownBookType.leftAnchor, constant: 0).isActive = true
        borderBookType.rightAnchor.constraint(equalTo: dropdownBookType.rightAnchor, constant: 0).isActive = true
        borderBookType.bottomAnchor.constraint(equalTo: dropdownBookType.bottomAnchor, constant: 0).isActive = true
        borderBookType.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // lblTitle
        formView.addSubview(lblTitle)
        lblTitle.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblTitle.widthAnchor.constraint(equalToConstant: (lblTitle.text?.sizeOfString(usingFont: lblTitle.font).width)! + 2).isActive = true
        lblTitle.topAnchor.constraint(equalTo: dropdownBookType.bottomAnchor, constant: 10).isActive = true
        lblTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblTitlecount
        formView.addSubview(lblTitleCount)
        lblTitleCount.leftAnchor.constraint(equalTo: lblTitle.rightAnchor, constant: 10).isActive = true
        lblTitleCount.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        lblTitleCount.topAnchor.constraint(equalTo: lblTitle.topAnchor, constant: 0).isActive = true
        lblTitleCount.bottomAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 0).isActive = true
        
        // tvTitle
        formView.addSubview(tvTitle)
        tvTitle.delegateOSOTextView = self
        tvTitle.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 4).isActive = true
        tvTitle.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 5).isActive = true
        tvTitle.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -5).isActive = true
        tvTitle.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        /*if Constants.APPConfig.inDevelopment {
            tvTitle.text = "Daily snippets 25-Apr-19"
        }*/
        
        // lblMessage
        formView.addSubview(lblMessage)
        lblMessage.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 10).isActive = true
        lblMessage.widthAnchor.constraint(equalToConstant: (lblMessage.text?.sizeOfString(usingFont: lblMessage.font).width)! + 2).isActive = true
        lblMessage.topAnchor.constraint(equalTo: tvTitle.bottomAnchor, constant: 10).isActive = true
        lblMessage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblMessageCount
        formView.addSubview(lblMessageCount)
        lblMessageCount.leftAnchor.constraint(equalTo: lblMessage.rightAnchor, constant: 10).isActive = true
        lblMessageCount.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -10).isActive = true
        lblMessageCount.topAnchor.constraint(equalTo: lblMessage.topAnchor, constant: 0).isActive = true
        lblMessageCount.bottomAnchor.constraint(equalTo: lblMessage.bottomAnchor, constant: 0).isActive = true
        
        
        // tvMessage
        formView.addSubview(tvMessage)
        tvMessage.delegateOSOMultilineTextView = self
        tvMessage.addDoneButtonOnKeyboard()
        tvMessage.topAnchor.constraint(equalTo: lblMessage.bottomAnchor, constant: 4).isActive = true
        tvMessage.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 5).isActive = true
        tvMessage.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -5).isActive = true
        tvMessage.heightAnchor.constraint(equalToConstant: 160).isActive = true
        
        /*if Constants.APPConfig.inDevelopment {
            tvMessage.text = "1. Nifty and Bank Nifty opened positive but saw major selling pressure in second half losing all the morning gain and ended the April series expiry in red.\n2. Cement, IT and Oil and Gas stocks were the top performer while NBFC, Steel and PSU stocks were worst performer.\n3. Nifty Option data shows for weekly expiry (2nd May) 11600 could act as a strong support with Highest Put OI and 11800CE with highest Call OI can act as an immediate resistance.\n4. Today Nifty and Bank nifty both saw long unwinding as Nifty was down by 0.73% and OI down 8% while Bank Nifty was down by 1 % and OI down by 26%.\n5. Expiry on expiry basis  Nifty saw Long Buildup with Price up by 0.7% and OI up by 35% and Bank nifty saw Long unwinding with Price down by -2.81% and down by 23.21.\n6. Expiry on Expiry basis Automobile and cement sector saw long built-up and NBFC, PSU and Telecom sector saw Short Build-up.\n7. Nifty Rollover stands at 80% with rollover cost of 0.67% while Bank nifty Rollover stands at 74.5"
            
            /*tvMessage.text = "1. Nifty and Bank Nifty opened positive but saw major selling pressure in second half losing all the morning gain and ended the April series expiry in red. 2. Cement, IT and Oil and Gas stocks were the top performer while NBFC, Steel and PSU stocks were worst performer. 3. Nifty Option data shows for weekly expiry (2nd May) 11600 could act as a strong support with Highest Put OI and 11800CE with highest Call OI can act as an immediate resistance. 4. Today Nifty and Bank nifty both saw long unwinding as Nifty was down by 0.73% and OI down 8% while Bank Nifty was down by 1 % and OI down by 26%. 5. Expiry on expiry basis  Nifty saw Long Buildup with Price up by 0.7% and OI up by 35% and Bank nifty saw Long unwinding with Price down by -2.81% and down by 23.21. 6. Expiry on Expiry basis Automobile and cement sector saw long built-up and NBFC, PSU and Telecom sector saw Short Build-up. 7. Nifty Rollover stands at 80% with rollover cost of 0.67% while Bank nifty Rollover stands at 74.5% with rollover cost of 0.73%.Highest Rollover of more than 98% is seen in MRPL, Century Textile, UBL and Apollo Hospital. Lowest Rollover is seen in TCS, Coal India, DLF and India bull finance."*/
        }*/
        
        // dropdownBookTypeButtonWidth
        dropdownBookTypeButtonWidth = screenWidth - (lblBookType.text?.sizeOfString(usingFont: lblBookType.font).width)!
        dropdownBookTypeButtonWidth -= (10 + 10 + 8 + 40 + 10)
        
        
        // update count labels
        updateCharacterCountLabel(label: lblTitleCount, count: tvTitle.text.count, maxCharacters: tvTitle.maxCharacterLimit)
        updateCharacterCountLabel(label: lblMessageCount, count: tvMessage.text.count, maxCharacters: tvMessage.maxCharacterLimit)
    }
    
    @objc private func showMultipleBookTypePicker(_ sender: DropdownButton) {
        print("► showMultipleBookTypePicker()")
        resignAllResponders()
        
        getBookTypes(sender: sender) { (osoBooks) in
            if let books = osoBooks {
                self.selectedBooks?.removeAll()
                self.selectedBooks = books
                
                var titleArray = [String]()
                
                for book in books {
                    if let title = book.title {
                        titleArray.append(title)
                    }
                }
                
                if titleArray.count > 0 {
                    let bookString = titleArray.joined(separator: " + ")
                    let bookStringHeight = bookString.height(constraintedWidth: self.dropdownBookTypeButtonWidth, font: UIFont.boldSystemFont(ofSize: 14)) + 14
                    var extraHeight: CGFloat = 0
                    
                    if bookStringHeight > self.fieldHeight {
                        extraHeight = bookStringHeight - self.fieldHeight
                    }
                    
                    self.dropdownBookTypeHeightConstraint.constant = self.fieldHeight + extraHeight
                    self.formHeightConstraint.constant = self.defaultFormHeight + extraHeight
                    self.view.layoutIfNeeded()
                    
                    DispatchQueue.main.async {
                        sender.setTitle(titleArray.joined(separator: " + "), for: .normal)
                        sender.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
                        sender.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                    }
                } else {
                    self.dropdownBookTypeHeightConstraint.constant = self.fieldHeight
                    self.formHeightConstraint.constant = self.defaultFormHeight
                    self.view.layoutIfNeeded()
                    
                    DispatchQueue.main.async {
                        sender.setTitle("Select Book", for: .normal)
                        sender.setTitleColor(UIColor(white: 1.0, alpha: 0.3), for: .normal)
                        sender.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                    }
                }
            } else {
                self.selectedBooks = nil
                
                self.dropdownBookTypeHeightConstraint.constant = self.fieldHeight
                self.formHeightConstraint.constant = self.defaultFormHeight
                self.view.layoutIfNeeded()
                
                DispatchQueue.main.async {
                    sender.setTitle("Select Book", for: .normal)
                    sender.setTitleColor(UIColor(white: 1.0, alpha: 0.3), for: .normal)
                    sender.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                }
            }
        }
    }
    
    private func getBookTypes(sender: UIButton, completion: @escaping((_ book: [OSOBook]?) -> Void)) {
        if allBooks.count > 0 {
            
            var items = [ActionSheetItem]()
            var trueFlagCounter: Int = 0
            
            let toggler = ActionSheetMultiSelectToggleItem(title: "Select Books", state: .selectAll, group: "books", selectAllTitle: "Select All", deselectAllTitle: "Deselect All")
            items.append(toggler)
            
            ActionSheetSelectItemCell.appearance().selectedIcon = UIImage(named: "ic_checkmark")
            ActionSheetSelectItemCell.appearance().unselectedIcon = UIImage(named: "ic_empty")
            ActionSheetCancelButtonCell.appearance().titleColor = .lightGray
            ActionSheetTitleCell.appearance().titleFont = UIFont.boldSystemFont(ofSize: 13)
            ActionSheetMultiSelectToggleItemCell.appearance().titleFont = UIFont.systemFont(ofSize: 12)
            ActionSheetMultiSelectToggleItemCell.appearance().selectAllSubtitleColor = .lightGray
            ActionSheetMultiSelectToggleItemCell.appearance().deselectAllSubtitleColor = .red
            
            for book in allBooks {
                if let title = book.title, let type = book.bookType {
                    
                    var flagSelected = false
                    
                    if let selected = selectedBooks {
                        for b in selected {
                            if let bTitle = b.title, let bType = b.bookType {
                                if bTitle == title && bType == type {
                                    flagSelected = true
                                    trueFlagCounter += 1
                                }
                            }
                        }
                    }
                    
                    let item = ActionSheetMultiSelectItem(title: title, subtitle: "", isSelected: flagSelected, group: "books", value: type, image: nil)
                    items.append(item)
                }
            }
            
            toggler.state = (trueFlagCounter == allBooks.count) ? .deselectAll : .selectAll
            
            let ok = ActionSheetOkButton(title: "Done")
            items.append(ok)
            
            let cancel = ActionSheetCancelButton(title: "Cancel")
            items.append(cancel)
            
            let sheet = ActionSheet(items: items) { (sheet, item) in
                guard item.isOkButton else { return }
                let books = sheet.items.compactMap { $0 as? ActionSheetMultiSelectItem }
                let selectedBooks = books.filter { $0.isSelected }
                print("Selected Books: \(selectedBooks.count)")
                
                var tempArray = [OSOBook]()
                
                for book in selectedBooks {
                    if let title = book.title as? String {
                        
                        for b in self.allBooks {
                            if let bTitle = b.title {
                                if bTitle == title {
                                    print("- \(bTitle)")
                                    tempArray.append(b)
                                }
                            }
                        }
                    }
                }
                
                completion(tempArray.count > 0 ? tempArray : nil)
            }
            
            sheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
            sheet.present(in: self, from: sender)
        }
    }
    
    private func updateCharacterCountLabel(label: UILabel, count: Int, maxCharacters: Int) {
        DispatchQueue.main.async {
            label.text = "\(count)/\(maxCharacters)"
            if count >= maxCharacters {
                label.textColor = Constants.Charts.chartRedColor
            } else {
                label.textColor = UIColor(white: 1.0, alpha: 0.3)
            }
        }
    }
    
    private func resignAllResponders() {
        view.endEditing(true)
    }
    
    private func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect.zero, title: "Market Update", subTitle: nil, leftbuttonImage: UIImage(named: "icon_left"), rightButtonImage: nil)
        osoNavBar.translatesAutoresizingMaskIntoConstraints = false
        osoNavBar.delegate = self
        view.addSubview(osoNavBar)
        
        osoNavBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        osoNavBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        osoNavBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        osoNavBar.heightAnchor.constraint(equalToConstant: Constants.OSONavigationBarConstants.barHeight).isActive = true
    }
    
    @objc private func actionSend(_ sender: UIButton) {
        print("► actionSend()")
        resignAllResponders()
        
        let title = tvTitle.text.stripUnicodeCharacters()
        let message = tvMessage.text.trimmed.stripUnicodeCharacters()
        
        if selectedBooks == nil {
            showZAlertView(withTitle: "Error", andMessage: "Please select Book Type.", cancelTitle: "OK", isError: true) {
                self.showMultipleBookTypePicker(self.dropdownBookType)
            }
        } else if title.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter title for the message.", cancelTitle: "OK", isError: true) {
                self.tvTitle.becomeFirstResponder()
            }
        } else if message.isEmpty {
            showZAlertView(withTitle: "Error", andMessage: "Please enter message.", cancelTitle: "OK", isError: true) {
                self.tvMessage.becomeFirstResponder()
            }
        } else  {
            confirmSend(title: title, message: message) { (confirmed) in
                if confirmed {
                    self.sendMarketUpdate(title: title, message: message)
                }
            }
        }
    }
    
    private func sendMarketUpdate(title: String, message: String) {
        print("► sendMarketUpdate()")
        guard let books = selectedBooks else { return }
        
        var bookTypes = [String]()
        
        for book in books {
            if let type = book.bookType {
                bookTypes.append(type)
            }
        }
        
        SVProgressHUD.show()
        
        OSOWebService.sharedInstance.sendMarketUpdate(title: title, message: message, bookTypes: bookTypes) { (success, error, message) in
            SVProgressHUD.dismiss()
            
            if let err = error {
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
            } else if success {
                var alertMessage = "Market update has been sent successfully."
                
                if let responseMessage = message {
                    alertMessage = responseMessage
                }
                
                self.showSuccessAlert(message: alertMessage)
            } else {
                self.showZAlertView(withTitle: "Error", andMessage: "Failed to send market update.", cancelTitle: "OK", isError: true, completion: {})
            }
        }
    }
    
    private func confirmSend(title: String, message: String, completion: @escaping ((_ save: Bool) -> Void)) {
        
        let limitedTitle = title.index(title.startIndex, offsetBy: title.count > 60 ? 60 : title.count)
        let limitedMessage = message.index(message.startIndex, offsetBy: message.count > 500 ? 500 : message.count)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "\(title[..<limitedTitle])", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.black]))
        attributedString.append(NSAttributedString(string: "\n\(message[..<limitedMessage])", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor(white: 0.0, alpha: 0.6)]))
        
        let dialog = ZAlertView()
        dialog.alertTitle = nil
        dialog.messageAttributedString = attributedString
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        
        dialog.addButton("Send", color: Constants.UIConfig.themeColor, titleColor: UIColor.white) { (alertView) in
            alertView.dismissWithDuration(0.3)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                completion(true)
            })
        }
        
        dialog.addButton("cancel", color: UIColor(white: 0.0, alpha: 0.3), titleColor: UIColor.white) { (alertView) in
            alertView.dismissAlertView()
        }
        
        dialog.show()
    }
    
    private func resetForm() {
        selectedBooks?.removeAll()
        selectedBooks = nil
        
        dropdownBookTypeHeightConstraint.constant = self.fieldHeight
        formHeightConstraint.constant = self.defaultFormHeight
        view.layoutIfNeeded()
        
        DispatchQueue.main.async {
            self.dropdownBookType.setTitle("Select Book", for: .normal)
            self.dropdownBookType.setTitleColor(UIColor(white: 1.0, alpha: 0.3), for: .normal)
            self.dropdownBookType.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            
            self.tvTitle.text = ""
            self.tvMessage.text = ""
        }
    }
    
    private func showSuccessAlert(message: String) {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "\(message)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.black]))
        
        let dialog = ZAlertView()
        dialog.alertTitle = nil
        dialog.messageAttributedString = attributedString
        dialog.alertType = .multipleChoice
        dialog.allowTouchOutsideToDismiss = false
        
        dialog.addButton("Send another market udpate", color: Constants.UIConfig.themeColor, titleColor: UIColor.white) { (alertView) in
            // reset form
            self.resetForm()
            alertView.dismissAlertView()
        }
        
        dialog.addButton("Exit", color: UIColor(white: 0.0, alpha: 0.3), titleColor: UIColor.white) { (alertView) in
            alertView.dismissAlertView()
            self.navigationController?.popViewController(animated: true)
        }
        
        dialog.show()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:- Keyboard Events
    
    @objc func keyBoardDidShow(notification: NSNotification) {
        //handle appearing of keyboard here
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            scrollViewContainerBottomAnchorConstraint.isActive = false
            
            scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -(keyboardSize.height))
            
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollViewContainerBottomAnchorConstraint.isActive = true
                self.view.layoutIfNeeded()
            }) { (finished) in
                if let firstResponder = self.view.firstResponder() {
                    self.scrollViewContainer.scrollRectToVisible(firstResponder.frame, animated: true)
                }
            }
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
        
        scrollViewContainerBottomAnchorConstraint.isActive = false
        
        scrollViewContainerBottomAnchorConstraint = NSLayoutConstraint(item: scrollViewContainer, attribute: .bottom, relatedBy: .equal, toItem: btnSend, attribute: .top, multiplier: 1.0, constant: -16)
        
        UIView.animate(withDuration: 0.3) {
            self.scrollViewContainerBottomAnchorConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }

}

// MARK:- OSOTextViewDelegate

extension MarketUpdateViewController: OSOTextViewDelegate {
    
    func characterCountChanged(textView: OSOTextView, count: Int) {
        if textView == tvTitle {
            updateCharacterCountLabel(label: lblTitleCount, count: count, maxCharacters: textView.maxCharacterLimit)
        }
    }
    
}

// MARK:- OSOMultilineTextViewDelegate

extension MarketUpdateViewController: OSOMultilineTextViewDelegate {
    
    func characterCountChanged(textView: OSOMultilineTextView, count: Int) {
        if textView == tvMessage {
            updateCharacterCountLabel(label: lblMessageCount, count: count, maxCharacters: textView.maxCharacterLimit)
        }
    }
    
}

// MARK:- OSONavigationBarDelegate

extension MarketUpdateViewController: OSONavigationBarDelegate {
    
    func leftButtonTapped(sender: OSONavigationBar) {
        // ask for confirmation
        let dialog = ZAlertView(title: "Confirm",
                                message: "Do you want to exit the Market Update?",
                                isOkButtonLeft: false,
                                okButtonText: "No",
                                cancelButtonText: "Yes",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
        }) { (alertView) in
            alertView.dismissAlertView()
            self.navigationController?.popViewController(animated: true)
        }
        dialog.show()
    }
}
