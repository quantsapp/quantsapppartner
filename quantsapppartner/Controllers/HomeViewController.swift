//
//  HomeViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import Sheeeeeeeeet
import ZAlertView

public enum CallForType: String {
    case PreDefinedStrategies = "predefined_strategies"
    case CustomStrategy = "custom_strategy"
    
    func title() -> String {
        switch self {
        case .CustomStrategy:
            return "Custom Strategy"
        case .PreDefinedStrategies:
            return "Pre-Defined Strategies"
        }
    }
}

class HomeViewController: UIViewController {
    
    private var osoNavBar: OSONavigationBar!
    
    private let osoMenu: OSOMenu = {
        let menu = OSOMenu()
        menu.translatesAutoresizingMaskIntoConstraints = false
        return menu
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        // initial setup of all views
        setupViews()
    }
    

    private func setupViews() {
        // osoMenu
        view.addSubview(osoMenu)
        osoMenu.delegate = self
        osoMenu.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        osoMenu.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        osoMenu.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor).isActive = true
        if #available(iOS 11.0, *) {
            osoMenu.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            osoMenu.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
        let itemAddCall = OSOMenuItem(title: "Add Call", icon: "icon_add_call")
        let itemOpenCall = OSOMenuItem(title: "Open Calls", icon: "icon_open_calls")
        let itemCallHistory = OSOMenuItem(title: "Call History", icon: "icon_call_history")
        let itemMarketUpdate = OSOMenuItem(title: "Market Update", icon: "icon_market_update")
        let sectionA = OSOMenuSection(title: "Advisory", items: [itemAddCall, itemOpenCall, itemCallHistory, itemMarketUpdate])
        
        osoMenu.sections = [sectionA]
    }
    
    @objc private func showAddCallViewController(_ sender: UIView) {
        
        var items = [ActionSheetItem]()
        
        let sectionA = ActionSheetSectionTitle(title: "Add Call For")
        items.append(sectionA)
        
        let itemPredefined = ActionSheetItem(title: CallForType.PreDefinedStrategies.title(), value: CallForType.PreDefinedStrategies.rawValue, image: UIImage(named: "icon_predefined"))
        items.append(itemPredefined)
        
        let itemCustom = ActionSheetItem(title: CallForType.CustomStrategy.title(), value: CallForType.CustomStrategy.rawValue, image: UIImage(named: "icon_custom"))
        items.append(itemCustom)
        
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            guard let callForType = CallForType(rawValue: value) else { return }
            
            let vc = AddCallViewController()
            vc.callForType = callForType
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
    }
    
    private func showOpenCallsViewController() {
        self.navigationController?.pushViewController(OpenCallsViewController(), animated: true)
    }
    
    private func showCallHistoryViewController() {
        self.navigationController?.pushViewController(CallHistoryViewController(), animated: true)
    }
    
    private func showMarketUpdateViewController() {
        self.navigationController?.pushViewController(MarketUpdateViewController(), animated: true)
    }
    
    private func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect.zero, title: "Quantsapp", subTitle: "PARTNER APP", leftbuttonImage: nil, rightButtonImage: UIImage(named: "icon_logout"))
        osoNavBar.translatesAutoresizingMaskIntoConstraints = false
        osoNavBar.delegate = self
        view.addSubview(osoNavBar)
        
        osoNavBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        osoNavBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        osoNavBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        osoNavBar.heightAnchor.constraint(equalToConstant: Constants.OSONavigationBarConstants.barHeight).isActive = true
    }
    
    private func confirmLogout() {
        let dialog = ZAlertView(title: "Confirm",
                                message: "Do you want to logout?",
                                isOkButtonLeft: false,
                                okButtonText: "No",
                                cancelButtonText: "Yes",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
        }) { (alertView) in
            SessionTokenManager.sharedInstance.setToken(token: nil)
            SessionTokenManager.sharedInstance.setUsername(username: nil)
            alertView.dismissAlertView()
            self.navigationController?.popViewController(animated: true)
        }
        dialog.show()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

// MARK:- OSOMenuDelegate

extension HomeViewController: OSOMenuDelegate {
    func menuItemSelected(menuItem: OSOMenuItem, sender: UIView) {
        if let title = menuItem.title {
            switch title {
            case "Add Call":
                showAddCallViewController(sender)
            case "Open Calls":
                showOpenCallsViewController()
            case "Call History":
                showCallHistoryViewController()
            case "Market Update":
                showMarketUpdateViewController()
            default:
                ()
            }
        }
    }
}

// MARK:- OSONavigationBarDelegate

extension HomeViewController: OSONavigationBarDelegate {
    func leftButtonTapped(sender: OSONavigationBar) {
        //
    }
    
    func rightButtonTapped(sender: OSONavigationBar) {
        confirmLogout()
    }
}
