//
//  CallHistoryViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import ZAlertView

class CallHistoryViewController: UIViewController {

    private var osoNavBar: OSONavigationBar!
    
    // step progress
    var osoStepProgress: OSOStepProgress!
    
    private var stepPages: CallHistoryStepsPages?
    
    private var fieldHeight: CGFloat = 30
    
    private let stepperContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // navigation bar
        addOSONavigationBar()
        
        // initial setup of all views
        setupViews()
    }
    
    
    private func setupViews() {
        // stepperContainerView
        view.addSubview(stepperContainerView)
        stepperContainerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        stepperContainerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        stepperContainerView.topAnchor.constraint(equalTo: osoNavBar.bottomAnchor, constant: 0).isActive = true
        stepperContainerView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        // osoStepProgress
        osoStepProgress = OSOStepProgress(steps: ["Positions", "Details"])
        osoStepProgress.translatesAutoresizingMaskIntoConstraints = false
        stepperContainerView.addSubview(osoStepProgress)
        osoStepProgress.leftAnchor.constraint(equalTo: stepperContainerView.leftAnchor, constant: 0).isActive = true
        osoStepProgress.rightAnchor.constraint(equalTo: stepperContainerView.rightAnchor, constant: 0).isActive = true
        osoStepProgress.topAnchor.constraint(equalTo: stepperContainerView.topAnchor, constant: 0).isActive = true
        osoStepProgress.bottomAnchor.constraint(equalTo: stepperContainerView.bottomAnchor, constant: 0).isActive = true
        
        // stepsPageVC
        let stepsPageVC = CallHistoryStepsPages(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        stepsPageVC.osoNavBar = osoNavBar
        stepsPageVC.osoStepProgress = osoStepProgress
        addChild(stepsPageVC)
        view.addSubview(stepsPageVC.view)
        
        stepsPageVC.view.translatesAutoresizingMaskIntoConstraints = false
        stepsPageVC.view.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        stepsPageVC.view.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        stepsPageVC.view.topAnchor.constraint(equalTo: stepperContainerView.bottomAnchor, constant: 0).isActive = true
        if #available(iOS 11.0, *) {
            stepsPageVC.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            stepsPageVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -49).isActive = true
        }
        
        stepsPageVC.didMove(toParent: self)
        stepPages = stepsPageVC
    }
    
    @objc private func showBookTypePicker(_ sender: DropdownButton) {
        print("► showBookTypePicker()")
    }
    
    private func addOSONavigationBar() {
        osoNavBar = OSONavigationBar(frame: CGRect.zero, title: "Call History", subTitle: nil, leftbuttonImage: UIImage(named: "icon_left"), rightButtonImage: UIImage(named: "icon_refresh"))
        osoNavBar.translatesAutoresizingMaskIntoConstraints = false
        osoNavBar.delegate = self
        view.addSubview(osoNavBar)
        
        osoNavBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        osoNavBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        osoNavBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        osoNavBar.heightAnchor.constraint(equalToConstant: Constants.OSONavigationBarConstants.barHeight).isActive = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK:- OSONavigationBarDelegate

extension CallHistoryViewController: OSONavigationBarDelegate {
    
    func leftButtonTapped(sender: OSONavigationBar) {
        // ask for confirmation
        let dialog = ZAlertView(title: "Confirm",
                                message: "Do you want to exit the Call History?",
                                isOkButtonLeft: false,
                                okButtonText: "No",
                                cancelButtonText: "Yes",
                                okButtonHandler: { (alertView) in
                                    alertView.dismissAlertView()
        }) { (alertView) in
            alertView.dismissAlertView()
            self.navigationController?.popViewController(animated: true)
        }
        dialog.show()
    }
    
    func rightButtonTapped(sender: OSONavigationBar) {
        if let sp = stepPages {
            sp.showOptions()
        }
    }
}
