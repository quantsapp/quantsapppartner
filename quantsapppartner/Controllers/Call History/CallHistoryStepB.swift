//
//  CallHistoryStepB.swift
//  quantsapppartner
//
//  Created by Quantsapp on 29/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SVProgressHUD
import ZAlertView
import Sheeeeeeeeet
import SwifterSwift

protocol CallHistoryStepBDelegate: class {
    func stepBErrorOccurred()
}

extension CallHistoryStepBDelegate {
    func stepBErrorOccurred() {}
}

class CallHistoryStepB: UIViewController {
    
    weak var delegate: CallHistoryStepBDelegate?

    // navigation bar
    private var osoNavBar: OSONavigationBar!
    
    var viewAppeared: Bool = false
    private var screenWidth = SwifterSwift.screenWidth
    
    public var type: String?
    
    public var analyzerSummary: AnalyzerSummaryList?
    private var positionDetails: PositionDetails?
    private var managerDetailLegs = [ManagerDetailLegs]()
    private var lot: Int = -1
    
    private var expired: Int = 0
    private var notExpired: Int = 0
    
    // constraints
    private var buttonsContainerBottomAnchorConstraint: NSLayoutConstraint!
    
    let buttonsContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    private let detailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // setup views
        setupViews()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !viewAppeared {
            viewAppeared = true
            
            // get position details
            initPosition()
        }
    }
    
    private func setupViews() {
        
        // buttonsContainer
        view.addSubview(buttonsContainer)
        buttonsContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        buttonsContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        buttonsContainer.heightAnchor.constraint(equalToConstant: 64).isActive = true
        buttonsContainerBottomAnchorConstraint = NSLayoutConstraint(item: buttonsContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 64)
        buttonsContainerBottomAnchorConstraint.isActive = true
        
        
        
        // detailsTableView
        view.addSubview(detailsTableView)
        detailsTableView.isHidden = true
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        detailsTableView.backgroundColor = UIColor.clear
        detailsTableView.separatorStyle = .none
        detailsTableView.scrollsToTop = true
        detailsTableView.register(CallHistoryStepBCell1.self, forCellReuseIdentifier: CallHistoryStepBCell1.cellIdentifier)
        detailsTableView.register(CallHistoryStepBCell2.self, forCellReuseIdentifier: CallHistoryStepBCell2.cellIdentifier)
        detailsTableView.tableFooterView = UIView()
        detailsTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        detailsTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        detailsTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        detailsTableView.bottomAnchor.constraint(equalTo: buttonsContainer.topAnchor, constant: 0).isActive = true
    }
    
    private func initPosition() {
        if let summary = analyzerSummary {
            /*if let symbol = summary.symbol {
                
                // get lot for symbol
                lot = DatabaseManager.sharedInstance.getLot(forSymbol: symbol, inTable: Constants.DBTables.OptMaster)
                print("❖ Lot for \"\(symbol)\" is \(lot)")
                
                getUpdatedCMP(symbol: symbol)
            }*/
            
            calculatePnL(summary: summary)
        }
    }
    
    private func getUpdatedCMP(symbol: String) {
        if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbol) {
            
            SVProgressHUD.show()
            
            let instrument = "\(symbol.uppercased())_\(expiry)_1"
            
            var counter: Int = 0
            
            let dispatchGroup = DispatchGroup()
            let dispatchQueue = DispatchQueue(label: "oso_cmp")
            let dispatchSemaphore = DispatchSemaphore(value: 0)
            
            dispatchQueue.async {
                dispatchGroup.enter()
                
                OSOWebService.sharedInstance.getPrice(forInstrument: instrument) { (error, price) in
                    if let priceValue = price {
                        if let summary = self.analyzerSummary {
                            summary.cmp = priceValue.roundToDecimal(2).toString()
                        }
                    }
                    
                    counter += 1
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                }
                
                dispatchSemaphore.wait()
            }
            
            dispatchGroup.notify(queue: dispatchQueue) {
                print("Finished for \(dispatchQueue.label)")
                if counter == 1 && dispatchQueue.label == "oso_cmp" {
                    if let summary = self.analyzerSummary {
                        self.getInstruments(summary: summary)
                    }
                }
            }
            
        }
    }
    
    private func getInstruments(summary: AnalyzerSummaryList) {
        var instruments = [String]()
        
        SVProgressHUD.show()
        
        if let positionNo = summary.positionNo {
            DatabaseManager.sharedInstance.getPositionDetailsFromCallHistory(positionNo: positionNo, completion: { (error, positions) in
                if let err = error {
                    print("Error: \(err.localizedDescription)")
                } else {
                    if let positionsList = positions {
                        for pos in positionsList {
                            if let symbol = summary.symbol, let expiry = pos.expiry, let instrument = pos.instrument, let strike = pos.strike, let optType = pos.optType {
                                var instrumentString = "\(symbol)_\(expiry)_1"
                                if instrument == "Opt" {
                                    instrumentString.append("_\(optType)_\(strike)")
                                }
                                
                                if !instruments.contains(instrumentString) {
                                    instruments.append(instrumentString)
                                }
                            }
                        }
                        
                        self.getInstrumentPrice(instruments: instruments, summary: summary)
                    }
                }
            })
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    private func getInstrumentPrice(instruments: [String], summary: AnalyzerSummaryList) {
        var counter: Int = 0
        var instrumentsAndCmp: [String: Double] = [:]
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "oso_cmp")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async {
            
            dispatchGroup.enter()
            
            OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, priceList, oiList, volumeList) in
                if let priceArray = priceList {
                    print("priceArray[\(priceArray.count)] = \(priceArray)")
                    for i in 0..<instruments.count {
                        instrumentsAndCmp[instruments[i]] = priceArray[i]
                    }
                }
                
                counter += 1
                dispatchSemaphore.signal()
                dispatchGroup.leave()
            }
            
            dispatchSemaphore.wait()
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == 1 && dispatchQueue.label == "oso_cmp" {
                self.calculateMTM(summary: summary, instrumentsAndCmp: instrumentsAndCmp)
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    private func calculateMTM(summary: AnalyzerSummaryList, instrumentsAndCmp: [String: Double]) {
        var counter: Int = 0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "oso_mtm")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async {
            if let positionNo = summary.positionNo {
                
                dispatchGroup.enter()
                
                DatabaseManager.sharedInstance.getPositionDetailsFromCallHistory(positionNo: positionNo, completion: { (error, positions) in
                    if let err = error {
                        print("Error: \(err.localizedDescription)")
                    } else {
                        if let positionsList = positions {
                            
                            var totalMtm: Double = 0
                            
                            self.managerDetailLegs.removeAll()
                            
                            for pos in positionsList {
                                
                                let leg = ManagerDetailLegs()
                                
                                if let instrument = pos.instrument {
                                    leg.instrument = instrument
                                }
                                
                                if let expiry = pos.expiry {
                                    leg.expiry = expiry
                                }
                                
                                if let strike = pos.strike {
                                    leg.strike = strike
                                }
                                
                                if let optType = pos.optType {
                                    leg.optType = optType
                                }
                                
                                if let expired = pos.expired {
                                    leg.expired = expired
                                    
                                    if expired == "1" {
                                        self.expired += 1
                                    } else {
                                        self.notExpired += 1
                                    }
                                }
                                
                                if let qty = pos.qty {
                                    leg.qty = qty
                                }
                                
                                if let price = pos.price {
                                    leg.price = price
                                }
                                
                                if let papi = pos.Papi, let symbol = pos.symbol {
                                    print("\(positionNo): \(papi)")
                                    
                                    let splitPapi = papi.components(separatedBy: ",")
                                    
                                    if let qty = splitPapi[2].double(), let price = splitPapi[1].double() {
                                        let fullInstrument = splitPapi[0].components(separatedBy: " ")
                                        var instrumentString = "\(symbol)_\(fullInstrument[1])_1"
                                        if fullInstrument[0] == "Opt" {
                                            instrumentString.append("_\(fullInstrument[3])_\(fullInstrument[2])")
                                        }
                                        
                                        if let cmp = instrumentsAndCmp[instrumentString] {
                                            
                                            leg.cmp = cmp.roundToDecimal(2).toString()
                                            print("\(instrumentString) (CMP): \(cmp)")
                                            
                                            var mtm: Double = 0
                                            
                                            mtm = (cmp * qty) - (price * qty)
                                            
                                            leg.mtm = mtm.roundToDecimal(2).toString()
                                            
                                            totalMtm += mtm
                                        }
                                    }
                                }
                                
                                self.managerDetailLegs.append(leg)
                            }
                            
                            print("total MTM = \(totalMtm)")
                            summary.mtm = totalMtm.roundToDecimal(0).toString()
                        }
                    }
                    
                    counter += 1
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                })
                
                dispatchSemaphore.wait()
            } else {
                SVProgressHUD.dismiss()
            }
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == 1 && dispatchQueue.label == "oso_mtm" {
                
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.detailsTableView.isHidden = false
                    self.detailsTableView.reloadData()
                }
            }
        }
    }
    
    private func calculatePnL(summary: AnalyzerSummaryList) {
        var counter: Int = 0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "oso_pnl")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async {
            if let positionNo = summary.positionNo {
                
                dispatchGroup.enter()
                
                DatabaseManager.sharedInstance.getPositionDetailsFromCallHistory(positionNo: positionNo, completion: { (error, positions) in
                    if let err = error {
                        print("Error: \(err.localizedDescription)")
                    } else {
                        if let positionsList = positions {
                            
                            var totalPnl: Double = 0
                            
                            self.managerDetailLegs.removeAll()
                            
                            for pos in positionsList {
                                
                                let leg = ManagerDetailLegs()
                                
                                if let instrument = pos.instrument {
                                    leg.instrument = instrument
                                }
                                
                                if let expiry = pos.expiry {
                                    leg.expiry = expiry
                                }
                                
                                if let strike = pos.strike {
                                    leg.strike = strike
                                }
                                
                                if let optType = pos.optType {
                                    leg.optType = optType
                                }
                                
                                if let expired = pos.expired {
                                    leg.expired = expired
                                    
                                    if expired == "1" {
                                        self.expired += 1
                                    } else {
                                        self.notExpired += 1
                                    }
                                }
                                
                                if let qty = pos.qty {
                                    leg.qty = qty
                                }
                                
                                if let price = pos.initiationPrice {
                                    leg.price = price
                                }
                                
                                if let price = pos.closingPrice {
                                    leg.cmp = price
                                }
                                
                                if let fixedProfit = (pos.fixedProfit)?.double() {
                                    print("\(fixedProfit): \(fixedProfit)")
                                    
                                    totalPnl += fixedProfit
                                    leg.mtm = fixedProfit.roundToDecimal(2).toString()
                                }
                                
                                self.managerDetailLegs.append(leg)
                            }
                            
                            print("total PnL = \(totalPnl)")
                            summary.mtm = totalPnl.roundToDecimal(0).toString()
                        }
                    }
                    
                    counter += 1
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                })
                
                dispatchSemaphore.wait()
            } else {
                SVProgressHUD.dismiss()
            }
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == 1 && dispatchQueue.label == "oso_pnl" {
                
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.detailsTableView.isHidden = false
                    self.detailsTableView.reloadData()
                }
            }
        }
    }
    
    
    func createGradientLayer(topColor:UIColor, bottomColor:UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        view.layer.addSublayer(gradientLayer)
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
}

// MARK:- UITableViewDataSource

extension CallHistoryStepB: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return managerDetailLegs.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CallHistoryStepBCell1.cellIdentifier, for: indexPath) as! CallHistoryStepBCell1
            
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            
            if let summary = analyzerSummary {
                
                // date created
                if let createdOn = summary.dateCreated {
                    let date = formatter.string(from: createdOn)
                    let attrString = NSMutableAttributedString()
                    attrString.append(NSAttributedString(string: "Created On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                    attrString.append(NSAttributedString(string: "\(date)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                    cell.lblDateCreated.attributedText = attrString
                } else {
                    let attrString = NSMutableAttributedString()
                    attrString.append(NSAttributedString(string: "Created On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                    attrString.append(NSAttributedString(string: "NA", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                    cell.lblDateCreated.attributedText = attrString
                }
                
                // date closed
                if let closedOn = summary.dateClosed {
                    let date = formatter.string(from: closedOn)
                    let attrString = NSMutableAttributedString()
                    attrString.append(NSAttributedString(string: "Closed On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                    attrString.append(NSAttributedString(string: "\(date)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                    cell.lblDateClosed.attributedText = attrString
                } else {
                    let attrString = NSMutableAttributedString()
                    attrString.append(NSAttributedString(string: "Closed On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                    attrString.append(NSAttributedString(string: "NA", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                    cell.lblDateClosed.attributedText = attrString
                }
                
                // PnL
                if let pnl = summary.mtm {
                    cell.lblPnLValue.text = pnl
                    
                    if let pnlDbl = pnl.double() {
                        if pnlDbl > 0 {
                            cell.lblPnLValue.textColor = Constants.Charts.chartGreenColor
                        } else if pnlDbl < 0 {
                            cell.lblPnLValue.textColor = Constants.Charts.chartRedColor
                        } else {
                            cell.lblPnLValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                        }
                    } else {
                        cell.lblPnLValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                    }
                } else {
                    cell.lblPnLValue.text = "-"
                    cell.lblPnLValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                }
                
                // target spread
                if let spread = summary.spreadTarget {
                    if let dblVal = spread.double() {
                        cell.lblSpreadTargetValue.text = dblVal.roundToDecimal(2).toString()
                    } else {
                        cell.lblSpreadTargetValue.text = spread
                    }
                } else {
                    cell.lblSpreadTargetValue.text = "-"
                }
                
                // stop loss spread
                if let spread = summary.spreadStopLoss {
                    if let dblVal = spread.double() {
                        cell.lblSpreadStopLossValue.text = dblVal.roundToDecimal(2).toString()
                    } else {
                        cell.lblSpreadStopLossValue.text = spread
                    }
                } else {
                    cell.lblSpreadStopLossValue.text = "-"
                }
                
                // Margin
                /*if let margin = summary.margin {
                    cell.lblMarginValue.text = margin
                } else {
                    cell.lblMarginValue.text = "-"
                }*/
            } else {
                cell.lblDateCreated.text = "-"
                cell.lblDateClosed.text = "-"
                cell.lblPnLValue.text = "-"
                cell.lblSpreadTargetValue.text = "-"
                cell.lblSpreadStopLossValue.text = "-"
                //cell.lblMarginValue.text = "-"
            }
            
            return cell
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CallHistoryStepBCell2.cellIdentifier, for: indexPath) as! CallHistoryStepBCell2
            
            let leg = self.managerDetailLegs[indexPath.row]
            
            if let closePrice = leg.cmp {
                if let dblVal = closePrice.double() {
                    cell.lblClosePriceValue.text = dblVal.roundToDecimal(2).toString()
                } else {
                    cell.lblClosePriceValue.text = closePrice
                }
            } else {
                cell.lblClosePriceValue.text = "-"
            }
            
            if let pnl = leg.mtm {
                cell.lblPnLValue.text = pnl
            } else {
                cell.lblPnLValue.text = "-"
            }
            
            if let instrument = leg.instrument {
                cell.lblInstrumentValue.text = instrument
            } else {
                cell.lblInstrumentValue.text = "-"
            }
            
            if let expiry = leg.expiry {
                cell.lblExpiryValue.text = expiry
            } else {
                cell.lblExpiryValue.text = "-"
            }
            
            if let strike = leg.strike {
                cell.lblStrikeValue.text = strike == "-1" ? "-" : strike
            } else {
                cell.lblStrikeValue.text = "-"
            }
            
            if let optType = leg.optType {
                cell.lblOptionTypeValue.text = optType
            } else {
                cell.lblOptionTypeValue.text = "-"
            }
            
            if let price = leg.price {
                if let dblVal = price.double() {
                    cell.lblPriceValue.text = dblVal.roundToDecimal(2).toString()
                } else {
                    cell.lblPriceValue.text = price
                }
            } else {
                cell.lblPriceValue.text = "-"
            }
            
            if let qty = leg.qty {
                cell.lblQuantityValue.text = qty
            } else {
                cell.lblQuantityValue.text = "-"
            }
            
            //cell.bottomBorder.isHidden = indexPath.row == self.managerDetailLegs.count - 1 ? true : false
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
    }
}

// MARK:- UITableViewDelegate

extension CallHistoryStepB: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 10, height: 30))
        header.backgroundColor = UIColor(white: 0.0, alpha: 0.9)
        
        if section == 0 {
            if let summary = self.analyzerSummary {
                /*let lblTradeStatus = UILabel()
                lblTradeStatus.textColor = UIColor(white: 1.0, alpha: 0.5)
                lblTradeStatus.font = UIFont.systemFont(ofSize: 13)
                lblTradeStatus.textAlignment = .right
                lblTradeStatus.translatesAutoresizingMaskIntoConstraints = false
                
                var tradeStatus: String = ""
                
                if let status = summary.tradeStatus {
                    tradeStatus = status
                }
                
                if self.notExpired == 0 {
                    lblTradeStatus.text = "Expired"
                    lblTradeStatus.textColor = Constants.Charts.chartRedColor
                } else if self.notExpired > 0 && self.expired > 0 {
                    let tempStatus = tradeStatus == "1" ? "Traded" : "Tracking"
                    lblTradeStatus.text = "\(tempStatus): Partially Expired"
                    lblTradeStatus.textColor = UIColor.red
                } else if tradeStatus == "1" {
                    lblTradeStatus.text = "Traded"
                    lblTradeStatus.textColor = Constants.Charts.chartGreenColor
                } else {
                    lblTradeStatus.text = "Tracking"
                    lblTradeStatus.textColor = UIColor.yellow
                }
                
                header.addSubview(lblTradeStatus)
                lblTradeStatus.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
                lblTradeStatus.topAnchor.constraint(equalTo: header.topAnchor, constant: 0).isActive = true
                lblTradeStatus.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: 0).isActive = true
                lblTradeStatus.widthAnchor.constraint(equalToConstant: (lblTradeStatus.text?.sizeOfString(usingFont: lblTradeStatus.font).width)! + 4).isActive = true*/
                
                let lblSymbolAndPosition = UILabel()
                lblSymbolAndPosition.adjustsFontSizeToFitWidth = true
                lblSymbolAndPosition.minimumScaleFactor = 0.8
                lblSymbolAndPosition.translatesAutoresizingMaskIntoConstraints = false
                header.addSubview(lblSymbolAndPosition)
                lblSymbolAndPosition.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 10).isActive = true
                //lblSymbolAndPosition.rightAnchor.constraint(equalTo: lblTradeStatus.leftAnchor, constant: -8).isActive = true
                lblSymbolAndPosition.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
                lblSymbolAndPosition.topAnchor.constraint(equalTo: header.topAnchor, constant: 0).isActive = true
                lblSymbolAndPosition.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: 0).isActive = true
                
                if let symbol = summary.symbol, let positionName = summary.positionName {
                    let attrString = NSMutableAttributedString()
                    attrString.append(NSAttributedString(string: "\(symbol) ", attributes: [NSAttributedString.Key.foregroundColor: Constants.UIConfig.themeColor, NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13)]))
                    attrString.append(NSAttributedString(string: " \(positionName)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.75)]))
                    
                    lblSymbolAndPosition.attributedText = attrString
                }
            }
            
        } else if section == 1 {
            let lblPositions = UILabel()
            lblPositions.text = "POSITIONS"
            lblPositions.font = UIFont.boldSystemFont(ofSize: 12)
            lblPositions.textColor = UIColor(white: 1.0, alpha: 0.7)
            lblPositions.translatesAutoresizingMaskIntoConstraints = false
            header.addSubview(lblPositions)
            lblPositions.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 10).isActive = true
            lblPositions.heightAnchor.constraint(equalToConstant: 20).isActive = true
            lblPositions.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
            lblPositions.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
        }
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 90
        case 1:
            return 76
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
