//
//  CallHistoryStepA.swift
//  quantsapppartner
//
//  Created by Quantsapp on 29/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SVProgressHUD
import ZAlertView
import Sheeeeeeeeet
import ActionSheetPicker_3_0

protocol CallHistoryStepADelegate: class {
    func stepACompleted(summary: AnalyzerSummaryList, book: OSOBook?)
}

private enum SortPositionsBy: String {
    case DateCreated = "1"
    case TradeStatus = "2"
    case Symbol = "3"
    case MTM = "4"
    case CMP = "5"
    case ROI = "6"
    case Margin = "7"
    case DateClosed = "8"
    case PnL = "9"
}


class CallHistoryStepA: UIViewController {
    
    weak var delegate: CallHistoryStepADelegate?

    var viewAppeared: Bool = false
    
    private var allBooks = DatabaseManager.sharedInstance.getBooks()
    private var selectedBook: OSOBook?
    
    private var syncAttempts: Int = 0
    
    public var updateType: String = "0"
    private var sorting: String = "1"
    
    private var sortBy = ["Date Created", "Date Closed", "Symbol" ,"MTM" ,"PnL"]
    private var sortByMaxWidth: CGFloat = 0
    
    private var analyzerSummaryList: [AnalyzerSummaryList]?
    
    //constraints
    private var mtmContainerHeightAnchorConstraint: NSLayoutConstraint!
    
    private let lblBookType: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.text = "Book Type"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dropdownBookType: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.5), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.layer.cornerRadius = 4
        button.layer.borderColor = UIColor(white: 1.0, alpha: 0.12).cgColor
        button.layer.borderWidth = 0.5
        button.layer.masksToBounds = true
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.08)
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let borderBookType: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let btnFilter: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.setImage(UIImage(named: "icon_filter")?.withRenderingMode(.alwaysTemplate), for: [])
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionShowFilter(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let lblMessage: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let mtmContainer: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let mtmHeaderView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.9)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblMTM: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "MTM"
        label.textColor = UIColor(white: 1.0, alpha: 0.7)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDataTime: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTraded: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.text = "Traded"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTradedValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "0"
        label.textAlignment = .right
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTracking: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.text = "Tracking"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTrackingValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "0"
        label.textAlignment = .right
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblExpired: UILabel = {
        let label = UILabel()
        label.font = Constants.UIConfig.fieldLabelFont
        label.text = "Expired"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblExpiredValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "0"
        label.textAlignment = .right
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let positionsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    /*private lazy var refreshControl: UIRefreshControl = {
     let rc = UIRefreshControl()
     rc.tintColor = Constants.UIConfig.themeColor
     return rc
     }()*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // init OSOBook
        if let book = allBooks.first {
            selectedBook = book
        }
        
        // get sorting
        self.sorting = DatabaseManager.sharedInstance.getContraintsSettingForKey(keyName: "manSetSorting")
        self.sortByMaxWidth = maxWidthForSortBy(sortByTextArray: self.sortBy, sortTextFont: UIFont.systemFont(ofSize: 12))
        
        // setup views
        setupViews()
        
        // sync/update MTM
        if self.updateType == "1" {
            syncAndUpdateMTM()
        } else {
            //updateMTM()
            showZAlertView(withTitle: "Error", andMessage: "Nothing to update", cancelTitle: "OK", isError: true, completion: {})
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !viewAppeared {
            viewAppeared = true
        } /*else {
         //updateMTM()
         syncAndUpdateMTM()
         }*/
        
    }
    
    private func setupViews() {
        
        // MARK:- lblBookType
        // lblBookType
        view.addSubview(lblBookType)
        lblBookType.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        lblBookType.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        lblBookType.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblBookType.widthAnchor.constraint(equalToConstant: (lblBookType.text?.sizeOfString(usingFont: lblBookType.font).width)! + 8).isActive = true
        
        if Constants.APPConfig.inDevelopment {
            // btnFilter
            view.addSubview(btnFilter)
            btnFilter.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
            btnFilter.heightAnchor.constraint(equalToConstant: 30).isActive = true
            btnFilter.widthAnchor.constraint(equalToConstant: 30).isActive = true
            btnFilter.centerYAnchor.constraint(equalTo: lblBookType.centerYAnchor, constant: 0).isActive = true
        }
        
        // dropdownBookType
        view.addSubview(dropdownBookType)
        if let book = selectedBook {
            if let bookTitle = book.title {
                DispatchQueue.main.async {
                    self.dropdownBookType.setTitle(bookTitle, for: .normal)
                    self.dropdownBookType.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
                }
                
            } else {
                dropdownBookType.setTitle("Select Book Type", for: .normal)
            }
        } else {
            dropdownBookType.setTitle("Select Book Type", for: .normal)
        }
        
        dropdownBookType.setTitle("Select Book Type", for: .normal)
        dropdownBookType.addTarget(self, action: #selector(showBookTypePicker(_:)), for: .touchUpInside)
        dropdownBookType.leftAnchor.constraint(equalTo: lblBookType.rightAnchor, constant: 8).isActive = true
        dropdownBookType.centerYAnchor.constraint(equalTo: lblBookType.centerYAnchor, constant: 0).isActive = true
        dropdownBookType.rightAnchor.constraint(equalTo: Constants.APPConfig.inDevelopment ? btnFilter.leftAnchor : view.rightAnchor, constant: -10).isActive = true
        dropdownBookType.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        // borderBookType
        /*view.addSubview(borderBookType)
         borderBookType.leftAnchor.constraint(equalTo: dropdownBookType.leftAnchor, constant: 0).isActive = true
         borderBookType.rightAnchor.constraint(equalTo: dropdownBookType.rightAnchor, constant: 0).isActive = true
         borderBookType.bottomAnchor.constraint(equalTo: dropdownBookType.bottomAnchor, constant: 0).isActive = true
         borderBookType.heightAnchor.constraint(equalToConstant: 1).isActive = true*/
        
        // MARK:- lblMessage
        view.addSubview(lblMessage)
        lblMessage.isHidden = true
        lblMessage.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        lblMessage.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        lblMessage.topAnchor.constraint(equalTo: lblBookType.bottomAnchor, constant: 10).isActive = true
        lblMessage.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
        
        // MARK: mtmContainer
        view.addSubview(mtmContainer)
        mtmContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        mtmContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        mtmContainer.topAnchor.constraint(equalTo: lblBookType.bottomAnchor, constant: 10).isActive = true
        mtmContainerHeightAnchorConstraint = NSLayoutConstraint(item: mtmContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        mtmContainerHeightAnchorConstraint.isActive = true
        
        // mtmHeaderView
        mtmContainer.addSubview(mtmHeaderView)
        mtmHeaderView.leftAnchor.constraint(equalTo: mtmContainer.leftAnchor, constant: 0).isActive = true
        mtmHeaderView.rightAnchor.constraint(equalTo: mtmContainer.rightAnchor, constant: 0).isActive = true
        mtmHeaderView.topAnchor.constraint(equalTo: mtmContainer.topAnchor, constant: 0).isActive = true
        mtmHeaderView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        // lblMTM
        mtmHeaderView.addSubview(lblMTM)
        lblMTM.leftAnchor.constraint(equalTo: mtmHeaderView.leftAnchor, constant: 10).isActive = true
        lblMTM.widthAnchor.constraint(equalToConstant: (lblMTM.text?.sizeOfString(usingFont: lblMTM.font).width)! + 4).isActive = true
        lblMTM.topAnchor.constraint(equalTo: mtmHeaderView.topAnchor, constant: 0).isActive = true
        lblMTM.bottomAnchor.constraint(equalTo: mtmHeaderView.bottomAnchor, constant: 0).isActive = true
        
        // lblDataTime
        mtmHeaderView.addSubview(lblDataTime)
        lblDataTime.leftAnchor.constraint(equalTo: lblMTM.rightAnchor, constant: 8).isActive = true
        lblDataTime.rightAnchor.constraint(equalTo: mtmHeaderView.rightAnchor, constant: -10).isActive = true
        lblDataTime.topAnchor.constraint(equalTo: mtmHeaderView.topAnchor, constant: 0).isActive = true
        lblDataTime.bottomAnchor.constraint(equalTo: mtmHeaderView.bottomAnchor, constant: 0).isActive = true
        
        // lblTraded
        mtmContainer.addSubview(lblTraded)
        lblTraded.leftAnchor.constraint(equalTo: mtmContainer.leftAnchor, constant: 10).isActive = true
        lblTraded.topAnchor.constraint(equalTo: mtmHeaderView.bottomAnchor, constant: 0).isActive = true
        lblTraded.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblTraded.widthAnchor.constraint(equalToConstant: (lblTraded.text?.sizeOfString(usingFont: lblTraded.font).width)! + 4).isActive = true
        
        // lblTradedValue
        mtmContainer.addSubview(lblTradedValue)
        lblTradedValue.leftAnchor.constraint(equalTo: lblTraded.rightAnchor, constant: 8).isActive = true
        lblTradedValue.topAnchor.constraint(equalTo: lblTraded.topAnchor, constant: 0).isActive = true
        lblTradedValue.heightAnchor.constraint(equalTo: lblTraded.heightAnchor, multiplier: 1.0).isActive = true
        lblTradedValue.rightAnchor.constraint(equalTo: mtmContainer.rightAnchor, constant: -10).isActive = true
        
        // lblTracking
        mtmContainer.addSubview(lblTracking)
        lblTracking.leftAnchor.constraint(equalTo: mtmContainer.leftAnchor, constant: 10).isActive = true
        lblTracking.topAnchor.constraint(equalTo: lblTraded.bottomAnchor, constant: 0).isActive = true
        lblTracking.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblTracking.widthAnchor.constraint(equalToConstant: (lblTracking.text?.sizeOfString(usingFont: lblTracking.font).width)! + 4).isActive = true
        
        // lblTrackingValue
        mtmContainer.addSubview(lblTrackingValue)
        lblTrackingValue.leftAnchor.constraint(equalTo: lblTracking.rightAnchor, constant: 8).isActive = true
        lblTrackingValue.topAnchor.constraint(equalTo: lblTracking.topAnchor, constant: 0).isActive = true
        lblTrackingValue.heightAnchor.constraint(equalTo: lblTracking.heightAnchor, multiplier: 1.0).isActive = true
        lblTrackingValue.rightAnchor.constraint(equalTo: mtmContainer.rightAnchor, constant: -10).isActive = true
        
        // lblExpired
        mtmContainer.addSubview(lblExpired)
        lblExpired.leftAnchor.constraint(equalTo: mtmContainer.leftAnchor, constant: 10).isActive = true
        lblExpired.topAnchor.constraint(equalTo: lblTracking.bottomAnchor, constant: 0).isActive = true
        lblExpired.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblExpired.widthAnchor.constraint(equalToConstant: (lblExpired.text?.sizeOfString(usingFont: lblExpired.font).width)! + 4).isActive = true
        
        // lblExpiredValue
        mtmContainer.addSubview(lblExpiredValue)
        lblExpiredValue.leftAnchor.constraint(equalTo: lblExpired.rightAnchor, constant: 8).isActive = true
        lblExpiredValue.topAnchor.constraint(equalTo: lblExpired.topAnchor, constant: 0).isActive = true
        lblExpiredValue.heightAnchor.constraint(equalTo: lblExpired.heightAnchor, multiplier: 1.0).isActive = true
        lblExpiredValue.rightAnchor.constraint(equalTo: mtmContainer.rightAnchor, constant: -10).isActive = true
        
        // positionsTableView
        view.addSubview(positionsTableView)
        positionsTableView.isHidden = true
        positionsTableView.dataSource = self
        positionsTableView.delegate = self
        positionsTableView.backgroundColor = UIColor.clear
        positionsTableView.separatorStyle = .none
        positionsTableView.scrollsToTop = true
        positionsTableView.register(CallHistoryStepACell.self, forCellReuseIdentifier: CallHistoryStepACell.cellIdentifier)
        positionsTableView.tableFooterView = UIView()
        //positionsTableView.addSubview(self.refreshControl)
        //refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        positionsTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        positionsTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        positionsTableView.topAnchor.constraint(equalTo: mtmContainer.bottomAnchor, constant: 0).isActive = true
        positionsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
    
    /*@objc private func handleRefresh(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        updateMTM()
    }*/
    
    public func refresh() {
        syncAndUpdateMTM()
        //updateMTM()
    }
    
    @objc private func syncAndUpdateMTM() {
        
        guard let bookType = selectedBook?.bookType else { return }
        
        analyzerSummaryList = nil
        
        SVProgressHUD.show()
        
        let API = DatabaseManager.sharedInstance.getSyncCallHistoryAPI(forBookType: bookType)
        
        DispatchQueue.main.async {
            self.positionsTableView.isHidden = true
            self.mtmContainerHeightAnchorConstraint.constant = 0
            
            self.lblMessage.text = "GETTING POSITIONS..."
            self.lblMessage.isHidden = false
        }
        
        OSOWebService.sharedInstance.syncCallHistory(api: API, bookType: bookType) { (response, message, mtmAPI, dataTime) in
            
            if response == -1 {
                SVProgressHUD.dismiss()
                
                if let msg = message {
                    self.showZAlertView(withTitle: "", andMessage: msg, cancelTitle: "LOGIN", isError: true, completion: {
                        self.logout()
                    })
                } else {
                    self.showZAlertView(withTitle: "", andMessage: "Invalid Session. Please login again.", cancelTitle: "LOGIN", isError: true, completion: {
                        self.logout()
                    })
                }
            } else if response == 0 {
                SVProgressHUD.dismiss()
                
                DispatchQueue.main.async {
                    self.positionsTableView.isHidden = true
                    self.lblMessage.text = "NO CLOSED CALLS FOUND"
                    self.lblMessage.isHidden = false
                }
                
                if let msg = message {
                    self.showZAlertView(withTitle: "Error", andMessage: msg, cancelTitle: "Close", isError: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                } else {
                    self.showZAlertView(withTitle: "Error", andMessage: "Something went wrong. Please try again.", cancelTitle: "Close", isError: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            } else if response == 1 {
                SVProgressHUD.dismiss()
                // No position exists
                DispatchQueue.main.async {
                    self.positionsTableView.isHidden = true
                    self.lblMessage.text = "NO CALLS CLOSED"
                    self.lblMessage.isHidden = false
                }
            } else {
                
                DatabaseManager.sharedInstance.getAnalyzerSummaryPositionsFromCallHistory(forBookType: bookType, completion: { (success, summaryList) in
                    if success {
                        if let summaries = summaryList {
                            if summaries.count == 0 {
                                SVProgressHUD.dismiss()
                                // No position exists
                                self.positionsTableView.isHidden = true
                                self.lblMessage.text = "NO CALLS CLOSED"
                                self.lblMessage.isHidden = false
                            } else {
                                self.analyzerSummaryList = summaries
                                //self.prepareInstrumentsList(summaryList: summaries)
                                self.calculatePnL(summaryList: summaries)
                            }
                            
                        } else {
                            SVProgressHUD.dismiss()
                            
                            DispatchQueue.main.async {
                                self.positionsTableView.isHidden = true
                                self.lblMessage.text = "NO CLOSED  CALLS FOUND"
                                self.lblMessage.isHidden = false
                            }
                            
                            self.showZAlertView(withTitle: "Error", andMessage: "No closed calls found", cancelTitle: "Close", isError: true, completion: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        
                        DispatchQueue.main.async {
                            self.positionsTableView.isHidden = true
                            self.lblMessage.text = "NO CLOSED  CALLS FOUND"
                            self.lblMessage.isHidden = false
                        }
                        
                        self.showZAlertView(withTitle: "Error", andMessage: "Failed to load closed calls", cancelTitle: "Close", isError: true, completion: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                })
            }
        }
    }
    
    private func prepareInstrumentsList(summaryList: [AnalyzerSummaryList]) {
        
        print("► prepareInstrumentsList()")
        
        var instruments = [String]()
        var symbols = [String]()
        
        var counter: Int = 0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "prepareInstrumentsList")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        for summary in summaryList {
            if let symbol = summary.symbol {
                if !symbols.contains(symbol) {
                    symbols.append(symbol)
                }
            }
        }
        
        for symbol in symbols {
            if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbol) {
                instruments.append("\(symbol.uppercased())_\(expiry)_1")
            }
        }
        
        dispatchQueue.async {
            for summary in summaryList {
                if let positionNo = summary.positionNo {
                    
                    dispatchGroup.enter()
                    
                    DatabaseManager.sharedInstance.getPositionDetailsFromCallHistory(positionNo: positionNo, completion: { (error, positions) in
                        if let err = error {
                            print("Error: \(err.localizedDescription)")
                        } else {
                            if let positionsList = positions {
                                for pos in positionsList {
                                    if let symbol = summary.symbol, let expiry = pos.expiry, let instrument = pos.instrument, let strike = pos.strike, let optType = pos.optType {
                                        var instrumentString = "\(symbol)_\(expiry)_1"
                                        if instrument == "Opt" {
                                            instrumentString.append("_\(optType)_\(strike)")
                                        }
                                        
                                        if !instruments.contains(instrumentString) {
                                            instruments.append(instrumentString)
                                        }
                                    }
                                }
                            }
                        }
                        
                        counter += 1
                        dispatchSemaphore.signal()
                        dispatchGroup.leave()
                    })
                    
                    dispatchSemaphore.wait()
                }
            }
        }
        
        
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished for \(dispatchQueue.label)")
            if counter == summaryList.count && dispatchQueue.label == "prepareInstrumentsList" {
                print("► symbols:\n\(symbols)")
                print("► instruments:\n\(instruments)")
                
                self.prepareInstrumentsPriceList(summaryList: summaryList, instruments: instruments)
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    private func prepareInstrumentsPriceList(summaryList: [AnalyzerSummaryList], instruments: [String]) {
        print("► prepareInstrumentsPriceList()")
        
        var instrumentsAndCmp: [String: Double] = [:]
        
        var counter: Int = 0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "prepareInstrumentsPriceList")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async {
            
            dispatchGroup.enter()
            
            OSOWebService.sharedInstance.getPrice(forInstruments: instruments) { (error, priceList, oiList, volumeList) in
                if let priceArray = priceList {
                    print("priceArray[\(priceArray.count)] = \(priceArray)")
                    for i in 0..<instruments.count {
                        instrumentsAndCmp[instruments[i]] = priceArray[i]
                    }
                }
                
                counter += 1
                dispatchSemaphore.signal()
                dispatchGroup.leave()
            }
            
            dispatchSemaphore.wait()
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == 1 && dispatchQueue.label == "prepareInstrumentsPriceList" {
                print("instrumentsAndCmp:\n\(instrumentsAndCmp)")
                
                for summary in summaryList {
                    if let symbol = summary.symbol {
                        if let expiry = DatabaseManager.sharedInstance.getExpiryFromFutMaster(forSymbol: symbol) {
                            let instrument = "\(symbol.uppercased())_\(expiry)_1"
                            
                            if let cmp = instrumentsAndCmp[instrument] {
                                summary.cmp = cmp.toString()
                                summary.tradeStatus = "1"
                            }
                        }
                    }
                }
                
                self.calculateMTM(summaryList: summaryList, instrumentsAndCmp: instrumentsAndCmp)
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    
    private func calculateMTM(summaryList: [AnalyzerSummaryList], instrumentsAndCmp: [String: Double]) {
        var counter: Int = 0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "oso_mtm")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async {
            for summary in summaryList {
                if let positionNo = summary.positionNo {
                    
                    dispatchGroup.enter()
                    
                    DatabaseManager.sharedInstance.getPositionDetailsFromCallHistory(positionNo: positionNo, completion: { (error, positions) in
                        if let err = error {
                            print("Error: \(err.localizedDescription)")
                        } else {
                            if let positionsList = positions {
                                
                                var totalMtm: Double = 0
                                
                                for pos in positionsList {
                                    if let papi = pos.Papi, let symbol = pos.symbol {
                                        print("\(positionNo): \(papi)")
                                        
                                        let splitPapi = papi.components(separatedBy: ",")
                                        
                                        if let qty = splitPapi[2].double(), let price = splitPapi[1].double() {
                                            let fullInstrument = splitPapi[0].components(separatedBy: " ")
                                            var instrumentString = "\(symbol)_\(fullInstrument[1])_1"
                                            if fullInstrument[0] == "Opt" {
                                                instrumentString.append("_\(fullInstrument[3])_\(fullInstrument[2])")
                                            }
                                            
                                            if let cmp = instrumentsAndCmp[instrumentString] {
                                                print("\(instrumentString) (CMP): \(cmp)")
                                                
                                                var mtm: Double = 0
                                                
                                                mtm = (cmp * qty) - (price * qty)
                                                
                                                /*if qty > 0 {
                                                 mtm = (cmp * qty) - (price * qty)
                                                 } else if qty < 0 {
                                                 mtm = (price * qty) - (cmp * qty)
                                                 }*/
                                                
                                                totalMtm += mtm
                                            }
                                        }
                                    }
                                }
                                
                                print("total MTM = \(totalMtm)")
                                summary.mtm = totalMtm.roundToDecimal(2).toString()
                            }
                        }
                        
                        counter += 1
                        dispatchSemaphore.signal()
                        dispatchGroup.leave()
                    })
                    
                    dispatchSemaphore.wait()
                }
            }
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == summaryList.count && dispatchQueue.label == "oso_mtm" {
                self.displaySummary(summaryList: summaryList)
            }
        }
    }
    
    private func calculatePnL(summaryList: [AnalyzerSummaryList]) {
        var counter: Int = 0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "oso_pnl")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async {
            for summary in summaryList {
                if let positionNo = summary.positionNo {
                    
                    dispatchGroup.enter()
                    
                    DatabaseManager.sharedInstance.getPositionDetailsFromCallHistory(positionNo: positionNo, completion: { (error, positions) in
                        if let err = error {
                            print("Error: \(err.localizedDescription)")
                        } else {
                            if let positionsList = positions {
                                
                                var totalPnl: Double = 0
                                
                                for pos in positionsList {
                                    if let fixedProfit = (pos.fixedProfit)?.double() {
                                        print("\(fixedProfit): \(fixedProfit)")
                                        
                                        totalPnl += fixedProfit
                                    }
                                }
                                
                                print("total PnL = \(totalPnl)")
                                summary.mtm = totalPnl.roundToDecimal(2).toString()
                            }
                        }
                        
                        counter += 1
                        dispatchSemaphore.signal()
                        dispatchGroup.leave()
                    })
                    
                    dispatchSemaphore.wait()
                }
            }
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            print("Finished \"\(dispatchQueue.label)\"")
            if counter == summaryList.count && dispatchQueue.label == "oso_pnl" {
                self.displaySummary(summaryList: summaryList)
            }
        }
    }
    
    private func displaySummary(summaryList: [AnalyzerSummaryList]) {
        print("summaryList.count = \(summaryList.count)")
        SVProgressHUD.dismiss()
        
        DispatchQueue.main.async {
            //self.positionsTableView.reloadData()
            self.positionsTableView.isHidden = false
            self.lblMessage.text = ""
            self.lblMessage.isHidden = true
            self.sortPositions(by: SortPositionsBy(rawValue: self.sorting)!)
        }
    }
    
    
    @objc private func actionShowFilter(_ sender: UIButton) {
        print("actionShowFilter()")
        let createdBy = ["User 1", "User 2", "User 3", "User 4"]
        let picker = ActionSheetStringPicker(title: "Select", rows: createdBy, initialSelection: 0, doneBlock: { (picker, value, index) in
            print(index as? String ?? "-")
        }, cancel: { (picker) in
            //
        }, origin: sender)
        
        picker?.show()
    }
    
    @objc private func showBookTypePicker(_ sender: UIButton) {
        print("actionShowFilter()")
        
        getBookType(sender: sender) { (osoBook) in
            if let book = osoBook {
                self.selectedBook = book
                
                if let bookTitle = book.title {
                    print("selectedBook = \(bookTitle)")
                    if let prevBookTitle = sender.titleLabel?.text?.lowercased() {
                        if prevBookTitle !=  bookTitle.lowercased() {
                            sender.setTitle(bookTitle, for: [])
                            SVProgressHUD.show()
                            self.perform(#selector(self.syncAndUpdateMTM), with: nil, afterDelay: 0.3)
                        }
                    } else {
                        sender.setTitle(bookTitle, for: [])
                        SVProgressHUD.show()
                        self.perform(#selector(self.syncAndUpdateMTM), with: nil, afterDelay: 0.3)
                    }
                }
            }
            
        }
    }
    
    private func getBookType(sender: UIButton, completion: @escaping((_ book: OSOBook?) -> Void)) {
        
        if allBooks.count > 0 {
            
            var bookTitles = [String]()
            var initialSelection: Int = 0
            var i: Int = 0
            
            for book in allBooks {
                if let title = book.title {
                    bookTitles.append(title)
                    
                    if let currentBookTitle = selectedBook?.title {
                        if title == currentBookTitle {
                            initialSelection = i
                        }
                    }
                }
                
                i += 1
            }
            
            if bookTitles.count > 0 {
                
                let picker = ActionSheetStringPicker(title: "Select Book Type", rows: bookTitles, initialSelection: initialSelection, doneBlock: { (picker, value, index) in
                    sender.setTitleColor(Constants.UIConfig.themeColor, for: [])
                    //completion(index as? String)
                    completion(self.allBooks[value])
                }, cancel: { (picker) in
                    //
                }, origin: sender)
                
                picker?.show()
                
            }
        }
    }
    
    @objc private func actionSortBy(_ sender: UIButton) {
        var items = [ActionSheetItem]()
        
        items.append(ActionSheetTitle(title: "Sort By"))
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        let sortByDateCreated = ActionSheetItem(title: "Date Created", value: SortPositionsBy.DateCreated.rawValue, image: nil)
        items.append(sortByDateCreated)
        
        let sortByTradeStatus = ActionSheetItem(title: "Date Closed", value: SortPositionsBy.DateClosed.rawValue, image: nil)
        items.append(sortByTradeStatus)
        
        let sortBySymbol = ActionSheetItem(title: "Symbol", value: SortPositionsBy.Symbol.rawValue, image: nil)
        items.append(sortBySymbol)
        
        let sortByMTM = ActionSheetItem(title: "MTM", value: SortPositionsBy.MTM.rawValue, image: nil)
        items.append(sortByMTM)
        
        let sortByPnL = ActionSheetItem(title: "PnL", value: SortPositionsBy.PnL.rawValue, image: nil)
        items.append(sortByPnL)
        
        //let sortByMargin = ActionSheetItem(title: "Margin", value: SortPositionsBy.Margin.rawValue, image: nil)
        //items.append(sortByMargin)
        
        
        let actionSheet = ActionSheet(items: items) { (action, item) in
            guard let value = item.value as? String else { return }
            print("selected = \(value)")
            self.sorting = value
            self.sortPositions(by: SortPositionsBy(rawValue: value)!)
        }
        
        actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
        actionSheet.present(in: self, from: sender)
    }
    
    private func sortPositions(by: SortPositionsBy) {
        
        switch by {
        case .DateCreated:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let dateCreated0 = $0.dateCreated, let dateCreated1 = $1.dateCreated else { return false }
                    return dateCreated0 > dateCreated1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { $0.dateCreated! > $1.dateCreated! })
            }
            
        case .DateClosed:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let dateClosed0 = $0.dateClosed, let dateClosed1 = $1.dateClosed else { return false }
                    return dateClosed0 > dateClosed1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { $0.dateCreated! > $1.dateCreated! })
            }
            
        case .TradeStatus:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let tradeStatus0 = $0.tradeStatus?.int, let tradeStatus1 = $1.tradeStatus?.int else { return false }
                    return tradeStatus0 < tradeStatus1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { ($0.tradeStatus?.int)! < ($1.tradeStatus?.int)! })
            }
            
        case .Symbol:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let symbol0 = $0.symbol, let symbol1 = $1.symbol else { return false }
                    return symbol0 < symbol1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { $0.symbol! < $1.symbol! })
            }
            
        case .MTM:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let mtm0 = $0.mtm?.double(), let mtm1 = $1.mtm?.double() else { return false }
                    return mtm0 > mtm1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { ($0.mtm?.double())! > ($1.mtm?.double())! })
            }
            
        case .PnL:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let mtm0 = $0.mtm?.double(), let mtm1 = $1.mtm?.double() else { return false }
                    return mtm0 > mtm1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { ($0.mtm?.double())! > ($1.mtm?.double())! })
            }
            
        case .CMP:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let cmp0 = $0.cmp?.double(), let cmp1 = $1.cmp?.double() else { return false }
                    return cmp0 > cmp1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { ($0.mtm?.double())! > ($1.mtm?.double())! })
            }
            
        case .ROI:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let roi0 = $0.roi?.replacingOccurrences(of: "%", with: "").double(), let roi1 = $1.roi?.replacingOccurrences(of: "%", with: "").double() else { return false }
                    return roi0 > roi1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { ($0.roi?.replacingOccurrences(of: "%", with: "").double())! > ($1.roi?.replacingOccurrences(of: "%", with: "").double())! })
            }
            
        case .Margin:
            if let summaries = self.analyzerSummaryList {
                
                self.analyzerSummaryList = summaries.sorted {
                    guard let margin0 = $0.margin?.double(), let margin1 = $1.margin?.double() else { return false }
                    return margin0 > margin1
                }
                
                //self.analyzerSummaryList = summaries.sorted(by: { ($0.margin?.double())! > ($1.margin?.double())! })
            }
        }
        
        self.positionsTableView.reloadData()
        
        
        //perform(#selector(tableScrollToTop), with: nil, afterDelay: 0.1)
    }
    
    @objc private func tableScrollToTop() {
        //positionsTableView.scrollToTop(animated: true)
        //self.positionsTableView.setContentOffset(.zero, animated: false)
        self.positionsTableView.scrollToRow(at: NSIndexPath(row: 0, section: 0) as IndexPath, at: .top, animated: true)
    }
}

// MARK:- UITableViewDataSource

extension CallHistoryStepA: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let summaries = self.analyzerSummaryList {
            if self.positionsTableView.isHidden && summaries.count > 0 {
                self.positionsTableView.isHidden = false
            }
            return summaries.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CallHistoryStepACell.cellIdentifier, for: indexPath) as! CallHistoryStepACell
        
        if let summaries = analyzerSummaryList {
            
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            
            if let symbol = summaries[indexPath.row].symbol, let positionName = summaries[indexPath.row].positionName {
                
                let attrString = NSMutableAttributedString()
                attrString.append(NSAttributedString(string: "\(symbol)", attributes: [NSAttributedString.Key.foregroundColor: Constants.UIConfig.themeColor, NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)]))
                attrString.append(NSAttributedString(string: " \(positionName)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.7)]))
                
                cell.lblSymbolAndPosition.attributedText = attrString
            } else {
                cell.lblSymbolAndPosition.text = "-"
            }
            
            /*if let tradeStatus = summaries[indexPath.row].tradeStatus {
                
                
                switch tradeStatus {
                case "1":
                    cell.tradeStatus = "Traded"
                    cell.lblTradeStatus.textColor = Constants.Charts.chartGreenColor
                case "2":
                    cell.tradeStatus = "Tracking"
                    cell.lblTradeStatus.textColor = UIColor.yellow
                case "3":
                    cell.tradeStatus = "Expired"
                    cell.lblTradeStatus.textColor = Constants.Charts.chartRedColor
                default:
                    cell.tradeStatus = "-"
                    cell.lblTradeStatus.textColor = UIColor(white: 1.0, alpha: 0.5)
                }
            } else {
                cell.tradeStatus = "-"
                cell.lblTradeStatus.textColor = UIColor(white: 1.0, alpha: 0.5)
            }*/
            
            // DATE CREAED
            if let dateCreated = summaries[indexPath.row].dateCreated {
                let attrString = NSMutableAttributedString()
                attrString.append(NSAttributedString(string: "Created On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                attrString.append(NSAttributedString(string: "\(dateCreated.string(withFormat: "dd-MMM-yy"))", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                cell.lblDateCreated.attributedText = attrString
            } else {
                let attrString = NSMutableAttributedString()
                attrString.append(NSAttributedString(string: "Created On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                attrString.append(NSAttributedString(string: "NA", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                cell.lblDateCreated.attributedText = attrString
            }
            
            // Date Closed On
            if let dateClosed = summaries[indexPath.row].dateClosed {
                let attrString = NSMutableAttributedString()
                attrString.append(NSAttributedString(string: "Closed On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                attrString.append(NSAttributedString(string: "\(dateClosed.string(withFormat: "dd-MMM-yy"))", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                cell.lblDateClosed.attributedText = attrString
            } else {
                let attrString = NSMutableAttributedString()
                attrString.append(NSAttributedString(string: "Closed On ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.4), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]))
                attrString.append(NSAttributedString(string: "NA", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)]))
                cell.lblDateClosed.attributedText = attrString
            }
            
            // PnL
            if let pnl = summaries[indexPath.row].mtm {
                cell.lblPnLValue.text = pnl
                cell.lblPnLValue.textColor = UIColor.white
                
                if let pnlDbl = pnl.double() {
                    if pnlDbl > 0 {
                        cell.lblPnLValue.textColor = Constants.Charts.chartGreenColor
                    } else if  pnlDbl < 0 {
                        cell.lblPnLValue.textColor = Constants.Charts.chartRedColor
                    } else {
                        cell.lblPnLValue.textColor = UIColor.white
                    }
                }
            } else {
                cell.lblPnLValue.text = "-"
                cell.lblPnLValue.textColor = UIColor(white: 1.0, alpha: 0.5)
            }
            
            // Margin
            /*if let margin = summaries[indexPath.row].margin {
                cell.lblMarginValue.text = margin
                cell.lblMarginValue.textColor = UIColor.white
            } else {
                cell.lblMarginValue.text = "-"
                cell.lblMarginValue.textColor = UIColor(white: 1.0, alpha: 0.5)
            }*/
            
            // MTM
            /*if let mtm = summaries[indexPath.row].mtm {
                if let mtmDouble = mtm.double()?.roundToDecimal(0) {
                    cell.lblMTMValue.text = mtmDouble.toString()
                    
                    if mtmDouble > 0 {
                        cell.lblMTMValue.textColor = Constants.Charts.chartGreenColor
                    } else if mtmDouble < 0 {
                        cell.lblMTMValue.textColor = Constants.Charts.chartRedColor
                    } else {
                        cell.lblMTMValue.textColor = UIColor.white
                    }
                } else {
                    cell.lblMTMValue.text = "-"
                    cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
                }
            } else {
                cell.lblMTMValue.text = "-"
                cell.lblMTMValue.textColor = UIColor(white: 1.0, alpha: 0.5)
            }*/
            
            // CMP
            /*if let cmp = summaries[indexPath.row].cmp {
                if let doubleValue = cmp.double()?.roundToDecimal(2) {
                    cell.lblCMPValue.text = doubleValue.toString()
                } else {
                    cell.lblCMPValue.text = cmp
                }
                
            } else {
                cell.lblCMPValue.text = "-"
            }*/
        }
        
        
        return cell
    }
}

// MARK:- UITableViewDelegate

extension CallHistoryStepA: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 10, height: 40))
        header.backgroundColor = UIColor(white: 0.0, alpha: 0.9)
        
        let lblHeader = UILabel()
        lblHeader.text = "POSITIONS"
        lblHeader.font = UIFont.boldSystemFont(ofSize: 12)
        lblHeader.textColor = UIColor(white: 1.0, alpha: 0.7)
        lblHeader.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(lblHeader)
        lblHeader.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 10).isActive = true
        lblHeader.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblHeader.centerYAnchor.constraint(equalTo: header.centerYAnchor, constant: 0).isActive = true
        lblHeader.widthAnchor.constraint(equalToConstant: (lblHeader.text?.sizeOfString(usingFont: lblHeader.font).width)! + 4).isActive = true
        
        let btnSortBy = SortByButton()
        btnSortBy.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        btnSortBy.titleLabel?.textAlignment = .left
        btnSortBy.contentHorizontalAlignment = .right
        btnSortBy.contentEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
        btnSortBy.setImage(UIImage(named: "icon_sort")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnSortBy.tintColor = Constants.UIConfig.themeColor
        btnSortBy.translatesAutoresizingMaskIntoConstraints = false
        btnSortBy.addTarget(self, action: #selector(actionSortBy(_:)), for: .touchUpInside)
        header.addSubview(btnSortBy)
        
        switch self.sorting {
        case "1":
            btnSortBy.setTitle("Date Created", for: .normal)
        case "2":
            btnSortBy.setTitle("Trade Status", for: .normal)
        case "3":
            btnSortBy.setTitle("Symbol", for: .normal)
        case "4":
            btnSortBy.setTitle("MTM", for: .normal)
        case "5":
            btnSortBy.setTitle("CMP", for: .normal)
        case "6":
            btnSortBy.setTitle("ROI", for: .normal)
        case "7":
            btnSortBy.setTitle("Margin", for: .normal)
        case "8":
            btnSortBy.setTitle("Date Closed", for: .normal)
        case "9":
            btnSortBy.setTitle("PnL", for: .normal)
        default:
            btnSortBy.setTitle("-", for: .normal)
        }
        
        btnSortBy.topAnchor.constraint(equalTo: header.topAnchor, constant: 0).isActive = true
        btnSortBy.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: 0).isActive = true
        btnSortBy.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -10).isActive = true
        btnSortBy.widthAnchor.constraint(equalToConstant: (btnSortBy.titleLabel?.text?.sizeOfString(usingFont: (btnSortBy.titleLabel?.font)!).width)! + 10).isActive = true
        
        
        let lblSortBy = UILabel()
        lblSortBy.text = "Sort By"
        lblSortBy.textAlignment = .right
        lblSortBy.font = UIFont.systemFont(ofSize: 12)
        lblSortBy.textColor = UIColor(white: 1.0, alpha: 0.3)
        lblSortBy.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(lblSortBy)
        lblSortBy.topAnchor.constraint(equalTo: header.topAnchor, constant: 0).isActive = true
        lblSortBy.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: 0).isActive = true
        lblSortBy.rightAnchor.constraint(equalTo: btnSortBy.leftAnchor, constant: -4).isActive = true
        lblSortBy.leftAnchor.constraint(equalTo: lblHeader.rightAnchor, constant: 8).isActive = true
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let summaries = self.analyzerSummaryList {
            delegate?.stepACompleted(summary: summaries[indexPath.row], book: selectedBook)
        }
    }
    
}

// MARK:- Helper Functions

extension CallHistoryStepA {
    
    private func maxWidthForSortBy(sortByTextArray: [String], sortTextFont: UIFont) -> CGFloat {
        
        var maxWidth: CGFloat = 0
        
        for sortText in sortByTextArray {
            
            let width = sortText.sizeOfString(usingFont: sortTextFont).width
            if width > maxWidth {
                maxWidth = width
            }
            
        }
        
        return maxWidth
    }
    
}


