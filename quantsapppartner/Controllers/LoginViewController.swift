//
//  LoginViewController.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import MessageUI
import ZAlertView
import SVProgressHUD

class LoginViewController: UIViewController {
    
    private var viewAppeared: Bool = false
    
    private let loginView: LoginView = {
        let view = LoginView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
        // add background gradient
        addGradientLayer(topColor: Constants.UIConfig.topColor, bottomColor: Constants.UIConfig.bottomColor)
        
        // initial setup of all views
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // empty tables
        emptyTables()
        
        if viewAppeared {
            if !Constants.APPConfig.inDevelopment {
                loginView.clearForm()
            }
        } else {
            viewAppeared = true
        }
        
        
    }
    
    private func setupViews() {
        // loginView
        view.addSubview(loginView)
        loginView.delegate = self
        loginView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        loginView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        loginView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        if #available(iOS 11.0, *) {
            loginView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            loginView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
    }
    
    private func emptyTables() {
        let tables = [ Constants.DBTables.StrategistResult,
                       Constants.DBTables.AnalyzerSummary,
                       Constants.DBTables.CallHistory,
                       Constants.DBTables.ErrorLog,
                       Constants.DBTables.AppLog,
                       Constants.DBTables.OptForecasts,
                       Constants.DBTables.Archive,
                       Constants.DBTables.PendingApis,
                       Constants.DBTables.Notifications,
                       Constants.DBTables.HelpCase,
                       Constants.DBTables.GcmRefresh,
                       Constants.DBTables.Books]
        
        DatabaseManager.sharedInstance.emptyTables(tables: tables) { (success, error) in
            if let err = error {
                print("Error: \(err.localizedDescription)")
            } else {
                if success {
                    print("All tables emptied successfully.")
                } else {
                    print("Something went wrong emptying tables.")
                }
            }
        }
    }
    
    private func showHome() {
        self.navigationController?.pushViewController(HomeViewController(), animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK:- LoginViewDelegate

extension LoginViewController: LoginViewDelegate {
    
    func actionBecomeAPartner() {
        print("► actionBecomeAPartner()")
        
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients(["support@quantsapp.com"])
            composeVC.setSubject("Quantsapp: Become a Partner")
            present(composeVC, animated: true, completion: nil)
        } else {
            self.showZAlertView(withTitle: "Error", andMessage: "Mail service is not available.", cancelTitle: "OK", isError: true, completion: {
                //
            })
        }
    }
    
    func actionSignIn(email: String, password: String) {
        print("► actionSignIn()")
        print("Email: \(email)\nPassword: \(password)")
        
        SVProgressHUD.show()
        
        OSOWebService.sharedInstance.login(username: email, password: password) { (success, error) in
            SVProgressHUD.dismiss()
            
            if let err = error {
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true, completion: {})
            } else if success {
                self.loginView.clearForm()
                self.showHome()
            } else {
                self.showZAlertView(withTitle: "Error", andMessage: "Login Failed.", cancelTitle: "OK", isError: true, completion: {})
            }
        }
        
        /*
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            SVProgressHUD.dismiss(withDelay: 0.3, completion: {
                self.showHome()
            })
        }*/
    }
    
    func actionForgotPassword() {
        print("► actionForgotPassword()")
    }
}

// MARK:- MFMailComposeViewControllerDelegate

extension LoginViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            if let err = error {
                self.showZAlertView(withTitle: "Error", andMessage: err.localizedDescription, cancelTitle: "OK", isError: true) {
                    //
                }
            } else {
                if result == .sent {
                    self.showZAlertView(withTitle: "Message Sent", andMessage: "Your message has been sent successfully. Out team will get back to you as soon as possible.", cancelTitle: "OK", isError: false, completion: {})
                }
            }
        }
    }
}
