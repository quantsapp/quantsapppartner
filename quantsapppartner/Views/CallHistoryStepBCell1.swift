//
//  CallHistoryStepBCell1.swift
//  quantsapppartner
//
//  Created by Quantsapp on 03/04/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift

class CallHistoryStepBCell1: UITableViewCell {

    static let cellIdentifier = "CallHistoryStepBCell1"
    private var screenWidth: CGFloat = SwifterSwift.screenWidth
    
    private let fieldHeight: CGFloat = 20.0
    
    let lblDateCreated: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblDateClosed: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPnL: UILabel = {
        let label = UILabel()
        label.text = "PnL"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblPnLValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "0"
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSpreadTarget: UILabel = {
        let label = UILabel()
        label.text = "Target Spread"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblSpreadTargetValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSpreadStopLoss: UILabel = {
        let label = UILabel()
        label.text = "Stop Loss Spread"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblSpreadStopLossValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    /*private let lblMargin: UILabel = {
        let label = UILabel()
        label.text = "Margin"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblMarginValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()*/
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        
        // lblDateCreated
        addSubview(lblDateCreated)
        lblDateCreated.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblDateCreated.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: -5).isActive = true
        lblDateCreated.heightAnchor.constraint(equalToConstant: 12).isActive = true
        lblDateCreated.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        
        // lblDateClosed
        addSubview(lblDateClosed)
        lblDateClosed.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 5).isActive = true
        lblDateClosed.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        lblDateClosed.heightAnchor.constraint(equalToConstant: 12).isActive = true
        lblDateClosed.topAnchor.constraint(equalTo: lblDateCreated.topAnchor, constant: 0).isActive = true
        
        // lblPnL
        addSubview(lblPnL)
        lblPnL.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblPnL.topAnchor.constraint(equalTo: lblDateCreated.bottomAnchor, constant: 8).isActive = true
        lblPnL.heightAnchor.constraint(equalToConstant: 14).isActive = true
        lblPnL.widthAnchor.constraint(equalToConstant: (screenWidth - 40)/2).isActive = true
        
        // lblPnLValue
        addSubview(lblPnLValue)
        lblPnLValue.leftAnchor.constraint(equalTo: lblPnL.leftAnchor, constant: 0).isActive = true
        lblPnLValue.rightAnchor.constraint(equalTo: lblPnL.rightAnchor, constant: 0).isActive = true
        lblPnLValue.topAnchor.constraint(equalTo: lblPnL.bottomAnchor, constant: 0).isActive = true
        lblPnLValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblSpreadTarget
        addSubview(lblSpreadTarget)
        lblSpreadTarget.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblSpreadTarget.topAnchor.constraint(equalTo: lblPnLValue.bottomAnchor, constant: 8).isActive = true
        lblSpreadTarget.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSpreadTarget.widthAnchor.constraint(equalToConstant: (lblSpreadTarget.text?.sizeOfString(usingFont: lblSpreadTarget.font).width)! + 4).isActive = true
        
        // lblSpreadTargetValue
        addSubview(lblSpreadTargetValue)
        lblSpreadTargetValue.leftAnchor.constraint(equalTo: lblSpreadTarget.rightAnchor, constant: 4).isActive = true
        lblSpreadTargetValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -5).isActive = true
        lblSpreadTargetValue.topAnchor.constraint(equalTo: lblSpreadTarget.topAnchor, constant: 0).isActive = true
        lblSpreadTargetValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblSpreadStopLoss
        addSubview(lblSpreadStopLoss)
        lblSpreadStopLoss.leftAnchor.constraint(equalTo: lblSpreadTargetValue.rightAnchor, constant: 10).isActive = true
        lblSpreadStopLoss.topAnchor.constraint(equalTo: lblSpreadTarget.topAnchor, constant: 0).isActive = true
        lblSpreadStopLoss.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSpreadStopLoss.widthAnchor.constraint(equalToConstant: (lblSpreadStopLoss.text?.sizeOfString(usingFont: lblSpreadStopLoss.font).width)! + 4).isActive = true
        
        // lblSpreadStopLossValue
        addSubview(lblSpreadStopLossValue)
        lblSpreadStopLossValue.leftAnchor.constraint(equalTo: lblSpreadStopLoss.rightAnchor, constant: 4).isActive = true
        lblSpreadStopLossValue.topAnchor.constraint(equalTo: lblSpreadStopLoss.topAnchor, constant: 0).isActive = true
        lblSpreadStopLossValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSpreadStopLossValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblMargin & lblMarginValue
        /*addSubview(lblMargin)
        lblMargin.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblMargin.topAnchor.constraint(equalTo: lblPnL.topAnchor, constant: 0).isActive = true
        lblMargin.heightAnchor.constraint(equalTo: lblPnL.heightAnchor, multiplier: 1.0).isActive = true
        lblMargin.widthAnchor.constraint(equalToConstant: (lblMargin.text?.sizeOfString(usingFont: lblMargin.font).width)! + 4).isActive = true
        
        addSubview(lblMarginValue)
        lblMarginValue.isHidden = false
        lblMarginValue.leftAnchor.constraint(equalTo: lblMargin.rightAnchor, constant: 4).isActive = true
        lblMarginValue.topAnchor.constraint(equalTo: lblPnL.topAnchor, constant: 0).isActive = true
        lblMarginValue.heightAnchor.constraint(equalTo: lblPnL.heightAnchor, multiplier: 1.0).isActive = true
        lblMarginValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true*/
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
