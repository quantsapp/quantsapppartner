//
//  StrategyTableViewCell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 28/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift

class StrategyTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "StrategyTableViewCell"
    
    private var screenWidth: CGFloat = 0
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let lblStrategy: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = Constants.UIConfig.themeColor
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let starRating: OSOStarRating = {
        let sr = OSOStarRating(numberOfStars: 5, leftToRight: false)
        sr.translatesAutoresizingMaskIntoConstraints = false
        return sr
    }()
    
    let chartImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.clear
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let lblRewardToRisk: UILabel = {
        let label = UILabel()
        label.text = "Reward To Risk"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblRewardToRiskValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .right
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTargetProfit: UILabel = {
        let label = UILabel()
        label.text = "Target Profit"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblTargetProfitValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .right
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStopLoss: UILabel = {
        let label = UILabel()
        label.text = "Stop Loss"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblStopLossValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .right
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDrawUp: UILabel = {
        let label = UILabel()
        label.text = "Draw Up"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblDrawUpValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .right
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDrawDn: UILabel = {
        let label = UILabel()
        label.text = "Draw Down"
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblDrawDnValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .right
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        
        screenWidth = SwifterSwift.screenWidth
        
        if screenWidth <= Constants.Screen.minimumScreenWidth {
            let font = UIFont.systemFont(ofSize: 10)
            let fontBold = UIFont.boldSystemFont(ofSize: 10)
            lblRewardToRisk.font = font
            lblRewardToRiskValue.font = fontBold
            lblTargetProfit.font = font
            lblTargetProfitValue.font = fontBold
            lblStopLoss.font = font
            lblStopLossValue.font = fontBold
            lblDrawUp.font = font
            lblDrawUpValue.font = fontBold
            lblDrawDn.font = font
            lblDrawDnValue.font = fontBold
        }
        
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        // starRating
        addSubview(starRating)
        starRating.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        starRating.heightAnchor.constraint(equalToConstant: 26).isActive = true
        starRating.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        starRating.widthAnchor.constraint(equalToConstant: 68).isActive = true
        
        // lblStrategy
        addSubview(lblStrategy)
        lblStrategy.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblStrategy.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblStrategy.heightAnchor.constraint(equalToConstant: 26).isActive = true
        lblStrategy.rightAnchor.constraint(equalTo: starRating.leftAnchor, constant: -8).isActive = true
        
        // lblRewardToRisk
        addSubview(lblRewardToRisk)
        lblRewardToRisk.leftAnchor.constraint(equalTo: lblStrategy.leftAnchor, constant: 0).isActive = true
        lblRewardToRisk.topAnchor.constraint(equalTo: lblStrategy.bottomAnchor, constant: 4).isActive = true
        lblRewardToRisk.widthAnchor.constraint(equalToConstant: (lblRewardToRisk.text?.sizeOfString(usingFont: lblRewardToRisk.font).width)! + 4).isActive = true
        lblRewardToRisk.heightAnchor.constraint(equalToConstant: Constants.UIConfig.fieldHeight).isActive = true
        
        // lblTargetProfit
        addSubview(lblTargetProfit)
        lblTargetProfit.leftAnchor.constraint(equalTo: lblStrategy.leftAnchor, constant: 0).isActive = true
        lblTargetProfit.topAnchor.constraint(equalTo: lblRewardToRisk.bottomAnchor, constant: 4).isActive = true
        lblTargetProfit.widthAnchor.constraint(equalToConstant: (lblTargetProfit.text?.sizeOfString(usingFont: lblTargetProfit.font).width)! + 4).isActive = true
        lblTargetProfit.heightAnchor.constraint(equalToConstant: Constants.UIConfig.fieldHeight).isActive = true
        
        // lblStopLoss
        addSubview(lblStopLoss)
        lblStopLoss.leftAnchor.constraint(equalTo: lblStrategy.leftAnchor, constant: 0).isActive = true
        lblStopLoss.topAnchor.constraint(equalTo: lblTargetProfit.bottomAnchor, constant: 4).isActive = true
        lblStopLoss.widthAnchor.constraint(equalToConstant: (lblStopLoss.text?.sizeOfString(usingFont: lblStopLoss.font).width)! + 4).isActive = true
        lblStopLoss.heightAnchor.constraint(equalToConstant: Constants.UIConfig.fieldHeight).isActive = true
        
        // lblDrawUp
        addSubview(lblDrawUp)
        lblDrawUp.leftAnchor.constraint(equalTo: lblStrategy.leftAnchor, constant: 0).isActive = true
        lblDrawUp.topAnchor.constraint(equalTo: lblStopLoss.bottomAnchor, constant: 4).isActive = true
        lblDrawUp.widthAnchor.constraint(equalToConstant: (lblDrawUp.text?.sizeOfString(usingFont: lblDrawUp.font).width)! + 4).isActive = true
        lblDrawUp.heightAnchor.constraint(equalToConstant: Constants.UIConfig.fieldHeight).isActive = true
        
        // lblDrawDn
        addSubview(lblDrawDn)
        lblDrawDn.leftAnchor.constraint(equalTo: lblStrategy.leftAnchor, constant: 0).isActive = true
        lblDrawDn.topAnchor.constraint(equalTo: lblDrawUp.bottomAnchor, constant: 4).isActive = true
        lblDrawDn.widthAnchor.constraint(equalToConstant: (lblDrawDn.text?.sizeOfString(usingFont: lblDrawDn.font).width)! + 4).isActive = true
        lblDrawDn.heightAnchor.constraint(equalToConstant: Constants.UIConfig.fieldHeight).isActive = true
        
        // chartImageView
        addSubview(chartImageView)
        chartImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        chartImageView.topAnchor.constraint(equalTo: lblRewardToRisk.topAnchor, constant: -4).isActive = true
        chartImageView.bottomAnchor.constraint(equalTo: lblDrawDn.bottomAnchor, constant: 4).isActive = true
        chartImageView.widthAnchor.constraint(equalTo: chartImageView.heightAnchor, multiplier: 1.6).isActive = true
        
        // lblRewardToRiskValue
        addSubview(lblRewardToRiskValue)
        lblRewardToRiskValue.rightAnchor.constraint(equalTo: chartImageView.leftAnchor, constant: -10).isActive = true
        lblRewardToRiskValue.topAnchor.constraint(equalTo: lblRewardToRisk.topAnchor, constant: 0).isActive = true
        lblRewardToRiskValue.heightAnchor.constraint(equalTo: lblRewardToRisk.heightAnchor, multiplier: 1.0).isActive = true
        lblRewardToRiskValue.leftAnchor.constraint(equalTo: lblRewardToRisk.rightAnchor, constant: 10).isActive = true
        
        // lblTargetProfitValue
        addSubview(lblTargetProfitValue)
        lblTargetProfitValue.rightAnchor.constraint(equalTo: chartImageView.leftAnchor, constant: -10).isActive = true
        lblTargetProfitValue.topAnchor.constraint(equalTo: lblTargetProfit.topAnchor, constant: 0).isActive = true
        lblTargetProfitValue.heightAnchor.constraint(equalTo: lblTargetProfit.heightAnchor, multiplier: 1.0).isActive = true
        lblTargetProfitValue.leftAnchor.constraint(equalTo: lblTargetProfit.rightAnchor, constant: 10).isActive = true
        
        // lblStopLossValue
        addSubview(lblStopLossValue)
        lblStopLossValue.rightAnchor.constraint(equalTo: chartImageView.leftAnchor, constant: -10).isActive = true
        lblStopLossValue.topAnchor.constraint(equalTo: lblStopLoss.topAnchor, constant: 0).isActive = true
        lblStopLossValue.heightAnchor.constraint(equalTo: lblStopLoss.heightAnchor, multiplier: 1.0).isActive = true
        lblStopLossValue.leftAnchor.constraint(equalTo: lblStopLoss.rightAnchor, constant: 10).isActive = true
        
        // lblDrawUpValue
        addSubview(lblDrawUpValue)
        lblDrawUpValue.rightAnchor.constraint(equalTo: chartImageView.leftAnchor, constant: -10).isActive = true
        lblDrawUpValue.topAnchor.constraint(equalTo: lblDrawUp.topAnchor, constant: 0).isActive = true
        lblDrawUpValue.heightAnchor.constraint(equalTo: lblDrawUp.heightAnchor, multiplier: 1.0).isActive = true
        lblDrawUpValue.leftAnchor.constraint(equalTo: lblDrawUp.rightAnchor, constant: 10).isActive = true
        
        // lblDrawUpValue
        addSubview(lblDrawDnValue)
        lblDrawDnValue.rightAnchor.constraint(equalTo: chartImageView.leftAnchor, constant: -10).isActive = true
        lblDrawDnValue.topAnchor.constraint(equalTo: lblDrawDn.topAnchor, constant: 0).isActive = true
        lblDrawDnValue.heightAnchor.constraint(equalTo: lblDrawDn.heightAnchor, multiplier: 1.0).isActive = true
        lblDrawDnValue.leftAnchor.constraint(equalTo: lblDrawDn.rightAnchor, constant: 10).isActive = true
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
