//
//  MyPositionStepACell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 19/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift

class MyPositionStepACell: UITableViewCell {

    static let cellIdentifier = "MyPositionStepACell"
    private let screenWidth = SwifterSwift.screenWidth
    
    // contraints
    private var lblTradeStatusWidthAnchorContraint: NSLayoutConstraint!
    private var lblDateCreatedWidthAnchorContraint: NSLayoutConstraint!
    
    public var dateCreated: String = "" {
        didSet {
            lblDateCreated.text = dateCreated
            //updateCellConstraint()
        }
    }
    
    private let disclosureIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage(named: "icon_disclosure")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor(white: 1.0, alpha: 0.4)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let lblSymbolAndPosition: UILabel = {
        let label = UILabel()
        label.text = "Symbol Position"
        label.textColor = Constants.UIConfig.themeColor
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblDateCreated: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    private let lblMTM: UILabel = {
        let label = UILabel()
        label.text = "MTM"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblMTMValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "0"
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.text = "CMP"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblCMPValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "0"
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblMargin: UILabel = {
        let label = UILabel()
        label.text = "Margin"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblMarginValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        // disclosureIcon
        addSubview(disclosureIcon)
        disclosureIcon.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        disclosureIcon.heightAnchor.constraint(equalToConstant: 15).isActive = true
        disclosureIcon.widthAnchor.constraint(equalToConstant: 15).isActive = true
        disclosureIcon.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        // lblSymbolAndPosition
        addSubview(lblSymbolAndPosition)
        lblSymbolAndPosition.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblSymbolAndPosition.rightAnchor.constraint(equalTo: disclosureIcon.leftAnchor, constant: -5).isActive = true
        lblSymbolAndPosition.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblSymbolAndPosition.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        // lblDateCreated
        addSubview(lblDateCreated)
        lblDateCreated.text = dateCreated
        lblDateCreated.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblDateCreated.rightAnchor.constraint(equalTo: lblSymbolAndPosition.rightAnchor, constant: 0).isActive = true
        lblDateCreated.heightAnchor.constraint(equalToConstant: 12).isActive = true
        lblDateCreated.topAnchor.constraint(equalTo: lblSymbolAndPosition.bottomAnchor, constant: 0).isActive = true
        
        
        // lblMTM
        addSubview(lblMTM)
        lblMTM.leftAnchor.constraint(equalTo: lblSymbolAndPosition.leftAnchor, constant: 0).isActive = true
        lblMTM.topAnchor.constraint(equalTo: lblDateCreated.bottomAnchor, constant: 8).isActive = true
        lblMTM.heightAnchor.constraint(equalToConstant: 14).isActive = true
        //lblMTM.widthAnchor.constraint(equalToConstant: (lblMTM.text?.sizeOfString(usingFont: lblMTM.font).width)! + 2).isActive = true
        lblMTM.widthAnchor.constraint(equalToConstant: (screenWidth - 40)/2).isActive = true
        
        // lblMTMValue
        addSubview(lblMTMValue)
        lblMTMValue.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblMTMValue.rightAnchor.constraint(equalTo: lblMTM.rightAnchor, constant: 0).isActive = true
        lblMTMValue.topAnchor.constraint(equalTo: lblMTM.bottomAnchor, constant: 0).isActive = true
        lblMTMValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblCMP
        addSubview(lblCMP)
        lblCMP.leftAnchor.constraint(equalTo: lblMTM.rightAnchor, constant: 8).isActive = true
        lblCMP.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblCMP.heightAnchor.constraint(equalToConstant: 14).isActive = true
        //lblCMP.widthAnchor.constraint(equalToConstant: (lblCMP.text?.sizeOfString(usingFont: lblCMP.font).width)! + 2).isActive = true
        lblCMP.widthAnchor.constraint(equalToConstant: (screenWidth - 40)/2).isActive = true
        
        // lblCMPValue
        addSubview(lblCMPValue)
        lblCMPValue.leftAnchor.constraint(equalTo: lblCMP.leftAnchor, constant: 0).isActive = true
        lblCMPValue.topAnchor.constraint(equalTo: lblCMP.bottomAnchor, constant: 0).isActive = true
        lblCMPValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblCMPValue.rightAnchor.constraint(equalTo: lblCMP.rightAnchor, constant: 0).isActive = true
        
        
        
    }
    
    private func updateCellConstraint() {
        lblDateCreatedWidthAnchorContraint.constant = dateCreated.sizeOfString(usingFont: lblDateCreated.font).width + 4
        layoutIfNeeded()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
