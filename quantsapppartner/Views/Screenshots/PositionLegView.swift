//
//  PositionLegView.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 12/12/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class PositionLegView: UIView {
    
    private let fieldHeight: CGFloat = 16.0
    
    public var expired: String = "0" {
        didSet {
            if expired == "1" {
                showExpiredSticker(show: true)
            } else {
                showExpiredSticker(show: false)
            }
        }
    }
    
    
    
    public var deltaValue: String = "0" {
        didSet {
            lblDeltaValue.text = deltaValue
            
            if deltaValue == "GO PRO" {
                lblDeltaValue.isHidden = true
                stickerDelta.isHidden = false
            } else {
                lblDeltaValue.isHidden = false
                stickerDelta.isHidden = true
            }
        }
    }
    
    public var thetaValue: String = "0" {
        didSet {
            lblThetaValue.text = thetaValue
            
            if thetaValue == "GO PRO" {
                lblThetaValue.isHidden = true
                stickerTheta.isHidden = false
            } else {
                lblThetaValue.isHidden = false
                stickerTheta.isHidden = true
            }
        }
    }
    
    public var vegaValue: String = "0" {
        didSet {
            lblVegaValue.text = vegaValue
            
            if vegaValue == "GO PRO" {
                lblVegaValue.isHidden = true
                stickerVega.isHidden = false
            } else {
                lblVegaValue.isHidden = false
                stickerVega.isHidden = true
            }
        }
    }
    
    public var gammaValue: String = "0" {
        didSet {
            lblGammaValue.text = gammaValue
            
            if gammaValue == "GO PRO" {
                lblGammaValue.isHidden = true
                stickerGamma.isHidden = false
            } else {
                lblGammaValue.isHidden = false
                stickerGamma.isHidden = true
            }
        }
    }
    
    private let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblMTM: UILabel = {
        let label = UILabel()
        label.text = "MTM"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblMTMValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.text = "CMP"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblCMPValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblInstrument: UILabel = {
        let label = UILabel()
        label.text = "Instrument"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblInstrumentValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblExpiry: UILabel = {
        let label = UILabel()
        label.text = "Expiry"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblExpiryValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrike: UILabel = {
        let label = UILabel()
        label.text = "Strike"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblStrikeValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblOptionType: UILabel = {
        let label = UILabel()
        label.text = "Option Type"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblOptionTypeValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPrice: UILabel = {
        let label = UILabel()
        label.text = "Price"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblPriceValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQuantity: UILabel = {
        let label = UILabel()
        label.text = "Quantity"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblQuantityValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDelta: UILabel = {
        let label = UILabel()
        label.text = "Delta"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDeltaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTheta: UILabel = {
        let label = UILabel()
        label.text = "Theta"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblThetaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblVega: UILabel = {
        let label = UILabel()
        label.text = "Vega"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblVegaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblGamma: UILabel = {
        let label = UILabel()
        label.text = "Gamma"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblGammaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let stickerDelta: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    private let stickerTheta: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    
    private let stickerVega: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    
    private let stickerGamma: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    private var stickerExpired: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.image = UIImage(named: "sticker_expired")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Constants.Charts.chartRedColor
        imageView.alpha = 0.5
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    func setupView() {
        
        clipsToBounds = true
        backgroundColor = UIColor.clear
        
        // stickerExpired
        addSubview(stickerExpired)
        stickerExpired.isHidden =  true
        stickerExpired.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        stickerExpired.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        stickerExpired.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        stickerExpired.widthAnchor.constraint(equalTo: heightAnchor, multiplier: 1.0, constant: -20).isActive = true
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // lblMTM
        addSubview(lblMTM)
        lblMTM.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblMTM.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblMTM.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblMTM.widthAnchor.constraint(equalToConstant: (lblMTM.text?.sizeOfString(usingFont: lblMTM.font).width)! + 4).isActive = true
        
        // lblMTMValue
        addSubview(lblMTMValue)
        lblMTMValue.leftAnchor.constraint(equalTo: lblMTM.rightAnchor, constant: 4).isActive = true
        lblMTMValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblMTMValue.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblMTMValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblCMP
        addSubview(lblCMP)
        lblCMP.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblCMP.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblCMP.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCMP.widthAnchor.constraint(equalToConstant: (lblCMP.text?.sizeOfString(usingFont: lblCMP.font).width)! + 4).isActive = true
        
        // lblCMPValue
        addSubview(lblCMPValue)
        lblCMPValue.leftAnchor.constraint(equalTo: lblCMP.rightAnchor, constant: 4).isActive = true
        lblCMPValue.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblCMPValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCMPValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        
        // lblInstrument
        addSubview(lblInstrument)
        lblInstrument.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblInstrument.topAnchor.constraint(equalTo: lblMTM.bottomAnchor, constant: 0).isActive = true
        lblInstrument.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblInstrument.widthAnchor.constraint(equalToConstant: (lblInstrument.text?.sizeOfString(usingFont: lblInstrument.font).width)! + 4).isActive = true
        
        // lblInstrumentValue
        addSubview(lblInstrumentValue)
        lblInstrumentValue.leftAnchor.constraint(equalTo: lblInstrument.rightAnchor, constant: 4).isActive = true
        lblInstrumentValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblInstrumentValue.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblInstrumentValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblExpiry
        addSubview(lblExpiry)
        lblExpiry.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblExpiry.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblExpiry.widthAnchor.constraint(equalToConstant: (lblExpiry.text?.sizeOfString(usingFont: lblExpiry.font).width)! + 4).isActive = true
        
        // lblExpiryValue
        addSubview(lblExpiryValue)
        lblExpiryValue.leftAnchor.constraint(equalTo: lblExpiry.rightAnchor, constant: 4).isActive = true
        lblExpiryValue.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblExpiryValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblExpiryValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblStrike
        addSubview(lblStrike)
        lblStrike.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblStrike.topAnchor.constraint(equalTo: lblInstrument.bottomAnchor, constant: 0).isActive = true
        lblStrike.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblStrike.widthAnchor.constraint(equalToConstant: (lblStrike.text?.sizeOfString(usingFont: lblStrike.font).width)! + 4).isActive = true
        
        // lblStrikeValue
        addSubview(lblStrikeValue)
        lblStrikeValue.leftAnchor.constraint(equalTo: lblStrike.rightAnchor, constant: 4).isActive = true
        lblStrikeValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblStrikeValue.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblStrikeValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblOptionType
        addSubview(lblOptionType)
        lblOptionType.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblOptionType.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblOptionType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblOptionType.widthAnchor.constraint(equalToConstant: (lblOptionType.text?.sizeOfString(usingFont: lblOptionType.font).width)!).isActive = true
        
        // lblOptionTypeValue
        addSubview(lblOptionTypeValue)
        lblOptionTypeValue.leftAnchor.constraint(equalTo: lblOptionType.rightAnchor, constant: 2).isActive = true
        lblOptionTypeValue.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblOptionTypeValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblOptionTypeValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblPrice
        addSubview(lblPrice)
        lblPrice.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblPrice.topAnchor.constraint(equalTo: lblStrike.bottomAnchor, constant: 0).isActive = true
        lblPrice.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPrice.widthAnchor.constraint(equalToConstant: (lblPrice.text?.sizeOfString(usingFont: lblPrice.font).width)! + 4).isActive = true
        
        // lblPriceValue
        addSubview(lblPriceValue)
        lblPriceValue.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 4).isActive = true
        lblPriceValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblPriceValue.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        lblPriceValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblQuantity
        addSubview(lblQuantity)
        lblQuantity.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblQuantity.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        lblQuantity.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblQuantity.widthAnchor.constraint(equalToConstant: (lblQuantity.text?.sizeOfString(usingFont: lblQuantity.font).width)! + 4).isActive = true
        
        // lblQuantityValue
        addSubview(lblQuantityValue)
        lblQuantityValue.leftAnchor.constraint(equalTo: lblQuantity.rightAnchor, constant: 4).isActive = true
        lblQuantityValue.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        lblQuantityValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblQuantityValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblDelta
        addSubview(lblDelta)
        lblDelta.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblDelta.topAnchor.constraint(equalTo: lblPrice.bottomAnchor, constant: 0).isActive = true
        lblDelta.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblDelta.widthAnchor.constraint(equalToConstant: (lblDelta.text?.sizeOfString(usingFont: lblDelta.font).width)! + 4).isActive = true
        
        // lblDeltaValue
        addSubview(lblDeltaValue)
        lblDeltaValue.isHidden = false
        lblDeltaValue.leftAnchor.constraint(equalTo: lblDelta.rightAnchor, constant: 4).isActive = true
        lblDeltaValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblDeltaValue.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblDeltaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // stickerDelta
        addSubview(stickerDelta)
        stickerDelta.isHidden = true
        stickerDelta.rightAnchor.constraint(equalTo: lblDeltaValue.rightAnchor, constant: 0).isActive = true
        stickerDelta.topAnchor.constraint(equalTo: lblDeltaValue.topAnchor, constant: 1).isActive = true
        stickerDelta.bottomAnchor.constraint(equalTo: lblDeltaValue.bottomAnchor, constant: -1).isActive = true
        stickerDelta.widthAnchor.constraint(equalToConstant: stickerDelta.labelWidth).isActive = true
        
        // lblTheta
        addSubview(lblTheta)
        lblTheta.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblTheta.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblTheta.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblTheta.widthAnchor.constraint(equalToConstant: (lblTheta.text?.sizeOfString(usingFont: lblTheta.font).width)! + 4).isActive = true
        
        // lblThetaValue
        addSubview(lblThetaValue)
        lblThetaValue.isHidden = false
        lblThetaValue.leftAnchor.constraint(equalTo: lblTheta.rightAnchor, constant: 4).isActive = true
        lblThetaValue.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblThetaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblThetaValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // stickerTheta
        addSubview(stickerTheta)
        stickerTheta.isHidden = true
        stickerTheta.rightAnchor.constraint(equalTo: lblThetaValue.rightAnchor, constant: 0).isActive = true
        stickerTheta.topAnchor.constraint(equalTo: lblThetaValue.topAnchor, constant: 1).isActive = true
        stickerTheta.bottomAnchor.constraint(equalTo: lblThetaValue.bottomAnchor, constant: -1).isActive = true
        stickerTheta.widthAnchor.constraint(equalToConstant: stickerTheta.labelWidth).isActive = true
        
        
        // lblVega
        addSubview(lblVega)
        lblVega.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblVega.topAnchor.constraint(equalTo: lblDelta.bottomAnchor, constant: 0).isActive = true
        lblVega.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblVega.widthAnchor.constraint(equalToConstant: (lblVega.text?.sizeOfString(usingFont: lblVega.font).width)! + 4).isActive = true
        
        // lblVegaValue
        addSubview(lblVegaValue)
        lblVegaValue.isHidden = false
        lblVegaValue.leftAnchor.constraint(equalTo: lblVega.rightAnchor, constant: 4).isActive = true
        lblVegaValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblVegaValue.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblVegaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // stickerVega
        addSubview(stickerVega)
        stickerVega.isHidden = true
        stickerVega.rightAnchor.constraint(equalTo: lblVegaValue.rightAnchor, constant: 0).isActive = true
        stickerVega.topAnchor.constraint(equalTo: lblVegaValue.topAnchor, constant: 1).isActive = true
        stickerVega.bottomAnchor.constraint(equalTo: lblVegaValue.bottomAnchor, constant: -1).isActive = true
        stickerVega.widthAnchor.constraint(equalToConstant: stickerVega.labelWidth).isActive = true
        
        // lblGamma
        addSubview(lblGamma)
        lblGamma.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblGamma.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblGamma.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblGamma.widthAnchor.constraint(equalToConstant: (lblGamma.text?.sizeOfString(usingFont: lblGamma.font).width)! + 4).isActive = true
        
        // lblGammaValue
        addSubview(lblGammaValue)
        lblGammaValue.isHidden = false
        lblGammaValue.leftAnchor.constraint(equalTo: lblGamma.rightAnchor, constant: 4).isActive = true
        lblGammaValue.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblGammaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblGammaValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // stickerGamma
        addSubview(stickerGamma)
        stickerGamma.isHidden = true
        stickerGamma.rightAnchor.constraint(equalTo: lblGammaValue.rightAnchor, constant: 0).isActive = true
        stickerGamma.topAnchor.constraint(equalTo: lblGammaValue.topAnchor, constant: 1).isActive = true
        stickerGamma.bottomAnchor.constraint(equalTo: lblGammaValue.bottomAnchor, constant: -1).isActive = true
        stickerGamma.widthAnchor.constraint(equalToConstant: stickerGamma.labelWidth).isActive = true
        
    }
    
    private func showExpiredSticker(show: Bool) {
        stickerExpired.isHidden = !show
    }
    
    public func hideBottomBorder(hide: Bool) {
        bottomBorder.isHidden = hide
    }

}
