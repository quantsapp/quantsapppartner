//
//  ScreenshotViewer.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 13/12/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class ScreenshotViewer: UIViewController {
    
    // navigation bar
    private var osoNavBar: OSONavigationBar!
    private var osoNavBarHidden: Bool = false
    private var originalOsoNavBarFrame: CGRect = CGRect.zero
    
    public var isPushed: Bool = true
    
    public var screenshot: UIImage? {
        didSet {
            if let image = screenshot {
                showImage(image: image)
            }
        }
    }
    
    private let imageScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = false
        scrollView.alwaysBounceHorizontal = false
        scrollView.showsVerticalScrollIndicator = true
        scrollView.showsHorizontalScrollIndicator = true
        scrollView.flashScrollIndicators()
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 5.0
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = false
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.black
        
        // setup views
        setupViews()
        
        // navigation bar
        addOSONavigationBar()
    }
    
    private func setupViews() {
        
        // imageScrollView
        view.addSubview(imageScrollView)
        imageScrollView.delegate = self
        imageScrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageScrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imageScrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGesture.numberOfTapsRequired = 2
        doubleTapGesture.numberOfTouchesRequired = 1
        imageScrollView.addGestureRecognizer(doubleTapGesture)
        
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSingleTapScrollView(recognizer:)))
        singleTapGesture.numberOfTapsRequired = 1
        singleTapGesture.numberOfTouchesRequired = 1
        imageScrollView.addGestureRecognizer(singleTapGesture)
        
        singleTapGesture.require(toFail: doubleTapGesture)
        
        // imageView
        imageScrollView.addSubview(imageView)
        imageView.leftAnchor.constraint(equalTo: imageScrollView.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: imageScrollView.rightAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: imageScrollView.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: imageScrollView.bottomAnchor).isActive = true
    }
    
    private func showImage(image: UIImage) {
        imageView.image = image
    }
    
    private func shareImage(image: UIImage) {
        
        let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        if !Constants.APPConfig.isDeviceIphone {
            activityVC.popoverPresentationController?.sourceRect = CGRect(x: self.view.frame.midX, y: self.view.frame.midY, width: 0, height: 0)
        }
        
        if #available(iOS 11.0, *) {
            activityVC.excludedActivityTypes = [.addToReadingList, .openInIBooks]
        } else {
            // Fallback on earlier versions
            activityVC.excludedActivityTypes = [.addToReadingList, .openInIBooks]
        }
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @objc private func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if imageScrollView.zoomScale == 1 {
            imageScrollView.zoom(to: zoomRectForScale(scale: imageScrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            imageScrollView.setZoomScale(1.0, animated: true)
        }
    }
    
    @objc private func handleSingleTapScrollView(recognizer: UITapGestureRecognizer) {
        if osoNavBarHidden {
            self.osoNavBarHidden = false
            
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                self.osoNavBar.frame = self.originalOsoNavBarFrame
                //self.setNeedsStatusBarAppearanceUpdate()
                self.view.layoutIfNeeded()
            }) { (finished) in
                
            }
        } else {
            self.osoNavBarHidden = true
            
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
                self.osoNavBar.frame = CGRect(x: 0, y: -(Constants.OSONavigationBarConstants.barHeight - UIApplication.shared.statusBarFrame.height), width: UIScreen.main.bounds.width, height: Constants.OSONavigationBarConstants.barHeight - UIApplication.shared.statusBarFrame.height)
                //self.setNeedsStatusBarAppearanceUpdate()
                self.view.layoutIfNeeded()
            }) { (finished) in
                //self.osoNavBarHidden = true
            }
        }
    }
    
    private func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        
        let newCenter = imageView.convert(center, from: imageScrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        
        return zoomRect
    }
    
    private func addOSONavigationBar() {
        originalOsoNavBarFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: Constants.OSONavigationBarConstants.barHeight - UIApplication.shared.statusBarFrame.height)
        osoNavBar = OSONavigationBar(frame: originalOsoNavBarFrame, title: "Share", subTitle: nil, leftbuttonImage: UIImage(named: "icon_close"), rightButtonImage: UIImage(named: "icon_share_small"))
        view.addSubview(osoNavBar)
        osoNavBar.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool{
        return true // osoNavBarHidden
    }

}

// MARK:- OSONavigationBarDelegate

extension ScreenshotViewer: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
}

// MARK:- OSONavigationBarDelegate

extension ScreenshotViewer: OSONavigationBarDelegate {
    
    func leftButtonTapped() {
        if isPushed {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func rightButtonTapped() {
        print("Share")
        
        if let image = screenshot {
            shareImage(image: image)
        }
    }
    
}
