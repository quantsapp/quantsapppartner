//
//  ScreenshotFooter.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 12/12/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class ScreenshotFooter: UIView {
    
    private let iconLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "AppIcon")
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 3
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let lblUserName: UILabel = {
        let label = UILabel()
        label.text = DatabaseManager.sharedInstance.getUserName()
        label.font = UIFont.boldSystemFont(ofSize: 10)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDisclaimer: UILabel = {
        let label = UILabel()
        label.text = "Disclaimer: \(Constants.WebURLs.disclaimer)"
        label.font = UIFont.systemFont(ofSize: 10)
        label.numberOfLines = 0
        label.lineBreakMode = .byCharWrapping
        label.sizeToFit()
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    func setupView() {
        
        clipsToBounds = true
        backgroundColor = UIColor.clear
        
        // iconLogo
        addSubview(iconLogo)
        iconLogo.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        iconLogo.widthAnchor.constraint(equalToConstant: 20).isActive = true
        iconLogo.heightAnchor.constraint(equalTo: iconLogo.widthAnchor, multiplier: 1).isActive = true
        iconLogo.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        
        // lblUsername
        addSubview(lblUserName)
        lblUserName.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        lblUserName.widthAnchor.constraint(equalToConstant: (lblUserName.text?.sizeOfString(usingFont: lblUserName.font).width)! + 4).isActive = true
        lblUserName.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        lblUserName.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        
        
        // lblDisclaimer
        addSubview(lblDisclaimer)
        lblDisclaimer.leftAnchor.constraint(equalTo: iconLogo.rightAnchor, constant: 8).isActive = true
        lblDisclaimer.rightAnchor.constraint(equalTo: lblUserName.leftAnchor, constant: -8).isActive = true
        lblDisclaimer.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        lblDisclaimer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
    }

}
