//
//  ScreenshotHeader.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 12/12/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class ScreenshotHeader: UIView {
    
    private var icon: String?
    private var title: String?
    
    private let iconTitle: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor(white: 1.0, alpha: 0.1)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let iconCalendar: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "icon_calendar")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor(white: 1.0, alpha: 0.5)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let lblDate: UILabel = {
        let label = UILabel()
        label.text = "\(Date().string(withFormat: "dd-MMM-yy"))"
        label.font = UIFont.systemFont(ofSize: 12)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(white: 1.0, alpha: 0.8)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    
    init(icon: String, title: String) {
        super.init(frame: CGRect.zero)
        
        self.icon = icon
        self.title = title
        
        setupView()
    }
    
    func setupView() {
        
        clipsToBounds = true
        backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        
        // iconTitle
        addSubview(iconTitle)
        iconTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        iconTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        iconTitle.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        iconTitle.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        if let iconName = icon {
            iconTitle.image = UIImage(named: iconName)?.withRenderingMode(.alwaysTemplate)
            iconTitle.tintColor = UIColor(white: 1.0, alpha: 0.8)
        }
        
        // iconCalendar
        addSubview(iconCalendar)
        iconCalendar.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        iconCalendar.heightAnchor.constraint(equalToConstant: 20).isActive = true
        iconCalendar.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        iconCalendar.widthAnchor.constraint(equalTo: iconCalendar.heightAnchor, multiplier: 1).isActive = true
        
        // lblDate
        addSubview(lblDate)
        lblDate.rightAnchor.constraint(equalTo: iconCalendar.leftAnchor, constant: -4).isActive = true
        lblDate.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        lblDate.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblDate.widthAnchor.constraint(equalToConstant: (lblDate.text?.sizeOfString(usingFont: lblDate.font).width)! + 2).isActive = true
        
        // lblTitle
        addSubview(lblTitle)
        lblTitle.leftAnchor.constraint(equalTo: iconTitle.rightAnchor, constant: 8).isActive = true
        lblTitle.rightAnchor.constraint(equalTo: lblDate.leftAnchor, constant: -8).isActive = true
        lblTitle.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        lblTitle.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        
        if let titleString = title {
            lblTitle.text = titleString
        }
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

}
