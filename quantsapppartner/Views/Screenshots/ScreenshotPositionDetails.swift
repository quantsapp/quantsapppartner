//
//  ScreenshotPositionDetails.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 12/12/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class ScreenshotPositionDetails: UIView {
    
    // constraints
    private var lblTradeStatusWidthAnchor: NSLayoutConstraint!
    private var sectionCContentViewHeightAnchor: NSLayoutConstraint!
    
    private var canvasHeight: CGFloat = 0.0
    
    public var screenshotHeight: CGFloat {
        get {
            return canvasHeight
        }
    }
    
    public var numberOfLegs: Int = 0 {
        didSet {
            createLegs(legsCount: numberOfLegs)
        }
    }
    
    public var positionLegViews = [PositionLegView]()
    
    public var tradeStatusText: String? {
        didSet {
            if let text = tradeStatusText {
                setTradeStatusText(text: text)
            }
        }
    }
    
    public var tradeStatusTextColor: UIColor? {
        didSet {
            if let color = tradeStatusTextColor {
                setTradeStatusTextColor(textColor: color)
            }
        }
    }
    
    public var symbolText: NSAttributedString? {
        didSet {
            if let text = symbolText {
                setSymbolText(attributedString: text)
            }
        }
    }
    
    public var deltaValue: String = "0" {
        didSet {
            lblDeltaValue.text = deltaValue
            
            if deltaValue == "GO PRO" {
                lblDeltaValue.isHidden = true
                stickerDelta.isHidden = false
            } else {
                lblDeltaValue.isHidden = false
                stickerDelta.isHidden = true
            }
        }
    }
    
    public var thetaValue: String = "0" {
        didSet {
            lblThetaValue.text = thetaValue
            
            if thetaValue == "GO PRO" {
                lblThetaValue.isHidden = true
                stickerTheta.isHidden = false
            } else {
                lblThetaValue.isHidden = false
                stickerTheta.isHidden = true
            }
        }
    }
    
    public var vegaValue: String = "0" {
        didSet {
            lblVegaValue.text = vegaValue
            
            if vegaValue == "GO PRO" {
                lblVegaValue.isHidden = true
                stickerVega.isHidden = false
            } else {
                lblVegaValue.isHidden = false
                stickerVega.isHidden = true
            }
        }
    }
    
    public var gammaValue: String = "0" {
        didSet {
            lblGammaValue.text = gammaValue
            
            if gammaValue == "GO PRO" {
                lblGammaValue.isHidden = true
                stickerGamma.isHidden = false
            } else {
                lblGammaValue.isHidden = false
                stickerGamma.isHidden = true
            }
        }
    }
    
    public var dataTimeText: NSAttributedString? {
        didSet {
            if let text = dataTimeText {
                setDataTimeText(attributedString: text)
            }
        }
    }
    
    
    
    private let watermark: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "watermark_logo")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor(white: 1.0, alpha: 0.1)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let screenshotHeader: ScreenshotHeader = {
        let view = ScreenshotHeader(icon: "icon_positions", title: "Position Details")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let screenshotFooter: ScreenshotFooter = {
        let view = ScreenshotFooter()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let sectionAHeaderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblTradeStatus: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSymbol: UILabel = {
        let label = UILabel()
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let sectionAContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblMTM: UILabel = {
        let label = UILabel()
        label.text = "MTM"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblMTMValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.text = "CMP"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblCMPValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblMargin: UILabel = {
        let label = UILabel()
        label.text = "Margin"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblMarginValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblROI: UILabel = {
        let label = UILabel()
        label.text = "ROI"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblROIValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblAtmVol: UILabel = {
        let label = UILabel()
        label.text = "Atm Vol"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblAtmVolValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDateCreated: UILabel = {
        let label = UILabel()
        label.text = "Date Created"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblDateCreatedValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblAlertAbove: UILabel = {
        let label = UILabel()
        label.text = "Alert Above"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblAlertAboveValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblAlertBelow: UILabel = {
        let label = UILabel()
        label.text = "Alert Below"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let lblAlertBelowValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let sectionBHeaderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblGreeks: UILabel = {
        let label = UILabel()
        label.text = "GREEKS"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor(white: 1.0, alpha: 0.7)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let sectionBContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblDelta: UILabel = {
        let label = UILabel()
        label.text = "Delta"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDeltaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTheta: UILabel = {
        let label = UILabel()
        label.text = "Theta"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblThetaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblVega: UILabel = {
        let label = UILabel()
        label.text = "Vega"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblVegaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblGamma: UILabel = {
        let label = UILabel()
        label.text = "Gamma"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblGammaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let stickerDelta: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    private let stickerTheta: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    
    private let stickerVega: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    
    private let stickerGamma: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    private let sectionCHeaderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblPositions: UILabel = {
        let label = UILabel()
        label.text = "POSITIONS"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor(white: 1.0, alpha: 0.7)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDataTime: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let sectionCContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    private func setupView() {
        
        clipsToBounds = true
        backgroundColor = Constants.UIConfig.bottomColor
        
        // MARK: Watermark
        addSubview(watermark)
        watermark.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7, constant: 0).isActive = true
        watermark.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        watermark.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        watermark.heightAnchor.constraint(equalTo: watermark.widthAnchor, multiplier: 1).isActive = true
        
        // MARK: screenshotHeader
        addSubview(screenshotHeader)
        screenshotHeader.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        screenshotHeader.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        screenshotHeader.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        screenshotHeader.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        canvasHeight += 40
        
        // MARK: screenshotFooter
        addSubview(screenshotFooter)
        screenshotFooter.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        screenshotFooter.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        screenshotFooter.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        screenshotFooter.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        canvasHeight += 40
        
        // MARK: sectionAHeaderView
        addSubview(sectionAHeaderView)
        sectionAHeaderView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        sectionAHeaderView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        sectionAHeaderView.topAnchor.constraint(equalTo: screenshotHeader.bottomAnchor, constant: 0).isActive = true
        sectionAHeaderView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        canvasHeight += 30
        
        sectionAHeaderView.addSubview(lblTradeStatus)
        lblTradeStatus.rightAnchor.constraint(equalTo: sectionAHeaderView.rightAnchor, constant: -10).isActive = true
        lblTradeStatus.topAnchor.constraint(equalTo: sectionAHeaderView.topAnchor, constant: 0).isActive = true
        lblTradeStatus.bottomAnchor.constraint(equalTo: sectionAHeaderView.bottomAnchor, constant: 0).isActive = true
        lblTradeStatusWidthAnchor = NSLayoutConstraint(item: lblTradeStatus, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        lblTradeStatusWidthAnchor.isActive = true
        
        sectionAHeaderView.addSubview(lblSymbol)
        lblSymbol.leftAnchor.constraint(equalTo: sectionAHeaderView.leftAnchor, constant: 10).isActive = true
        lblSymbol.rightAnchor.constraint(equalTo: lblTradeStatus.leftAnchor, constant: -8).isActive = true
        lblSymbol.topAnchor.constraint(equalTo: sectionAHeaderView.topAnchor, constant: 0).isActive = true
        lblSymbol.bottomAnchor.constraint(equalTo: sectionAHeaderView.bottomAnchor, constant: 0).isActive = true
        
        // MARK: sectionAContentView
        addSubview(sectionAContentView)
        sectionAContentView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        sectionAContentView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        sectionAContentView.topAnchor.constraint(equalTo: sectionAHeaderView.bottomAnchor, constant: 0).isActive = true
        sectionAContentView.heightAnchor.constraint(equalToConstant: 88).isActive = true
        
        canvasHeight += 88
        
        // lblMTM
        sectionAContentView.addSubview(lblMTM)
        lblMTM.leftAnchor.constraint(equalTo: sectionAContentView.leftAnchor, constant: 10).isActive = true
        lblMTM.topAnchor.constraint(equalTo: sectionAContentView.topAnchor, constant: 4).isActive = true
        lblMTM.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblMTM.widthAnchor.constraint(equalToConstant: (lblMTM.text?.sizeOfString(usingFont: lblMTM.font).width)! + 4).isActive = true
        
        // lblMTMValue
        sectionAContentView.addSubview(lblMTMValue)
        lblMTMValue.leftAnchor.constraint(equalTo: lblMTM.rightAnchor, constant: 4).isActive = true
        lblMTMValue.rightAnchor.constraint(equalTo: sectionAContentView.centerXAnchor, constant: -8).isActive = true
        lblMTMValue.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblMTMValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblCMP
        sectionAContentView.addSubview(lblCMP)
        lblCMP.leftAnchor.constraint(equalTo: sectionAContentView.centerXAnchor, constant: 8).isActive = true
        lblCMP.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblCMP.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblCMP.widthAnchor.constraint(equalToConstant: (lblCMP.text?.sizeOfString(usingFont: lblCMP.font).width)! + 4).isActive = true
        
        // lblCMPValue
        sectionAContentView.addSubview(lblCMPValue)
        lblCMPValue.leftAnchor.constraint(equalTo: lblCMP.rightAnchor, constant: 4).isActive = true
        lblCMPValue.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblCMPValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblCMPValue.rightAnchor.constraint(equalTo: sectionAContentView.rightAnchor, constant: -10).isActive = true
        
        
        // lblMargin
        sectionAContentView.addSubview(lblMargin)
        lblMargin.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblMargin.topAnchor.constraint(equalTo: lblMTM.bottomAnchor, constant: 0).isActive = true
        lblMargin.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblMargin.widthAnchor.constraint(equalToConstant: (lblMargin.text?.sizeOfString(usingFont: lblMargin.font).width)! + 4).isActive = true
        
        // lblMarginValue
        sectionAContentView.addSubview(lblMarginValue)
        lblMarginValue.leftAnchor.constraint(equalTo: lblMargin.rightAnchor, constant: 4).isActive = true
        lblMarginValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblMarginValue.topAnchor.constraint(equalTo: lblMargin.topAnchor, constant: 0).isActive = true
        lblMarginValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblROI
        sectionAContentView.addSubview(lblROI)
        lblROI.leftAnchor.constraint(equalTo: sectionAContentView.centerXAnchor, constant: 8).isActive = true
        lblROI.topAnchor.constraint(equalTo: lblMargin.topAnchor, constant: 0).isActive = true
        lblROI.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblROI.widthAnchor.constraint(equalToConstant: (lblROI.text?.sizeOfString(usingFont: lblROI.font).width)! + 4).isActive = true
        
        // lblROIValue
        sectionAContentView.addSubview(lblROIValue)
        lblROIValue.leftAnchor.constraint(equalTo: lblROI.rightAnchor, constant: 4).isActive = true
        lblROIValue.topAnchor.constraint(equalTo: lblMargin.topAnchor, constant: 0).isActive = true
        lblROIValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblROIValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblAtmVol
        sectionAContentView.addSubview(lblAtmVol)
        lblAtmVol.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblAtmVol.topAnchor.constraint(equalTo: lblMargin.bottomAnchor, constant: 0).isActive = true
        lblAtmVol.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblAtmVol.widthAnchor.constraint(equalToConstant: (lblAtmVol.text?.sizeOfString(usingFont: lblAtmVol.font).width)! + 4).isActive = true
        
        // lblAtmVolValue
        sectionAContentView.addSubview(lblAtmVolValue)
        lblAtmVolValue.leftAnchor.constraint(equalTo: lblAtmVol.rightAnchor, constant: 4).isActive = true
        lblAtmVolValue.rightAnchor.constraint(equalTo: sectionAContentView.centerXAnchor, constant: -8).isActive = true
        lblAtmVolValue.topAnchor.constraint(equalTo: lblAtmVol.topAnchor, constant: 0).isActive = true
        lblAtmVolValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblDateCreated
        sectionAContentView.addSubview(lblDateCreated)
        lblDateCreated.leftAnchor.constraint(equalTo: sectionAContentView.centerXAnchor, constant: 8).isActive = true
        lblDateCreated.topAnchor.constraint(equalTo: lblAtmVol.topAnchor, constant: 0).isActive = true
        lblDateCreated.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblDateCreated.widthAnchor.constraint(equalToConstant: (lblDateCreated.text?.sizeOfString(usingFont: lblDateCreated.font).width)!).isActive = true
        
        // lblDateCreatedValue
        sectionAContentView.addSubview(lblDateCreatedValue)
        lblDateCreatedValue.leftAnchor.constraint(equalTo: lblDateCreated.rightAnchor, constant: 2).isActive = true
        lblDateCreatedValue.topAnchor.constraint(equalTo: lblAtmVol.topAnchor, constant: 0).isActive = true
        lblDateCreatedValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblDateCreatedValue.rightAnchor.constraint(equalTo: sectionAContentView.rightAnchor, constant: -10).isActive = true
        
        // lblAlertAbove
        sectionAContentView.addSubview(lblAlertAbove)
        lblAlertAbove.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblAlertAbove.topAnchor.constraint(equalTo: lblAtmVol.bottomAnchor, constant: 0).isActive = true
        lblAlertAbove.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblAlertAbove.widthAnchor.constraint(equalToConstant: (lblAlertAbove.text?.sizeOfString(usingFont: lblAlertAbove.font).width)! + 4).isActive = true
        
        // lblAlertAboveValue
        sectionAContentView.addSubview(lblAlertAboveValue)
        lblAlertAboveValue.leftAnchor.constraint(equalTo: lblAlertAbove.rightAnchor, constant: 4).isActive = true
        lblAlertAboveValue.rightAnchor.constraint(equalTo: sectionAContentView.centerXAnchor, constant: -8).isActive = true
        lblAlertAboveValue.topAnchor.constraint(equalTo: lblAlertAbove.topAnchor, constant: 0).isActive = true
        lblAlertAboveValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblAlertBelow
        sectionAContentView.addSubview(lblAlertBelow)
        lblAlertBelow.leftAnchor.constraint(equalTo: sectionAContentView.centerXAnchor, constant: 8).isActive = true
        lblAlertBelow.topAnchor.constraint(equalTo: lblAlertAbove.topAnchor, constant: 0).isActive = true
        lblAlertBelow.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblAlertBelow.widthAnchor.constraint(equalToConstant: (lblAlertBelow.text?.sizeOfString(usingFont: lblAlertBelow.font).width)! + 4).isActive = true
        
        // lblAlertBelowValue
        sectionAContentView.addSubview(lblAlertBelowValue)
        lblAlertBelowValue.leftAnchor.constraint(equalTo: lblAlertBelow.rightAnchor, constant: 4).isActive = true
        lblAlertBelowValue.topAnchor.constraint(equalTo: lblAlertAbove.topAnchor, constant: 0).isActive = true
        lblAlertBelowValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblAlertBelowValue.rightAnchor.constraint(equalTo: sectionAContentView.rightAnchor, constant: -10).isActive = true
        
        
        // MARK: sectionBHeaderView
        addSubview(sectionBHeaderView)
        sectionBHeaderView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        sectionBHeaderView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        sectionBHeaderView.topAnchor.constraint(equalTo: sectionAContentView.bottomAnchor, constant: 0).isActive = true
        sectionBHeaderView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        canvasHeight += 30
        
        sectionBHeaderView.addSubview(lblGreeks)
        lblGreeks.leftAnchor.constraint(equalTo: sectionBHeaderView.leftAnchor, constant: 10).isActive = true
        lblGreeks.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblGreeks.centerYAnchor.constraint(equalTo: sectionBHeaderView.centerYAnchor, constant: 0).isActive = true
        lblGreeks.rightAnchor.constraint(equalTo: sectionBHeaderView.rightAnchor, constant: -10).isActive = true
        
        // MARK: sectionBContentView
        addSubview(sectionBContentView)
        sectionBContentView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        sectionBContentView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        sectionBContentView.topAnchor.constraint(equalTo: sectionBHeaderView.bottomAnchor, constant: 0).isActive = true
        sectionBContentView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        canvasHeight += 48
        
        // lblDelta
        sectionBContentView.addSubview(lblDelta)
        lblDelta.leftAnchor.constraint(equalTo: sectionBContentView.leftAnchor, constant: 10).isActive = true
        lblDelta.topAnchor.constraint(equalTo: sectionBContentView.topAnchor, constant: 4).isActive = true
        lblDelta.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblDelta.widthAnchor.constraint(equalToConstant: (lblDelta.text?.sizeOfString(usingFont: lblDelta.font).width)! + 4).isActive = true
        
        // lblDeltaValue
        sectionBContentView.addSubview(lblDeltaValue)
        lblDeltaValue.isHidden = false
        lblDeltaValue.leftAnchor.constraint(equalTo: lblDelta.rightAnchor, constant: 4).isActive = true
        lblDeltaValue.rightAnchor.constraint(equalTo: sectionBContentView.centerXAnchor, constant: -8).isActive = true
        lblDeltaValue.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblDeltaValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // stickerDelta
        sectionBContentView.addSubview(stickerDelta)
        stickerDelta.isHidden = true
        stickerDelta.rightAnchor.constraint(equalTo: lblDeltaValue.rightAnchor, constant: 0).isActive = true
        stickerDelta.topAnchor.constraint(equalTo: lblDeltaValue.topAnchor, constant: 1).isActive = true
        stickerDelta.bottomAnchor.constraint(equalTo: lblDeltaValue.bottomAnchor, constant: -1).isActive = true
        stickerDelta.widthAnchor.constraint(equalToConstant: stickerDelta.labelWidth).isActive = true
        
        // lblTheta
        sectionBContentView.addSubview(lblTheta)
        lblTheta.leftAnchor.constraint(equalTo: sectionBContentView.centerXAnchor, constant: 8).isActive = true
        lblTheta.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblTheta.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblTheta.widthAnchor.constraint(equalToConstant: (lblTheta.text?.sizeOfString(usingFont: lblTheta.font).width)! + 4).isActive = true
        
        // lblThetaValue
        sectionBContentView.addSubview(lblThetaValue)
        lblThetaValue.isHidden = false
        lblThetaValue.leftAnchor.constraint(equalTo: lblTheta.rightAnchor, constant: 4).isActive = true
        lblThetaValue.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblThetaValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblThetaValue.rightAnchor.constraint(equalTo: sectionBContentView.rightAnchor, constant: -10).isActive = true
        
        // stickerTheta
        sectionBContentView.addSubview(stickerTheta)
        stickerTheta.isHidden = true
        stickerTheta.rightAnchor.constraint(equalTo: lblThetaValue.rightAnchor, constant: 0).isActive = true
        stickerTheta.topAnchor.constraint(equalTo: lblThetaValue.topAnchor, constant: 1).isActive = true
        stickerTheta.bottomAnchor.constraint(equalTo: lblThetaValue.bottomAnchor, constant: -1).isActive = true
        stickerTheta.widthAnchor.constraint(equalToConstant: stickerTheta.labelWidth).isActive = true
        
        
        // lblVega
        sectionBContentView.addSubview(lblVega)
        lblVega.leftAnchor.constraint(equalTo: lblDelta.leftAnchor, constant: 0).isActive = true
        lblVega.topAnchor.constraint(equalTo: lblDelta.bottomAnchor, constant: 0).isActive = true
        lblVega.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblVega.widthAnchor.constraint(equalToConstant: (lblVega.text?.sizeOfString(usingFont: lblVega.font).width)! + 4).isActive = true
        
        // lblVegaValue
        sectionBContentView.addSubview(lblVegaValue)
        lblVegaValue.isHidden = false
        lblVegaValue.leftAnchor.constraint(equalTo: lblVega.rightAnchor, constant: 4).isActive = true
        lblVegaValue.rightAnchor.constraint(equalTo: sectionBContentView.centerXAnchor, constant: -8).isActive = true
        lblVegaValue.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblVegaValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // stickerVega
        sectionBContentView.addSubview(stickerVega)
        stickerVega.isHidden = true
        stickerVega.rightAnchor.constraint(equalTo: lblVegaValue.rightAnchor, constant: 0).isActive = true
        stickerVega.topAnchor.constraint(equalTo: lblVegaValue.topAnchor, constant: 1).isActive = true
        stickerVega.bottomAnchor.constraint(equalTo: lblVegaValue.bottomAnchor, constant: -1).isActive = true
        stickerVega.widthAnchor.constraint(equalToConstant: stickerVega.labelWidth).isActive = true
        
        // lblGamma
        sectionBContentView.addSubview(lblGamma)
        lblGamma.leftAnchor.constraint(equalTo: sectionBContentView.centerXAnchor, constant: 8).isActive = true
        lblGamma.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblGamma.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblGamma.widthAnchor.constraint(equalToConstant: (lblGamma.text?.sizeOfString(usingFont: lblGamma.font).width)! + 4).isActive = true
        
        // lblGammaValue
        sectionBContentView.addSubview(lblGammaValue)
        lblGammaValue.isHidden = false
        lblGammaValue.leftAnchor.constraint(equalTo: lblGamma.rightAnchor, constant: 4).isActive = true
        lblGammaValue.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblGammaValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblGammaValue.rightAnchor.constraint(equalTo: sectionBContentView.rightAnchor, constant: -10).isActive = true
        
        // stickerGamma
        sectionBContentView.addSubview(stickerGamma)
        stickerGamma.isHidden = true
        stickerGamma.rightAnchor.constraint(equalTo: lblGammaValue.rightAnchor, constant: 0).isActive = true
        stickerGamma.topAnchor.constraint(equalTo: lblGammaValue.topAnchor, constant: 1).isActive = true
        stickerGamma.bottomAnchor.constraint(equalTo: lblGammaValue.bottomAnchor, constant: -1).isActive = true
        stickerGamma.widthAnchor.constraint(equalToConstant: stickerGamma.labelWidth).isActive = true
        
        
        // MARK: sectionCHeaderView
        addSubview(sectionCHeaderView)
        sectionCHeaderView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        sectionCHeaderView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        sectionCHeaderView.topAnchor.constraint(equalTo: sectionBContentView.bottomAnchor, constant: 0).isActive = true
        sectionCHeaderView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        canvasHeight += 30
        
        // lblPositions
        sectionCHeaderView.addSubview(lblPositions)
        lblPositions.leftAnchor.constraint(equalTo: sectionCHeaderView.leftAnchor, constant: 10).isActive = true
        lblPositions.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblPositions.centerYAnchor.constraint(equalTo: sectionCHeaderView.centerYAnchor, constant: 0).isActive = true
        lblPositions.widthAnchor.constraint(equalToConstant: (lblPositions.text?.sizeOfString(usingFont: lblPositions.font).width)! + 4).isActive = true
        
        // lblDataTime
        sectionCHeaderView.addSubview(lblDataTime)
        lblDataTime.rightAnchor.constraint(equalTo: sectionCHeaderView.rightAnchor, constant: -10).isActive = true
        lblDataTime.leftAnchor.constraint(equalTo: lblPositions.rightAnchor, constant: 8).isActive = true
        lblDataTime.topAnchor.constraint(equalTo: sectionCHeaderView.topAnchor, constant: 0).isActive = true
        lblDataTime.bottomAnchor.constraint(equalTo: sectionCHeaderView.bottomAnchor, constant: 0).isActive = true
        
        // MARK: sectionCContentView
        addSubview(sectionCContentView)
        sectionCContentView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        sectionCContentView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        sectionCContentView.topAnchor.constraint(equalTo: sectionCHeaderView.bottomAnchor, constant: 0).isActive = true
        sectionCContentViewHeightAnchor = NSLayoutConstraint(item: sectionCContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        sectionCContentViewHeightAnchor.isActive = true
        
        
        // layout subviews
        layoutIfNeeded()
    }
    
    private func createLegs(legsCount: Int) {
        sectionCContentViewHeightAnchor.isActive = false
        sectionCContentViewHeightAnchor.constant = legsCount.cgFloat * 104.0
        sectionCContentViewHeightAnchor.isActive = true
        
        canvasHeight += legsCount.cgFloat * 104.0
        
        for i in 0..<legsCount {
            let positionLegView = PositionLegView()
            positionLegView.translatesAutoresizingMaskIntoConstraints = false
            sectionCContentView.addSubview(positionLegView)
            positionLegViews.append(positionLegView)
            
            if i == legsCount - 1 {
                positionLegView.hideBottomBorder(hide: true)
            } else {
                positionLegView.hideBottomBorder(hide: false)
            }
            
            positionLegView.leftAnchor.constraint(equalTo: sectionCContentView.leftAnchor).isActive = true
            positionLegView.rightAnchor.constraint(equalTo: sectionCContentView.rightAnchor).isActive = true
            positionLegView.heightAnchor.constraint(equalToConstant: 104).isActive = true
            positionLegView.topAnchor.constraint(equalTo: sectionCContentView.topAnchor, constant: CGFloat(i * 104)).isActive = true
        }
        
        layoutIfNeeded()
    }
    
    private func setTradeStatusText(text: String) {
        lblTradeStatus.text = text
        
        lblTradeStatusWidthAnchor.isActive = false
        lblTradeStatusWidthAnchor.constant = text.sizeOfString(usingFont: lblTradeStatus.font).width + 4
        lblTradeStatusWidthAnchor.isActive = true
        layoutIfNeeded()
    }
    
    private func setTradeStatusTextColor(textColor: UIColor) {
        lblTradeStatus.textColor = textColor
    }
    
    private func setSymbolText(attributedString: NSAttributedString) {
        lblSymbol.attributedText = attributedString
    }
    
    private func setDataTimeText(attributedString: NSAttributedString) {
        lblDataTime.attributedText = attributedString
    }

}
