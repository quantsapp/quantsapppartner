//
//  ProSticker.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 24/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class ProSticker: UIView {
    
    private let lblPro: UILabel = {
        let label = UILabel()
        label.text = "PRO"
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 9)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public var labelWidth: CGFloat {
        get {
            return (lblPro.text?.sizeOfString(usingFont: lblPro.font).width)! * 1.4
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = Constants.UIConfig.themeColor
        self.layer.cornerRadius = 2
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        
        addSubview(lblPro)
        lblPro.leftAnchor.constraint(equalTo: leftAnchor, constant: 2).isActive = true
        lblPro.rightAnchor.constraint(equalTo: rightAnchor, constant: -2).isActive = true
        lblPro.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        lblPro.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
    }
}
