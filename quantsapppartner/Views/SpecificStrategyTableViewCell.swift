//
//  SpecificStrategyTableViewCell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 09/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class SpecificStrategyTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "SpecificStrategyTableViewCell"
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let chartImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.clear
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let lblStrategy: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.numberOfLines = 0
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        accessoryType = .disclosureIndicator
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // chartImageView
        addSubview(chartImageView)
        chartImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 4).isActive = true
        chartImageView.topAnchor.constraint(equalTo: topAnchor, constant: -4).isActive = true
        chartImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4).isActive = true
        chartImageView.widthAnchor.constraint(equalTo: chartImageView.heightAnchor, multiplier: 1.6).isActive = true
        
        // lblStrategy
        addSubview(lblStrategy)
        lblStrategy.leftAnchor.constraint(equalTo: chartImageView.rightAnchor, constant: 4).isActive = true
        lblStrategy.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblStrategy.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
        lblStrategy.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
