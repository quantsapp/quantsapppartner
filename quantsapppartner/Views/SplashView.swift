//
//  SplashView.swift
//  quantsapppartner
//
//  Created by Quantsapp on 12/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit

class SplashView: UIView {
    
    private let imageViewLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "splash_logo")
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    public let loadingActivityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.style = .white
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicatorView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "PARTNER APP"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let footerLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "© \(Date().string(withFormat: "yyyy")) Quantsapp. All Rights Reserved."
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        
        // add logo
        addSubview(imageViewLogo)
        
        // constraints for imageViewLogo
        imageViewLogo.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        if #available(iOS 11.0, *) {
            imageViewLogo.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
        } else {
            imageViewLogo.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        }
        imageViewLogo.widthAnchor.constraint(equalToConstant: 240).isActive = true
        imageViewLogo.heightAnchor.constraint(equalToConstant: 57).isActive = true
        
        
        // titleLabel
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: imageViewLogo.bottomAnchor, constant: 20).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        // add footer label
        addSubview(footerLabel)
        
        // constraints for footerLabel
        if #available(iOS 11.0, *) {
            footerLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        } else {
            footerLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        }
        footerLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        footerLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        footerLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        // add activity indicator
        addSubview(loadingActivityIndicatorView)
        //loadingActivityIndicatorView.startAnimating()
        
        // constraints for loadingActivityIndicatorView
        loadingActivityIndicatorView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loadingActivityIndicatorView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        loadingActivityIndicatorView.bottomAnchor.constraint(equalTo: footerLabel.topAnchor).isActive = true
        
        
    }

}
