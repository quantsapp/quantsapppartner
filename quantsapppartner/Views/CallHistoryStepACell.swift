//
//  CallHistoryStepACell.swift
//  quantsapppartner
//
//  Created by Quantsapp on 03/04/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift

class CallHistoryStepACell: UITableViewCell {

    static let cellIdentifier = "CallHistoryStepACell"
    private let screenWidth = SwifterSwift.screenWidth
    
    // contraints
    private var lblTradeStatusWidthAnchorContraint: NSLayoutConstraint!
    
    /*public var tradeStatus: String = "Status" {
        didSet {
            lblTradeStatus.text = tradeStatus
            updateCellConstraint()
        }
    }*/
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let disclosureIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage(named: "icon_disclosure")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor(white: 1.0, alpha: 0.4)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let lblSymbolAndPosition: UILabel = {
        let label = UILabel()
        label.text = "Symbol Position"
        label.textColor = Constants.UIConfig.themeColor
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    /*let lblTradeStatus: UILabel = {
        let label = UILabel()
        label.text = "Trade Status"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    private let lblMTM: UILabel = {
        let label = UILabel()
        label.text = "MTM"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblMTMValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.text = "CMP"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblCMPValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()*/
    
    let lblDateCreated: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblDateClosed: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPnL: UILabel = {
        let label = UILabel()
        label.text = "PnL"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblPnLValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "0"
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    /*private let lblMargin: UILabel = {
        let label = UILabel()
        label.text = "Margin"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()*/
    
    /*let lblMarginValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "0"
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()*/
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // disclosureIcon
        addSubview(disclosureIcon)
        disclosureIcon.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        disclosureIcon.heightAnchor.constraint(equalToConstant: 15).isActive = true
        disclosureIcon.widthAnchor.constraint(equalToConstant: 15).isActive = true
        disclosureIcon.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        
        // lblSymbolAndPosition
        addSubview(lblSymbolAndPosition)
        lblSymbolAndPosition.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblSymbolAndPosition.rightAnchor.constraint(equalTo: disclosureIcon.leftAnchor, constant: -5).isActive = true
        lblSymbolAndPosition.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblSymbolAndPosition.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        
        // lblDateCreated
        addSubview(lblDateCreated)
        lblDateCreated.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblDateCreated.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: -5).isActive = true
        lblDateCreated.heightAnchor.constraint(equalToConstant: 12).isActive = true
        lblDateCreated.topAnchor.constraint(equalTo: lblSymbolAndPosition.bottomAnchor, constant: 0).isActive = true
        
        // lblDateClosed
        addSubview(lblDateClosed)
        lblDateClosed.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 5).isActive = true
        lblDateClosed.rightAnchor.constraint(equalTo: lblSymbolAndPosition.rightAnchor, constant: 0).isActive = true
        lblDateClosed.heightAnchor.constraint(equalToConstant: 12).isActive = true
        lblDateClosed.topAnchor.constraint(equalTo: lblDateCreated.topAnchor, constant: 0).isActive = true
        
        
        // lblPnL
        addSubview(lblPnL)
        lblPnL.leftAnchor.constraint(equalTo: lblSymbolAndPosition.leftAnchor, constant: 0).isActive = true
        lblPnL.topAnchor.constraint(equalTo: lblDateCreated.bottomAnchor, constant: 8).isActive = true
        lblPnL.heightAnchor.constraint(equalToConstant: 14).isActive = true
        lblPnL.widthAnchor.constraint(equalToConstant: (screenWidth - 40)/2).isActive = true
        
        // lblPnLValue
        addSubview(lblPnLValue)
        lblPnLValue.leftAnchor.constraint(equalTo: lblPnL.leftAnchor, constant: 0).isActive = true
        lblPnLValue.rightAnchor.constraint(equalTo: lblPnL.rightAnchor, constant: 0).isActive = true
        lblPnLValue.topAnchor.constraint(equalTo: lblPnL.bottomAnchor, constant: 0).isActive = true
        lblPnLValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        // lblMargin & lblMarginValue
        /*addSubview(lblMargin)
        lblMargin.leftAnchor.constraint(equalTo: lblPnL.rightAnchor, constant: 8).isActive = true
        lblMargin.topAnchor.constraint(equalTo: lblPnL.topAnchor, constant: 0).isActive = true
        lblMargin.heightAnchor.constraint(equalToConstant: 14).isActive = true
        lblMargin.widthAnchor.constraint(equalToConstant: (screenWidth - 40)/2).isActive = true
        
        addSubview(lblMarginValue)
        lblMarginValue.leftAnchor.constraint(equalTo: lblMargin.leftAnchor, constant: 0).isActive = true
        lblMarginValue.topAnchor.constraint(equalTo: lblMargin.bottomAnchor, constant: 0).isActive = true
        lblMarginValue.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblMarginValue.rightAnchor.constraint(equalTo: lblMargin.rightAnchor, constant: 0).isActive = true*/
    }
    
    /*private func updateCellConstraint() {
        lblTradeStatusWidthAnchorContraint.constant = tradeStatus.sizeOfString(usingFont: lblTradeStatus.font).width + 4
        layoutIfNeeded()
    }*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
