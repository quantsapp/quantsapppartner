//
//  PositionListCell.swift
//  quantsapppartner
//
//  Created by Quantsapp on 29/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit

class PositionListCell: UITableViewCell {

    static let cellIdentifier = "PositionListCell"
    
    private let fieldHeight: CGFloat = 20.0
    
    // constraints
    private var checkBoxWidthConstraint: NSLayoutConstraint!
    private var lblSymbolLeftConstraint: NSLayoutConstraint!
    
    public var checkboxId: Int? {
        didSet {
            checkBox.id = checkboxId
        }
    }
    
    public var showCheckBox: Bool = false {
        didSet {
            checkBoxWidthConstraint.constant = showCheckBox ? 16 : 0
            lblSymbolLeftConstraint.constant = showCheckBox ? 10 : 0
            layoutIfNeeded()
        }
    }
    
    public var positionSelected: Bool = false {
        didSet {
            checkBox.isSelected = positionSelected
            
            backgroundColor = positionSelected ? UIColor(white: 1.0, alpha: 0.05) : UIColor.clear
        }
    }
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let lblSymbol: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = Constants.UIConfig.themeColor
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblInstrument: UILabel = {
        let label = UILabel()
        label.text = "Instrument"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblInstrumentValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblExpiry: UILabel = {
        let label = UILabel()
        label.text = "Expiry"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblExpiryValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrike: UILabel = {
        let label = UILabel()
        label.text = "Strike"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblStrikeValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblOptionType: UILabel = {
        let label = UILabel()
        label.text = "Option Type"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblOptionTypeValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQuantity: UILabel = {
        let label = UILabel()
        label.text = "Quantity"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblQuantityValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPrice: UILabel = {
        let label = UILabel()
        label.text = "Price"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblPriceValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let checkBox: OSOCheckBox = {
        let box = OSOCheckBox()
        box.translatesAutoresizingMaskIntoConstraints = false
        return box
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        // checkBox
        addSubview(checkBox)
        checkBox.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        checkBox.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        checkBox.heightAnchor.constraint(equalToConstant: 16).isActive = true
        //checkBox.widthAnchor.constraint(equalToConstant: 16).isActive = true
        checkBoxWidthConstraint = NSLayoutConstraint(item: checkBox, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 16)
        checkBoxWidthConstraint.isActive = true
        
        // lblInstrument
        addSubview(lblSymbol)
        //lblSymbol.leftAnchor.constraint(equalTo: checkBox.rightAnchor, constant: 10).isActive = true
        lblSymbolLeftConstraint = NSLayoutConstraint(item: lblSymbol, attribute: .left, relatedBy: .equal, toItem: checkBox, attribute: .right, multiplier: 1.0, constant: 10)
        lblSymbolLeftConstraint.isActive = true
        lblSymbol.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        lblSymbol.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        lblSymbol.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblInstrument
        addSubview(lblInstrument)
        lblInstrument.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblInstrument.topAnchor.constraint(equalTo: lblSymbol.bottomAnchor, constant: 0).isActive = true
        lblInstrument.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblInstrument.widthAnchor.constraint(equalToConstant: (lblInstrument.text?.sizeOfString(usingFont: lblInstrument.font).width)! + 4).isActive = true
        
        // lblInstrumentValue
        addSubview(lblInstrumentValue)
        lblInstrumentValue.leftAnchor.constraint(equalTo: lblInstrument.rightAnchor, constant: 4).isActive = true
        lblInstrumentValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblInstrumentValue.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblInstrumentValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblExpiry
        addSubview(lblExpiry)
        lblExpiry.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblExpiry.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblExpiry.widthAnchor.constraint(equalToConstant: (lblExpiry.text?.sizeOfString(usingFont: lblExpiry.font).width)! + 4).isActive = true
        
        // lblExpiryValue
        addSubview(lblExpiryValue)
        lblExpiryValue.leftAnchor.constraint(equalTo: lblExpiry.rightAnchor, constant: 4).isActive = true
        lblExpiryValue.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblExpiryValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblExpiryValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblStrike
        addSubview(lblStrike)
        lblStrike.leftAnchor.constraint(equalTo: lblInstrument.leftAnchor, constant: 0).isActive = true
        lblStrike.topAnchor.constraint(equalTo: lblInstrument.bottomAnchor, constant: 0).isActive = true
        lblStrike.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblStrike.widthAnchor.constraint(equalToConstant: (lblStrike.text?.sizeOfString(usingFont: lblStrike.font).width)! + 4).isActive = true
        
        // lblStrikeValue
        addSubview(lblStrikeValue)
        lblStrikeValue.leftAnchor.constraint(equalTo: lblStrike.rightAnchor, constant: 4).isActive = true
        lblStrikeValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblStrikeValue.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblStrikeValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblOptionType
        addSubview(lblOptionType)
        lblOptionType.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblOptionType.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblOptionType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblOptionType.widthAnchor.constraint(equalToConstant: (lblOptionType.text?.sizeOfString(usingFont: lblOptionType.font).width)! + 4).isActive = true
        
        // lblOptionTypeValue
        addSubview(lblOptionTypeValue)
        lblOptionTypeValue.leftAnchor.constraint(equalTo: lblOptionType.rightAnchor, constant: 4).isActive = true
        lblOptionTypeValue.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblOptionTypeValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblOptionTypeValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblQuantity
        addSubview(lblQuantity)
        lblQuantity.leftAnchor.constraint(equalTo: lblInstrument.leftAnchor, constant: 0).isActive = true
        lblQuantity.topAnchor.constraint(equalTo: lblStrike.bottomAnchor, constant: 0).isActive = true
        lblQuantity.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblQuantity.widthAnchor.constraint(equalToConstant: (lblQuantity.text?.sizeOfString(usingFont: lblQuantity.font).width)! + 4).isActive = true
        
        // lblQuantityValue
        addSubview(lblQuantityValue)
        lblQuantityValue.leftAnchor.constraint(equalTo: lblQuantity.rightAnchor, constant: 4).isActive = true
        lblQuantityValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblQuantityValue.topAnchor.constraint(equalTo: lblQuantity.topAnchor, constant: 0).isActive = true
        lblQuantityValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblPrice
        addSubview(lblPrice)
        lblPrice.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblPrice.topAnchor.constraint(equalTo: lblQuantity.topAnchor, constant: 0).isActive = true
        lblPrice.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPrice.widthAnchor.constraint(equalToConstant: (lblPrice.text?.sizeOfString(usingFont: lblPrice.font).width)!).isActive = true
        
        // lblPriceValue
        addSubview(lblPriceValue)
        lblPriceValue.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 2).isActive = true
        lblPriceValue.topAnchor.constraint(equalTo: lblQuantity.topAnchor, constant: 0).isActive = true
        lblPriceValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPriceValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
