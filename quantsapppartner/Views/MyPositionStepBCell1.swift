//
//  MyPositionStepBCell1.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 22/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class MyPositionStepBCell1: UITableViewCell {
    
    static let cellIdentifier = "MyPositionStepBCell1"
    
    private let fieldHeight: CGFloat = 20.0
    
    private let lblMTM: UILabel = {
        let label = UILabel()
        label.text = "MTM"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblMTMValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCMP: UILabel = {
        let label = UILabel()
        label.text = "CMP"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblCMPValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblInitiationSpread: UILabel = {
        let label = UILabel()
        label.text = "Initiation Spread"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblInitiationSpreadValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblCurrentSpread: UILabel = {
        let label = UILabel()
        label.text = "Current Spread"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblCurrentSpreadValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblMargin: UILabel = {
        let label = UILabel()
        label.text = "Margin"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblMarginValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblROI: UILabel = {
        let label = UILabel()
        label.text = "ROI"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblROIValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblAtmVol: UILabel = {
        let label = UILabel()
        label.text = "Atm Vol"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblAtmVolValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDateCreated: UILabel = {
        let label = UILabel()
        label.text = "Date Created"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblDateCreatedValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSpreadTarget: UILabel = {
        let label = UILabel()
        label.text = "Target Spread"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblSpreadTargetValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSpreadStopLoss: UILabel = {
        let label = UILabel()
        label.text = "Stop Loss Spread"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblSpreadStopLossValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        // lblMTM
        addSubview(lblMTM)
        lblMTM.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblMTM.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblMTM.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblMTM.widthAnchor.constraint(equalToConstant: (lblMTM.text?.sizeOfString(usingFont: lblMTM.font).width)! + 4).isActive = true
        
        // lblMTMValue
        addSubview(lblMTMValue)
        lblMTMValue.leftAnchor.constraint(equalTo: lblMTM.rightAnchor, constant: 4).isActive = true
        lblMTMValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblMTMValue.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblMTMValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblCMP
        addSubview(lblCMP)
        lblCMP.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblCMP.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblCMP.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCMP.widthAnchor.constraint(equalToConstant: (lblCMP.text?.sizeOfString(usingFont: lblCMP.font).width)! + 4).isActive = true
        
        // lblCMPValue
        addSubview(lblCMPValue)
        lblCMPValue.leftAnchor.constraint(equalTo: lblCMP.rightAnchor, constant: 4).isActive = true
        lblCMPValue.topAnchor.constraint(equalTo: lblMTM.topAnchor, constant: 0).isActive = true
        lblCMPValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCMPValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblInitiationSpread
        addSubview(lblInitiationSpread)
        lblInitiationSpread.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblInitiationSpread.topAnchor.constraint(equalTo: lblMTM.bottomAnchor, constant: 0).isActive = true
        lblInitiationSpread.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblInitiationSpread.widthAnchor.constraint(equalToConstant: (lblInitiationSpread.text?.sizeOfString(usingFont: lblInitiationSpread.font).width)! + 4).isActive = true
        
        // lblInitiationSpreadValue
        addSubview(lblInitiationSpreadValue)
        lblInitiationSpreadValue.leftAnchor.constraint(equalTo: lblInitiationSpread.rightAnchor, constant: 4).isActive = true
        lblInitiationSpreadValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        lblInitiationSpreadValue.topAnchor.constraint(equalTo: lblInitiationSpread.topAnchor, constant: 0).isActive = true
        lblInitiationSpreadValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblCurrentSpread
        addSubview(lblCurrentSpread)
        lblCurrentSpread.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblCurrentSpread.topAnchor.constraint(equalTo: lblInitiationSpread.bottomAnchor, constant: 0).isActive = true
        lblCurrentSpread.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblCurrentSpread.widthAnchor.constraint(equalToConstant: (lblCurrentSpread.text?.sizeOfString(usingFont: lblCurrentSpread.font).width)! + 4).isActive = true
        
        // lblCurrentSpreadValue
        addSubview(lblCurrentSpreadValue)
        lblCurrentSpreadValue.leftAnchor.constraint(equalTo: lblCurrentSpread.rightAnchor, constant: 4).isActive = true
        lblCurrentSpreadValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        lblCurrentSpreadValue.topAnchor.constraint(equalTo: lblCurrentSpread.topAnchor, constant: 0).isActive = true
        lblCurrentSpreadValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblSpreadTarget
        addSubview(lblSpreadTarget)
        lblSpreadTarget.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblSpreadTarget.topAnchor.constraint(equalTo: lblCurrentSpread.bottomAnchor, constant: 0).isActive = true
        lblSpreadTarget.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSpreadTarget.widthAnchor.constraint(equalToConstant: (lblSpreadTarget.text?.sizeOfString(usingFont: lblSpreadTarget.font).width)! + 4).isActive = true
        
        // lblSpreadTargetValue
        addSubview(lblSpreadTargetValue)
        lblSpreadTargetValue.leftAnchor.constraint(equalTo: lblSpreadTarget.rightAnchor, constant: 4).isActive = true
        lblSpreadTargetValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        lblSpreadTargetValue.topAnchor.constraint(equalTo: lblSpreadTarget.topAnchor, constant: 0).isActive = true
        lblSpreadTargetValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblSpreadStopLoss
        addSubview(lblSpreadStopLoss)
        lblSpreadStopLoss.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblSpreadStopLoss.topAnchor.constraint(equalTo: lblSpreadTarget.bottomAnchor, constant: 0).isActive = true
        lblSpreadStopLoss.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSpreadStopLoss.widthAnchor.constraint(equalToConstant: (lblSpreadStopLoss.text?.sizeOfString(usingFont: lblSpreadStopLoss.font).width)! + 4).isActive = true
        
        // lblSpreadStopLossValue
        addSubview(lblSpreadStopLossValue)
        lblSpreadStopLossValue.leftAnchor.constraint(equalTo: lblSpreadStopLoss.rightAnchor, constant: 4).isActive = true
        lblSpreadStopLossValue.topAnchor.constraint(equalTo: lblSpreadStopLoss.topAnchor, constant: 0).isActive = true
        lblSpreadStopLossValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblSpreadStopLossValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblDateCreated
        addSubview(lblDateCreated)
        lblDateCreated.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblDateCreated.topAnchor.constraint(equalTo: lblSpreadStopLoss.bottomAnchor, constant: 0).isActive = true
        lblDateCreated.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblDateCreated.widthAnchor.constraint(equalToConstant: (lblDateCreated.text?.sizeOfString(usingFont: lblDateCreated.font).width)!).isActive = true
        
        // lblDateCreatedValue
        addSubview(lblDateCreatedValue)
        lblDateCreatedValue.leftAnchor.constraint(equalTo: lblDateCreated.rightAnchor, constant: 2).isActive = true
        lblDateCreatedValue.topAnchor.constraint(equalTo: lblDateCreated.topAnchor, constant: 0).isActive = true
        lblDateCreatedValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblDateCreatedValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        
        /*// lblMargin
        addSubview(lblMargin)
        lblMargin.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblMargin.topAnchor.constraint(equalTo: lblMTM.bottomAnchor, constant: 0).isActive = true
        lblMargin.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblMargin.widthAnchor.constraint(equalToConstant: (lblMargin.text?.sizeOfString(usingFont: lblMargin.font).width)! + 4).isActive = true
        
        // lblMarginValue
        addSubview(lblMarginValue)
        lblMarginValue.leftAnchor.constraint(equalTo: lblMargin.rightAnchor, constant: 4).isActive = true
        lblMarginValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblMarginValue.topAnchor.constraint(equalTo: lblMargin.topAnchor, constant: 0).isActive = true
        lblMarginValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblROI
        addSubview(lblROI)
        lblROI.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblROI.topAnchor.constraint(equalTo: lblMargin.topAnchor, constant: 0).isActive = true
        lblROI.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblROI.widthAnchor.constraint(equalToConstant: (lblROI.text?.sizeOfString(usingFont: lblROI.font).width)! + 4).isActive = true
        
        // lblROIValue
        addSubview(lblROIValue)
        lblROIValue.leftAnchor.constraint(equalTo: lblROI.rightAnchor, constant: 4).isActive = true
        lblROIValue.topAnchor.constraint(equalTo: lblMargin.topAnchor, constant: 0).isActive = true
        lblROIValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblROIValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblAtmVol
        addSubview(lblAtmVol)
        lblAtmVol.leftAnchor.constraint(equalTo: lblMTM.leftAnchor, constant: 0).isActive = true
        lblAtmVol.topAnchor.constraint(equalTo: lblMargin.bottomAnchor, constant: 0).isActive = true
        lblAtmVol.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblAtmVol.widthAnchor.constraint(equalToConstant: (lblAtmVol.text?.sizeOfString(usingFont: lblAtmVol.font).width)! + 4).isActive = true
        
        // lblAtmVolValue
        addSubview(lblAtmVolValue)
        lblAtmVolValue.leftAnchor.constraint(equalTo: lblAtmVol.rightAnchor, constant: 4).isActive = true
        lblAtmVolValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblAtmVolValue.topAnchor.constraint(equalTo: lblAtmVol.topAnchor, constant: 0).isActive = true
        lblAtmVolValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblDateCreated
        addSubview(lblDateCreated)
        lblDateCreated.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblDateCreated.topAnchor.constraint(equalTo: lblAtmVol.topAnchor, constant: 0).isActive = true
        lblDateCreated.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblDateCreated.widthAnchor.constraint(equalToConstant: (lblDateCreated.text?.sizeOfString(usingFont: lblDateCreated.font).width)!).isActive = true
        //lblDateCreated.widthAnchor.constraint(lessThanOrEqualTo: widthAnchor, multiplier: 1/5).isActive = true
        
        // lblDateCreatedValue
        addSubview(lblDateCreatedValue)
        lblDateCreatedValue.leftAnchor.constraint(equalTo: lblDateCreated.rightAnchor, constant: 2).isActive = true
        lblDateCreatedValue.topAnchor.constraint(equalTo: lblAtmVol.topAnchor, constant: 0).isActive = true
        lblDateCreatedValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblDateCreatedValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true*/
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
