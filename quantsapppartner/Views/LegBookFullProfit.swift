//
//  LegBookFullProfit.swift
//  quantsapppartner
//
//  Created by Quantsapp on 29/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift

protocol LegBookFullProfitDelegate: class {
    func legSelected(leg: ManagerDetailLegs, id: Int)
    func legDeselected(leg: ManagerDetailLegs, id: Int)
    func quantityUpdated(qty: Int, id: Int)
    func legRemoved(leg: ManagerDetailLegs, id: Int)
}

extension LegBookFullProfitDelegate {
    func legSelected(leg: ManagerDetailLegs, id: Int) {}
    func legDeselected(leg: ManagerDetailLegs, id: Int) {}
    func quantityUpdated(qty: Int, id: Int) {}
    func legRemoved(leg: ManagerDetailLegs, id: Int) {}
}

public enum ModifyLegAction: String {
    case add = "add"
    case update = "update"
}

private enum ModifySegment: Int {
    case Buy = 0
    case Sell = 1
}

class LegBookFullProfit: UIView {
    
    weak var delegate: LegBookFullProfitDelegate?
    
    private var screenWidth = SwifterSwift.screenWidth
    
    private var lot: Int = -1
    private var id: Int?
    private var legAction: ModifyLegAction = .update
    private var showCheckBox: Bool = false
    private var showDeleteButton: Bool = false
    private var managerDetailLeg: ManagerDetailLegs?
    private var selectedToolType: OpenCallToolType?
    private var selectedSegment: ModifySegment = .Buy
    
    public var actionOfLeg: ModifyLegAction {
        get {
            return legAction
        }
    }
    
    private var originalQty: Int = 0
    private var tempQty: Int = 0
    private var updatedQty: Int = 0
    private var netQty: Int = 0
    private var actionQty: Int = 0
    private var oneThird: CGFloat = (SwifterSwift.screenWidth - 60)/3
    //private var oneThird: CGFloat = (SwifterSwift.screenWidth - 70)/4
    
    public var containerId: Int? {
        get {
            return id
        }
    }
    
    // constraints
    private var lblQtyWidthConstraint: NSLayoutConstraint!
    private var lblUpdateQtyWidthConstraint: NSLayoutConstraint!
    
    private let checkBox: OSOCheckBox = {
        let box = OSOCheckBox()
        box.translatesAutoresizingMaskIntoConstraints = false
        return box
    }()
    
    private let lblInstrument: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblExpiry: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblOptType: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrike: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPrice: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQty: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblNetQty: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrikeValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQtyValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblNetQtyValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblUpdateQty: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPriceValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let btnAddQty: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_increment")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDeleteQty(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnDeleteQty: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_decrement")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.UIConfig.themeColor
        button.addTarget(self, action: #selector(actionAddOrDeleteQty(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let btnDeleteLeg: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_trash_filled")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Constants.Charts.chartRedColor
        button.addTarget(self, action: #selector(actionDeleteLeg(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let segmentBuySell: UISegmentedControl = {
        let segmented = UISegmentedControl(items: ["B", "S"])
        segmented.selectedSegmentIndex = 0
        segmented.tintColor = Constants.Charts.chartGreenColor
        let font = UIFont.boldSystemFont(ofSize: 14)
        segmented.setTitleTextAttributes([NSAttributedString.Key.font: font], for: [])
        segmented.addTarget(self, action: #selector(segmentedValueChanged(_:)), for: .valueChanged)
        segmented.translatesAutoresizingMaskIntoConstraints = false
        return segmented
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    init(id: Int?, managerDetailLeg: ManagerDetailLegs, showCheckBox: Bool = false, showDeleteButton: Bool = false, selectedToolType: OpenCallToolType?, lot: Int = -1, legAction: ModifyLegAction = .update) {
        super.init(frame: CGRect.zero)
        self.id = id
        self.lot = lot
        self.updatedQty = lot
        self.legAction = legAction
        self.managerDetailLeg = managerDetailLeg
        self.showCheckBox = showCheckBox
        self.showDeleteButton = showDeleteButton
        self.selectedToolType = selectedToolType
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    
    private func setupView() {
        
        guard let toolType = selectedToolType else { return }
        
        /*if toolType == .Modify {
            if legAction == .add {
                oneThird = (screenWidth - 140)/4
                //oneThird = (screenWidth - 130)/3 //let oneThird: CGFloat = (screenWidth - (toolType == .Modify ? 130 : 60))/3
            } else if legAction == .update {
                oneThird = (screenWidth - 70)/4
            }
        } else {
            if let instrument = managerDetailLeg?.instrument {
                if instrument.uppercased() == "FUT" {
                    oneThird = (screenWidth - 40)/3
                } else {
                    oneThird = (screenWidth - 50)/4
                }
            }
        }*/
        
        
        backgroundColor = .clear
        layer.cornerRadius = 8
        layer.masksToBounds = true
        layer.borderColor = UIColor(white: 1.0, alpha: 0.3).cgColor
        layer.borderWidth = 1.0
        clipsToBounds = true
        
        // checkBox
        if showCheckBox {
            addSubview(checkBox)
            checkBox.id = self.id
            checkBox.delegate = self
            checkBox.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
            checkBox.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
            checkBox.heightAnchor.constraint(equalToConstant: 16).isActive = true
            checkBox.widthAnchor.constraint(equalToConstant: 16).isActive = true
        }
        
        // lblInstrument
        addSubview(lblInstrument)
        lblInstrument.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        lblInstrument.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblInstrument.leftAnchor.constraint(equalTo: showCheckBox ? checkBox.rightAnchor : leftAnchor, constant: 10).isActive = true
        
        if let instrument = managerDetailLeg?.instrument?.uppercased() {
            lblInstrument.text = instrument
            lblInstrument.widthAnchor.constraint(equalToConstant: instrument.sizeOfString(usingFont: lblInstrument.font).width + 2.0).isActive = true
        } else {
            lblInstrument.widthAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        // btnDeleteLeg
        if showDeleteButton {
            addSubview(btnDeleteLeg)
            btnDeleteLeg.rightAnchor.constraint(equalTo: rightAnchor, constant: -4).isActive = true
            btnDeleteLeg.heightAnchor.constraint(equalToConstant: 30).isActive = true
            btnDeleteLeg.widthAnchor.constraint(equalToConstant: 30).isActive = true
            btnDeleteLeg.centerYAnchor.constraint(equalTo: lblInstrument.centerYAnchor).isActive = true
        }
        
        // lblExpiry
        addSubview(lblExpiry)
        lblExpiry.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        lblExpiry.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblExpiry.leftAnchor.constraint(equalTo: lblInstrument.rightAnchor, constant: 10).isActive = true
        
        if let expiry = managerDetailLeg?.expiry {
            lblExpiry.text = expiry
            lblExpiry.widthAnchor.constraint(equalToConstant: expiry.sizeOfString(usingFont: lblExpiry.font).width + 2.0).isActive = true
        } else {
            lblExpiry.widthAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        // lblOptType
        addSubview(lblOptType)
        lblOptType.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        lblOptType.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblOptType.leftAnchor.constraint(equalTo: lblExpiry.rightAnchor, constant: 10).isActive = true
        
        if let optType = managerDetailLeg?.optType {
            if optType.uppercased() == "XX" {
                lblOptType.widthAnchor.constraint(equalToConstant: 0).isActive = true
            } else {
                lblOptType.text = optType
                
                if showDeleteButton {
                    lblOptType.rightAnchor.constraint(equalTo: btnDeleteLeg.leftAnchor, constant: -10).isActive = true
                } else {
                    lblOptType.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
                }
                
            }
        } else {
            lblOptType.widthAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        // lblStrike
        addSubview(lblStrike)
        lblStrike.topAnchor.constraint(equalTo: lblInstrument.bottomAnchor, constant: 10).isActive = true
        lblStrike.heightAnchor.constraint(equalToConstant: 12).isActive = true
        
        if let strike = managerDetailLeg?.strike {
            if strike == "-1" {
                lblStrike.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
                lblStrike.widthAnchor.constraint(equalToConstant: 0).isActive = true
            } else {
                lblStrike.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
                lblStrike.widthAnchor.constraint(equalToConstant: oneThird).isActive = true
                
                lblStrike.text = "STRIKE"
            }
        } else {
            lblStrike.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
            lblStrike.widthAnchor.constraint(equalToConstant: oneThird).isActive = true
        }
        
        // lblStrikeValue
        addSubview(lblStrikeValue)
        lblStrikeValue.topAnchor.constraint(equalTo: lblStrike.bottomAnchor, constant: 0).isActive = true
        lblStrikeValue.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblStrikeValue.leftAnchor.constraint(equalTo: lblStrike.leftAnchor).isActive = true
        lblStrikeValue.rightAnchor.constraint(equalTo: lblStrike.rightAnchor).isActive = true
        
        if let strike = managerDetailLeg?.strike {
            if strike != "-1" {
                if let dblVal = strike.double() {
                    lblStrikeValue.text = dblVal.toString()
                } else {
                    lblStrikeValue.text = strike
                }
            }
        }
        
        // lblPrice
        addSubview(lblPrice)
        lblPrice.text = "PRICE"
        lblPrice.topAnchor.constraint(equalTo: lblStrike.topAnchor).isActive = true
        lblPrice.heightAnchor.constraint(equalTo: lblStrike.heightAnchor).isActive = true
        lblPrice.leftAnchor.constraint(equalTo: lblStrike.rightAnchor, constant: 10).isActive = true
        lblPrice.widthAnchor.constraint(equalToConstant: oneThird).isActive = true
        
        // lblPriceValue
        addSubview(lblPriceValue)
        lblPriceValue.topAnchor.constraint(equalTo: lblPrice.bottomAnchor).isActive = true
        lblPriceValue.heightAnchor.constraint(equalTo: lblStrikeValue.heightAnchor).isActive = true
        lblPriceValue.leftAnchor.constraint(equalTo: lblPrice.leftAnchor).isActive = true
        lblPriceValue.rightAnchor.constraint(equalTo: lblPrice.rightAnchor).isActive = true
        
        if let price = managerDetailLeg?.cmp {
            if let dblVal = price.double()?.roundToDecimal(2) {
                lblPriceValue.text = dblVal.toString()
            } else {
                lblPriceValue.text = price
            }
        }
        
        // lblQty & lblQtyValue
        addSubview(lblQty)
        addSubview(lblQtyValue)
        
        var qtyString = ""
        
        if let qty = managerDetailLeg?.qty {
            
            if let intVal = qty.int {
                originalQty = intVal
                tempQty = originalQty
                
                if toolType == .Modify {
                    if legAction == .update {
                        netQty = originalQty + updatedQty
                    } else if legAction == .add {
                        netQty = originalQty
                        updatedQty = originalQty.abs
                    }
                } else if toolType == .BookFullProfit || toolType == .Exit {
                    netQty = tempQty - tempQty
                    updatedQty = tempQty * -1
                } else if toolType == .PartProfit {
                    netQty = tempQty - (tempQty/2)
                    updatedQty = (tempQty/2) * -1
                }
            }
            
            lblQty.text = "QTY"
            
            
            if toolType == .BookFullProfit || toolType == .Exit {
                if let intVal = qty.int {
                    var qtyColor = UIColor.white
                    
                    if intVal > 0 {
                        qtyColor = Constants.Charts.chartGreenColor
                    } else if intVal < 0 {
                        qtyColor = Constants.Charts.chartRedColor
                    }
                    
                    lblQtyValue.text = "\(intVal)"
                    lblQtyValue.textColor = qtyColor
                } else {
                    lblQtyValue.text = "\(qty)"
                    lblQtyValue.textColor = UIColor.white
                }
                
                qtyString = "\(qty)"
                
            } else if toolType == .PartProfit {
                if let intVal = qty.int {
                    var qtyColor = UIColor.white
                    
                    if intVal > 0 {
                        qtyColor = Constants.Charts.chartGreenColor
                    } else if intVal < 0 {
                        qtyColor = Constants.Charts.chartRedColor
                    }
                    
                    lblQtyValue.text = "\(intVal)"
                    lblQtyValue.textColor = qtyColor
                } else {
                    lblQtyValue.text = "\(qty)"
                    lblQtyValue.textColor = UIColor.white
                }
                
                qtyString = "\(qty)"
                
            } else {
                if let intVal = qty.int {
                    var qtyColor = UIColor.white
                    if intVal > 0 {
                        qtyColor = Constants.Charts.chartGreenColor
                    } else if intVal < 0 {
                        qtyColor = Constants.Charts.chartRedColor
                    }
                    
                    lblQtyValue.text = "\(intVal)"
                    lblQtyValue.textColor = qtyColor
                    
                    qtyString = "\(intVal)"
                } else {
                    lblQtyValue.text = "\(qty)"
                    lblQtyValue.textColor = UIColor.white
                    
                    qtyString = "\(qty)"
                }
            }
        }
        
        //let qtyStringWidth = qtyString.sizeOfString(usingFont: UIFont.boldSystemFont(ofSize: 16)).width + 2
        
        
        
        
        lblQty.topAnchor.constraint(equalTo: lblStrike.topAnchor).isActive = true
        lblQty.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 10).isActive = true
        lblQty.heightAnchor.constraint(equalTo: lblStrike.heightAnchor).isActive = true
        
        if toolType == .Modify && legAction == .add {
            lblQty.widthAnchor.constraint(equalToConstant: 0).isActive = true
        } else {
            lblQty.widthAnchor.constraint(equalToConstant: oneThird).isActive = true
        }
        
        lblQtyValue.topAnchor.constraint(equalTo: lblStrikeValue.topAnchor).isActive = true
        lblQtyValue.leftAnchor.constraint(equalTo: lblPriceValue.rightAnchor, constant: 10).isActive = true
        lblQtyValue.heightAnchor.constraint(equalTo: lblStrikeValue.heightAnchor).isActive = true
        lblQtyValue.widthAnchor.constraint(equalTo: lblQty.widthAnchor, multiplier: 1.0).isActive = true
        
        
        // lblNetQty & lblNetQtyValue
        addSubview(lblNetQty)
        lblNetQty.text = "NET QTY"
        lblNetQty.topAnchor.constraint(equalTo: lblStrikeValue.bottomAnchor, constant: 8).isActive = true
        lblNetQty.heightAnchor.constraint(equalTo: lblStrike.heightAnchor, multiplier: 1.0).isActive = true
        lblNetQty.widthAnchor.constraint(equalToConstant: oneThird).isActive = true
        lblNetQty.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        addSubview(lblNetQtyValue)
        lblNetQtyValue.text = "\(netQty)"
        lblNetQtyValue.topAnchor.constraint(equalTo: lblNetQty.bottomAnchor).isActive = true
        lblNetQtyValue.leftAnchor.constraint(equalTo: lblNetQty.leftAnchor).isActive = true
        lblNetQtyValue.rightAnchor.constraint(equalTo: lblNetQty.rightAnchor).isActive = true
        lblNetQtyValue.heightAnchor.constraint(equalTo: lblStrikeValue.heightAnchor, multiplier: 1.0).isActive = true
        
        if toolType == .Modify && legAction == .update {
            lblNetQty.alpha = 0
            lblNetQtyValue.alpha = 0
        }
        
        // segmentBuySell
        addSubview(segmentBuySell)
        segmentBuySell.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        segmentBuySell.topAnchor.constraint(equalTo: lblPriceValue.bottomAnchor, constant: 8).isActive = true
        segmentBuySell.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        if toolType == .BookFullProfit || toolType == .Exit || toolType == .PartProfit {
            segmentBuySell.widthAnchor.constraint(equalToConstant: 30).isActive = true
            
            if updatedQty > 0 {
                segmentBuySell.removeSegment(at: 1, animated: false)
                segmentBuySell.selectedSegmentIndex = 0
                selectedSegment = .Buy
                segmentBuySell.tintColor = Constants.Charts.chartGreenColor
            } else if updatedQty < 0 {
                segmentBuySell.removeSegment(at: 0, animated: false)
                segmentBuySell.selectedSegmentIndex = 0
                selectedSegment = .Sell
                segmentBuySell.tintColor = Constants.Charts.chartRedColor
            } else {
                segmentBuySell.isHidden = true
            }
        } else {
            segmentBuySell.isEnabled = false
            segmentBuySell.widthAnchor.constraint(equalToConstant: 60).isActive = true
            
            if legAction == .add {
                if netQty > 0 {
                    segmentBuySell.selectedSegmentIndex = 0
                    selectedSegment = .Buy
                    segmentBuySell.tintColor = Constants.Charts.chartGreenColor
                } else if netQty < 0 {
                    segmentBuySell.selectedSegmentIndex = 1
                    selectedSegment = .Sell
                    segmentBuySell.tintColor = Constants.Charts.chartRedColor
                }
            }
        }
        
        
        if toolType == .BookFullProfit || toolType == .Exit || toolType == .PartProfit {
            // lblUpdatedQty
            addSubview(lblUpdateQty)
            lblUpdateQty.text = "\(updatedQty.abs)"
            lblUpdateQty.textColor = updatedQty == 0 ? .white : (selectedSegment == .Buy) ? Constants.Charts.chartGreenColor : Constants.Charts.chartRedColor
            
            lblUpdateQty.leftAnchor.constraint(equalTo: segmentBuySell.rightAnchor, constant: 10).isActive = true
            lblUpdateQty.heightAnchor.constraint(equalToConstant: 30).isActive = true
            lblUpdateQty.centerYAnchor.constraint(equalTo: segmentBuySell.centerYAnchor, constant: 0).isActive = true
            lblUpdateQtyWidthConstraint = NSLayoutConstraint(item: lblUpdateQty, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: "\(updatedQty)".sizeOfString(usingFont: UIFont.boldSystemFont(ofSize: 20)).width + 2)
            lblUpdateQtyWidthConstraint.isActive = true
        } else {
            // btnDeleteQty
            addSubview(btnDeleteQty)
            btnDeleteQty.isEnabled = false
            btnDeleteQty.leftAnchor.constraint(equalTo: segmentBuySell.rightAnchor, constant: 20).isActive = true
            btnDeleteQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
            btnDeleteQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
            btnDeleteQty.centerYAnchor.constraint(equalTo: segmentBuySell.centerYAnchor, constant: 0).isActive = true
            
            // lblUpdatedQty
            addSubview(lblUpdateQty)
            lblUpdateQty.text = "\(updatedQty.abs)"
            lblUpdateQty.textColor = updatedQty == 0 ? .white : (selectedSegment == .Buy) ? Constants.Charts.chartGreenColor : Constants.Charts.chartRedColor
            
            lblUpdateQty.leftAnchor.constraint(equalTo: btnDeleteQty.rightAnchor, constant: 5).isActive = true
            lblUpdateQty.heightAnchor.constraint(equalToConstant: 30).isActive = true
            lblUpdateQty.centerYAnchor.constraint(equalTo: segmentBuySell.centerYAnchor, constant: 0).isActive = true
            lblUpdateQtyWidthConstraint = NSLayoutConstraint(item: lblUpdateQty, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: "\(updatedQty)".sizeOfString(usingFont: UIFont.boldSystemFont(ofSize: 20)).width + 2)
            lblUpdateQtyWidthConstraint.isActive = true
            
            // btnAddQty
            addSubview(btnAddQty)
            btnAddQty.isEnabled = false
            btnAddQty.leftAnchor.constraint(equalTo: lblUpdateQty.rightAnchor, constant: 5).isActive = true
            btnAddQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
            btnAddQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
            btnAddQty.centerYAnchor.constraint(equalTo: segmentBuySell.centerYAnchor, constant: 0).isActive = true
        }
        
        
        /*if toolType == .Modify && legAction == .add {
            // btnDeleteQty
            addSubview(btnDeleteQty)
            btnDeleteQty.leftAnchor.constraint(equalTo: lblPriceValue.rightAnchor, constant: 10).isActive = true
            btnDeleteQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
            btnDeleteQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
            btnDeleteQty.bottomAnchor.constraint(equalTo: lblPriceValue.bottomAnchor, constant: 0).isActive = true
        }
        
        // lblQty
        addSubview(lblQty)
        
        // lblQtyValue
        addSubview(lblQtyValue)
        
        var qtyString = ""
        
        if let qty = managerDetailLeg?.qty {
            
            if let intVal = qty.int {
                originalQty = intVal
                tempQty = originalQty
                
                if toolType == .Modify {
                    netQty = originalQty + updatedQty
                } else if toolType == .BookFullProfit || toolType == .Exit {
                    netQty = tempQty - tempQty
                } else if toolType == .PartProfit {
                    netQty = tempQty - (tempQty/2)
                }
            }
            
            lblQty.text = "QTY"
            
            
            if toolType == .BookFullProfit || toolType == .Exit {
                if let intVal = qty.int {
                    var qtyColor = UIColor.white
                    
                    if intVal > 0 {
                        qtyColor = Constants.Charts.chartGreenColor
                    } else if intVal < 0 {
                        qtyColor = Constants.Charts.chartRedColor
                    }
                    
                    lblQtyValue.text = "\(intVal)"
                    lblQtyValue.textColor = qtyColor
                } else {
                    lblQtyValue.text = "\(qty)"
                    lblQtyValue.textColor = UIColor.white
                }
                
                qtyString = "\(qty)"
                
            } else if toolType == .PartProfit {
                if let intVal = qty.int {
                    var qtyColor = UIColor.white
                    
                    if intVal > 0 {
                        qtyColor = Constants.Charts.chartGreenColor
                    } else if intVal < 0 {
                        qtyColor = Constants.Charts.chartRedColor
                    }
                    
                    lblQtyValue.text = "\(intVal)"
                    lblQtyValue.textColor = qtyColor
                } else {
                    lblQtyValue.text = "\(qty)"
                    lblQtyValue.textColor = UIColor.white
                }
                
                qtyString = "\(qty)"
                
            } else {
                if let intVal = qty.int {
                    var qtyColor = UIColor.white
                    if intVal > 0 {
                        qtyColor = Constants.Charts.chartGreenColor
                    } else if intVal < 0 {
                        qtyColor = Constants.Charts.chartRedColor
                    }
                    
                    lblQtyValue.text = "\(intVal)"
                    lblQtyValue.textColor = qtyColor
                    
                    qtyString = "\(intVal)"
                } else {
                    lblQtyValue.text = "\(qty)"
                    lblQtyValue.textColor = UIColor.white
                    
                    qtyString = "\(qty)"
                }
            }
        }
        
        let qtyStringWidth = qtyString.sizeOfString(usingFont: UIFont.boldSystemFont(ofSize: 16)).width + 2
        
        lblQty.topAnchor.constraint(equalTo: lblStrike.topAnchor).isActive = true
        lblQty.heightAnchor.constraint(equalTo: lblStrike.heightAnchor).isActive = true
        
        lblQtyValue.topAnchor.constraint(equalTo: lblStrikeValue.topAnchor).isActive = true
        lblQtyValue.heightAnchor.constraint(equalTo: lblStrikeValue.heightAnchor).isActive = true
        
        if toolType == .Modify && legAction == .add {
            lblQtyValue.leftAnchor.constraint(equalTo: btnDeleteQty.rightAnchor, constant: 10).isActive = true
            lblQtyWidthConstraint = NSLayoutConstraint(item: lblQtyValue, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: qtyStringWidth < oneThird ? qtyStringWidth : oneThird)
            lblQtyWidthConstraint.isActive = true
            
            lblQty.leftAnchor.constraint(equalTo: lblQtyValue.leftAnchor, constant: 0).isActive = true
            lblQty.widthAnchor.constraint(equalTo: lblQtyValue.widthAnchor).isActive = true
        } else {
            lblQtyValue.leftAnchor.constraint(equalTo: lblPriceValue.rightAnchor, constant: 10).isActive = true
            lblQtyWidthConstraint = NSLayoutConstraint(item: lblQtyValue, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: oneThird)
            lblQtyWidthConstraint.isActive = true
            
            lblQty.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 10).isActive = true
            lblQty.widthAnchor.constraint(equalTo: lblQtyValue.widthAnchor).isActive = true
        }
        
        
        
        
        if toolType == .Modify && legAction == .add {
            // btnAddQty
            addSubview(btnAddQty)
            btnAddQty.leftAnchor.constraint(equalTo: lblQtyValue.rightAnchor, constant: 10).isActive = true
            btnAddQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
            btnAddQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
            btnAddQty.centerYAnchor.constraint(equalTo: lblQtyValue.centerYAnchor, constant: 0).isActive = true
        }
        
        if toolType == .Modify && legAction == .update {
            
            // lblNetQty
            addSubview(lblNetQty)
            lblNetQty.text = "NET QTY"
            lblNetQty.alpha = 0
            lblNetQty.topAnchor.constraint(equalTo: lblQty.topAnchor).isActive = true
            lblNetQty.heightAnchor.constraint(equalTo: lblQty.heightAnchor, multiplier: 1.0).isActive = true
            lblNetQty.leftAnchor.constraint(equalTo: lblQtyValue.rightAnchor, constant: 10).isActive = true
            lblNetQty.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
            
            // lblNetQtyValue
            addSubview(lblNetQtyValue)
            lblNetQtyValue.text = "\(netQty)"
            lblNetQtyValue.alpha = 0
            lblNetQtyValue.topAnchor.constraint(equalTo: lblNetQty.bottomAnchor).isActive = true
            lblNetQtyValue.leftAnchor.constraint(equalTo: lblNetQty.leftAnchor).isActive = true
            lblNetQtyValue.rightAnchor.constraint(equalTo: lblNetQty.rightAnchor).isActive = true
            lblNetQtyValue.heightAnchor.constraint(equalTo: lblQtyValue.heightAnchor, multiplier: 1.0).isActive = true
            
            // segmentBuySell
            addSubview(segmentBuySell)
            segmentBuySell.isEnabled = false
            segmentBuySell.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
            segmentBuySell.topAnchor.constraint(equalTo: lblPriceValue.bottomAnchor, constant: 8).isActive = true
            segmentBuySell.heightAnchor.constraint(equalToConstant: 30).isActive = true
            segmentBuySell.widthAnchor.constraint(equalToConstant: 70).isActive = true
            
            // btnDeleteQty
            addSubview(btnDeleteQty)
            btnDeleteQty.isEnabled = false
            btnDeleteQty.leftAnchor.constraint(equalTo: segmentBuySell.rightAnchor, constant: 20).isActive = true
            btnDeleteQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
            btnDeleteQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
            btnDeleteQty.centerYAnchor.constraint(equalTo: segmentBuySell.centerYAnchor, constant: 0).isActive = true
            
            // lblUpdatedQty
            addSubview(lblUpdateQty)
            lblUpdateQty.text = "\(updatedQty)"
            lblUpdateQty.textColor = updatedQty == 0 ? .white : (selectedSegment == .Buy) ? Constants.Charts.chartGreenColor : Constants.Charts.chartRedColor
            
            lblUpdateQty.leftAnchor.constraint(equalTo: btnDeleteQty.rightAnchor, constant: 10).isActive = true
            lblUpdateQty.heightAnchor.constraint(equalToConstant: 30).isActive = true
            lblUpdateQty.centerYAnchor.constraint(equalTo: segmentBuySell.centerYAnchor, constant: 0).isActive = true
            lblUpdateQtyWidthConstraint = NSLayoutConstraint(item: lblUpdateQty, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: "\(updatedQty)".sizeOfString(usingFont: UIFont.boldSystemFont(ofSize: 20)).width + 2)
            lblUpdateQtyWidthConstraint.isActive = true
            
            // btnAddQty
            addSubview(btnAddQty)
            btnAddQty.isEnabled = false
            btnAddQty.leftAnchor.constraint(equalTo: lblUpdateQty.rightAnchor, constant: 10).isActive = true
            btnAddQty.widthAnchor.constraint(equalToConstant: 25).isActive = true
            btnAddQty.heightAnchor.constraint(equalToConstant: 25).isActive = true
            btnAddQty.centerYAnchor.constraint(equalTo: segmentBuySell.centerYAnchor, constant: 0).isActive = true
        } else if toolType == .PartProfit || toolType == .BookFullProfit || toolType == .Exit  {
            // lblNetQty
            addSubview(lblNetQty)
            lblNetQty.text = "NET QTY"
            lblNetQty.topAnchor.constraint(equalTo: lblQty.topAnchor).isActive = true
            lblNetQty.heightAnchor.constraint(equalTo: lblQty.heightAnchor, multiplier: 1.0).isActive = true
            lblNetQty.leftAnchor.constraint(equalTo: lblQtyValue.rightAnchor, constant: 10).isActive = true
            lblNetQty.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
            
            // lblNetQtyValue
            addSubview(lblNetQtyValue)
            lblNetQtyValue.text = "\(netQty)"
            lblNetQtyValue.topAnchor.constraint(equalTo: lblNetQty.bottomAnchor).isActive = true
            lblNetQtyValue.leftAnchor.constraint(equalTo: lblNetQty.leftAnchor).isActive = true
            lblNetQtyValue.rightAnchor.constraint(equalTo: lblNetQty.rightAnchor).isActive = true
            lblNetQtyValue.heightAnchor.constraint(equalTo: lblQtyValue.heightAnchor, multiplier: 1.0).isActive = true
        }*/
        
        if showCheckBox {
            lblInstrument.isUserInteractionEnabled = true
            let lblInstrumentTap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
            lblInstrumentTap.numberOfTapsRequired = 1
            lblInstrumentTap.numberOfTouchesRequired = 1
            lblInstrument.addGestureRecognizer(lblInstrumentTap)
            
            lblExpiry.isUserInteractionEnabled = true
            let lblExpiryTap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
            lblExpiryTap.numberOfTapsRequired = 1
            lblExpiryTap.numberOfTouchesRequired = 1
            lblExpiry.addGestureRecognizer(lblExpiryTap)
            
            lblOptType.isUserInteractionEnabled = true
            let lblOptTypeTap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
            lblOptTypeTap.numberOfTapsRequired = 1
            lblOptTypeTap.numberOfTouchesRequired = 1
            lblOptType.addGestureRecognizer(lblOptTypeTap)
        }
    }
    
    @objc private func handleTap(_ recognizer: UITapGestureRecognizer) {
        /*if let v = recognizer.view {
            print(v)
        }*/
        checkBox.isSelected = !checkBox.isSelected
    }
    
    private func enableInteractivity(enable: Bool) {
        
        segmentBuySell.isEnabled = enable
        
        if legAction == .add {
            /*btnAddQty.isEnabled = enable
            btnDeleteQty.isEnabled = enable*/
            
            btnAddQty.isEnabled = enable
            
            btnDeleteQty.isEnabled = enable ? (updatedQty == lot) ? false : enable : enable
        } else if legAction == .update {
            btnAddQty.isEnabled = enable
            
            btnDeleteQty.isEnabled = enable ? (updatedQty == lot) ? false : enable : enable
            
            if selectedSegment == .Buy {
                if originalQty < 0 && originalQty.abs == updatedQty.abs {
                    btnAddQty.isEnabled = enable ? false : enable
                } else {
                    btnAddQty.isEnabled = enable ? true : enable
                }
            } else if selectedSegment == .Sell {
                if originalQty > 0 && originalQty.abs == updatedQty.abs {
                    btnAddQty.isEnabled = enable ? false : enable
                } else {
                    btnAddQty.isEnabled = enable ? true : enable
                }
            }
        }
        
        UIView.animate(withDuration: 0.1) {
            self.lblNetQty.alpha = enable ? 1 : 0
            self.lblNetQtyValue.alpha = enable ? 1 : 0
        }
    }
    
    @objc private func actionDeleteLeg(_ sender: UIButton) {
        print("► actionDeleteLeg()")
        guard let leg = managerDetailLeg else { return }
        guard let idValue = id else { return }
        self.delegate?.legRemoved(leg: leg, id: idValue)
    }
    
    
    @objc private func actionAddOrDeleteQty(_ sender: UIButton) {
        
        guard let toolType = selectedToolType else { return }
        if lot == -1 && toolType != .Modify { return }
        
        if sender == btnAddQty {
            if legAction == .add {
                
                updatedQty += lot
                netQty = updatedQty.abs
                
                if updatedQty != lot {
                    btnDeleteQty.isEnabled = true
                }
                
                updateQtyLabel(qty: updatedQty)
                
                /*if tempQty/lot == -1 {
                    tempQty += (lot * 2)
                } else if tempQty/lot == 1 {
                    tempQty += lot
                } else if tempQty/lot == -2 {
                    tempQty += lot
                } else if (tempQty.abs/lot) % 2 == 0 {
                    tempQty += (lot * 2)
                }
                
                updateQtyLabel(qty: tempQty)*/
            } else if legAction == .update {
                updatedQty += lot
                
                if updatedQty != lot {
                    btnDeleteQty.isEnabled = true
                }
                
                if selectedSegment == .Buy {
                    if originalQty < 0 && originalQty.abs == updatedQty.abs {
                        btnAddQty.isEnabled = false
                    } else {
                        btnAddQty.isEnabled = true
                    }
                } else if selectedSegment == .Sell {
                    if originalQty > 0 && originalQty.abs == updatedQty.abs {
                        btnAddQty.isEnabled = false
                    } else {
                        btnAddQty.isEnabled = true
                    }
                }
                
                updateQtyLabel(qty: updatedQty)
            }
        } else if sender == btnDeleteQty {
            if legAction == .add {
                /*if tempQty/lot == 1 {
                    tempQty -= (lot * 2)
                } else if tempQty/lot == -1 {
                    tempQty -= lot
                } else if tempQty/lot == 2 {
                    tempQty -= lot
                } else if (tempQty.abs/lot) % 2 == 0 {
                    tempQty -= (lot * 2)
                }*/
                
                updatedQty -= lot
                netQty = updatedQty.abs * -1
                
                if updatedQty == lot {
                    btnDeleteQty.isEnabled = false
                }
                
                updateQtyLabel(qty: updatedQty)
            } else if legAction == .update {
                updatedQty -= lot
                
                if updatedQty == lot {
                    btnDeleteQty.isEnabled = false
                }
                
                if selectedSegment == .Buy {
                    if originalQty < 0 && originalQty.abs == updatedQty.abs {
                        btnAddQty.isEnabled = false
                    } else {
                        btnAddQty.isEnabled = true
                    }
                } else if selectedSegment == .Sell {
                    if originalQty > 0 && originalQty.abs == updatedQty.abs {
                        btnAddQty.isEnabled = false
                    } else {
                        btnAddQty.isEnabled = true
                    }
                }
                
                updateQtyLabel(qty: updatedQty)
            }
        }
    }
    
    private func updateQtyLabel(qty: Int) {
        var qtyColor = UIColor.white
        
        if legAction == .add {
            
            if segmentBuySell.selectedSegmentIndex == 0 {
                qtyColor = Constants.Charts.chartGreenColor
            } else {
                qtyColor = Constants.Charts.chartRedColor
            }
            
            /*let lblQtyWidth = "QTY".sizeOfString(usingFont: UIFont.systemFont(ofSize: 10)).width + 2
            let qtyStringWidth = "\(qty)".sizeOfString(usingFont: UIFont.boldSystemFont(ofSize: 16)).width + 2
            let maxWidth = max(lblQtyWidth, qtyStringWidth)
            lblQtyWidthConstraint.constant = maxWidth < oneThird ? maxWidth : oneThird
            layoutIfNeeded()*/
            
            lblUpdateQtyWidthConstraint.constant = "\(qty)".sizeOfString(usingFont: lblUpdateQty.font).width + 2
            layoutIfNeeded()
            
            DispatchQueue.main.async {
                self.lblUpdateQty.text = "\(qty)"
                self.lblUpdateQty.textColor = qtyColor
                
                self.lblNetQtyValue.text = "\(self.netQty)"
            }
            
            guard let id = checkBox.id else { return }
            self.delegate?.quantityUpdated(qty: (segmentBuySell.selectedSegmentIndex == 1) ? (qty * -1) : qty, id: id)
        } else if legAction == .update {
            
            if segmentBuySell.selectedSegmentIndex == 0 {
                netQty = originalQty + qty
                qtyColor = Constants.Charts.chartGreenColor
            } else {
                netQty = originalQty - qty
                qtyColor = Constants.Charts.chartRedColor
            }
            
            lblUpdateQtyWidthConstraint.constant = "\(updatedQty)".sizeOfString(usingFont: lblUpdateQty.font).width + 2
            layoutIfNeeded()
            
            DispatchQueue.main.async {
                self.lblUpdateQty.text = "\(qty)"
                self.lblUpdateQty.textColor = qtyColor
                
                self.lblNetQtyValue.text = "\(self.netQty)"
            }
            
            guard let id = checkBox.id else { return }
            self.delegate?.quantityUpdated(qty: (segmentBuySell.selectedSegmentIndex == 1) ? (qty * -1) : qty, id: id)
        }
        
        
    }
    
    public func markCheckBoxSelected() {
        checkBox.isSelected = true
    }
    
    @objc private func segmentedValueChanged(_ sender: UISegmentedControl) {
        guard let selectedSegmentIndex = ModifySegment(rawValue: sender.selectedSegmentIndex) else  { return }
        selectedSegment = selectedSegmentIndex
        
        if selectedSegment == .Buy {
            if legAction == .add {
                netQty = netQty.abs
                self.updateQtyLabel(qty: self.updatedQty)
            } else if legAction == .update {
                if originalQty < 0 && updatedQty.abs >= originalQty.abs {
                    updatedQty = originalQty.abs
                    btnAddQty.isEnabled = false
                    
                    if updatedQty == lot {
                        btnDeleteQty.isEnabled = false
                    }
                    
                    self.updateQtyLabel(qty: self.updatedQty)
                } else if originalQty > 0 {
                    btnAddQty.isEnabled = true
                }
            }
        } else if selectedSegment == .Sell {
            if legAction == .add {
                netQty = netQty.abs * -1
                self.updateQtyLabel(qty: self.updatedQty)
            } else if legAction == .update {
                if originalQty > 0 && updatedQty.abs >= originalQty.abs {
                    updatedQty = originalQty.abs
                    btnAddQty.isEnabled = false
                    
                    if updatedQty == lot {
                        btnDeleteQty.isEnabled = false
                    }
                    
                    self.updateQtyLabel(qty: self.updatedQty)
                } else if originalQty < 0 {
                    btnAddQty.isEnabled = true
                }
            }
        }
        
        DispatchQueue.main.async {
            sender.tintColor = sender.selectedSegmentIndex == 0 ? Constants.Charts.chartGreenColor : Constants.Charts.chartRedColor
            self.updateQtyLabel(qty: self.updatedQty)
        }
    }
}

extension LegBookFullProfit: OSOCheckBoxDelegate {
    func checkboxToggled(id: Int, selected: Bool) {
        guard let leg = managerDetailLeg else { return }
        
        if selected {
            self.delegate?.legSelected(leg: leg, id: id)
        } else {
            self.delegate?.legDeselected(leg: leg, id: id)
        }
        
        enableInteractivity(enable: selected)
        
        DispatchQueue.main.async {
            self.layer.borderColor = selected ? Constants.UIConfig.themeColor.cgColor : UIColor(white: 1.0, alpha: 0.3).cgColor
        }
        
        if selected {
            if legAction == .update {
                self.delegate?.quantityUpdated(qty: (segmentBuySell.selectedSegmentIndex == 1) ? (updatedQty * -1) : updatedQty, id: id)
            } else if legAction == .add {
                self.delegate?.quantityUpdated(qty: tempQty, id: id)
            }
            
        }
    }
}
