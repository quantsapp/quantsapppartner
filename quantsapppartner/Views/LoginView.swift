//
//  LoginView.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import ActiveLabel
import SwifterSwift
import SkyFloatingLabelTextField

protocol LoginViewDelegate: class {
    func actionBecomeAPartner()
    func actionSignIn(email: String, password: String)
    func actionForgotPassword()
}

extension LoginViewDelegate {
    func actionBecomeAPartner() {}
    func actionSignIn(email: String, password: String) {}
    func actionForgotPassword() {}
}

class LoginView: UIView, UITextFieldDelegate {
    
    weak var delegate: LoginViewDelegate?
    
    private var screenWidth = SwifterSwift.screenWidth
    
    // constraints
    private var imageViewLogoTopAnchorConstraint: NSLayoutConstraint!
    private var imageViewLogoBottomAnchorConstraint: NSLayoutConstraint!
    private var imageViewLogoWidthConstraint: NSLayoutConstraint!
    private var formViewBottomAnchorConstraint: NSLayoutConstraint!
    
    private let lblBecomePartner: ActiveLabel = {
        let label = ActiveLabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor(white: 1.0, alpha: 0.6)
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let formView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear //UIColor(white: 1.0, alpha: 0.2)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    private let imageViewLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "splash_logo")
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let signInButton: UIButton = {
        let button = UIButton()
        button.setTitle("LOG IN", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.backgroundColor = UIColor.rgb(red: 244, green: 155, blue: 15)
        button.layer.cornerRadius = Constants.UIConfig.buttonCornerRadius
        button.layer.masksToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(actionSignIn(_:)), for: .touchUpInside)
        return button
    }()
    
    let forgotPasswordButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.backgroundColor = UIColor.clear
        button.contentHorizontalAlignment = .left
        
        let attrString = NSMutableAttributedString()
        attrString.append(NSAttributedString(string: "Forgot Password?", attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.5), NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13)]))
        button.setAttributedTitle(attrString, for: .normal)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(actionForgotPassword(_:)), for: .touchUpInside)
        return button
    }()
    
    private let tfUsername: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        textField.placeholder = "Email"
        textField.title = "Email"
        if #available(iOS 11.0, *) {
            textField.textContentType = .username
        }
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.clearButtonMode = .whileEditing
        //textField.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
        textField.modifyClearButton(with: UIImage(named: "icon_clear")!)
        textField.tintColor = Constants.SkyFloating.tintColor // the color of the blinking cursor
        textField.textColor = Constants.SkyFloating.textColor
        textField.lineColor = Constants.SkyFloating.lineColor
        textField.selectedTitleColor = Constants.SkyFloating.selectedTitleColor
        textField.selectedLineColor = Constants.SkyFloating.selectedLineColor
        textField.lineHeight = Constants.SkyFloating.lineHeight // bottom line height in points
        textField.selectedLineHeight = Constants.SkyFloating.selectedLineHeight
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.keyboardType = .emailAddress
        /*textField.iconType = .image
         textField.iconImage = UIImage(named: "user_icon")?.withRenderingMode(.alwaysTemplate)*/
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let tfPassword: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        textField.placeholder = "Password"
        textField.title = "Password"
        if #available(iOS 11.0, *) {
            textField.textContentType = .password
        }
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.isSecureTextEntry = true
        textField.modifyClearButton(with: UIImage(named: "icon_clear")!)
        textField.tintColor = Constants.SkyFloating.tintColor // the color of the blinking cursor
        textField.textColor = Constants.SkyFloating.textColor
        textField.lineColor = Constants.SkyFloating.lineColor
        textField.selectedTitleColor = Constants.SkyFloating.selectedTitleColor
        textField.selectedLineColor = Constants.SkyFloating.selectedLineColor
        textField.lineHeight = Constants.SkyFloating.lineHeight // bottom line height in points
        textField.selectedLineHeight = Constants.SkyFloating.selectedLineHeight
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        if #available(iOS 10, *) {
            // Disables the password autoFill accessory view.
            textField.textContentType = UITextContentType(rawValue: "")
        }
        
        return textField
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    public func clearForm() {
        tfUsername.text = ""
        tfPassword.text = ""
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        
        // keyboard observer
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // lblBecomePartner
        addSubview(lblBecomePartner)
        
        let customType = ActiveType.custom(pattern: "\\bBecome a Partner\\b") //Regex that looks for "with" //"\\swith\\b"
        lblBecomePartner.enabledTypes = [customType]
        lblBecomePartner.text = "Don't have an account? Become a Partner"
        lblBecomePartner.customColor[customType] = Constants.UIConfig.themeColor
        lblBecomePartner.customSelectedColor[customType] = UIColor.white
        lblBecomePartner.handleCustomTap(for: customType) { element in
            self.delegate?.actionBecomeAPartner()
        }
        
        lblBecomePartner.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        lblBecomePartner.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        lblBecomePartner.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        lblBecomePartner.heightAnchor.constraint(equalToConstant: (lblBecomePartner.text?.height(constraintedWidth: screenWidth - 40, font: lblBecomePartner.font))!).isActive = true
        
        
        // formView
        addSubview(formView)
        
        formView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        formView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        formView.heightAnchor.constraint(equalToConstant: 280).isActive = true
        
        formViewBottomAnchorConstraint = NSLayoutConstraint(item: formView,
                                                            attribute: .bottom,
                                                            relatedBy: .equal,
                                                            toItem: lblBecomePartner,
                                                            attribute: .top,
                                                            multiplier: 1.0,
                                                            constant: -10)
        
        formViewBottomAnchorConstraint.isActive = true
        
        // signInButton
        formView.addSubview(signInButton)
        signInButton.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -10).isActive = true
        signInButton.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 20).isActive = true
        signInButton.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -20).isActive = true
        signInButton.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        
        // forgotPasswordButton
        formView.addSubview(forgotPasswordButton)
        forgotPasswordButton.bottomAnchor.constraint(equalTo: signInButton.topAnchor, constant: -10).isActive = true
        forgotPasswordButton.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        forgotPasswordButton.widthAnchor.constraint(equalToConstant: (forgotPasswordButton.titleLabel?.text?.sizeOfString(usingFont: (forgotPasswordButton.titleLabel?.font)!).width)! + 20).isActive = true
        forgotPasswordButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        
        // tfPassword
        formView.addSubview(tfPassword)
        tfPassword.delegate = self
        //tfPassword.returnKeyType = .done
        
        tfPassword.bottomAnchor.constraint(equalTo: forgotPasswordButton.topAnchor, constant: -40).isActive = true
        tfPassword.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 30).isActive = true
        tfPassword.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -30).isActive = true
        tfPassword.heightAnchor.constraint(equalToConstant: Constants.SkyFloating.textFieldHeight).isActive = true
        
        
        // tfUsername
        formView.addSubview(tfUsername)
        tfUsername.delegate = self
        //tfUsername.returnKeyType = .continue
        
        tfUsername.bottomAnchor.constraint(equalTo: tfPassword.topAnchor, constant: -20).isActive = true
        tfUsername.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 30).isActive = true
        tfUsername.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -30).isActive = true
        tfUsername.heightAnchor.constraint(equalToConstant: Constants.SkyFloating.textFieldHeight).isActive = true
        
        
        
        // imageViewLogo
        addSubview(imageViewLogo)
        
        imageViewLogo.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        if #available(iOS 11.0, *) {
            imageViewLogo.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        } else {
            imageViewLogo.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        }
        
        imageViewLogoBottomAnchorConstraint = NSLayoutConstraint(item: imageViewLogo,
                                                                 attribute: .bottom,
                                                                 relatedBy: .equal,
                                                                 toItem: formView,
                                                                 attribute: .top,
                                                                 multiplier: 1.0,
                                                                 constant: 0)
        
        imageViewLogoWidthConstraint = NSLayoutConstraint(item: imageViewLogo,
                                                          attribute: .width,
                                                          relatedBy: .equal,
                                                          toItem: self,
                                                          attribute: .width,
                                                          multiplier: Constants.APPConfig.isDeviceIphone ? 0.65 : 0.4,
                                                          constant: 0)
        
        imageViewLogoBottomAnchorConstraint.isActive = true
        imageViewLogoWidthConstraint.isActive = true
        
        if Constants.APPConfig.inDevelopment {
            insertDummyData()
        } else {
            clearForm()
        }
    }
    
    private func insertDummyData() {
        tfUsername.text = "tushar@quantsapp.com"
        tfPassword.text = "tusharq007"
    }
    
    @objc private func actionSignIn(_ sender: UIButton) {
        resignAllResponders()
        
        guard let parentVC = parentViewController else { return }
        
        if let email = tfUsername.text?.trimmed {
            if email.isEmpty {
                parentVC.showZAlertView(withTitle: "Error", andMessage: "Please enter your email.", cancelTitle: "OK") {
                    self.tfUsername.becomeFirstResponder()
                }
            } else if !email.isEmail {
                parentVC.showZAlertView(withTitle: "Error", andMessage: "Please enter valid email.", cancelTitle: "OK") {
                    self.tfUsername.becomeFirstResponder()
                }
            } else if let password = tfPassword.text?.trimmed {
                if password.isEmpty {
                    parentVC.showZAlertView(withTitle: "Error", andMessage: "Please enter password.", cancelTitle: "OK") {
                        self.tfPassword.becomeFirstResponder()
                    }
                } else {
                    self.delegate?.actionSignIn(email: email, password: password)
                }
            } else {
                parentVC.showZAlertView(withTitle: "Error", andMessage: "Please enter password.", cancelTitle: "OK") {
                    self.tfPassword.becomeFirstResponder()
                }
            }
        } else {
            parentVC.showZAlertView(withTitle: "Error", andMessage: "Please enter your email.", cancelTitle: "OK") {
                self.tfUsername.becomeFirstResponder()
            }
        }
    }
    
    @objc private func actionForgotPassword(_ sender: UIButton) {
        resignAllResponders()
        self.delegate?.actionForgotPassword()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc private func keyBoardDidShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            formViewBottomAnchorConstraint.isActive = false
            
            formViewBottomAnchorConstraint = NSLayoutConstraint(item: formView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: -(keyboardSize.height))
            
            formViewBottomAnchorConstraint.isActive = true
            
            UIView.animate(withDuration: 0.3) {
                self.layoutIfNeeded()
            }
            
        } else {
            UIView.animate(withDuration: 0.3) {
                self.formViewBottomAnchorConstraint.constant = -50
                self.imageViewLogoWidthConstraint.constant = -20
                self.layoutIfNeeded()
            }
        }
        
        
    }
    
    
    @objc private func keyBoardWillHide(notification: NSNotification) {
        
        formViewBottomAnchorConstraint.isActive = false
        
        formViewBottomAnchorConstraint = NSLayoutConstraint(item: formView,
                                                            attribute: .bottom,
                                                            relatedBy: .equal,
                                                            toItem: lblBecomePartner,
                                                            attribute: .top,
                                                            multiplier: 1.0,
                                                            constant: -10)
        
        formViewBottomAnchorConstraint.isActive = true
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
}
