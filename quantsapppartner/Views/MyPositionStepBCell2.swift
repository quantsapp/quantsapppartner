//
//  MyPositionStepBCell2.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 23/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class MyPositionStepBCell2: UITableViewCell {
    
    static let cellIdentifier = "MyPositionStepBCell2"
    
    private let fieldHeight: CGFloat = 20.0
    
    public var deltaValue: String = "0" {
        didSet {
            lblDeltaValue.text = deltaValue
            
            if deltaValue == "GO PRO" {
                lblDeltaValue.isHidden = true
                stickerDelta.isHidden = false
            } else {
                lblDeltaValue.isHidden = false
                stickerDelta.isHidden = true
            }
        }
    }
    
    public var thetaValue: String = "0" {
        didSet {
            lblThetaValue.text = thetaValue
            
            if thetaValue == "GO PRO" {
                lblThetaValue.isHidden = true
                stickerTheta.isHidden = false
            } else {
                lblThetaValue.isHidden = false
                stickerTheta.isHidden = true
            }
        }
    }
    
    public var vegaValue: String = "0" {
        didSet {
            lblVegaValue.text = vegaValue
            
            if vegaValue == "GO PRO" {
                lblVegaValue.isHidden = true
                stickerVega.isHidden = false
            } else {
                lblVegaValue.isHidden = false
                stickerVega.isHidden = true
            }
        }
    }
    
    public var gammaValue: String = "0" {
        didSet {
            lblGammaValue.text = gammaValue
            
            if gammaValue == "GO PRO" {
                lblGammaValue.isHidden = true
                stickerGamma.isHidden = false
            } else {
                lblGammaValue.isHidden = false
                stickerGamma.isHidden = true
            }
        }
    }
    
    private let lblDelta: UILabel = {
        let label = UILabel()
        label.text = "Delta"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblDeltaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblTheta: UILabel = {
        let label = UILabel()
        label.text = "Theta"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblThetaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblVega: UILabel = {
        let label = UILabel()
        label.text = "Vega"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblVegaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblGamma: UILabel = {
        let label = UILabel()
        label.text = "Gamma"
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblGammaValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "0"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let stickerDelta: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    private let stickerTheta: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    
    private let stickerVega: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    
    
    private let stickerGamma: ProSticker = {
        let sticker = ProSticker()
        sticker.translatesAutoresizingMaskIntoConstraints = false
        return sticker
    }()
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        // lblDelta
        addSubview(lblDelta)
        lblDelta.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblDelta.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblDelta.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblDelta.widthAnchor.constraint(equalToConstant: (lblDelta.text?.sizeOfString(usingFont: lblDelta.font).width)! + 4).isActive = true
        
        // lblDeltaValue
        addSubview(lblDeltaValue)
        lblDeltaValue.isHidden = false
        lblDeltaValue.leftAnchor.constraint(equalTo: lblDelta.rightAnchor, constant: 4).isActive = true
        lblDeltaValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblDeltaValue.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblDeltaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // stickerDelta
        addSubview(stickerDelta)
        stickerDelta.isHidden = true
        stickerDelta.rightAnchor.constraint(equalTo: lblDeltaValue.rightAnchor, constant: 0).isActive = true
        stickerDelta.topAnchor.constraint(equalTo: lblDeltaValue.topAnchor, constant: 1).isActive = true
        stickerDelta.bottomAnchor.constraint(equalTo: lblDeltaValue.bottomAnchor, constant: -1).isActive = true
        stickerDelta.widthAnchor.constraint(equalToConstant: stickerDelta.labelWidth).isActive = true
        
        // lblTheta
        addSubview(lblTheta)
        lblTheta.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblTheta.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblTheta.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblTheta.widthAnchor.constraint(equalToConstant: (lblTheta.text?.sizeOfString(usingFont: lblTheta.font).width)! + 4).isActive = true
        
        // lblThetaValue
        addSubview(lblThetaValue)
        lblThetaValue.isHidden = false
        lblThetaValue.leftAnchor.constraint(equalTo: lblTheta.rightAnchor, constant: 4).isActive = true
        lblThetaValue.topAnchor.constraint(equalTo: lblDelta.topAnchor, constant: 0).isActive = true
        lblThetaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblThetaValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // stickerTheta
        addSubview(stickerTheta)
        stickerTheta.isHidden = true
        stickerTheta.rightAnchor.constraint(equalTo: lblThetaValue.rightAnchor, constant: 0).isActive = true
        stickerTheta.topAnchor.constraint(equalTo: lblThetaValue.topAnchor, constant: 1).isActive = true
        stickerTheta.bottomAnchor.constraint(equalTo: lblThetaValue.bottomAnchor, constant: -1).isActive = true
        stickerTheta.widthAnchor.constraint(equalToConstant: stickerTheta.labelWidth).isActive = true
        
        
        // lblVega
        addSubview(lblVega)
        lblVega.leftAnchor.constraint(equalTo: lblDelta.leftAnchor, constant: 0).isActive = true
        lblVega.topAnchor.constraint(equalTo: lblDelta.bottomAnchor, constant: 0).isActive = true
        lblVega.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblVega.widthAnchor.constraint(equalToConstant: (lblVega.text?.sizeOfString(usingFont: lblVega.font).width)! + 4).isActive = true
        
        // lblVegaValue
        addSubview(lblVegaValue)
        lblVegaValue.isHidden = false
        lblVegaValue.leftAnchor.constraint(equalTo: lblVega.rightAnchor, constant: 4).isActive = true
        lblVegaValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblVegaValue.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblVegaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // stickerVega
        addSubview(stickerVega)
        stickerVega.isHidden = true
        stickerVega.rightAnchor.constraint(equalTo: lblVegaValue.rightAnchor, constant: 0).isActive = true
        stickerVega.topAnchor.constraint(equalTo: lblVegaValue.topAnchor, constant: 1).isActive = true
        stickerVega.bottomAnchor.constraint(equalTo: lblVegaValue.bottomAnchor, constant: -1).isActive = true
        stickerVega.widthAnchor.constraint(equalToConstant: stickerVega.labelWidth).isActive = true
        
        // lblGamma
        addSubview(lblGamma)
        lblGamma.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblGamma.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblGamma.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblGamma.widthAnchor.constraint(equalToConstant: (lblGamma.text?.sizeOfString(usingFont: lblGamma.font).width)! + 4).isActive = true
        
        // lblGammaValue
        addSubview(lblGammaValue)
        lblGammaValue.isHidden = false
        lblGammaValue.leftAnchor.constraint(equalTo: lblGamma.rightAnchor, constant: 4).isActive = true
        lblGammaValue.topAnchor.constraint(equalTo: lblVega.topAnchor, constant: 0).isActive = true
        lblGammaValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblGammaValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // stickerGamma
        addSubview(stickerGamma)
        stickerGamma.isHidden = true
        stickerGamma.rightAnchor.constraint(equalTo: lblGammaValue.rightAnchor, constant: 0).isActive = true
        stickerGamma.topAnchor.constraint(equalTo: lblGammaValue.topAnchor, constant: 1).isActive = true
        stickerGamma.bottomAnchor.constraint(equalTo: lblGammaValue.bottomAnchor, constant: -1).isActive = true
        stickerGamma.widthAnchor.constraint(equalToConstant: stickerGamma.labelWidth).isActive = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
