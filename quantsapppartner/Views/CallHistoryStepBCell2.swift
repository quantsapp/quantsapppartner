//
//  CallHistoryStepBCell2.swift
//  quantsapppartner
//
//  Created by Quantsapp on 03/04/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift

class CallHistoryStepBCell2: UITableViewCell {

    static let cellIdentifier = "CallHistoryStepBCell2"
    
    private let fieldHeight: CGFloat = 16.0
    private let screenWidth = SwifterSwift.screenWidth
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.borderColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblClosePrice: UILabel = {
        let label = UILabel()
        label.text = "Close Price"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblClosePriceValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPnL: UILabel = {
        let label = UILabel()
        label.text = "PnL"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblPnLValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblInstrument: UILabel = {
        let label = UILabel()
        label.text = "Instrument"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblInstrumentValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblExpiry: UILabel = {
        let label = UILabel()
        label.text = "Expiry"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblExpiryValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblStrike: UILabel = {
        let label = UILabel()
        label.text = "Strike"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblStrikeValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblOptionType: UILabel = {
        let label = UILabel()
        label.text = "Option Type"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblOptionTypeValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblPrice: UILabel = {
        let label = UILabel()
        label.text = SwifterSwift.screenWidth <= Constants.Screen.minimumScreenWidth ? "Init. Price" : "Initiation Price"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblPriceValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblQuantity: UILabel = {
        let label = UILabel()
        label.text = "Quantity"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblQuantityValue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.5)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "-"
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        // lblClosePrice
        addSubview(lblClosePrice)
        lblClosePrice.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblClosePrice.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblClosePrice.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblClosePrice.widthAnchor.constraint(equalToConstant: (lblClosePrice.text?.sizeOfString(usingFont: lblClosePrice.font).width)! + 4).isActive = true
        
        // lblClosePriceValue
        addSubview(lblClosePriceValue)
        lblClosePriceValue.leftAnchor.constraint(equalTo: lblClosePrice.rightAnchor, constant: 4).isActive = true
        lblClosePriceValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblClosePriceValue.topAnchor.constraint(equalTo: lblClosePrice.topAnchor, constant: 0).isActive = true
        lblClosePriceValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblPnL
        addSubview(lblPnL)
        lblPnL.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblPnL.topAnchor.constraint(equalTo: lblClosePrice.topAnchor, constant: 0).isActive = true
        lblPnL.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPnL.widthAnchor.constraint(equalToConstant: (lblPnL.text?.sizeOfString(usingFont: lblPnL.font).width)! + 4).isActive = true
        
        // lblPnLValue
        addSubview(lblPnLValue)
        lblPnLValue.leftAnchor.constraint(equalTo: lblPnL.rightAnchor, constant: 4).isActive = true
        lblPnLValue.topAnchor.constraint(equalTo: lblClosePrice.topAnchor, constant: 0).isActive = true
        lblPnLValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPnLValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblInstrument
        addSubview(lblInstrument)
        lblInstrument.leftAnchor.constraint(equalTo: lblClosePrice.leftAnchor, constant: 0).isActive = true
        lblInstrument.topAnchor.constraint(equalTo: lblClosePrice.bottomAnchor, constant: 0).isActive = true
        lblInstrument.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblInstrument.widthAnchor.constraint(equalToConstant: (lblInstrument.text?.sizeOfString(usingFont: lblInstrument.font).width)! + 4).isActive = true
        
        // lblInstrumentValue
        addSubview(lblInstrumentValue)
        lblInstrumentValue.leftAnchor.constraint(equalTo: lblInstrument.rightAnchor, constant: 4).isActive = true
        lblInstrumentValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblInstrumentValue.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblInstrumentValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblExpiry
        addSubview(lblExpiry)
        lblExpiry.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblExpiry.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblExpiry.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblExpiry.widthAnchor.constraint(equalToConstant: (lblExpiry.text?.sizeOfString(usingFont: lblExpiry.font).width)!).isActive = true
        
        // lblExpiryValue
        addSubview(lblExpiryValue)
        lblExpiryValue.leftAnchor.constraint(equalTo: lblExpiry.rightAnchor, constant: 2).isActive = true
        lblExpiryValue.topAnchor.constraint(equalTo: lblInstrument.topAnchor, constant: 0).isActive = true
        lblExpiryValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblExpiryValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblStrike
        addSubview(lblStrike)
        lblStrike.leftAnchor.constraint(equalTo: lblClosePrice.leftAnchor, constant: 0).isActive = true
        lblStrike.topAnchor.constraint(equalTo: lblInstrument.bottomAnchor, constant: 0).isActive = true
        lblStrike.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblStrike.widthAnchor.constraint(equalToConstant: (lblStrike.text?.sizeOfString(usingFont: lblStrike.font).width)! + 4).isActive = true
        
        // lblStrikeValue
        addSubview(lblStrikeValue)
        lblStrikeValue.leftAnchor.constraint(equalTo: lblStrike.rightAnchor, constant: 4).isActive = true
        lblStrikeValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblStrikeValue.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblStrikeValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        
        // lblOptionType
        addSubview(lblOptionType)
        lblOptionType.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblOptionType.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblOptionType.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblOptionType.widthAnchor.constraint(equalToConstant: (lblOptionType.text?.sizeOfString(usingFont: lblOptionType.font).width)! + 4).isActive = true
        
        // lblOptionTypeValue
        addSubview(lblOptionTypeValue)
        lblOptionTypeValue.leftAnchor.constraint(equalTo: lblOptionType.rightAnchor, constant: 4).isActive = true
        lblOptionTypeValue.topAnchor.constraint(equalTo: lblStrike.topAnchor, constant: 0).isActive = true
        lblOptionTypeValue.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblOptionTypeValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        // lblPrice
        addSubview(lblPrice)
        lblPrice.leftAnchor.constraint(equalTo: lblClosePrice.leftAnchor, constant: 0).isActive = true
        lblPrice.topAnchor.constraint(equalTo: lblStrike.bottomAnchor, constant: 0).isActive = true
        lblPrice.heightAnchor.constraint(equalToConstant: fieldHeight).isActive = true
        lblPrice.widthAnchor.constraint(equalToConstant: (lblPrice.text?.sizeOfString(usingFont: lblPrice.font).width)! + 4).isActive = true
        
        // lblPriceValue
        addSubview(lblPriceValue)
        lblPriceValue.isHidden = false
        lblPriceValue.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 4).isActive = true
        lblPriceValue.rightAnchor.constraint(equalTo: centerXAnchor, constant: -8).isActive = true
        lblPriceValue.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        lblPriceValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // lblQuantity
        addSubview(lblQuantity)
        lblQuantity.leftAnchor.constraint(equalTo: centerXAnchor, constant: 8).isActive = true
        lblQuantity.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        lblQuantity.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblQuantity.widthAnchor.constraint(equalToConstant: (lblQuantity.text?.sizeOfString(usingFont: lblQuantity.font).width)! + 4).isActive = true
        
        // lblQuantityValue
        addSubview(lblQuantityValue)
        lblQuantityValue.isHidden = false
        lblQuantityValue.leftAnchor.constraint(equalTo: lblQuantity.rightAnchor, constant: 4).isActive = true
        lblQuantityValue.topAnchor.constraint(equalTo: lblPrice.topAnchor, constant: 0).isActive = true
        lblQuantityValue.heightAnchor.constraint(equalToConstant: 16).isActive = true
        lblQuantityValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
