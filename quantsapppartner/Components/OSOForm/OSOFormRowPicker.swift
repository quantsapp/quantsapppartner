//
//  OSOFormRowPicker.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

protocol OSOFormRowPickerDelegate: class {
    func optionChanged(row: OSOFormRowPicker, option: String, optionIndex: Int)
}

class OSOFormRowPicker: OSOFormRow {
    
    weak var delegate: OSOFormRowPickerDelegate?
    
    public var options: [String]?
    public var selectedOption: String? {
        didSet {
            if let option = selectedOption, let optionsArray = options {
                delegate?.optionChanged(row: self, option: option, optionIndex: optionsArray.index(of: option)!)
            }
        }
    }
    public var placeholder: String?
    
    init(rowIdentifier: String, rowText: String, isHidden: Bool, options: [String], selectedOption: String?, placeholder: String?) {
        super.init(rowType: OSOFormRowType.RowPicker, rowText: rowText, isHidden: isHidden, rowIdentifier: rowIdentifier)
        self.options = options
        self.selectedOption = selectedOption
        self.placeholder = placeholder
    }
    
}
