//
//  OSOFormRowDate.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 13/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OSOFormRowDate: OSOFormRow {
    
    public var selectedDate: Date?
    public var minimumDate: Date?
    
    init(rowIdentifier: String, rowText: String, isHidden: Bool, selectedDate: Date?, minimumDate: Date?) {
        super.init(rowType: OSOFormRowType.RowPicker, rowText: rowText, isHidden: isHidden, rowIdentifier: rowIdentifier)
        self.selectedDate = selectedDate
        self.minimumDate = minimumDate
    }
    
}
