//
//  OSOFormTextCell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class OSOFormTextCell: UITableViewCell {
    
    static let cellIdentifier = "OSOFormTextCell"
    
    public var row: OSOFormRowText?
    public var inputType: OSOFORMRowInputType?
    
    public var placeHolderText: String = "" {
        didSet {
            setPlaceholderText()
        }
    }
    
    let lblFieldName: UILabel = {
        let label = UILabel()
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = Constants.UIConfig.fieldLabelFont
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tfFieldValue: UITextField = {
        let textField = UITextField()
        textField.textColor = Constants.UIConfig.themeColor
        textField.font = UIFont.boldSystemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.cellSeparatorColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        
        // tfFieldValue
        addSubview(tfFieldValue)
        tfFieldValue.widthAnchor.constraint(equalToConstant: 130).isActive = true
        tfFieldValue.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        tfFieldValue.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1.0).isActive = true
        tfFieldValue.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        // lblFieldName
        addSubview(lblFieldName)
        lblFieldName.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        lblFieldName.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1.0).isActive = true
        lblFieldName.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        lblFieldName.rightAnchor.constraint(equalTo: tfFieldValue.leftAnchor, constant: -8).isActive = true
        
        
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    
    
    
    private func setPlaceholderText() {
        /*DispatchQueue.main.async {
            
        }*/
        
        tfFieldValue.attributedPlaceholder = NSAttributedString(string: placeHolderText, attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.3), NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        } else {
            backgroundColor = UIColor.clear
        }
    }

}
