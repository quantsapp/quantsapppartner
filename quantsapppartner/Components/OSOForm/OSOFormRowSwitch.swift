//
//  OSOFormRowSwitch.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OSOFormRowSwitch: OSOFormRow {
    
    public var isOn: Bool? {
        didSet {
            updateDependentRows(on: isOn!)
        }
    }
    
    public var dependentRows: [OSOFormRow]?
    
    init(rowIdentifier: String, rowText: String, isHidden: Bool, isOn: Bool) {
        super.init(rowType: OSOFormRowType.RowSwitch, rowText: rowText, isHidden: isHidden, rowIdentifier: rowIdentifier)
        self.isOn = isOn
    }
    
    private func updateDependentRows(on: Bool) {
        if let rows = dependentRows {
            for row in rows {
                row.isHidden = !on
            }
        }
    }
    
}
