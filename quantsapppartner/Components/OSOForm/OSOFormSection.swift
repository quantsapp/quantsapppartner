//
//  OSOFormSection.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OSOFormSection {
    
    public var sectionIndex: Int?
    private var sectionTitle: String?
    private var formRows = [OSOFormRow]()
    
    public var numberOfRows: Int {
        get {
            return formRows.count
        }
    }
    
    public var rowsInSection: [OSOFormRow] {
        get {
            return formRows
        }
    }
    
    public var titleForSection: String? {
        set {
            sectionTitle = titleForSection
        }
        
        get {
            return sectionTitle
        }
    }
    
    init(sectionTitle: String?) {
        self.sectionTitle = sectionTitle
    }
    
    public func addRows(formRows: OSOFormRow...) {
        
        for row in formRows {
            row.rowIndex = self.formRows.count
            row.section = self
            self.formRows.append(row)
        }
    }
    
    public func formRow(forIndex index: Int) -> OSOFormRow {
        return formRows[index]
    }
    
}
