//
//  OSOFormRadioCell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class OSOFormRadioCell: UITableViewCell {
    
    static let cellIdentifier = "OSOFormRadioCell"
    
    public var row: OSOFormRowRadio?
    public var options: [String]?
    
    let radioButton: RadioOptionButton = {
        let button = RadioOptionButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_check")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .left
        button.contentHorizontalAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.cellSeparatorColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        
        // radioButton
        addSubview(radioButton)
        radioButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        radioButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        //radioButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        radioButton.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1.0).isActive = true
        radioButton.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        } else {
            backgroundColor = UIColor.clear
        }
    }

}
