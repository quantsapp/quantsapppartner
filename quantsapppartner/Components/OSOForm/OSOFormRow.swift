//
//  OSOFormRow.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OSOFormRow {
    
    public var rowIndex: Int?
    public var rowType: OSOFormRowType?
    public var isHidden: Bool?
    public var rowText: String = ""
    public var section: OSOFormSection?
    public var indexPath: IndexPath?
    public var rowIdentifier: String?
    
    init(rowType: OSOFormRowType, rowText: String, isHidden: Bool, rowIdentifier: String) {
        self.rowType = rowType
        self.rowText = rowText
        self.isHidden = isHidden
        self.rowIdentifier = rowIdentifier
    }
}
