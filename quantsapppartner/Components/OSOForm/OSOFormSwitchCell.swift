//
//  OSOFormSwitchCell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

protocol OSOFormSwitchCellDelegate: class {
    func switchToggled(rowSwitch: OSOFormRowSwitch)
}

class OSOFormSwitchCell: UITableViewCell {
    
    static let cellIdentifier = "OSOFormSwitchCell"
    
    weak var delegate: OSOFormSwitchCellDelegate?
    
    public var row: OSOFormRowSwitch?
    
    let fieldSwitch: UISwitch = {
        let btnSwitch = UISwitch()
        btnSwitch.tintColor = UIColor(white: 1.0, alpha: 0.4)
        btnSwitch.onTintColor = Constants.UIConfig.themeColor
        btnSwitch.translatesAutoresizingMaskIntoConstraints = false
        return btnSwitch
    }()
    
    let lblFieldName: UILabel = {
        let label = UILabel()
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = Constants.UIConfig.fieldLabelFont
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.cellSeparatorColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        // tfFieldValue
        addSubview(fieldSwitch)
        //fieldSwitch.widthAnchor.constraint(equalToConstant: 130).isActive = true
        //fieldSwitch.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        fieldSwitch.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        //fieldSwitch.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
        fieldSwitch.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        // lblFieldName
        addSubview(lblFieldName)
        lblFieldName.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        lblFieldName.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1.0).isActive = true
        lblFieldName.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        lblFieldName.rightAnchor.constraint(equalTo: fieldSwitch.leftAnchor, constant: -8).isActive = true
        
        
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        } else {
            backgroundColor = UIColor.clear
        }
    }

}
