//
//  OSOFormOptionsCell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class OSOFormOptionsCell: UITableViewCell {
    
    static let cellIdentifier = "OSOFormOptionsCell"
    
    public var row: OSOFormRowOptions?
    public var options: [String]? {
        didSet {
            updateWidthContraints()
        }
    }
    private var widthForOption: CGFloat = 160
    private var dropdownOptionWidthContraint: NSLayoutConstraint!
    
    let dropdownOption: DropdownButton = {
        let button = DropdownButton()
        button.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: "icon_dropdown")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentHorizontalAlignment = .right
        button.tintColor = UIColor(white: 1.0, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let lblFieldName: UILabel = {
        let label = UILabel()
        label.textColor = Constants.UIConfig.fieldLabelColor
        label.font = Constants.UIConfig.fieldLabelFont
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bottomBorder: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.cellSeparatorColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        clipsToBounds = true
        
        
        // tfFieldValue
        addSubview(dropdownOption)
        dropdownOption.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        dropdownOption.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        //dropdownOption.heightAnchor.constraint(equalToConstant: 30).isActive = true
        dropdownOption.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1.0).isActive = true
        dropdownOptionWidthContraint = NSLayoutConstraint(item: dropdownOption, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: widthForOption)
        dropdownOptionWidthContraint.isActive = true
        
        // lblFieldName
        addSubview(lblFieldName)
        lblFieldName.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        lblFieldName.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1.0).isActive = true
        lblFieldName.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        lblFieldName.rightAnchor.constraint(equalTo: dropdownOption.leftAnchor, constant: -8).isActive = true
        
        
        
        // bottomBorder
        addSubview(bottomBorder)
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    private func updateWidthContraints() {
        if let op = self.options {
            dropdownOptionWidthContraint.constant = maxWidth(array: op)
            layoutIfNeeded()
        }
    }
    
    private func maxWidth(array: [String]) -> CGFloat {
        var maximumWidth: CGFloat = 0
        
        for str in array {
            let w = str.sizeOfString(usingFont: (dropdownOption.titleLabel?.font)!).width
            if w > maximumWidth {
                maximumWidth = w
            }
        }
        
        return maximumWidth + 50
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        } else {
            backgroundColor = UIColor.clear
        }
    }

}
