//
//  OSOFormRowOptions.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

protocol OSOFormRowOptionsDelegate: class {
    func optionChanged(row: OSOFormRowOptions, option: String)
}

class OSOFormRowOptions: OSOFormRow {
    
    weak var delegate: OSOFormRowOptionsDelegate?
    
    public var options: [String]?
    public var selectedOption: String? {
        didSet {
            if let option = selectedOption {
                delegate?.optionChanged(row: self, option: option)
            }
        }
    }
    
    init(rowIdentifier: String, rowText: String, isHidden: Bool, options: [String], selectedOption: String) {
        super.init(rowType: OSOFormRowType.RowOptions, rowText: rowText, isHidden: isHidden, rowIdentifier: rowIdentifier)
        self.options = options
        self.selectedOption = selectedOption
    }
    
}
