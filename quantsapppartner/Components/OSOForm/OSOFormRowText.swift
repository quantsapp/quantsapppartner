//
//  OSOFormRowText.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation
import UIKit

class OSOFormRowText: OSOFormRow {
    
    public var placeholder: String?
    public var value: String?
    public var inputType: OSOFORMRowInputType?
    public var isTextEditable: Bool = true
    
    
    init(rowIdentifier: String, rowText: String, isHidden: Bool, rowValue: String?, placeholder: String?, inputType: OSOFORMRowInputType = OSOFORMRowInputType.Default, isTextEditable: Bool = true) {
        super.init(rowType: OSOFormRowType.RowText, rowText: rowText, isHidden: isHidden, rowIdentifier: rowIdentifier)
        self.placeholder = placeholder
        self.value = rowValue
        self.inputType = inputType
        self.isTextEditable = isTextEditable
    }
}
