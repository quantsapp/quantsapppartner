//
//  OSOForm.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit
import Sheeeeeeeeet
import ActionSheetPicker_3_0

public enum OSOFormRowType: String {
    case RowText = "text"
    case RowRadio = "radio"
    case RowSwitch = "switch"
    case RowOptions = "options"
    case RowPicker = "picker"
}

public enum OSOFORMRowInputType: Int {
    case Default = 0
    case Decimal = 1
    case Number = 2
}

class OSOForm: UIView, UITextFieldDelegate {
    
    private var formSections = [OSOFormSection]()
    private var sectionIndex: Int = 0
    
    let delayInSeconds = 0.1
    
    let formTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = UIColor.clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        
        // formTableView
        addSubview(formTableView)
        formTableView.dataSource = self
        formTableView.delegate = self
        
        formTableView.tableFooterView = UIView()
        formTableView.showsVerticalScrollIndicator = false
        formTableView.alwaysBounceVertical = false
        formTableView.estimatedRowHeight = 44
        formTableView.rowHeight = UITableView.automaticDimension
        formTableView.keyboardDismissMode = .onDrag
        formTableView.separatorStyle = .none
        
        formTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        formTableView.register(OSOFormTextCell.self, forCellReuseIdentifier: OSOFormTextCell.cellIdentifier)
        formTableView.register(OSOFormSwitchCell.self, forCellReuseIdentifier: OSOFormSwitchCell.cellIdentifier)
        formTableView.register(OSOFormOptionsCell.self, forCellReuseIdentifier: OSOFormOptionsCell.cellIdentifier)
        formTableView.register(OSOFormPickerCell.self, forCellReuseIdentifier: OSOFormPickerCell.cellIdentifier)
        formTableView.register(OSOFormRadioCell.self, forCellReuseIdentifier: OSOFormRadioCell.cellIdentifier)
        
        formTableView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        formTableView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        formTableView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        formTableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    }
    
    public func addSection(sections: OSOFormSection...) {
        
        for section in sections {
            section.sectionIndex = self.formSections.count
            self.formSections.append(section)
        }
    }
    
    public func reloadForm() {
        self.formTableView.reloadData()
    }
    
    
    @objc private func showOptions(_ sender: DropdownButton) {
        if let options = sender.options {
            
            guard let vc = getParentViewController() else { return }
            vc.view.endEditing(true)
            
            var items = [ActionSheetItem]()
            items.append(ActionSheetCancelButton(title: "Cancel"))
            
            for option in options {
                let item = ActionSheetItem(title: option, value: option, image: nil)
                items.append(item)
            }
            
            let actionSheet = ActionSheet(items: items) { (action, item) in
                guard let value = item.value as? String else { return }
                sender.setTitle(value, for: .normal)
                
                if let parentViewCell = sender.superview {
                    if parentViewCell.isKind(of: OSOFormOptionsCell.self) {
                        let cell = parentViewCell as! OSOFormOptionsCell
                        if let row = cell.row {
                            row.selectedOption = value
                        }
                    }
                }
            }
            
            actionSheet.presenter.isDismissableWithTapOnBackground = Constants.APPConfig.isDeviceIphone ? false : true
            actionSheet.present(in: vc, from: sender)
        }
    }
    
    @objc private func showPicker(_ sender: DropdownButton) {
        if let options = sender.options {
            
            guard let vc = getParentViewController() else { return }
            vc.view.endEditing(true)
            
            var initialSelection = 0
            
            if let selectedOption = sender.selectedOption {
                initialSelection = options.index(of: selectedOption)!
            }
            
            let picker = ActionSheetStringPicker(title: sender.placeholder!, rows: options, initialSelection: initialSelection, doneBlock: { (picker, value, index) in
                sender.setTitle(index as? String, for: .normal)
                sender.selectedOption = index as? String
                if let parentViewCell = sender.superview {
                    if parentViewCell.isKind(of: OSOFormPickerCell.self) {
                        let cell = parentViewCell as! OSOFormPickerCell
                        if let row = cell.row {
                            row.selectedOption = index as? String
                        }
                    }
                }
            }, cancel: { (picker) in
                //
            }, origin: sender)
            
            picker?.show()
            
        }
    }
    
    @objc private func radioButtonSelected(_ sender: RadioOptionButton) {
        if let currentRow = sender.row {
            
            guard let vc = getParentViewController() else { return }
            vc.view.endEditing(true)
            
            let currentRadioRow = currentRow
            let sectionIndex = currentRow.section?.sectionIndex!
            
            var indexPaths = [IndexPath]()
            
            if !(currentRadioRow.isSelected!) {
                currentRadioRow.isSelected = true
                let indexPath = IndexPath(item: currentRadioRow.rowIndex!, section: sectionIndex!)
                indexPaths.append(indexPath)
                
                let cell = formTableView.cellForRow(at: indexPath) as! OSOFormRadioCell
                cell.radioButton.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
                cell.radioButton.imageView?.tintColor = Constants.UIConfig.themeColor
                
                if let relatedRows = currentRadioRow.relatedRows {
                    if !(currentRadioRow.selectMultipleEnabled!) {
                        for relatedRow in relatedRows {
                            relatedRow.isSelected = false
                            let indexPath = IndexPath(item: relatedRow.rowIndex!, section: sectionIndex!)
                            indexPaths.append(indexPath)
                            
                            let cell = formTableView.cellForRow(at: indexPath) as! OSOFormRadioCell
                            cell.radioButton.setTitleColor(UIColor(white: 1.0, alpha: 0.25), for: .normal)
                            cell.radioButton.imageView?.tintColor = UIColor(white: 1.0, alpha: 0.1)
                        }
                    }
                }
                
                formTableView.beginUpdates()
                formTableView.endUpdates()
            }
        }
    }
    
    
    private func showBottomBorder(indexPath: IndexPath) -> Bool {
        
        let formSection = formSections[indexPath.section]
        
        if (indexPath.row == formSection.numberOfRows - 1) {
            
            if indexPath.section < (formSections.count - 1) {
                
                let nextFormSection = formSections[indexPath.section + 1]
                
                if (nextFormSection.titleForSection != nil) {
                    
                    return true
                    
                } else {
                    return false
                }
                
            } else {
                
                return false
                
            }
        } else {
            
            return false
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let parentViewCell = textField.superview {
            if parentViewCell.isKind(of: OSOFormTextCell.self) {
                let cell = parentViewCell as! OSOFormTextCell
                if let row = cell.row {
                    if let value = cell.tfFieldValue.text {
                        row.value = value
                    }
                }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let parentViewCell = textField.superview {
            if parentViewCell.isKind(of: OSOFormTextCell.self) {
                let cell = parentViewCell as! OSOFormTextCell
                if let row = cell.row {
                    if row.inputType == OSOFORMRowInputType.Number {
                        let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
                        let components = string.components(separatedBy: inverseSet)
                        let filtered = components.joined(separator: "")
                        return string == filtered
                    } else if row.inputType == OSOFORMRowInputType.Decimal {
                        
                        let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
                        let components = string.components(separatedBy: inverseSet)
                        let filtered = components.joined(separator: "")
                        
                        let countdots =  filtered.components(separatedBy: ".").count - 1
                        if countdots > 0 && string == "." {
                            return false
                        } else {
                            return true
                        }
                    } else {
                        return true
                    }
                } else {
                    return true
                }
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    @objc private func toggleSwitch(_ sender: UISwitch) {
        guard let vc = getParentViewController() else { return }
        vc.view.endEditing(true)
        
        if let parentViewCell = sender.superview {
            if parentViewCell.isKind(of: OSOFormSwitchCell.self) {
                let cell = parentViewCell as! OSOFormSwitchCell
                
                if let row = cell.row {
                    row.isOn = cell.fieldSwitch.isOn
                }
                
                formTableView.beginUpdates()
                formTableView.endUpdates()
            }
        }
    }
}

extension OSOForm: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return formSections.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formSections[section].numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let formSection = formSections[indexPath.section]
        let formRow = formSection.formRow(forIndex: indexPath.row)
        
        switch formRow.rowType! {
        case OSOFormRowType.RowText:
            let formRowText = formRow as! OSOFormRowText
            formRowText.indexPath = indexPath
            let cell = tableView.dequeueReusableCell(withIdentifier: OSOFormTextCell.cellIdentifier, for: indexPath) as! OSOFormTextCell
            cell.placeHolderText = formRowText.placeholder ?? ""
            cell.lblFieldName.text = formRow.rowText
            cell.tfFieldValue.delegate = self
            cell.tfFieldValue.text = formRowText.value ?? ""
            cell.tfFieldValue.isEnabled = formRowText.isTextEditable
            cell.tfFieldValue.textColor = formRowText.isTextEditable ? Constants.UIConfig.themeColor : UIColor.white
            cell.tfFieldValue.modifyClearButtonWithImage(image: UIImage(named: "icon_clear")!)
            cell.bottomBorder.isHidden = showBottomBorder(indexPath: indexPath)
            cell.tfFieldValue.addTarget(self, action: #selector(textFieldDidEndEditing(_:)), for: .editingDidEnd)
            cell.row = formRowText
            
            if let inputType = formRowText.inputType {
                cell.inputType = inputType
                if inputType == OSOFORMRowInputType.Number {
                    cell.tfFieldValue.keyboardType = .decimalPad
                } else if inputType == OSOFORMRowInputType.Decimal {
                    cell.tfFieldValue.keyboardType = .decimalPad
                } else {
                    cell.inputType = OSOFORMRowInputType.Default
                    cell.tfFieldValue.keyboardType = .default
                }
            } else {
                cell.inputType = OSOFORMRowInputType.Default
                cell.tfFieldValue.keyboardType = .default
            }
            
            if let vc = cell.getParentViewController() {
                vc.addToolBar(textField: cell.tfFieldValue)
            }
            
            return cell
            
        case OSOFormRowType.RowSwitch:
            let formRowSwitch = formRow as! OSOFormRowSwitch
            formRowSwitch.indexPath = indexPath
            let cell = tableView.dequeueReusableCell(withIdentifier: OSOFormSwitchCell.cellIdentifier, for: indexPath) as! OSOFormSwitchCell
            cell.lblFieldName.text = formRow.rowText
            cell.fieldSwitch.setOn((formRow as! OSOFormRowSwitch).isOn ?? false, animated: false)
            cell.fieldSwitch.addTarget(self, action: #selector(toggleSwitch(_:)), for: .valueChanged)
            cell.bottomBorder.isHidden = showBottomBorder(indexPath: indexPath)
            cell.row = formRowSwitch
            return cell
            
        case OSOFormRowType.RowOptions:
            let formRowOption = formRow as! OSOFormRowOptions
            formRowOption.indexPath = indexPath
            let cell = tableView.dequeueReusableCell(withIdentifier: OSOFormOptionsCell.cellIdentifier, for: indexPath) as! OSOFormOptionsCell
            cell.options = (formRow as! OSOFormRowOptions).options!
            cell.lblFieldName.text = formRow.rowText
            cell.dropdownOption.options = (formRow as! OSOFormRowOptions).options!
            cell.dropdownOption.isUserInteractionEnabled = false
            cell.dropdownOption.setTitle((formRow as! OSOFormRowOptions).selectedOption!, for: .normal)
            cell.bottomBorder.isHidden = showBottomBorder(indexPath: indexPath)
            cell.row = formRowOption
            return cell
            
        case OSOFormRowType.RowPicker:
            let formRowPicker = formRow as! OSOFormRowPicker
            formRowPicker.indexPath = indexPath
            let cell = tableView.dequeueReusableCell(withIdentifier: OSOFormPickerCell.cellIdentifier, for: indexPath) as! OSOFormPickerCell
            //cell.lblFieldName.text = formRow.rowText
            cell.fieldNameText = formRow.rowText
            cell.options = (formRow as! OSOFormRowPicker).options!
            cell.dropdownOption.options = (formRow as! OSOFormRowPicker).options!
            cell.dropdownOption.isUserInteractionEnabled = false
            cell.dropdownOption.selectedOption = (formRow as! OSOFormRowPicker).selectedOption ?? nil
            cell.dropdownOption.placeholder = (formRow as! OSOFormRowPicker).placeholder!
            cell.dropdownOption.setTitle((formRow as! OSOFormRowPicker).selectedOption ?? (formRow as! OSOFormRowPicker).placeholder!, for: .normal)
            cell.bottomBorder.isHidden = showBottomBorder(indexPath: indexPath)
            cell.row = formRowPicker
            return cell
            
        case OSOFormRowType.RowRadio:
            let formRowRadio = formRow as! OSOFormRowRadio
            formRowRadio.indexPath = indexPath
            let cell = tableView.dequeueReusableCell(withIdentifier: OSOFormRadioCell.cellIdentifier, for: indexPath) as! OSOFormRadioCell
            cell.radioButton.row = (formRow as! OSOFormRowRadio)
            cell.radioButton.isUserInteractionEnabled = false
            cell.radioButton.setTitle(formRow.rowText, for: .normal)
            cell.bottomBorder.isHidden = showBottomBorder(indexPath: indexPath)
            
            if (formRow as! OSOFormRowRadio).isSelected! {
                cell.radioButton.setTitleColor(Constants.UIConfig.themeColor, for: .normal)
                cell.radioButton.imageView?.tintColor = Constants.UIConfig.themeColor
            } else {
                cell.radioButton.setTitleColor(UIColor(white: 1.0, alpha: 0.25), for: .normal)
                cell.radioButton.imageView?.tintColor = UIColor(white: 1.0, alpha: 0.1)
            }
            
            cell.radioButton.addTarget(self, action: #selector(radioButtonSelected(_:)), for: .touchUpInside)
            cell.row = formRowRadio
            return cell
        }
    }
}

extension OSOForm: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let formSection = formSections[indexPath.section]
        let formRow = formSection.formRow(forIndex: indexPath.row)
        
        endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds * 1) {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        switch formRow.rowType! {
        case OSOFormRowType.RowText:
            if let isRowEditable = (formRow as? OSOFormRowText)?.isTextEditable {
                if isRowEditable {
                    let cell = tableView.cellForRow(at: indexPath) as! OSOFormTextCell
                    cell.tfFieldValue.becomeFirstResponder()
                }
            }
            
        case OSOFormRowType.RowOptions:
            let cell = tableView.cellForRow(at: indexPath) as! OSOFormOptionsCell
            self.showOptions(cell.dropdownOption)
            
        case OSOFormRowType.RowPicker:
            let cell = tableView.cellForRow(at: indexPath) as! OSOFormPickerCell
            self.showPicker(cell.dropdownOption)
            
        case OSOFormRowType.RowRadio:
            let cell = tableView.cellForRow(at: indexPath) as! OSOFormRadioCell
            self.radioButtonSelected(cell.radioButton)
            
        default:
            ()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let formSection = formSections[section]
        
        if formSection.titleForSection != nil {
            return 30
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 16, y: 0, width: tableView.frame.size.width - 16, height: 30))
        headerView.backgroundColor = Constants.UIConfig.homeBarTintColor //UIColor(white: 0.0, alpha: 0.9)
        
        if let sectionTitle = formSections[section].titleForSection {
            let headerLabel = UILabel(frame: headerView.frame)
            headerLabel.text = "\(sectionTitle)"
            headerLabel.textAlignment = .left
            headerLabel.textColor = UIColor(white: 1.0, alpha: 0.5)
            headerLabel.font = UIFont.boldSystemFont(ofSize: 12)
            headerView.addSubview(headerLabel)
        }
        
        return headerView
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let formSection = formSections[indexPath.section]
        let formRow = formSection.formRow(forIndex: indexPath.row)
        
        if let hidden = formRow.isHidden {
            if hidden {
                return 0
            } else {
                return 44
            }
        } else {
            return 0
        }
    }
}






