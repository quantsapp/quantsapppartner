//
//  OSOFormRowRadio.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 21/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OSOFormRowRadio: OSOFormRow {
    
    public var isSelected: Bool? {
        didSet {
            updateDependentRows(selected: isSelected!)
        }
    }
    
    public var selectMultipleEnabled: Bool?
    public var relatedRows: [OSOFormRowRadio]?
    public var dependentRows: [OSOFormRow]?
    
    init(rowIdentifier: String, rowText: String, isHidden: Bool, isSelected: Bool, selectMultipleEnabled: Bool) {
        super.init(rowType: OSOFormRowType.RowRadio, rowText: rowText, isHidden: isHidden, rowIdentifier: rowIdentifier)
        self.isSelected = isSelected
        self.selectMultipleEnabled = selectMultipleEnabled
    }
    
    private func updateDependentRows(selected: Bool) {
        if let rows = dependentRows {
            for row in rows {
                row.isHidden = !selected
            }
        }
    }
    
}
