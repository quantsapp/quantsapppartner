//
//  OSONavigationBar.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 28/05/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

protocol OSONavigationBarDelegate: class {
    func leftButtonTapped(sender: OSONavigationBar)
    func rightButtonTapped(sender: OSONavigationBar)
}

extension OSONavigationBarDelegate {
    func leftButtonTapped(sender: OSONavigationBar) {}
    func rightButtonTapped(sender: OSONavigationBar) {}
}

class OSONavigationBar: UIView {

    weak var delegate: OSONavigationBarDelegate?
    
    public var title: String? {
        didSet {
            DispatchQueue.main.async {
                self.lblTitle.text = self.title!
            }
        }
    }
    
    private var subTitle: String?
    private var leftBarButtonImageTintColor: UIColor?
    private var rightBarButtonImageTintColor: UIColor?
    private var leftBarButtonImage: UIImage?
    private var rightBarButtonImage: UIImage? {
        didSet {
            if let image = rightBarButtonImage {
                DispatchQueue.main.async {
                    self.rightBarButton.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
                    
                    if let tintColor = self.rightBarButtonImageTintColor {
                        self.rightBarButton.tintColor = tintColor
                    } else {
                        self.rightBarButton.tintColor = UIColor.white
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.rightBarButton.setImage(nil, for: .normal)
                }
            }
            
        }
    }
    
    private var lblTitleTopAnchorConstraint: NSLayoutConstraint!
    private var lblTitleBottomAnchorConstraint: NSLayoutConstraint!
    private var lblSubTitleBottomAnchorConstraint: NSLayoutConstraint!
    private var lblSubTitleHeighAnchorConstraint: NSLayoutConstraint!
    
    private let barBackground: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.OSONavigationBarConstants.barBackgroundColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let lblTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = Constants.OSONavigationBarConstants.barTitleColor
        label.textAlignment = .center
        label.clipsToBounds = true
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lblSubTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = Constants.OSONavigationBarConstants.barSubTitleColor
        label.textAlignment = .center
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let leftBarButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        //button.addTarget(self, action: #selector(actionLeftBarButton(_:)), for: .touchUpInside)
        return button
    }()
    
    public let rightBarButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        //button.addTarget(self, action: #selector(actionRightBarButton(_:)), for: .touchUpInside)
        return button
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    
    init(frame: CGRect, title: String?, subTitle: String?, leftbuttonImage: UIImage?, rightButtonImage: UIImage?, leftButtonTintColor: UIColor? = nil, rightButtonTintColor: UIColor? = nil) {
        super.init(frame: frame)
        self.title = title
        self.subTitle = subTitle
        self.leftBarButtonImage = leftbuttonImage
        self.rightBarButtonImage = rightButtonImage
        self.leftBarButtonImageTintColor = leftButtonTintColor
        self.rightBarButtonImageTintColor = rightButtonTintColor
        setupView()
    }
    
    func setupView() {
        
        clipsToBounds = true
        
        addSubview(barBackground)
        barBackground.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        barBackground.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        barBackground.topAnchor.constraint(equalTo: topAnchor).isActive = true
        barBackground.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        
        if let title = self.title {
            
            if let subTitle = self.subTitle {
                // add subtitle
                addSubview(lblSubTitle)
                lblSubTitle.text = subTitle
                lblSubTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 52).isActive = true
                lblSubTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -52).isActive = true
                lblSubTitleBottomAnchorConstraint = NSLayoutConstraint(item: lblSubTitle, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: -4)
                lblSubTitleBottomAnchorConstraint.isActive = true
                lblSubTitleHeighAnchorConstraint = NSLayoutConstraint(item: lblSubTitle, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 16)
                lblSubTitleHeighAnchorConstraint.isActive = true
                
                // add title
                addSubview(lblTitle)
                lblTitle.text = title.uppercased()
                
                lblTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 52).isActive = true
                lblTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -52).isActive = true
                lblTitleTopAnchorConstraint = NSLayoutConstraint(item: lblTitle, attribute: .top, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: -40)
                lblTitleTopAnchorConstraint.isActive = true
                lblTitleBottomAnchorConstraint = NSLayoutConstraint(item: lblTitle, attribute: .bottom, relatedBy: .equal, toItem: lblSubTitle, attribute: .top, multiplier: 1.0, constant: -2)
                lblTitleBottomAnchorConstraint.isActive = true
                
                
            } else {
                // add subtitle
                addSubview(lblSubTitle)
                lblSubTitle.text = subTitle
                lblSubTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 52).isActive = true
                lblSubTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -52).isActive = true
                lblSubTitleBottomAnchorConstraint = NSLayoutConstraint(item: lblSubTitle, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0)
                lblSubTitleBottomAnchorConstraint.isActive = true
                lblSubTitleHeighAnchorConstraint = NSLayoutConstraint(item: lblSubTitle, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
                lblSubTitleHeighAnchorConstraint.isActive = true
                
                // add title
                addSubview(lblTitle)
                lblTitle.text = title.uppercased()
                
                lblTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 52).isActive = true
                lblTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -52).isActive = true
                lblTitleTopAnchorConstraint = NSLayoutConstraint(item: lblTitle, attribute: .top, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: -40)
                lblTitleTopAnchorConstraint.isActive = true
                lblTitleBottomAnchorConstraint = NSLayoutConstraint(item: lblTitle, attribute: .bottom, relatedBy: .equal, toItem: lblSubTitle, attribute: .top, multiplier: 1.0, constant: -4)
                lblTitleBottomAnchorConstraint.isActive = true
            }
            
        }
        
        if let leftButtonImage = self.leftBarButtonImage {
            addSubview(leftBarButton)
            leftBarButton.addTarget(self, action: #selector(actionLeftBarButton(_:)), for: .touchUpInside)
            leftBarButton.setImage(leftButtonImage.withRenderingMode(.alwaysTemplate), for: .normal)
            
            if let tintColor = leftBarButtonImageTintColor {
                leftBarButton.imageView?.tintColor = tintColor
            } else {
                leftBarButton.imageView?.tintColor = UIColor.white
            }
            
            
            /*leftBarButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
            leftBarButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -7).isActive = true
            leftBarButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
            leftBarButton.topAnchor.constraint(equalTo: topAnchor, constant: 27).isActive = true*/
            
            leftBarButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 4).isActive = true
            leftBarButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
            leftBarButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
            leftBarButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        
        if let rightButtonImage = self.rightBarButtonImage {
            addSubview(rightBarButton)
            rightBarButton.addTarget(self, action: #selector(actionRightBarButton(_:)), for: .touchUpInside)
            rightBarButton.setImage(rightButtonImage.withRenderingMode(.alwaysTemplate), for: .normal)
            
            if let tintColor = rightBarButtonImageTintColor {
                rightBarButton.imageView?.tintColor = tintColor
            } else {
                rightBarButton.imageView?.tintColor = UIColor.white
            }
            
            /*rightBarButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
            rightBarButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -7).isActive = true
            rightBarButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
            rightBarButton.topAnchor.constraint(equalTo: topAnchor, constant: 27).isActive = true*/
            
            rightBarButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -4).isActive = true
            rightBarButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
            rightBarButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
            rightBarButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        
    }
    
   
    public func showSubTitle(subTitleText: String) {
        
        UIView.transition(with: self,
                          duration: 0.3,
                          options: [.curveEaseOut],
                          animations: {
                            self.lblSubTitle.text = subTitleText
        }, completion: nil)
        
        // show subtitle with animation
        UIView.animate(withDuration: 0.3) {
            
            self.lblTitleTopAnchorConstraint.constant = -40
            self.lblTitleBottomAnchorConstraint.constant = -2
            
            self.lblSubTitleBottomAnchorConstraint.constant = -4
            self.lblSubTitleHeighAnchorConstraint.constant = 16
            
            self.layoutIfNeeded()
        }
        
        
    }
    
    public func hideSubTitle() {
        
        UIView.transition(with: self,
                          duration: 0.3,
                          options: [.curveEaseOut],
                          animations: {
                            self.lblSubTitle.text = ""
        }, completion: nil)
        
        // hide subtitle with animation
        UIView.animate(withDuration: 0.3) {
            
            self.lblTitleTopAnchorConstraint.constant = -40
            self.lblTitleBottomAnchorConstraint.constant = -4
            
            self.lblSubTitleBottomAnchorConstraint.constant = 0
            self.lblSubTitleHeighAnchorConstraint.constant = 0
            
            self.layoutIfNeeded()
        }
    }
    
    public func rightBarButtonEnabled(isEnabled: Bool) {
        self.rightBarButton.isHidden = !isEnabled
        
        if isEnabled {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    self.rightBarButton.alpha = 1
                })
            }
        } else {
            DispatchQueue.main.async {
                self.rightBarButton.alpha = 0
            }
        }
    }
    
    public func leftBarButtonEnabled(isEnabled: Bool) {
        self.leftBarButton.isHidden = !isEnabled
    }
    
    public func updateLeftBarButtonImage(image: UIImage) {
        DispatchQueue.main.async {
            self.leftBarButton.setImage(image, for: [])
        }
    }
    
    public func updateRightBarButtonImage(image: UIImage) {
        /*DispatchQueue.main.async {
            self.rightBarButton.setImage(image, for: [])
        }*/
        self.rightBarButtonImage = image
    }
    
    @objc func actionLeftBarButton(_ button: UIButton) {
        delegate?.leftButtonTapped(sender: self)
    }
    
    @objc func actionRightBarButton(_ button: UIButton) {
        delegate?.rightButtonTapped(sender: self)
    }

}
