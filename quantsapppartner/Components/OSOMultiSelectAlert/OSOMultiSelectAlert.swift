//
//  OSOMultiSelectAlert.swift
//  quantsapppartner
//
//  Created by Quantsapp on 04/04/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit

class OSOMultiSelectAlert: UIView {
    
    private let btnCancel: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = Constants.UIConfig.buttonCornerRadius
        button.layer.backgroundColor = UIColor.white.cgColor
        button.layer.masksToBounds = true
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(UIColor(white: 0.0, alpha: 0.5), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    
    private func setupView() {
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        
        // btnCancel
        addSubview(btnCancel)
        btnCancel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
        btnCancel.heightAnchor.constraint(equalToConstant: Constants.UIConfig.buttonHeight).isActive = true
        btnCancel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        btnCancel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
    }
    
    @objc private func actionCance(_ sender: UIButton) {
        
    }
}

extension UIViewController {
    
}
