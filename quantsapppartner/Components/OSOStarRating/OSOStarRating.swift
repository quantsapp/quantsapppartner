//
//  OSOStarRating.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 28/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class OSOStarRating: UIView {
    
    private let starSize: CGFloat = 12
    private var numberOfStars: Int = 5
    private var leftToRight: Bool = true
    
    
    var osoStars = [OSOStar]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    init(numberOfStars: Int = 5, leftToRight: Bool = true) {
        super.init(frame: CGRect.zero)
        self.numberOfStars = numberOfStars
        self.leftToRight = leftToRight
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        clipsToBounds = true
        
        var xOffset: CGFloat = 0
        
        for _ in 0..<self.numberOfStars {
            let star = OSOStar(frame: CGRect(x: xOffset, y: 0, width: starSize, height: 26))
            addSubview(star)
            osoStars.append(star)
            /*star.rightAnchor.constraint(equalTo: rightAnchor, constant: yOffset).isActive = true
            star.heightAnchor.constraint(equalToConstant: starSize).isActive = true
            star.widthAnchor.constraint(equalToConstant: starSize).isActive = true
            star.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true*/
            
            xOffset += (starSize + 2)
        }
    }
    
    private func highlightStars(num: Int) {
        
        resetAllStars()
        
        for i in 0..<num {
            let star = osoStars[i]
            star.hightlightStar()
        }
    }
    
    public func rate(rating: Int) {
        highlightStars(num: rating)
    }
    
    private func resetAllStars() {
        for i in 0..<numberOfStars {
            let star = osoStars[i]
            star.resetStar()
        }
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
