//
//  OSOStar.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 28/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class OSOStar: UIView {
    
    private var starImage: UIImage = UIImage(named: "icon_star")!
    private var starEnabledColor: UIColor = Constants.UIConfig.starEnabledColor
    private var starDisabledColor: UIColor = Constants.UIConfig.starDisabledColor
    
    let starImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        clipsToBounds = true
        
        addSubview(starImageView)
        starImageView.heightAnchor.constraint(equalToConstant: 12).isActive = true
        starImageView.widthAnchor.constraint(equalToConstant: 12).isActive = true
        starImageView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        starImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        /*starImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        starImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        starImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        starImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true*/
        starImageView.image = starImage.withRenderingMode(.alwaysTemplate)
        starImageView.tintColor = starDisabledColor
    }
    
    public func hightlightStar() {
        starImageView.tintColor = starEnabledColor
    }
    
    public func resetStar() {
        starImageView.tintColor = starDisabledColor
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
