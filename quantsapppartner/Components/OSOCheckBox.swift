//
//  OSOCheckBox.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 26/02/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit

protocol OSOCheckBoxDelegate: class {
    func checkboxToggled(id: Int, selected: Bool)
}

extension OSOCheckBoxDelegate {
    func checkboxToggled(id: Int, selected: Bool) {}
}


class OSOCheckBox: UIView {
    
    weak var delegate: OSOCheckBoxDelegate?
    
    public var id: Int?
    
    public var isSelected: Bool = false {
        didSet {
            toggleCheckBoxState(setSelected: isSelected)
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setupView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private let outLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.layer.borderColor = UIColor(white: 1.0, alpha: 0.3).cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let solidIn: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        clipsToBounds = true
        
        // outLine
        addSubview(outLine)
        outLine.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        outLine.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        outLine.topAnchor.constraint(equalTo: topAnchor).isActive = true
        outLine.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        // solidIn
        addSubview(solidIn)
        solidIn.leftAnchor.constraint(equalTo: outLine.leftAnchor, constant: 3).isActive = true
        solidIn.rightAnchor.constraint(equalTo: outLine.rightAnchor, constant: -3).isActive = true
        solidIn.topAnchor.constraint(equalTo: outLine.topAnchor, constant: 3).isActive = true
        solidIn.bottomAnchor.constraint(equalTo: outLine.bottomAnchor, constant: -3).isActive = true
        
        // add tap gesture
        addTapGesture()
    }
    
    private func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionCheckBoxTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func actionCheckBoxTapped(_ recognizer: UITapGestureRecognizer) {
        isSelected = !isSelected
    }
    
    private func toggleCheckBoxState(setSelected: Bool) {
        if setSelected {
            DispatchQueue.main.async {
                self.outLine.layer.borderColor = Constants.UIConfig.themeColor.cgColor
                self.solidIn.backgroundColor = Constants.UIConfig.themeColor
            }
        } else {
            DispatchQueue.main.async {
                self.outLine.layer.borderColor = UIColor(white: 1.0, alpha: 0.3).cgColor
                self.solidIn.backgroundColor = UIColor.clear
            }
        }
        
        if let idValue = id {
            self.delegate?.checkboxToggled(id: idValue, selected: isSelected)
        }
    }
}


