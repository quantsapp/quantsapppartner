//
//  OSOMenuItem.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation

class OSOMenuItem {
    
    public var title: String?
    public var icon: String?
    
    init(title: String?, icon: String?) {
        self.title = title
        self.icon = icon
    }
}
