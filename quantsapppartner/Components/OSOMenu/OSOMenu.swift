//
//  OSOMenu.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit
import SwifterSwift

protocol OSOMenuDelegate: class {
    func menuItemSelected(menuItem: OSOMenuItem, sender: UIView)
}

extension OSOMenuDelegate {
    func menuItemSelected(menuItem: OSOMenuItem, sender: UIView) {}
}

class OSOMenu: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var sections: [OSOMenuSection]? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    weak var delegate: OSOMenuDelegate?
    
    var collectionView: UICollectionView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setupView()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        flowLayout.scrollDirection = .vertical
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.register(OSOMenuItemCell.self, forCellWithReuseIdentifier: OSOMenuItemCell.cellIdentifier)
        collectionView.register(OSOMenuSectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: OSOMenuSectionHeader.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = UIColor.clear
        addSubview(collectionView)
        
        collectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let menuSections = sections {
            return menuSections.count
        } else {
            return 0
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let menuSections = sections {
            let menuSection = menuSections[section]
            
            if let items = menuSection.items {
                return items.count
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OSOMenuItemCell.cellIdentifier, for: indexPath) as! OSOMenuItemCell
        
        if let menuSections = sections {
            let menuSection = menuSections[indexPath.section]
            if let items = menuSection.items {
                cell.menuItem = items[indexPath.row]
            } else {
                cell.menuItem = nil
            }
        } else {
            cell.menuItem = nil
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width/(Constants.APPConfig.isDeviceIphone ? 3 : 4)
        return CGSize(width: width, height: Constants.APPConfig.isDeviceIphone ? 100 : width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let menuSections = sections {
            let menuSection = menuSections[indexPath.section]
            if let items = menuSection.items, let cell = collectionView.cellForItem(at: indexPath) {
                self.delegate?.menuItemSelected(menuItem: items[indexPath.row], sender: cell)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: OSOMenuSectionHeader.identifier, for: indexPath) as! OSOMenuSectionHeader
            
            if let menuSections = sections {
                let menuSection = menuSections[indexPath.section]
                header.sectionTitle = menuSection.title
            } else {
                header.sectionTitle = nil
            }
            
            return header
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let menuSections = sections {
            let menuSection = menuSections[section]
            if let _ = menuSection.title {
                return CGSize(width: SwifterSwift.screenWidth, height: 30)
            } else {
                return CGSize.zero
            }
        } else {
            return CGSize.zero
        }
    }

}
