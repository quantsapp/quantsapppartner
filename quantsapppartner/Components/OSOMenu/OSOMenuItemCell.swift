//
//  OSOMenuItemCell.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit

class OSOMenuItemCell: UICollectionViewCell {
    
    static let cellIdentifier = "OSOMenuItemCell"
    
    private let iconImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    imageView.clipsToBounds = true
    imageView.tintColor = UIColor(white: 1.0, alpha: 0.4)
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
    }()
    
    private let iconLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Constants.APPConfig.isDeviceIphone ? 12 : 14)
        label.textColor = UIColor(white: 1.0, alpha: 0.7)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public var menuItem: OSOMenuItem? {
        didSet {
            updateMenu(menuItem: menuItem)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        
        addSubview(iconImageView)
        addSubview(iconLabel)
        
        if Constants.APPConfig.isDeviceIphone {
            addConstraintsWithFormat(format: "V:|-16-[v0]-[v1(36)]-2-|", views: iconImageView, iconLabel)
            addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: iconImageView)
            addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: iconLabel)
        } else {
            iconImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -25).isActive = true
            iconImageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.3, constant: 0).isActive = true
            iconImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3, constant: 0).isActive = true
            
            iconLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
            iconLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
            iconLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
            iconLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 16).isActive = true
        }
    }
    
    private func updateMenu(menuItem: OSOMenuItem?) {
        if let item = menuItem {
            if let title = item.title {
                iconLabel.text = title
            } else {
                iconLabel.text = ""
            }
            
            if let icon = item.icon {
                iconImageView.image = UIImage(named: icon)?.withRenderingMode(.alwaysTemplate)
                iconImageView.tintColor = UIColor(white: 1.0, alpha: 0.5)
            } else {
                iconImageView.image = nil
            }
        } else {
            iconLabel.text = ""
            iconImageView.image = nil
        }
    }
    
}
