//
//  OSOMenuSection.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation

class OSOMenuSection {
    
    public var title: String?
    public var items: [OSOMenuItem]?
    
    init(title: String?, items: [OSOMenuItem]?) {
        self.title = title
        self.items = items
    }
}
