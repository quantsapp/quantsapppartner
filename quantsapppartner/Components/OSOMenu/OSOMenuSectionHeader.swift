//
//  OSOMenuSectionHeader.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import UIKit

class OSOMenuSectionHeader: UICollectionReusableView {
    
    static let identifier = "OSOMenuSectionHeader"
    
    public var sectionTitle: String? {
        didSet {
            if let title = sectionTitle {
                lblTitle.text = title.uppercased()
            } else {
                lblTitle.text = ""
            }
        }
    }
    
    private let lblTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor(white: 1.0, alpha: 0.7)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    private func setupView() {
        backgroundColor = UIColor(white: 0.0, alpha: 0.3)
        clipsToBounds = true
        
        // lblTitle
        addSubview(lblTitle)
        lblTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        lblTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        lblTitle.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        lblTitle.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
    }
}
