//
//  OSOStepProgressCell.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 11/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

class OSOStepProgressCell: UICollectionViewCell {
    
    static let cellIdentifier = "OSOStepProgressCell"
    
    private var stepIndex: Int?
    private var stepTitle: String?
    private let colorForCurrentState: UIColor = Constants.UIConfig.themeColor
    private let colorForIncompleteState: UIColor = UIColor(white: 1.0, alpha: 0.25)
    private let colorForCompleteState: UIColor = UIColor(white: 1.0, alpha: 0.75)
    
    public var index: Int {
        get {
            return stepIndex!
        }
    }
    
    public var stepState: OSOStepState = .Incomplete {
        didSet {
            updateStepState()
        }
    }
    
    public let lblStep: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        
        setupView()
    }
    
    func setupView() {
        
        backgroundColor = UIColor.clear
        
        // lblStep
        addSubview(lblStep)
        lblStep.leftAnchor.constraint(equalTo: leftAnchor, constant: 2).isActive = true
        lblStep.rightAnchor.constraint(equalTo: rightAnchor, constant: -2).isActive = true
        lblStep.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        lblStep.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
    }
    
    private func updateStepState() {
        switch stepState {
        case .Incomplete:
            let attributedString = NSMutableAttributedString()
            
            if let index = self.stepIndex {
                attributedString.append(NSAttributedString(string: "STEP \(index+1)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 8), NSAttributedString.Key.foregroundColor: colorForIncompleteState]))
            }
            
            if let title = self.stepTitle {
                attributedString.append(NSAttributedString(string: "\n\(title.uppercased())", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: colorForIncompleteState]))
            }
            
            lblStep.attributedText = attributedString
        case .Current:
            let attributedString = NSMutableAttributedString()
            
            if let index = self.stepIndex {
                attributedString.append(NSAttributedString(string: "STEP \(index+1)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 8), NSAttributedString.Key.foregroundColor: colorForCurrentState]))
            }
            
            if let title = self.stepTitle {
                attributedString.append(NSAttributedString(string: "\n\(title.uppercased())", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: colorForCurrentState]))
            }
            
            lblStep.attributedText = attributedString
        case .Complete:
            let attributedString = NSMutableAttributedString()
            
            if let index = self.stepIndex {
                attributedString.append(NSAttributedString(string: "STEP \(index+1)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 8), NSAttributedString.Key.foregroundColor: colorForCompleteState]))
            }
            
            if let title = self.stepTitle {
                attributedString.append(NSAttributedString(string: "\n\(title.uppercased())", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: colorForCompleteState]))
            }
            
            lblStep.attributedText = attributedString
        }
    }
}
