//
//  OSOStep.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 11/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

class OSOStep {
    
    private var stepIndex: Int?
    private var stepTitle: String?
    public var stepState: OSOStepState = .Incomplete
    
    public var index: Int {
        get {
            return stepIndex!
        }
    }
    
    public var title: String {
        get {
            return stepTitle!
        }
    }
    
    
    
    init(stepIndex: Int, stepTitle: String) {
        self.stepIndex = stepIndex
        self.stepTitle = stepTitle
    }
    
}
