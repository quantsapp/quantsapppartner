//
//  OSOStepProgress.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 05/07/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import UIKit

public enum OSOStepState: Int {
    case Incomplete = 0
    case Current = 1
    case Complete = 2
}

protocol OSOStepProgressDelegate: class {
    func stepSelected(stepIndex: Int, isPreviousStep: Bool)
}

class OSOStepProgress: UIView {
    
    weak var delegate: OSOStepProgressDelegate?
    
    private let colorForCurrentState: UIColor = Constants.UIConfig.themeColor
    private let colorForIncompleteState: UIColor = UIColor(white: 1.0, alpha: 0.25)
    private let colorForCompleteState: UIColor = UIColor(white: 1.0, alpha: 0.75)
    
    private var progressTrackWidthAnchorConstraint: NSLayoutConstraint!
    
    private let barHeight: CGFloat = 1
    
    private var osoSteps = [OSOStep]()
    private var progressSteps: [String]?
    private var currentStep: OSOStep?
    
    private let bottomTrack: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1.0, alpha: 0.15)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let progressTrack: UIView = {
        let view = UIView()
        view.backgroundColor = Constants.UIConfig.themeColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var stepsCollectionView: UICollectionView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    init(steps: [String]) {
        super.init(frame: CGRect.zero)
        self.progressSteps = steps
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        backgroundColor = UIColor.clear
        clipsToBounds = true
        
        // bottomTrack
        addSubview(bottomTrack)
        bottomTrack.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        bottomTrack.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        bottomTrack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        bottomTrack.heightAnchor.constraint(equalToConstant: barHeight).isActive = true
        
        // progressTrack
        addSubview(progressTrack)
        progressTrack.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        progressTrack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        progressTrack.heightAnchor.constraint(equalToConstant: barHeight).isActive = true
        progressTrackWidthAnchorConstraint = NSLayoutConstraint(item: progressTrack, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
        progressTrackWidthAnchorConstraint.isActive = true
        
        /*if let steps = progressSteps {
            for i in 0..<steps.count {
                let osoStep = OSOStep(stepIndex: i, stepTitle: steps[i])
                osoStep.translatesAutoresizingMaskIntoConstraints = false
                addSubview(osoStep)
                
                if i > 0 {
                    let prevOSOStep = osoSteps[i-1]
                    osoStep.leftAnchor.constraint(equalTo: prevOSOStep.rightAnchor, constant: 0).isActive = true
                } else {
                    osoStep.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
                    osoStep.stepState = .Current
                    self.currentStep = osoStep
                    
                    progressTrackWidthAnchorConstraint.constant = UIScreen.main.bounds.width/steps.count.cgFloat
                    layoutIfNeeded()
                }
                
                osoStep.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
                osoStep.bottomAnchor.constraint(equalTo: bottomTrack.topAnchor, constant: 0).isActive = true
                osoStep.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/steps.count.cgFloat).isActive = true
                osoStep.addTarget(self, action: #selector(stepSelected(_:)), for: .touchUpInside)
                self.osoSteps.append(osoStep)
            }
            
            layoutSubviews()
        }*/
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        flowLayout.scrollDirection = .horizontal
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        stepsCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout) //CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        stepsCollectionView.register(OSOStepProgressCell.self, forCellWithReuseIdentifier: OSOStepProgressCell.cellIdentifier)
        stepsCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        stepsCollectionView.delegate = self
        stepsCollectionView.dataSource = self
        stepsCollectionView.showsHorizontalScrollIndicator = false
        stepsCollectionView.alwaysBounceHorizontal = false
        stepsCollectionView.backgroundColor = UIColor.clear
        stepsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stepsCollectionView)
        
        stepsCollectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        stepsCollectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        stepsCollectionView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        stepsCollectionView.bottomAnchor.constraint(equalTo: bottomTrack.topAnchor, constant: 0).isActive = true
        
        
        if let steps = progressSteps {
            for i in 0..<steps.count {
                let osoStep = OSOStep(stepIndex: i, stepTitle: steps[i])
                self.osoSteps.append(osoStep)
                
                if i == 0 {
                    osoStep.stepState = .Current
                    self.currentStep = osoStep
                }
            }
            
            updateProgressTrack()
            stepsCollectionView.reloadData()
        }
    }
    
    public func setCurrentStepAsCompleted() {
        if let curStep = currentStep {
            curStep.stepState = .Complete
            self.goToNextStep()
        }
    }
    
    public func setCurrentStepAsIncomplete() {
        if let curStep = currentStep {
            curStep.stepState = .Incomplete
            self.goToPreviousStep()
        }
    }
    
    private func goToNextStep() {
        if let curStep = currentStep {
            let curStepIndex = curStep.index
            let nextStepIndex = curStepIndex + 1
            if nextStepIndex < osoSteps.count {
                let nextStep = osoSteps[nextStepIndex]
                nextStep.stepState = .Current
                self.currentStep = nextStep
                updateProgressTrack()
            }
        }
    }
    
    private func goToPreviousStep() {
        if let curStep = currentStep {
            let curStepIndex = curStep.index
            let previousStepIndex = curStepIndex - 1
            if previousStepIndex >= 0 {
                let previousStep = osoSteps[previousStepIndex]
                self.stepSelected(step: previousStep)
            }
        }
    }
    
    private func updateProgressTrack() {
        if let curStep = currentStep {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                let progressWidth = (curStep.index.cgFloat + 1) * (UIScreen.main.bounds.width / self.osoSteps.count.cgFloat)
                self.progressTrackWidthAnchorConstraint.constant = progressWidth//(curStep.index.cgFloat + 1) * UIScreen.main.bounds.width/self.osoSteps.count.cgFloat
                self.layoutIfNeeded()
            }, completion: nil)
            
            stepsCollectionView.reloadData()
            stepsCollectionView.scrollToItem(at: NSIndexPath(item: curStep.index, section: 0) as IndexPath, at: .centeredHorizontally, animated: true)
        }
        
        
    }
    
    private func stepSelected(step: OSOStep) {
        if step.stepState == .Complete {
            let stepIndex = step.index
            
            var isPreviousStep: Bool = false
            
            if let curStep = self.currentStep {
                if stepIndex < curStep.index {
                    isPreviousStep = true
                }
            }
            
            for i in stepIndex..<osoSteps.count {
                let step = osoSteps[i]
                if step.index != stepIndex {
                    step.stepState = .Incomplete
                } else {
                    step.stepState = .Current
                }
            }
            
            delegate?.stepSelected(stepIndex: stepIndex, isPreviousStep: isPreviousStep)
            currentStep = osoSteps[stepIndex]
            
            updateProgressTrack()
        }
    }
    
    /*@objc private func stepSelected(_ sender: OSOStep) {
        if sender.stepState == .Complete {
            let stepIndex = sender.index
            
            var isPreviousStep: Bool = false
            
            if let curStep = self.currentStep {
                if stepIndex < curStep.index {
                    isPreviousStep = true
                }
            }
            
            for i in stepIndex..<osoSteps.count {
                let step = osoSteps[i]
                if step.index != stepIndex {
                    step.stepState = .Incomplete
                } else {
                    step.stepState = .Current
                }
            }
            
            delegate?.stepSelected(stepIndex: stepIndex, isPreviousStep: isPreviousStep)
            currentStep = osoSteps[stepIndex]
            
            updateProgressTrack()
        }
    }*/
}

extension OSOStepProgress: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return osoSteps.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OSOStepProgressCell.cellIdentifier, for: indexPath) as! OSOStepProgressCell
        
        let step = osoSteps[indexPath.row]
        var stepColor: UIColor = colorForIncompleteState
        
        switch step.stepState {
        case .Complete:
            stepColor = colorForCompleteState
        case .Current:
            stepColor = colorForCurrentState
        default:
            stepColor = colorForIncompleteState
        }
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "STEP \(indexPath.row+1)", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 8), NSAttributedString.Key.foregroundColor: stepColor]))
        attributedString.append(NSAttributedString(string: "\n\(step.title.uppercased())", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: stepColor]))
        cell.lblStep.attributedText = attributedString
        return cell
    }
}

extension OSOStepProgress: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        stepSelected(step: osoSteps[indexPath.row])
    }
    
}

extension OSOStepProgress: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if osoSteps.count > 4 {
            return CGSize(width: (UIScreen.main.bounds.width/3.75).double, height: 44)
        } else {
            return CGSize(width: UIScreen.main.bounds.width/osoSteps.count.cgFloat, height: 44)
        }
    }
}








