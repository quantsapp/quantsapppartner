//
//  Constants.swift
//  quantsapppartner
//
//  Created by Quantsapp on 12/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct UIConfig {
        // gradient
        static let topColor: UIColor = UIColor.rgb(red: 4, green: 16, blue: 33) //UIColor.rgb(red: 0, green: 1, blue: 22)
        static let bottomColor: UIColor = UIColor.rgb(red: 8, green: 31, blue: 57) //UIColor.rgb(red: 0, green: 5, blue: 8)
        
        // colors
        static let themeColor: UIColor = UIColor.rgb(red: 244, green: 155, blue: 15)
        static let themeColorLight: UIColor = UIColor(red: 244/255, green: 155/255, blue: 15/255, alpha: 0.5)
        static let darkGreenColor: UIColor = UIColor.rgb(red: 38, green: 153, blue: 84)
        static let lightGreenColor: UIColor = UIColor(red: 38/255, green: 153/255, blue: 15/255, alpha: 0.2)
        static let lighterGreenColor: UIColor = UIColor(red: 38/255, green: 153/255, blue: 15/255, alpha: 0.1)
        static let starEnabledColor: UIColor = UIColor.rgb(red: 233, green: 215, blue: 0)
        static let starDisabledColor: UIColor = UIColor(white: 1.0, alpha: 0.1)
        
        // form
        static let buttonCornerRadius: CGFloat = 4
        static let buttonHeight: CGFloat = 44
        static let fieldLabelFont: UIFont = UIFont.systemFont(ofSize: 13)
        static let fieldLabelColor: UIColor = UIColor(white: 1.0, alpha: 0.6)
        static let borderColor: UIColor = UIColor(white: 1.0, alpha: 0.15)
        
        // table elements
        static let fieldFontSize: CGFloat = 9.0
        static let fieldVerticalSpacing: CGFloat = 0.0
        static let fieldHeight: CGFloat = 14.0
        static let fieldNameWidth: CGFloat = 48.0
        static let fieldNameTextAlignment: NSTextAlignment = .left
        static let fieldValueTextAlignment: NSTextAlignment = .right
        
        // cell separator
        static let cellSeparatorColor: UIColor = UIColor(white: 1.0, alpha: 0.1)
        
        // tab bar
        static let homeBarTintColor: UIColor = UIColor.rgb(red: 0, green: 0, blue: 7)
        static let homeTintColor: UIColor = UIColor.rgb(red: 244, green: 155, blue: 15)
    }
    
    struct APPConfig {
        static let inDevelopment: Bool = false // FIXME: Set to false for Live Version
        static let isBetaVersion: Bool = true // FIXME: Set to false for Live Version
        static let isDeviceIphone: Bool = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad ? false : true
        static let addLeftSwipeGesture: Bool = false // FIXME: decide whether to allow back on left swipe
    }
    
    struct OSONavigationBarConstants {
        static let barHeight: CGFloat = UIApplication.shared.statusBarFrame.height + 44
        static let barBackgroundColor: UIColor = UIColor.black
        static let barTitleColor: UIColor = UIColor.white
        static let barSubTitleColor: UIColor = UIColor(white: 1.0, alpha: 0.5)
    }
    
    struct NetworkingConfig {
        static let maximumAttempts: Int = 0 // FIXME: Set to 5 for Live Version
        static let timeoutInterval: TimeInterval = 30
    }
    
    struct APIConfig {
        static let baseUrl: String = "https://alb.quantsapp.net/m/api/"
        static let waitTime: TimeInterval = 10
        
        static let PriceUpdateRefreshTime: Double = 5.0
        static let AutomaticUpdateEnabled: Bool = false
    }
    
    struct ZAlertViewUI {
        static let successButtonColor: UIColor = UIColor(hexString: "#4cbb17")!
        static let errorButtonColor: UIColor = UIColor.red
    }
    
    struct SkyFloating {
        static let tintColor: UIColor = UIColor(white: 1.0, alpha: 0.4)//UIColor.rgb(red: 244, green: 155, blue: 15)
        static let textColor: UIColor = UIColor.rgb(red: 244, green: 155, blue: 15)
        static let lineColor: UIColor = UIColor(white: 1.0, alpha: 0.4)
        static let errorColor: UIColor = UIColor.red
        static let selectedTitleColor: UIColor = UIColor.white//UIColor.rgb(red: 244, green: 155, blue: 15)
        static let selectedLineColor: UIColor = UIColor.rgb(red: 244, green: 155, blue: 15)
        static let lineHeight: CGFloat = 1.0
        static let selectedLineHeight: CGFloat = 2.0
        static let textFieldHeight: CGFloat = 50
        static let disabledColor: UIColor = UIColor.rgb(red: 244, green: 155, blue: 15)
    }
    
    struct DB {
        static let dbName = "QuantsappPartner"
        static let dbExtension = "sqlite3"
        static let dbNameWithExtension = "QuantsappPartner.sqlite3"
    }
    
    struct DBTables {
        static let AnalyzerSummary: String = "AnalyzerSummary"
        static let CallHistory: String = "CallHistory"
        static let AppLog: String = "AppLog"
        static let Archive: String = "Archive"
        static let ErrorLog: String = "ErrorLog"
        static let FutMaster: String = "FutMaster"
        static let GcmRefresh: String = "GcmRefresh"
        static let GlobalNotification: String = "GlobalNotification"
        static let HelpCase: String = "HelpCase"
        static let MyLibrary: String = "MyLibrary"
        static let Notifications: String = "Notifications"
        static let OptForecasts: String = "OptForecasts"
        static let OptMaster: String = "OptMaster"
        static let PageInfo: String = "PageInfo"
        static let PendingApis: String = "PendingApis"
        static let StrategistMaster: String = "StrategistMaster"
        static let StrategistResult: String = "StrategistResult"
        static let Books: String = "Books"
    }
    
    struct Charts {
        static let chartRedColor: UIColor = UIColor(hexString: "#ff4500")! //UIColor.rgb(red: 242.0, green: 65.0, blue: 65.0)
        static let chartGreenColor: UIColor = UIColor(hexString: "#90ee90")! //UIColor.rgb(red: 69.0, green: 176.0, blue: 1.0)
        
        static let lineWidth: CGFloat = 2.0
    }
    
    struct Screen {
        static let minimumScreenWidth: CGFloat = 320.0
    }
    
    struct WebURLs {
        static let leadershipTeam: String = "https://s3.ap-south-1.amazonaws.com/server-master-files/about_us/aboutus.html"
        static let LinkedIn: String = "https://www.linkedin.com/company/13603241/"
        static let YouTube: String = "https://www.youtube.com/channel/UCKO21sN386P_8h713o6nkGA"
        static let Twitter: String = "https://www.twitter.com/quantsappsocial"
        static let Facebook: String = "https://www.facebook.com/Quantsapp-161061017958377/"
        static let disclaimer: String = "https://goo.gl/7MPDy7"
    }
}
