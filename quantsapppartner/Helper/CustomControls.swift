//
//  CustomControls.swift
//  quantsapppartner
//
//  Created by Quantsapp on 13/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation
import UIKit

// MARK:-
// MARK:- DropdownButton

class DropdownButton: UIButton {
    
    public var options: [String]?
    public var selectedOption: String?
    public var placeholder: String?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 7, left: (bounds.width - 20), bottom: (bounds.height - 23), right: 8) //imageEdgeInsets = UIEdgeInsets(top: 7, left: (bounds.width - 20), bottom: 7, right: 8)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)! + 16)
        }
    }
}

// MARK:-
// MARK:- DirectionButton

class DirectionButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: (bounds.width - 34), bottom: 0, right: 4)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)! + 12)
        }
    }
}

// MARK:-
// MARK:- RiskButton

class RiskButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: (bounds.width - 34), bottom: 0, right: 4)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)! + 12)
        }
    }
}

// MARK:-
// MARK:- FetchCMPButton

class FetchCMPButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 4, left: (bounds.width - 28), bottom: 4, right: 4)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -36, bottom: 0, right: (imageView?.frame.width)! + 12)
        }
    }
}

// MARK:-
// MARK:- RadioOptionButton

class RadioOptionButton: UIButton {
    
    public var row: OSOFormRowRadio?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: (bounds.width - 40))
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        }
    }
}

// MARK:-
// MARK:- SortByButton

class SortByButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 8, left: (bounds.width - 22), bottom: 8, right: 4)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -32, bottom: 0, right: (imageView?.frame.width)! + 8)
        }
    }
}

// MARK:-
// MARK:- OSOTextField

class OSOTextField: UITextField {
    
    init() {
        super.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    /*func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let inverseSet = NSCharacterSet(charactersIn:"0123456789-.").inverted
        let components = string.components(separatedBy: inverseSet)
        let filtered = components.joined(separator: "")
        
        if string == filtered {
            if string == "-" {
                let count = (textField.text?.components(separatedBy: "-").count)! - 1
                
                if count == 0 {
                    if textField.text?.count == 0 {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            } else if string == "." {
                let count = (textField.text?.components(separatedBy: ".").count)! - 1
                if count > 0 {
                    return false
                } else {
                    return true
                }
            } else {
                return true
            }
        } else {
            return false
        }
    }*/
}


// MARK:-
// MARK:- OSOTextView

protocol OSOTextViewDelegate: class {
    func characterCountChanged(textView: OSOTextView, count: Int)
}

extension OSOTextViewDelegate {
    func characterCountChanged(textView: OSOTextView, count: Int) {}
}

class OSOTextView: UITextView {
    
    weak var delegateOSOTextView: OSOTextViewDelegate?
    
    public var maxCharacterLimit: Int = 100
    
    init() {
        super.init(frame: CGRect.zero, textContainer: nil)
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return true
        } else {
            /*let inverseSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890-/:()&@\".,?![]{}#%+=_<>$~ ").inverted
            let components = text.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return text == filtered*/
            return true
        }
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        
        if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !self.text.isEmpty
        }
        
        guard let inputText = textView.text else { return }
        
        if inputText.count > maxCharacterLimit {
            let limitIndex = inputText.index(inputText.startIndex, offsetBy: inputText.count > maxCharacterLimit ? maxCharacterLimit : inputText.count)
            textView.text = "\(inputText[..<limitIndex])"
        }
        
        delegateOSOTextView?.characterCountChanged(textView: self, count: inputText.count > maxCharacterLimit ? maxCharacterLimit : inputText.count)
    }
}


// MARK:-
// MARK:- OSOMultilineTextView


protocol OSOMultilineTextViewDelegate: class {
    func characterCountChanged(textView: OSOMultilineTextView, count: Int)
}

extension OSOMultilineTextViewDelegate {
    func characterCountChanged(textView: OSOMultilineTextView, count: Int) {}
}

class OSOMultilineTextView: UITextView {
    
    weak var delegateOSOMultilineTextView: OSOMultilineTextViewDelegate?
    
    public var maxCharacterLimit: Int = 100
    
    init() {
        super.init(frame: CGRect.zero, textContainer: nil)
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            //textView.resignFirstResponder()
            return true
        } else {
            /*let inverseSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890-/:()&@\".,?![]{}#%+=_<>$~\\n\\r\\ ").inverted
            let components = text.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return text == filtered*/
            
            return true
        }
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        
        if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !self.text.isEmpty
        }
        
        guard let inputText = textView.text else { return }
        
        if inputText.count > maxCharacterLimit {
            let limitIndex = inputText.index(inputText.startIndex, offsetBy: inputText.count > maxCharacterLimit ? maxCharacterLimit : inputText.count)
            textView.text = "\(inputText[..<limitIndex])"
        }
        
        delegateOSOMultilineTextView?.characterCountChanged(textView: self, count: inputText.count > maxCharacterLimit ? maxCharacterLimit : inputText.count)
    }
}
