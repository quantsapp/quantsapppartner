//
//  OSOServerSettingsManager.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 14/02/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation

fileprivate struct DefaultServerURLs {
    static let Server1: String = "https://alb.quantsapp.net"
    static let Server2: String = "https://api.quantsapp.in"
}

public enum OSOServerName: String {
    
    case Server1 = "Server 1"
    case Server2 = "Server 2"
    
    func keyName() -> String {
        var key = ""
        
        switch self {
        case .Server1:
            key = "server_1"
        case .Server2:
            key = "server_2"
        }
        
        return key
    }
}

public enum OSOServerType: String {
    case Default = "default"
    case Custom = "custom"
    
    func serverURL(serverName: OSOServerName) -> String? {
        
        var url: String?
        
        switch self {
        case .Default:
            if serverName == .Server1 {
                url = DefaultServerURLs.Server1
            } else if serverName == .Server2 {
                url = DefaultServerURLs.Server2
            }
        case .Custom:
            url = UserDefaults.standard.string(forKey: "\(serverName.keyName())_url")
        }
        
        return url
    }
}

class OSOServer {
    
    public var serverType: OSOServerType?
    public var serverName: OSOServerName?
    public var serverUrl: String?
    
    init() {
        
    }
}



final class OSOServerSettingsManager {
    
    static let sharedInstance = OSOServerSettingsManager()
    
    public var allServers: [OSOServer?] {
        get {
            return [server1, server2]
        }
    }
    
    public var server1: OSOServer? {
        didSet {
            if let name = server1?.serverName, let type = server1?.serverType, let url = server1?.serverUrl {
                UserDefaults.standard.set(type.rawValue, forKey: name.keyName())
                UserDefaults.standard.set(url, forKey: "\(name.keyName())_url")
            } else {
                print("Could not set UserDefaults for Server 1")
            }
        }
    }
    
    public var server2: OSOServer? {
        didSet {
            if let name = server2?.serverName, let type = server2?.serverType, let url = server2?.serverUrl {
                UserDefaults.standard.set(type.rawValue, forKey: name.keyName())
                UserDefaults.standard.set(url, forKey: "\(name.keyName())_url")
            } else {
                print("Could not set UserDefaults for Server 2")
            }
        }
    }
    
    init() {
        print("OSOServerSettingsManager Initialized")
        configServer()
    }
    
    private func configServer() {
        
        // SERVER 1
        if let serverType = UserDefaults.standard.string(forKey: OSOServerName.Server1.keyName()) {
            if serverType.isEmpty {
                setDefaultForServer(withName: .Server1)
            } else {
                
                if serverType == OSOServerType.Custom.rawValue {
                    
                    if let url = OSOServerType.Custom.serverURL(serverName: .Server1) {
                        setCustomForServer(withName: .Server1, url: url)
                    } else {
                        setDefaultForServer(withName: .Server1)
                    }
                    
                } else {
                    setDefaultForServer(withName: .Server1)
                }
            }
        } else {
            print("No UserDefaults found for Server 1")
            
            setDefaultForServer(withName: .Server1)
        }
        
        // SERVER 2
        if let serverType = UserDefaults.standard.string(forKey: OSOServerName.Server2.keyName()) {
            if serverType.isEmpty {
                setDefaultForServer(withName: .Server2)
            } else {
                if serverType == OSOServerType.Custom.rawValue {
                    
                    if let url = OSOServerType.Custom.serverURL(serverName: .Server2) {
                        setCustomForServer(withName: .Server2, url: url)
                    } else {
                        setDefaultForServer(withName: .Server2)
                    }
                    
                } else {
                    setDefaultForServer(withName: .Server2)
                }
            }
        } else {
            print("No UserDefaults found for Server 2")
            
            setDefaultForServer(withName: .Server2)
        }
        
    }
    
    private func setDefaultForServer(withName serverName: OSOServerName) {
        let server = OSOServer()
        server.serverType = .Default
        server.serverName = serverName
        server.serverUrl = OSOServerType.Default.serverURL(serverName: serverName)
        
        if serverName == .Server1 {
            server1 = server
        } else if serverName == .Server2 {
            server2 = server
        }
    }
    
    private func setCustomForServer(withName serverName: OSOServerName, url: String?) {
        let server = OSOServer()
        server.serverType = .Custom
        server.serverName = serverName
        server.serverUrl = url
        
        if serverName == .Server1 {
            server1 = server
        } else if serverName == .Server2 {
            server2 = server
        }
    }
    
    public func updateServer(withName serverName: OSOServerName, serverType: OSOServerType, serverURL: String?) {
        
        print("► updateServer(withName serverName: \(serverName.rawValue), serverType: \(serverType.rawValue), serverURL: \(serverURL ?? "nil")")
        if serverType == .Custom {
            setCustomForServer(withName: serverName, url: serverURL)
        } else {
            setDefaultForServer(withName: serverName)
        }
    }
    
    private func readServerURLs() {
        // SERVER 1
        if let server = server1 {
            if let name = server.serverName {
                if let url = server.serverUrl {
                    if let type = server.serverType {
                        print("Server Type: \(type.rawValue)")
                    } else {
                        print("Server Type: Unknown")
                    }
                    print("Name: \(name)")
                    print("URL: \(url)")
                } else {
                    print("Server URL: Unknown")
                }
            } else {
                print("Server Name: Unknown")
            }
        }
        
        // SERVER 2
        if let server = server2 {
            if let name = server.serverName {
                if let url = server.serverUrl {
                    if let type = server.serverType {
                        print("Server Type: \(type.rawValue)")
                    } else {
                        print("Server Type: Unknown")
                    }
                    print("Name: \(name)")
                    print("URL: \(url)")
                } else {
                    print("Server URL: Unknown")
                }
            } else {
                print("Server Name: Unknown")
            }
        }
    }
    
    public func resetServerSettings() {
        setDefaultForServer(withName: .Server1)
        setDefaultForServer(withName: .Server2)
    }
}
