//
//  OSOWebService.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 29/05/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation
import Alamofire
import SwifterSwift

class NetworkRequestRetrier: RequestRetrier {
    
    // [Request url: Number of times retried]
    private var retriedRequests: [String: Int] = [:]
    
    internal func should(_ manager: SessionManager,
                         retry request: Request,
                         with error: Error,
                         completion: @escaping RequestRetryCompletion) {
        
        guard request.task?.response == nil, let url = request.request?.url?.absoluteString else {
            removeCachedUrlRequest(url: request.request?.url?.absoluteString)
            completion(false, 0.0) // don't retry
            return
        }
        
        guard let retryCount = retriedRequests[url] else {
            retriedRequests[url] = 1
            completion(true, 1.0) // retry after 1 second
            return
        }
        
        if retryCount <= Constants.NetworkingConfig.maximumAttempts {
            retriedRequests[url] = retryCount + 1
            completion(true, 1.0) // retry after 1 second
        } else {
            removeCachedUrlRequest(url: url)
            completion(false, 0.0) // don't retry
        }
        
    }
    
    private func removeCachedUrlRequest(url: String?) {
        guard let url = url else {
            return
        }
        retriedRequests.removeValue(forKey: url)
    }
    
}

final class OSOWebService {
    
    static let sharedInstance = OSOWebService()
    
    private var attempts: Int = 0
    
    init() {
        print("OSOWebService Initialized")
    }
    
    typealias Async = (_ success: @escaping (_ jsonData: [String: Any]) -> (), _ failure: @escaping (_ error: NSError) -> ()) -> ()
    
    private func retry(numberOfTimes: Int, task: @escaping ()-> Async, success: @escaping (_ jsonData: [String: Any]) -> (), failure: @escaping (_ error: NSError) -> ()) {
        print("*** RETRY (Attempt \((Constants.NetworkingConfig.maximumAttempts - numberOfTimes) + 1)) ***")
        
        task()(success, { error in
            if numberOfTimes > 1 {
                self.retry(numberOfTimes: numberOfTimes - 1, task: task, success: success, failure: failure)
            } else {
                failure(error)
            }
        })
    }
    
    private func attemptWebRequest(urlString: String, parameters: Parameters, httpMethod: String = "POST") -> Async {
        return { success, failure in
            let sessionManager = SessionManager()
            let requestRetrier = NetworkRequestRetrier()
            sessionManager.retrier = requestRetrier
            
            var request = URLRequest(urlString: urlString)
            request?.httpMethod = httpMethod
            request?.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request?.timeoutInterval = Constants.NetworkingConfig.timeoutInterval
            request?.httpBody = parameters.jsonData()
            
            if httpMethod == "GET" {
                
                sessionManager.request(request!).responseJSON(completionHandler: { (response) in
                    sessionManager.session.invalidateAndCancel()
                    
                    if let error = response.error as NSError? {
                        print("--------------------------- ERROR (A) ----------------------------")
                        print("\(error)")
                        print("------------------------------------------------------------------")
                        //failure(error)
                        let err = NSError.init(type: .Unknown)
                        failure(err)
                    } else {
                        switch response.result {
                        case .success(let value):
                            if let json = value as? [String: Any] {
                                success(json)
                            } else {
                                print("Error: response is not JSON")
                                let error = NSError.init(type: .Unknown)
                                failure(error)
                            }
                        case .failure(let error):
                            print("--------------------------- ERROR (B) ----------------------------")
                            print("\(error)")
                            print("------------------------------------------------------------------")
                            failure(error as NSError)
                        }
                    }
                })
                
                /*sessionManager.request(request!).responseData(completionHandler: { (data) in
                    
                    sessionManager.session.invalidateAndCancel()
                    
                    do {
                        if let d = try data.data?.jsonObject(options: .mutableContainers) {
                            if let da = d as? [String: Any] {
                                if let a = da["body"] as? [String] {
                                    print(a)
                                }
                            }
                        }
                        
                    } catch let error {
                        print("Error: \(error.localizedDescription)")
                    }
                    
                    
                    if let response = data.response {
                        print("response.statusCode = \(response.statusCode)")
                        
                        let status = response.statusCode == 200 ? "1" : "0"
                        let jsonObj = ["status": status]
                        
                        let valid = JSONSerialization.isValidJSONObject(jsonObj)
                        
                        if valid {
                            success(jsonObj)
                        } else {
                            let error = NSError(type: .Unknown)
                            failure(error)
                        }
                    }
                })*/
                
            } else {
                
                sessionManager.request(request!).responseJSON { (response) in
                    
                    sessionManager.session.invalidateAndCancel()
                    
                    if let error = response.error as NSError? {
                        print("--------------------------- ERROR (A) ----------------------------")
                        print("\(error)")
                        print("------------------------------------------------------------------")
                        //failure(error)
                        let err = NSError.init(type: .Unknown)
                        failure(err)
                    } else {
                        switch response.result {
                        case .success(let value):
                            if let json = value as? [String: Any] {
                                success(json)
                            } else {
                                print("Error: response is not JSON")
                                let error = NSError.init(type: .Unknown)
                                failure(error)
                            }
                        case .failure(let error):
                            print("--------------------------- ERROR (B) ----------------------------")
                            print("\(error)")
                            print("------------------------------------------------------------------")
                            failure(error as NSError)
                        }
                    }
                }
                
            }
        }
    }
    
    public func cancelWebRequest() {
        let sessionManager = SessionManager()
        sessionManager.session.invalidateAndCancel()
    }
    
    
    private func AlamofireRequestManager(urlString: String, parameters: Parameters, httpMethod: String = "POST", retryNumberOfTimes: Int = Constants.NetworkingConfig.maximumAttempts, success: @escaping (_ jsonData: [String: Any]) -> (), failure: @escaping (_ error: NSError) -> ()) {
        print("**** AlamofireRequestManager ***")
        print("Parameters = \(parameters)")
        
        if isConnected() {
            
            // attemptWebRequest
            retry(numberOfTimes: retryNumberOfTimes, task: { () -> OSOWebService.Async in
                self.attemptWebRequest(urlString: urlString, parameters: parameters, httpMethod: httpMethod)
            }, success: { (jsonData) in
                success(jsonData)
            }) { (error) in
                failure(error)
            }
            
        } else {
            
            let error = NSError(type: .Offline)
            failure(error)
            
        }
    }
    
    // MARK:- Server URL
    
    private func serverURL(forServer serverName: OSOServerName, path: String) -> String? {
        var urlPath: String?
        var server: OSOServer?
        
        switch serverName {
        case .Server1:
            server = OSOServerSettingsManager.sharedInstance.server1
        case .Server2:
            server = OSOServerSettingsManager.sharedInstance.server2
        }
        
        if let url = server?.serverUrl {
            urlPath = "\(url)/\(path)"
        }
        
        return urlPath
    }
    
    // MARK:- Rechability
    
    public func isConnected() -> Bool {
        if let reachable = NetworkReachabilityManager()?.isReachable {
            return reachable
        } else {
            return false
        }
    }
    
    // MARK:- Master API
    
    /// API: 5  (Master API)
    
    public func masterAPI(version: String, infoVersion: String, sAcType: String, sValidity: String, sLegs: String, sOutputs: String, aAcType: String, aValidity: String, aLegs: String, aOutputs: String, completion: @escaping((_ error: NSError?, _ data: [String: Any]?) -> Void)) {
        print("►  masterAPI()")
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let appVersion = DatabaseManager.sharedInstance.getAppVersion()
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(err, nil)
            return
        }
        
        let outApi = "5|\(userId)|\(token)|\(appVersion)|\(version)|\(infoVersion)|\(sAcType),\(sValidity),\(sLegs),\(sOutputs),\(aAcType),\(aValidity),\(aLegs),\(aOutputs)"
        
        let parameters: Parameters = ["api": outApi]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            completion(nil, json)
        }) { (error) in
        completion(error, nil)
        }
    }
    
    
    // MARK:- Positions
    /// API: 8  (Upload Position)
    public func uploadPosition(positionName: String, symbol: String, margin: String, legs: [String], tradeStatus: String, completion: @escaping((_ error: NSError?, _ positionNo: String?, _ margin: String?) -> Void)) {
        print("---------------------- uploadPosition ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(err, nil, nil)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "8|\(userId)|\(token)|\(positionName)|\(margin)|\(symbol)|\(legs.joined(separator: ";"))|\(tradeStatus)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(err, nil, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(err, nil, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        
                        if splitResult[1] == "Invalid session." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: splitResult[1]])
                            completion(err, nil, nil)
                        }
                        
                    } else {
                        completion(nil, splitResult[1], splitResult[2])
                    }
                    
                } else {
                    let err = NSError(type: OSOError.Unknown)
                    completion(err, nil, nil)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(err, nil, nil)
            }
            
        }) { (error) in
            completion(error, nil, nil)
        }
    }
    
    /// API: 17 (Edit Position)
    public func editPosition(positionNo: String, positionName: String, symbol: String, margin: String, legs: [String], tradeStatus: String, completion: @escaping((_ error: NSError?, _ positionNo: String?, _ margin: String?) -> Void)) {
        print("---------------------- editPosition ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(err, nil, nil)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "17|\(userId)|\(token)|\(positionNo)|\(positionName)|\(margin)|\(symbol)|\(legs.joined(separator: ";"))|\(tradeStatus)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(err, nil, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(err, nil, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        
                        let msg = splitResult[1]
                        
                        if msg == "Invalid session." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: splitResult[1]])
                            completion(err, nil, nil)
                        }
                        
                    } else {
                        completion(nil, splitResult[1], splitResult[2])
                    }
                    
                } else {
                    let err = NSError(type: OSOError.Unknown)
                    completion(err, nil, nil)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(err, nil, nil)
            }
            
        }) { (error) in
            completion(error, nil, nil)
        }
    }
    
    /// API: 16 (Delete Position)
    public func deletePosition(positionNo: String, completion: @escaping((_ error: NSError?) -> Void)) {
        print("---------------------- deletePosition ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(err)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "16|\(userId)|\(token)|\(positionNo)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(err)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(err)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        
                        let msg = splitResult[1]
                        
                        if msg == "Invalid session please login again." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                            completion(err)
                        }
                    } else {
                        // Success
                        completion(nil)
                    }
                } else {
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Delete was not successful."])
                    completion(err)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(err)
            }
            
        }) { (error) in
            completion(error)
        }
    }
    
    /// API: 37 (Archive)
    public func archivePosition(positionNo: String, legs: [ManagerDetailLegs], completion: @escaping((_ error: NSError?, _ id: String?) -> Void)) {
        print("---------------------- archivePosition ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(err, nil)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        var srno: String = ""
        var cmp: String = ""
        
        for i in 0..<legs.count {
            let leg = legs[i]
            
            if let legSrno = leg.srno, let legCmp = leg.cmp {
                srno.append(legSrno)
                cmp.append(legCmp)
                
                if i < legs.count - 1 {
                    srno.append(",")
                    cmp.append(",")
                }
            }
            
        }
        
        let details = "37|\(userId)|\(token)|\(positionNo)|\(srno)|\(cmp)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(err, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(err, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        let msg = splitResult[1]
                        
                        if msg == "Invalid session." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: splitResult[1]])
                            completion(err, nil)
                        }
                        
                    } else {
                        // Success
                        completion(nil, splitResult[1])
                    }
                } else {
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Could not archive position."])
                    completion(err, nil)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(err, nil)
            }
            
        }) { (error) in
            completion(error, nil)
        }
    }
    
    /// API: 9  (Sync Position)
    public func syncPosition(api: String, bookType: String, completion: @escaping((_ response: Int, _ message: String?, _ mtmAPI: String?, _ dataTime: String?) -> Void)) {
        print("---------------------- syncPosition ------------------------")
        
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-opencall/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(0, error.localizedDescription, nil, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(0, error.localizedDescription, nil, nil)
            return
        }
        
        let parameters: Parameters = ["userid": username, "token": token, "positions": api, "book_type": bookType]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "-1" {
                let msg = "Invalid Session. Please login again."
                completion(-1, msg, nil, nil)
            } else if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    completion(0, msg, nil, nil)
                } else {
                    let msg = "No message received."
                    completion(0, msg, nil, nil)
                }
            } else if status == "1" || status == "2"  {
                if let positions = json["positions"] as? [[String: Any]] {
                    
                    var summaries = [AnalyzerSummary]()
                    
                    for position in positions {
                        
                        print(position)
                        
                        var spreadTarget = "0"
                        var spreadStopLoss = "0"
                        
                        if let spread = position["spread_target"] as? String {
                            spreadTarget = spread
                        }
                        
                        if let spread = position["spread_stoploss"] as? String {
                            spreadStopLoss = spread
                        }
                        
                        if let legs = (position["leg"] as? String)?.components(separatedBy: ";") {
                            let nn = legs.count
                            let syncNo = "0"
                            let expired = "0"
                            
                            
                            var closingPrices = [String]()
                            var closingDates = [String]()
                            var fixedBookProfits = [String]()
                            
                            if let prices = (position["closing_price"] as? String)?.components(separatedBy: ";") {
                                closingPrices = prices
                            }
                            
                            if let dates = (position["closing_date"] as? String)?.components(separatedBy: ";") {
                                closingDates = dates
                            }
                            
                            if let profits = (position["fixed_book_profit"] as? String)?.components(separatedBy: ";") {
                                fixedBookProfits = profits
                            }
                            
                            
                            
                            for j in 0..<nn {
                                let srno = "\(j+1)"
                                let instPriceQty = legs[j].components(separatedBy: ",")
                                let fullInstrument = instPriceQty[0].components(separatedBy: " ")
                                let instrument = fullInstrument[0]
                                let expiryDate = fullInstrument[1]
                                
                                // If instrument is future which is detected by this length
                                var optType: String = ""
                                var strike: String = ""
                                if fullInstrument.count == 2 {
                                    strike = "-1"
                                    optType = "XX"
                                } else {
                                    strike = fullInstrument[2]
                                    optType = fullInstrument[3]
                                }
                                
                                let price = instPriceQty[1]
                                let qty = instPriceQty[2]
                                
                                // AnalyzerSummary
                                let pos = AnalyzerSummary()
                                
                                pos.bookType = bookType
                                
                                if let positionNo = position["position"] as? Int {
                                    pos.positionNo = "\(positionNo)"
                                } else if let positionNo = position["position"] as? String {
                                    pos.positionNo = positionNo
                                }
                                
                                if let symbol = position["symbol"] as? String {
                                    pos.symbol = symbol
                                }
                                
                                if let positionName = position["strategy"] as? String {
                                    pos.positionName = positionName
                                }
                                
                                if let dateCreated = (position["entry_date"] as? String)?.date(withFormat: "yyyy-MM-dd") {
                                    pos.dateCreated = dateCreated
                                }
                                
                                pos.margin = "0"
                                pos.alertOne = "0"
                                pos.alertTwo = "0"
                                pos.margin = "0"
                                pos.syncNo = syncNo
                                pos.srno = srno
                                pos.instrument = instrument
                                pos.expiry = expiryDate
                                pos.optType = optType
                                pos.strike = strike
                                pos.price = price
                                pos.qty = qty
                                pos.expired = expired
                                pos.Papi = legs[j]
                                pos.spreadTarget = spreadTarget
                                pos.spreadStopLoss = spreadStopLoss
                                
                                if closingPrices.indices.contains(j) {
                                    pos.closingPrice = closingPrices[j]
                                }
                                
                                if fixedBookProfits.indices.contains(j) {
                                    pos.fixedProfit = fixedBookProfits[j]
                                }
                                
                                if closingDates.indices.contains(j) {
                                    pos.dateLegClosed = closingDates[j]
                                }
                                
                                summaries.append(pos)
                            }
                        }
                    }
                    
                    // Delete all from database
                    if status == "1" {
                        let success = DatabaseManager.sharedInstance.deleteAllAnalyzerPositions()
                        print("deleteAllAnalyzerPositions : \(success ? "success" : "failed")")
                    }
                    
                    // delete residue
                    var positionsToDelete = [String]()
                    
                    if let residue = json["residue"] as? [Int] {
                        print("residue[Int] = \(residue)")
                        positionsToDelete = residue.map { String($0)}
                    } else if let residue = json["residue"] as? [String] {
                        print("residue[String] = \(residue)")
                        positionsToDelete = residue
                    }
                    
                    if positionsToDelete.count > 0 {
                        let success = DatabaseManager.sharedInstance.deletePositionsFromCallHistory(positions: positionsToDelete, bookType: bookType)
                        print("deletePositionsFromCallHistory : \(success ? "success" : "failed")")
                    }
                    
                    // Insert all in database
                    DatabaseManager.sharedInstance.addPosition(analyzerSummaries: summaries, completion: { (success) in
                        print("InsertAllSummariesIntoDatabase = \(success ? "success" : "failed")")
                        if success {
                            completion(2, nil, nil, nil)
                        } else {
                            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Failed to add positions to database."])
                            completion(0, error.localizedDescription, nil, nil)
                        }
                    })
                    
                    
                }
            } else if status == "3" {
                print("status = \(status)")
                
                var positionsToDelete = [String]()
                
                if let residue = json["residue"] as? [Int] {
                    print("residue[Int] = \(residue)")
                    positionsToDelete = residue.map { String($0)}
                } else if let residue = json["residue"] as? [String] {
                    print("residue[String] = \(residue)")
                    positionsToDelete = residue
                }
                
                if positionsToDelete.count > 0 {
                    let success = DatabaseManager.sharedInstance.deletePositionsFromOpenCalls(positions: positionsToDelete, bookType: bookType)
                    print("deletePositionsFromOpenCalls : \(success ? "success" : "failed")")
                }
                
                completion(3, nil, nil, nil)
            } else {
                print("Error: Status is unknown")
                let msg = "Status is unknown."
                completion(0, msg, nil, nil)
            }
            
        }) { (error) in
            completion(0, error.localizedDescription, nil, nil)
        }
    }
    
    /// API: 9  (Sync Position)
    public func syncCallHistory(api: String, bookType: String, completion: @escaping((_ response: Int, _ message: String?, _ mtmAPI: String?, _ dataTime: String?) -> Void)) {
        print("---------------------- syncCallHistory ------------------------")
        
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-calls-history/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(0, error.localizedDescription, nil, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(0, error.localizedDescription, nil, nil)
            return
        }
        
        let parameters: Parameters = ["userid": username, "token": token, "positions": api, "book_type": bookType]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "-1" {
                let msg = "Invalid Session. Please login again."
                completion(-1, msg, nil, nil)
            } else if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    completion(0, msg, nil, nil)
                } else {
                    let msg = "No message received."
                    completion(0, msg, nil, nil)
                }
            } else if status == "1" || status == "2" {
                if let positions = json["positions"] as? [[String: Any]] {
                    
                    var summaries = [AnalyzerSummary]()
                    
                    for position in positions {
                        
                        print(position)
                        
                        var spreadTarget = "0"
                        var spreadStopLoss = "0"
                        
                        if let spread = position["spread_target"] as? String {
                            spreadTarget = spread
                        }
                        
                        if let spread = position["spread_stoploss"] as? String {
                            spreadStopLoss = spread
                        }
                        
                        if let legs = (position["leg"] as? String)?.components(separatedBy: ";") {
                            let nn = legs.count
                            let syncNo = "0"
                            let expired = "0"
                            
                            var initiationPrices = [String]()
                            var closingPrices = [String]()
                            var fixedBookProfits = [String]()
                            
                            if let prices = (position["initiation_price"] as? String)?.components(separatedBy: ";") {
                                initiationPrices = prices
                            }
                            
                            if let prices = (position["closing_price"] as? String)?.components(separatedBy: ";") {
                                closingPrices = prices
                            }
                            
                            if let profits = (position["fixed_book_profit"] as? String)?.components(separatedBy: ";") {
                                fixedBookProfits = profits
                            }
                            
                            for j in 0..<nn {
                                let srno = "\(j+1)"
                                let instPriceQty = legs[j].components(separatedBy: ",")
                                let fullInstrument = instPriceQty[0].components(separatedBy: " ")
                                let instrument = fullInstrument[0]
                                let expiryDate = fullInstrument[1]
                                
                                // If instrument is future which is detected by this length
                                var optType: String = ""
                                var strike: String = ""
                                if fullInstrument.count == 2 {
                                    strike = "-1"
                                    optType = "XX"
                                } else {
                                    strike = fullInstrument[2]
                                    optType = fullInstrument[3]
                                }
                                
                                let price = instPriceQty[1]
                                let qty = instPriceQty[2]
                                
                                // AnalyzerSummary
                                let pos = AnalyzerSummary()
                                pos.bookType = bookType
                                
                                if let positionNo = position["position"] as? Int {
                                    pos.positionNo = "\(positionNo)"
                                } else if let positionNo = position["position"] as? String {
                                    pos.positionNo = positionNo
                                }
                                
                                if let symbol = position["symbol"] as? String {
                                    pos.symbol = symbol
                                }
                                
                                if let positionName = position["strategy"] as? String {
                                    pos.positionName = positionName
                                }
                                
                                if let dateCreated = (position["entry_date"] as? String)?.date(withFormat: "yyyy-MM-dd") {
                                    pos.dateCreated = dateCreated
                                }
                                
                                if let dateClosed = (position["closing_date"] as? String)?.date(withFormat: "yyyy-MM-dd") {
                                    pos.dateClosed = dateClosed
                                }
                                
                                pos.margin = "0"
                                pos.alertOne = "0"
                                pos.alertTwo = "0"
                                pos.margin = "0"
                                pos.syncNo = syncNo
                                pos.srno = srno
                                pos.instrument = instrument
                                pos.expiry = expiryDate
                                pos.optType = optType
                                pos.strike = strike
                                pos.price = price
                                pos.qty = qty
                                pos.expired = expired
                                pos.Papi = legs[j]
                                pos.dateLegClosed = "-"
                                pos.spreadTarget = spreadTarget
                                pos.spreadStopLoss = spreadStopLoss
                                
                                if initiationPrices.indices.contains(j) {
                                    pos.initiationPrice = initiationPrices[j] == "-" ? "0" : initiationPrices[j]
                                }
                                
                                if closingPrices.indices.contains(j) {
                                    pos.closingPrice = closingPrices[j] == "-" ? "0" : closingPrices[j]
                                }
                                
                                if fixedBookProfits.indices.contains(j) {
                                    pos.fixedProfit = fixedBookProfits[j] == "-" ? "0" : fixedBookProfits[j]
                                }
                                
                                summaries.append(pos)
                            }
                        } else {
                            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Positions not received"])
                            completion(0, error.localizedDescription, nil, nil)
                        }
                    }
                    
                    // Delete all from database
                    if status == "1" {
                        let success = DatabaseManager.sharedInstance.deleteAllFromCallsHistory()
                        print("deleteAllFromCallsHistory : \(success ? "success" : "failed")")
                    }
                    
                    // delete residue
                    var positionsToDelete = [String]()
                    
                    if let residue = json["residue"] as? [Int] {
                        print("residue[Int] = \(residue)")
                        positionsToDelete = residue.map { String($0)}
                    } else if let residue = json["residue"] as? [String] {
                        print("residue[String] = \(residue)")
                        positionsToDelete = residue
                    }
                    
                    if positionsToDelete.count > 0 {
                        let success = DatabaseManager.sharedInstance.deletePositionsFromCallHistory(positions: positionsToDelete, bookType: bookType)
                        print("deletePositionsFromCallHistory : \(success ? "success" : "failed")")
                    }
                    
                    
                    // Insert all in database
                    DatabaseManager.sharedInstance.addPositionInCallsHistory(analyzerSummaries: summaries, completion: { (success) in
                        print("InsertAllSummariesIntoDatabase = \(success ? "success" : "failed")")
                        if success {
                            completion(2, nil, nil, nil)
                        } else {
                            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Failed to add positions to database."])
                            completion(0, error.localizedDescription, nil, nil)
                        }
                    })
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Positions not received"])
                    completion(0, error.localizedDescription, nil, nil)
                }
            } else if status == "3" {
                print("status = \(status)")
                
                var positionsToDelete = [String]()
                
                if let residue = json["residue"] as? [Int] {
                    print("residue[Int] = \(residue)")
                    positionsToDelete = residue.map { String($0)}
                } else if let residue = json["residue"] as? [String] {
                    print("residue[String] = \(residue)")
                    positionsToDelete = residue
                }
                
                if positionsToDelete.count > 0 {
                    let success = DatabaseManager.sharedInstance.deletePositionsFromCallHistory(positions: positionsToDelete, bookType: bookType)
                    print("deletePositionsFromCallHistory : \(success ? "success" : "failed")")
                }
                
                completion(3, nil, nil, nil)
            } else {
                print("Error: Status is unknown")
                let msg = "Status is unknown."
                completion(0, msg, nil, nil)
            }
            
        }) { (error) in
            completion(0, error.localizedDescription, nil, nil)
        }
    }
    
    /// API: 10 (Analyzer Summary)
    public func getAnalyzerSummary(completion: @escaping((_ response: Int, _ message: String?, _ mtmAPI: String?, _ dataTime: String?) -> Void)) {
        print("---------------------- getAnalyzerSummary ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(0, err.localizedDescription, nil, nil)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "10|\(userId)|\(token)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    completion(0, msg, nil, nil)
                } else {
                    let msg = "No message received."
                    completion(0, msg, nil, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        let msg = "\(splitResult[1])"
                        
                        if msg == "Invalid session, please login again." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            completion(0, msg, nil, nil)
                        }
                        
                    } else if splitResult[0] == "3" {
                        // Success but no positions
                        let msg = splitResult[1]
                        completion(3, msg, nil, nil)
                    }else {
                        // Code for after reply is received to put to database
                        completion(1, nil, splitResult[1], splitResult[2].condensedWhitespace)
                    }
                } else {
                    let msg = "Unknown error occured."
                    completion(0, msg, nil, nil)
                }
            } else {
                print("Error: Status is unknown")
                let msg = "Status is unknown."
                completion(0, msg, nil, nil)
            }
            
        }) { (error) in
            completion(0, error.localizedDescription, nil, nil)
        }
    }
    
    /// API: 11 (Position Details)
    public func getPositionDetailsAnalyzer(positionNo: String, completion: @escaping((_ response: Int, _ message: String?, _ positionDetails: PositionDetails?) -> Void)) {
        print("---------------------- getPositionDetailsAnalyzer ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(0, err.localizedDescription, nil)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "11|\(userId)|\(token)|\(positionNo)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    completion(0, msg, nil)
                } else {
                    let msg = "No message received."
                    completion(0, msg, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        let msg = "\(splitResult[1])"
                        
                        if msg == "Invalid session." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            completion(0, msg, nil)
                        }
                        
                    } else {
                        // reply is received
                        let acType = splitResult[1]
                        
                        let positionDetails = PositionDetails()
                        
                        positionDetails.acType = acType
                        positionDetails.positionNo = positionNo
                        
                        if acType == "1" {
                            // Account with priviledge of greeks
                            positionDetails.symbolCmp = splitResult[2]
                            positionDetails.delta = splitResult[3]
                            positionDetails.theta = splitResult[4]
                            positionDetails.vega = splitResult[5]
                            positionDetails.gamma = splitResult[6]
                            positionDetails.iv = splitResult[7]
                            let temp = splitResult[8].components(separatedBy: ";") // Contains srno and cmp
                            positionDetails.srno = temp[0].components(separatedBy: ",")
                            positionDetails.cmp = temp[1].components(separatedBy: ",")
                            positionDetails.legDelta = temp[2].components(separatedBy: ",")
                            positionDetails.legTheta = temp[3].components(separatedBy: ",")
                            positionDetails.legVega = temp[4].components(separatedBy: ",")
                            positionDetails.legGamma = temp[5].components(separatedBy: ",")
                            positionDetails.dataTime = splitResult[9]
                        } else {
                            positionDetails.symbolCmp = splitResult[2]
                            positionDetails.iv = splitResult[3]
                            let temp = splitResult[4].components(separatedBy: ";")
                            positionDetails.srno = temp[0].components(separatedBy: ",")
                            positionDetails.cmp = temp[1].components(separatedBy: ",")
                            positionDetails.dataTime = splitResult[5]
                        }
                        
                        completion(1, nil, positionDetails)
                    }
                } else {
                    let msg = "Unknown error occured."
                    completion(0, msg, nil)
                }
            } else {
                print("Error: Status is unknown")
                let msg = "Status is unknown."
                completion(0, msg, nil)
            }
            
        }) { (error) in
            completion(0, error.localizedDescription, nil)
        }
    }
    
    /// API: 59 (Analyzer)
    public func analyzerChartDefault(positionNo: String, type: String, volatility: String, spot: String, days: String, bins: String, lBound: String, uBound: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ response: [String]?) -> Void)) {
        print("---------------------- analyzerChartDefault ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(false, err, nil)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        var details = ""
        
        if type == "1" {
            details = "59|\(userId)|\(token)|\(type)|\(positionNo)|\(volatility)|\(spot)"
        } else {
            details = "59|\(userId)|\(token)|\(type)|\(positionNo)|\(volatility)|\(spot)|\(days)|\(bins)|\(lBound)|\(uBound)"
        }
        
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, err, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(false, err, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        let msg = "\(splitResult[1])"
                        
                        if msg == "Invalid session." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                            completion(false, err, nil)
                        }
                        
                    } else {
                        completion(true, nil, splitResult)
                    }
                } else {
                    let err = NSError(type: OSOError.Unknown)
                    completion(false, err, nil)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(false, err, nil)
            }
            
        }) { (error) in
            completion(false, error, nil)
        }
    }
    
    // MARK:- Alert
    /// API: 19 (Upload Alert)
    public func uploadAlert(positionNo: String, alertOne: String, alertTwo: String, completion: @escaping((_ success: Bool, _ message: String?, _ error: NSError?) -> Void)) {
        print("---------------------- uploadAlert ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(false, nil, err)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "19|\(userId)|\(token)|\(positionNo)|\(alertOne)|\(alertTwo)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, nil, err)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(false, nil, err)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        let msg = splitResult[1]
                        
                        if msg == "Invalid session." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: splitResult[1]])
                            completion(false, nil, err)
                        }
                        
                    } else {
                        completion(true, splitResult[1], nil)
                    }
                } else {
                    let err = NSError(type: OSOError.Unknown)
                    completion(false, nil, err)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(false, nil, err)
            }
            
        }) { (error) in
            completion(false, nil, error)
        }
    }
    
    // MARK:- Strategy
    /// API: 14 (CMP)
    func getCMP(forSymbol symbol: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ CMP: Double?) -> Void)) {
        print("------------- getCMP ------------")
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "14|\(userId)|\(token)|\(symbol)"
        let parameters: Parameters = ["api": details]
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(false, err, nil)
            return
        }
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, err, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(false, err, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    if splitResult[0] == "0" {
                        
                        if splitResult[1] == "0" {
                            //NotificationCenter.default.post(name: .invalidSession, object: nil)
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: splitResult[1]])
                            completion(false, err, nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: splitResult[1]])
                            completion(false, err, nil)
                        }
                    } else {
                        completion(true, nil, Double(splitResult[1])!)
                    }
                } else {
                    let err = NSError(type: OSOError.Unknown)
                    completion(false, err, nil)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(false, err, nil)
            }
            
        }) { (error) in
            completion(false, error, nil)
        }
    }
    
    // MARK:- CMP
    /// API: 42 (Get multiple CMPs)
    func getMultipleOptionCmps(symbol: String, instruments: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ cmps: [String]?) -> Void)) {
        print("---------------------- getMultipleOptionCmps ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            let err = NSError.init(type: .NoURLFound)
            completion(false, err, nil)
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        let details = "42|\(userId)|\(token)|\(symbol.uppercased())|\(instruments)"
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, err, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(false, err, nil)
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        
                        if splitResult[1] == "Invalid session." {
                            NotificationCenter.default.post(name: .invalidSession, object: nil)
                        } else {
                            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: splitResult[1]])
                            completion(false, err, nil)
                        }
                        
                    } else {
                        completion(true, nil, splitResult[1].components(separatedBy: ","))
                    }
                    
                } else {
                    let err = NSError(type: OSOError.Unknown)
                    completion(false, err, nil)
                }
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(false, err, nil)
            }
            
        }) { (error) in
            completion(false, error, nil)
        }
    }
    
    // MARK:- Price
    
    public func getPrice(forInstruments instruments: [String], completion: @escaping((_ error: NSError?, _ price: [Double]?, _ oi: [Double]?, _ volume: [Double]?) -> Void)) {
        print("---------------------- getPrice ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-fetch-price/") else {
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "API Url not found."])
            completion(err, nil, nil, nil)
            return
        }
        
        let parameters: Parameters = ["instruments": instruments]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    //let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    //completion(err, nil, nil, nil)
                }/* else {
                    let err = NSError.init(type: .Unknown)
                    completion(err, nil, nil, nil)
                }*/
                
                self.getPriceAfterFailover(forInstruments: instruments, completion: { (error, price, oi, volume) in
                    completion(error, price, oi, volume)
                })
            } else if status == "1" {
                var priceArray: [Double]?
                var oiArray: [Double]?
                var volumeArray: [Double]?
                
                if let price = json["price"] as? [Double] {
                    priceArray = price
                }
                
                if let oi = json["oi"] as? [Double] {
                    oiArray = oi
                }
                
                if let volume = json["volume"] as? [Double] {
                    volumeArray = volume
                }
                
                completion(nil, priceArray, oiArray, volumeArray)
            } else {
                print("Error: Status is unknown")
                /*let err = NSError.init(type: .Unknown)
                completion(err, nil, nil, nil)*/
                
                self.getPriceAfterFailover(forInstruments: instruments, completion: { (error, price, oi, volume) in
                    completion(error, price, oi, volume)
                })
            }
        }) { (error) in
            //completion(error, nil, nil, nil)
            print("Error: \(error)")
            self.getPriceAfterFailover(forInstruments: instruments, completion: { (error, price, oi, volume) in
                completion(error, price, oi, volume)
            })
        }
    }
    
    public func getPriceAfterFailover(forInstruments instruments: [String], completion: @escaping((_ error: NSError?, _ price: [Double]?, _ oi: [Double]?, _ volume: [Double]?) -> Void)) {
        print("---------------------- getPriceAfterFailover ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "app_api/price/") else {
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "API Url not found."])
            completion(err, nil, nil, nil)
            return
        }
        
        let parameters: Parameters = ["instruments": instruments]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(err, nil, nil, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(err, nil, nil, nil)
                }
            } else if status == "1" {
                var priceArray: [Double]?
                var oiArray: [Double]?
                var volumeArray: [Double]?
                
                if let price = json["price"] as? [Double] {
                    priceArray = price
                }
                
                if let oi = json["oi"] as? [Double] {
                    oiArray = oi
                }
                
                if let volume = json["volume"] as? [Double] {
                    volumeArray = volume
                }
                
                completion(nil, priceArray, oiArray, volumeArray)
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(err, nil, nil, nil)
            }
        }) { (error) in
            completion(error, nil, nil, nil)
        }
    }
    
    public func getPrice(forInstrument instrument: String, completion: @escaping((_ error: NSError?, _ price: Double?) -> Void)) {
        print("---------------------- getPrice ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-fetch-price/") else {
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "API Url not found."])
            completion(err, nil)
            return
        }
        
        let parameters: Parameters = ["instruments": [instrument]]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    //let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    //completion(err, nil)
                } /*else {
                    let err = NSError.init(type: .Unknown)
                    completion(err, nil)
                }*/
                
                self.getPriceAfterFailover(forInstrument: instrument, completion: { (error, price) in
                    completion(error, price)
                })
            } else if status == "1" {
                var price: Double?
                
                if let value = (json["price"] as? [Double])?.first {
                    price = value
                }
                
                completion(nil, price)
            } else {
                print("Error: Status is unknown")
                //let err = NSError.init(type: .Unknown)
                //completion(err, nil)
                self.getPriceAfterFailover(forInstrument: instrument, completion: { (error, price) in
                    completion(error, price)
                })
            }
        }) { (error) in
            print("Error: \(error)")
            //completion(error, nil)
            self.getPriceAfterFailover(forInstrument: instrument, completion: { (error, price) in
                completion(error, price)
            })
        }
    }
    
    public func getPriceAfterFailover(forInstrument instrument: String, completion: @escaping((_ error: NSError?, _ price: Double?) -> Void)) {
        print("---------------------- getPriceAfterFailover ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "app_api/price/") else {
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "API Url not found."])
            completion(err, nil)
            return
        }
        
        let parameters: Parameters = ["instruments": [instrument]]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(err, nil)
                } else {
                    let err = NSError.init(type: .Unknown)
                    completion(err, nil)
                }
            } else if status == "1" {
                var price: Double?
                
                if let value = (json["price"] as? [Double])?.first {
                    price = value
                }
                
                completion(nil, price)
            } else {
                print("Error: Status is unknown")
                let err = NSError.init(type: .Unknown)
                completion(err, nil)
            }
        }) { (error) in
            completion(error, nil)
        }
    }
    
    /// API: 7 (CMP for Instrument)
    
    func getCMPForInstrument(instrumentType type: String, symbol: String, expiry: String, strike: String, optType: String, completion: @escaping((_ cmp: String) -> Void)) {
        print("---------------------- getCMPForInstrument ------------------------")
        
        guard let urlString = serverURL(forServer: .Server1, path: "m/api/") else {
            completion("0")
            return
        }
        
        let userId = DatabaseManager.sharedInstance.getUserId()
        let token = DatabaseManager.sharedInstance.getToken()
        var details = ""
        
        if type.uppercased() == "FUT" {
            details = "7|\(userId)|\(token)|\(symbol.uppercased())|\(expiry)|XX|-1"
        } else {
            details = "7|\(userId)|\(token)|\(symbol.uppercased())|\(expiry)|\(optType)|\(strike)"
        }
        
        let parameters: Parameters = ["api": details]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["msg"] as? String {
                    print("msg = \(msg)")
                    completion("0")
                } else {
                    completion("0")
                }
            } else if status == "1" {
                if let result = json["api"] {
                    let splitResult = (result as! String).components(separatedBy: "|")
                    
                    if splitResult[0] == "0" {
                        //completion("0")
                        NotificationCenter.default.post(name: .invalidSession, object: nil)
                    } else {
                        completion(splitResult[1])
                    }
                } else {
                    //completion("0")
                    NotificationCenter.default.post(name: .invalidSession, object: nil)
                }
            } else {
                print("Error: Status is unknown")
                completion("0")
            }
            
        }) { (error) in
            completion("0")
        }
    }
    
    // MARK:- ADD CALL
    
    public func uploadCall(positionName: String, strategyName: String, symbol: String, margin: String, legs: [String], tradeStatus: String, bookTypes: [String], spread: String, target: String, stopLoss: String, comment: String, completion: @escaping((_ error: NSError?, _ positionNo: [String]?, _ margin: String?, _ bookName: [String]?, _ notificationTitle: String?, _ notificationMessage: String?, _ dataValues: String?) -> Void)) {
        
        print("► OSOWebService - uploadCall()")
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(error, nil, nil, nil, nil, nil, nil)
            return
        }
        
        
        let urlString = "https://api.quantsapp.in/partner/partner-addcall/"
        let parameters: Parameters = ["api": "\(username)|\(token)|\(strategyName)|\(tradeStatus)|\(symbol)|\(legs.joined(separator: ";"))|\(bookTypes.joined(separator: ";"))|\(comment)|\(spread)|\(target)|\(stopLoss)|\(positionName)"]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "1" {
                var margin = "0"
                
                if let marginValue = json["margin"] as? Int {
                    margin = "\(marginValue)"
                } else if let marginValue = json["margin"] as? String {
                    margin = marginValue
                }
                
                if let positions = json["position"] as? [Int] {
                    if let notiMsg = json["notification"] as? String, let notfTitle = json["notif_title"] as? String, let bookName = json["book_name"] as? [String], let values = json["values"] as? String {
                        
                        completion(nil, positions.map { String($0)}, margin, bookName, notfTitle, notiMsg, values)
                    } else {
                        let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Notification message not received."])
                        completion(error, nil, nil, nil, nil, nil, nil)
                    }
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Data not received"])
                    completion(error, nil, nil, nil, nil, nil, nil)
                }
            } else if status == "0" {
                if let msg = json["message"] as? String  {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(error, nil, nil, nil, nil, nil, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Message not received"])
                    completion(error, nil, nil, nil, nil, nil, nil)
                }
            } else if status == "-1" {
                let err = NSError.init(type: .InvalidSession)
                completion(err, nil, nil, nil, nil, nil, nil)
            } else {
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Status Unknown"])
                completion(error, nil, nil, nil, nil, nil, nil)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(error, nil, nil, nil, nil, nil, nil)
        }
    }
    
    // MARK:- Send Notification
    /// Send Notification
    
    public func sendNotification(positions: [String], bookName: [String], notfTitle: String, notfBody: String, dataValues: String, confirm: Bool, completion: @escaping((_ success: Bool, _ error: NSError?, _ message: String?) -> Void)) {
        print("---------------------- sendNotification ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-notification/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(false, error, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(false, error, nil)
            return
        }
        
        let parameters: Parameters = ["userid": username, "token": token, "positions": positions, "book_name": bookName, "notif_title": notfTitle, "notification": notfBody, "values": dataValues, "confirm": confirm ? "1" : "0"]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, error, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Something went wrong."])
                    completion(false, error, nil)
                }
            } else if status == "1" {
                if confirm {
                    completion(true, nil, "Notification has been sent successfully.")
                } else {
                    completion(true, nil, "Notification has been cancelled.")
                }
            } else {
                print("Error: Status is unknown")
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Response status is unknown."])
                completion(false, error, nil)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(false, error, nil)
        }
    }
    
    // MARK:- BOOK FULL PROFIT
    
    public func bookFullProfit(symbol: String, position: String, bookType: String, legs: [String], completion: @escaping((_ success: Bool, _ error: NSError?, _ message: String?, _ bookName: String?, _ notificationTitle: String?, _ notificationMessage: String?, _ dataValues: String?) -> Void)) {
        print("---------------------- bookFullProfit ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-book-profit/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        let parameters: Parameters = ["userid": username, "token": token, "symbol": symbol, "position": position, "book_type": bookType, "api": legs.joined(separator: ";"), "comment": "0"]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "-1" {
                let error = NSError.init(type: .InvalidSession)
                completion(false, error, nil, nil, nil, nil, nil)
            } else if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, error, nil, nil, nil, nil, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Something went wrong."])
                    completion(false, error, nil, nil, nil, nil, nil)
                }
            } else if status == "1" {
                if let msg = json["message"] as? String, let notfMessage = json["notification"] as? String, let notfTitle = json["notif_title"] as? String, let bookName = json["book_name"] as? String, let dataValues = json["values"] as? String {
                    print("msg = \(msg)")
                    completion(true, nil, msg, bookName, notfTitle, notfMessage, dataValues)
                } else {
                    let msg = "Call closed successfully"
                    completion(false, nil, msg, nil, nil, nil, nil)
                }
            } else {
                print("Error: Status is unknown")
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Response status is unknown."])
                completion(false, error, nil, nil, nil, nil, nil)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(false, error, nil, nil, nil, nil, nil)
        }
    }
    
    // MARK:- BOOK PART PROFIT
    
    public func bookPartProfit(symbol: String, position: String, bookType: String, comment: String, legs: [String], target: String, stopLoss: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ message: String?, _ bookName: String?, _ notificationTitle: String?, _ notificationMessage: String?, _ dataValues: String?) -> Void)) {
        print("---------------------- bookPartProfit ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-partial-profit/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        let parameters: Parameters = ["userid": username, "token": token, "symbol": symbol, "position": position, "book_type": bookType, "comment": comment, "spread_target": target, "spread_stoploss": stopLoss, "api": legs.joined(separator: ";")]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "-1" {
                let error = NSError.init(type: .InvalidSession)
                completion(false, error, nil, nil, nil, nil, nil)
            } else if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, error, nil, nil, nil, nil, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Something went wrong."])
                    completion(false, error, nil, nil, nil, nil, nil)
                }
            } else if status == "1" {
                if let msg = json["message"] as? String, let bookName = json["book_name"] as? String, let notfTitle = json["notif_title"] as? String, let notfMessage = json["notification"] as? String, let dataValues = json["values"] as? String {
                    print("msg = \(msg)")
                    completion(true, nil, msg, bookName, notfTitle, notfMessage, dataValues)
                } else {
                    let msg = "Call closed successfully"
                    completion(false, nil, msg, nil, nil, nil, nil)
                }
            } else {
                print("Error: Status is unknown")
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Response status is unknown."])
                completion(false, error, nil, nil, nil, nil, nil)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(false, error, nil, nil, nil, nil, nil)
        }
    }
    
    // MARK:- Modify Call
    
    public func modifyCall(symbol: String, position: String, bookType: String, comment: String, legs: [String], target: String, stopLoss: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ message: String?, _ bookName: String?, _ notificationTitle: String?, _ notificationMessage: String?, _ dataValues: String?) -> Void)) {
        print("---------------------- modifyCall ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-modify-call/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        let parameters: Parameters = ["userid": username, "token": token, "symbol": symbol, "comment": comment, "position": position, "book_type": bookType, "api": legs.joined(separator: ";"), "spread_target": target, "spread_stoploss": stopLoss]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "-1" {
                let error = NSError.init(type: .InvalidSession)
                completion(false, error, nil, nil, nil, nil, nil)
            } else if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, error, nil, nil, nil, nil, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Something went wrong."])
                    completion(false, error, nil, nil, nil, nil, nil)
                }
            } else if status == "1" {
                if let msg = json["message"] as? String, let notification = json["notification"] as? String, let notfTitle = json["notif_title"] as? String, let bookName = json["book_name"] as? String, let dataValues = json["values"] as? String {
                    print("msg = \(msg)")
                    completion(true, nil, msg, bookName, notfTitle, notification, dataValues)
                } else {
                    let msg = "Call modified successfully"
                    completion(false, nil, msg, nil, nil, nil, nil)
                }
            } else {
                print("Error: Status is unknown")
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Response status is unknown."])
                completion(false, error, nil, nil, nil, nil, nil)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(false, error, nil, nil, nil, nil, nil)
        }
    }
    
    // MARK:- Exit Call
    
    public func exitCall(symbol: String, position: String, bookType: String, legs: [String], completion: @escaping((_ success: Bool, _ error: NSError?, _ message: String?, _ bookName: String?, _ notificationTitle: String?, _ notificationMessage: String?, _ dataValues: String?) -> Void)) {
        print("---------------------- exitCall ------------------------")
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-exit/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(false, error, nil, nil, nil, nil, nil)
            return
        }
        
        let parameters: Parameters = ["userid": username, "token": token, "symbol": symbol, "position": position, "book_type": bookType, "api": legs.joined(separator: ";"), "comment": "0"]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "-1" {
                let error = NSError.init(type: .InvalidSession)
                completion(false, error, nil, nil, nil, nil, nil)
            } else if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, error, nil, nil, nil, nil, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Something went wrong."])
                    completion(false, error, nil, nil, nil, nil, nil)
                }
            } else if status == "1" {
                if let msg = json["message"] as? String, let notfMessage = json["notification"] as? String, let bookName = json["book_name"] as? String, let notfTitle = json["notif_title"] as? String, let dataValues = json["values"] as? String {
                    print("msg = \(msg)")
                    completion(true, nil, msg, bookName, notfTitle, notfMessage, dataValues)
                } else {
                    let msg = "Call closed successfully"
                    completion(false, nil, msg, nil, nil, nil, nil)
                }
            } else {
                print("Error: Status is unknown")
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Response status is unknown."])
                completion(false, error, nil, nil, nil, nil, nil)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(false, error, nil, nil, nil, nil, nil)
        }
    }
    
    // MARK:- MARKET UPDATE
    /// MARKET UPDATE
    
    public func sendMarketUpdate(title: String, message: String, bookTypes: [String], completion: @escaping((_ success: Bool, _ error: NSError?, _ message: String?) -> Void)) {
        print("► OSOWebService - sendMarketUpdate()")
        
        guard let urlString = serverURL(forServer: .Server2, path: "partner/partner-custom-notification/") else {
            let error = NSError.init(type: .NoURLFound)
            completion(false, error, nil)
            return
        }
        
        guard let token = SessionTokenManager.sharedInstance.token?.int, let username = SessionTokenManager.sharedInstance.username else {
            let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid Token"])
            completion(false, error, nil)
            return
        }
        
        
        let parameters: Parameters = ["userid": username, "token": token, "title": title, "message": message, "book_type": bookTypes.joined(separator: ";")]
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, error, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Something went wrong."])
                    completion(false, error, nil)
                }
            } else if status == "1" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    completion(true, nil, msg)
                } else {
                    let msg = "Message sent successfully"
                    completion(false, nil, msg)
                }
            } else {
                print("Error: Status is unknown")
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Response status is unknown."])
                completion(false, error, nil)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(false, error, nil)
        }
    }
    
    // MARK:- LOGIN
    /// LOGIN
    
    public func login(username: String, password: String, completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        print("---------------------- login ------------------------")
        let urlString = "https://api.quantsapp.in/partner/partner-login/"
        let parameters: Parameters = ["email": username, "password": password, "token": "0"]
        
        SessionTokenManager.sharedInstance.setToken(token: nil)
        SessionTokenManager.sharedInstance.setUsername(username: nil)
        
        AlamofireRequestManager(urlString: urlString, parameters: parameters, success: { (json) in
            
            print(json)
            
            var status: String = ""
            
            if let statusStr = json["status"] as? String {
                status = statusStr
            } else if let statusStr = json["status"] as? Int {
                status = "\(statusStr)"
            }
            
            if status == "0" {
                if let msg = json["message"] as? String {
                    print("msg = \(msg)")
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(false, error)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Login failed."])
                    completion(false, error)
                }
            } else if status == "1" {
                if let email = json["email"] as? String {
                    print("Email: \(email)")
                }
                
                if let token = json["token"] as? Int {
                    print("token: \(token)")
                    SessionTokenManager.sharedInstance.setToken(token: "\(token)")
                    SessionTokenManager.sharedInstance.setUsername(username: username)
                }
                
                // books
                if let books = json["books"] as? [[String: Any]] {
                    var osoBooks = [OSOBook]()
                    
                    for book in books {
                        if let bookTitle = book["book_title"] as? String, let bookType = book["book_type"] as? String {
                            print("book_title : \(bookTitle)")
                            let osoBook = OSOBook()
                            osoBook.bookType = bookType
                            osoBook.title = bookTitle
                            osoBooks.append(osoBook)
                        }
                    }
                    
                    if osoBooks.count > 0 {
                        let flag = DatabaseManager.sharedInstance.addBooks(books: osoBooks)
                        print("Add Book flag: \(flag ? "success" : "failed")")
                    }
                }
                
                if let _ = SessionTokenManager.sharedInstance.token {
                    completion(true, nil)
                } else {
                    let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Invalid token"])
                    completion(false, error)
                }
            } else {
                print("Error: Status is unknown")
                let error = NSError.init(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Response status is unknown."])
                completion(false, error)
            }
        }) { (error) in
            print("Error: \(error.localizedDescription)")
            completion(false, error)
        }
    }
}


