//
//  OSOErrors.swift
//  quantsapppartner
//
//  Created by Quantsapp on 14/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation

public enum OSOError: Int {
    
    case Offline            = 1000
    case Unknown            = 2000
    case FileNotFound       = 2001
    case PathNotFound       = 2002
    case Logout             = 2003
    case InvalidResponse    = 2004
    case TableError         = 3000
    case TableNotCreated    = 3001
    case SignUpDataError    = 3002
    case SignUpDataDeleteError = 3003
    case InvalidLogin       = 4000
    case InvalidSession     = 4001
    case InvalidDate        = 5000
    case InvalidExpiry      = 5001
    case InvalidSymbol      = 5002
    case ForecastDeleteError = 5003
    case NoRecordsFound     = 5004
    case NoURLFound         = 6001
    
    
    func localizedUserInfo() -> [String: String] {
        var localizedDescription: String = ""
        var localizedFailureReasonError: String = ""
        var localizedRecoverySuggestionError: String = ""
        
        switch self {
        case .Offline:
            localizedDescription = NSLocalizedString("The Internet connection appears to be offline.", comment: "")
        case .Unknown:
            localizedDescription = NSLocalizedString("Something went wrong.", comment: "Something went wrong.")
            localizedFailureReasonError = NSLocalizedString("Unknown", comment: "Unknown")
        case .FileNotFound:
            localizedDescription = NSLocalizedString("File does not exist", comment: "File does not exist")
            localizedFailureReasonError = NSLocalizedString("File not found on specified path", comment: "File not found on specified path")
            localizedRecoverySuggestionError = NSLocalizedString("Create file at specified path", comment: "Create file at specified path")
        case .PathNotFound:
            localizedDescription = NSLocalizedString("Path not found", comment: "Path not found")
            localizedFailureReasonError = NSLocalizedString("Path does not exist", comment: "Path does not exist")
            localizedRecoverySuggestionError = NSLocalizedString("Please check the path components", comment: "Please check the path components")
        case .TableError:
            localizedDescription = NSLocalizedString("Error occurred while creating table", comment: "")
        case .Logout:
            localizedDescription = NSLocalizedString("Could not log out at the moment. Please try again.", comment: "")
        case .TableNotCreated:
            localizedDescription = NSLocalizedString("Failed to create table", comment: "")
        case .SignUpDataError:
            localizedDescription = NSLocalizedString("Failed to save Sign Up data", comment: "")
        case .SignUpDataDeleteError:
            localizedDescription = NSLocalizedString("Failed to delete Sign Up data", comment: "")
        case .InvalidLogin:
            localizedDescription = NSLocalizedString("Invalid Login. Please logout and try again.", comment: "")
        case .InvalidSession:
            localizedDescription = NSLocalizedString("Invalid Session. Please login again.", comment: "")
        case .InvalidDate:
            localizedDescription = NSLocalizedString("Security date requested for comparison is not available.", comment: "")
        case .InvalidExpiry:
            localizedDescription = NSLocalizedString("Expiry does not exist, re-launch App to update masters.", comment: "")
        case .InvalidSymbol:
            localizedDescription = NSLocalizedString("Invalid Symbol", comment: "")
        case .ForecastDeleteError:
            localizedDescription = NSLocalizedString("Failed to delete forecast.", comment: "")
        case .NoRecordsFound:
            localizedDescription = NSLocalizedString("No record found", comment: "")
        case .NoURLFound:
            localizedDescription = NSLocalizedString("Server URL not found", comment: "")
        case .InvalidResponse:
            localizedDescription = NSLocalizedString("Invalid response", comment: "")
        }
        
        return [
            NSLocalizedDescriptionKey: localizedDescription,
            NSLocalizedFailureReasonErrorKey: localizedFailureReasonError,
            NSLocalizedRecoverySuggestionErrorKey: localizedRecoverySuggestionError
        ]
    }
}
