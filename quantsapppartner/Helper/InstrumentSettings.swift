//
//  InstrumentSettings.swift
//  OptionStrategyOptimizer
//
//  Created by Quantsapp on 14/06/18.
//  Copyright © 2018 Quantsapp. All rights reserved.
//

import Foundation

fileprivate struct InstrumentSettingsKeys {
    static let symbol = "instrument_setting_symbol"
    static let expiry = "instrument_setting_expiry"
}

class InstrumentSettings {
    
    static let sharedInstance = InstrumentSettings()
    
    
    public var symbol: String? {
        didSet {
            UserDefaults.standard.set(symbol!, forKey: InstrumentSettingsKeys.symbol)
        }
    }
    
    public var expiry: Date? {
        didSet {
            UserDefaults.standard.set(expiry!, forKey: InstrumentSettingsKeys.expiry)
        }
    }
    
    
    
    private init() {
        // symbol
        if let lastSymbol = UserDefaults.standard.string(forKey: InstrumentSettingsKeys.symbol) {
            self.symbol = lastSymbol
        } else {
            self.symbol = "NIFTY"
        }
        
        // expiry
        if let lastExpiry = UserDefaults.standard.date(forKey: InstrumentSettingsKeys.expiry) {
            self.expiry = lastExpiry
        } else {
            self.expiry = nil
        }
    }
    
    fileprivate func setDefaultSetting() {
        
        // symbol
        self.symbol = "NIFTY"
        
        // expiry
        self.expiry = Date().dateFor(.yesterday)
        
    }
    
    public func clear() {
        
        print("------------ Clearing UserDefaults for InstrumentSettings ---------------")
        
        UserDefaults.standard.removeObject(forKey: InstrumentSettingsKeys.symbol)
        UserDefaults.standard.removeObject(forKey: InstrumentSettingsKeys.expiry)
        
        setDefaultSetting()
    }
}
