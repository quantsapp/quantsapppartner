//
//  SessionTokenManager.swift
//  quantsapppartner
//
//  Created by Quantsapp on 22/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import Foundation

final class SessionTokenManager {
    
    static let sharedInstance = SessionTokenManager()
    
    public var token: String? {
        get {
            return UserDefaults.standard.string(forKey: "session_token")
        }
    }
    
    public var username: String? {
        get {
            return UserDefaults.standard.string(forKey: "username")
        }
    }
    
    public func setToken(token: String?) {
        UserDefaults.standard.set(token, forKey: "session_token")
    }
    
    public func setUsername(username: String?) {
        UserDefaults.standard.set(username, forKey: "username")
    }
}
