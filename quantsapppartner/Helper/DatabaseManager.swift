//
//  DatabaseManager.swift
//  quantsapppartner
//
//  Created by Quantsapp on 14/03/19.
//  Copyright © 2019 Quantsapp. All rights reserved.
//

import SQLite
import SwifterSwift

final class DatabaseManager {
    
    static let sharedInstance = DatabaseManager()
    
    private var database: Connection!
    public var isConnectedToDB: Bool {
        get {
            if (self.database != nil) {
                return true
            } else {
                return false
            }
        }
    }
    
    init() {
        print("DatabaseManager Initialized")
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        
        if let pathComponent = url.appendingPathComponent("\(Constants.DB.dbName).\(Constants.DB.dbExtension)") {
            let filePath = pathComponent.path
            
            let fileManager = FileManager.default
            
            if fileManager.fileExists(atPath: filePath) {
                
                do {
                    let database = try Connection(filePath)
                    self.database = database
                } catch {
                    print("Error: \(error)")
                }
            } else {
                print("Error: File does not exist")
            }
        }
    }
    
    public func checkForDatabase(databaseName: String, dbExtension: String, completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        
        if let pathComponent = url.appendingPathComponent("\(databaseName).\(dbExtension)") {
            let filePath = pathComponent.path
            print("filePath: \(filePath)")
            
            let fileManager = FileManager.default
            
            if fileManager.fileExists(atPath: filePath) {
                
                do {
                    let database = try Connection(filePath)
                    self.database = database
                    
                    completion(true, nil)
                } catch {
                    print("Error: \(error)")
                    let err = NSError.init(type: .Unknown)
                    completion(false, err)
                }
            } else {
                let err = NSError.init(type: .FileNotFound)
                completion(false, err)
            }
        } else {
            let err = NSError.init(type: .PathNotFound)
            completion(false, err)
        }
        
    }
    
    public func createDatabase(databaseName: String, completion: @escaping ((_ success: Bool, _ error: Error?) -> Void)) {
        
        print("► createDatabase(databaseName: \(databaseName))")
        
        do {
            let documentsDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentsDirectory.appendingPathComponent(databaseName).appendingPathExtension("sqlite3")
            
            let database = try Connection(fileUrl.path)
            self.database = database
            
            completion(true, nil)
        } catch {
            completion(false, error)
        }
    }
}

// MARK:- Create Tables

extension DatabaseManager {
    
    func createTable(tableName: String, completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        
        print("► createTable(tableName: \(tableName))")
        
        switch tableName {
        case Constants.DBTables.AnalyzerSummary:
            self.createTableAnalyzerSummary { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.CallHistory:
            self.createTableCallHistory { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.AppLog:
            self.createTableAppLog { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.Archive:
            self.createTableArchive { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.ErrorLog:
            self.createTableErrorLog { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.FutMaster:
            self.createTableFutMaster { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.GcmRefresh:
            self.createTableGcmRefresh { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.GlobalNotification:
            self.createTableGlobalNotification { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.HelpCase:
            self.createTableHelpCase { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.MyLibrary:
            self.createTableMyLibrary { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.Notifications:
            self.createTableNotifications { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.OptForecasts:
            self.createTableOptForecasts { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.OptMaster:
            self.createTableOptMaster { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.PageInfo:
            self.createTablePageInfo { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.PendingApis:
            self.createTablePendingApis { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.StrategistMaster:
            self.createTableStrategistMaster { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.StrategistResult:
            self.createTableStrategistResult { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        case Constants.DBTables.Books:
            self.createTableBooks { (success, error) in
                if let error = error {
                    print("✖︎ Failed to create Table \"\(tableName)\" (\(error.localizedDescription)")
                    completion(false, error)
                } else {
                    print("✔︎ Table \"\(tableName)\" created successfully")
                    completion(true, nil)
                }
            }
            
        default:
            let err = NSError.init(type: .TableError)
            completion(false, err)
        }
    }
    
    
    
    // AnalyzerSummary
    func createTableAnalyzerSummary(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let bookType = Expression<String>("bookType")
        let positionNo = Expression<String>("positionNo")
        let srno = Expression<String>("srno")
        let positionName = Expression<String>("positionName")
        let dateCreated = Expression<Date>("dateCreated")
        let Papi = Expression<String>("Papi")
        let instrument = Expression<String>("instrument")
        let symbol = Expression<String>("symbol")
        let strike = Expression<String>("strike")
        let price = Expression<String>("price")
        let qty = Expression<String>("qty")
        let margin = Expression<String>("margin")
        let expiry = Expression<String>("expiry")
        let alertOne = Expression<String>("alertOne")
        let alertTwo = Expression<String>("alertTwo")
        let syncNo = Expression<String>("syncNo")
        let tradeStatus = Expression<String>("tradeStatus")
        let expired = Expression<String>("expired")
        let optType = Expression<String>("optType")
        let dateLegClosed = Expression<String>("dateLegClosed")
        let closingPrice = Expression<String>("closingPrice")
        let fixedProfit = Expression<String>("fixedProfit")
        let spreadTarget = Expression<String>("spreadTarget")
        let spreadStopLoss = Expression<String>("spreadStopLoss")
        
        
        let createTable = table.create { (table) in
            table.column(positionNo)
            table.column(srno)
            table.column(positionName)
            table.column(dateCreated)
            table.column(Papi)
            table.column(instrument)
            table.column(symbol)
            table.column(strike)
            table.column(price)
            table.column(qty)
            table.column(margin)
            table.column(expiry)
            table.column(alertOne)
            table.column(alertTwo)
            table.column(syncNo)
            table.column(tradeStatus)
            table.column(expired)
            table.column(optType)
            table.column(bookType)
            table.column(dateLegClosed)
            table.column(closingPrice)
            table.column(fixedProfit)
            table.column(spreadTarget)
            table.column(spreadStopLoss)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // CallHistory
    func createTableCallHistory(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.CallHistory)
        
        let bookType = Expression<String>("bookType")
        let positionNo = Expression<String>("positionNo")
        let srno = Expression<String>("srno")
        let positionName = Expression<String>("positionName")
        let dateCreated = Expression<Date>("dateCreated")
        let dateClosed = Expression<Date>("dateClosed")
        let Papi = Expression<String>("Papi")
        let instrument = Expression<String>("instrument")
        let symbol = Expression<String>("symbol")
        let strike = Expression<String>("strike")
        let price = Expression<String>("price")
        let qty = Expression<String>("qty")
        let margin = Expression<String>("margin")
        let expiry = Expression<String>("expiry")
        let alertOne = Expression<String>("alertOne")
        let alertTwo = Expression<String>("alertTwo")
        let syncNo = Expression<String>("syncNo")
        let tradeStatus = Expression<String>("tradeStatus")
        let expired = Expression<String>("expired")
        let optType = Expression<String>("optType")
        let dateLegClosed = Expression<String>("dateLegClosed")
        let initiationPrice = Expression<String>("initiationPrice")
        let closingPrice = Expression<String>("closingPrice")
        let fixedProfit = Expression<String>("fixedProfit")
        let spreadTarget = Expression<String>("spreadTarget")
        let spreadStopLoss = Expression<String>("spreadStopLoss")
        
        
        let createTable = table.create { (table) in
            table.column(positionNo)
            table.column(srno)
            table.column(positionName)
            table.column(dateCreated)
            table.column(dateClosed)
            table.column(Papi)
            table.column(instrument)
            table.column(symbol)
            table.column(strike)
            table.column(price)
            table.column(qty)
            table.column(margin)
            table.column(expiry)
            table.column(alertOne)
            table.column(alertTwo)
            table.column(syncNo)
            table.column(tradeStatus)
            table.column(expired)
            table.column(optType)
            table.column(bookType)
            table.column(dateLegClosed)
            table.column(initiationPrice)
            table.column(closingPrice)
            table.column(fixedProfit)
            table.column(spreadTarget)
            table.column(spreadStopLoss)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    
    // AppLog
    func createTableAppLog(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.AppLog)
        
        let time = Expression<Date>("time")
        let type = Expression<Int>("type")
        let seconds = Expression<Double>("seconds")
        
        let createTable = table.create { (table) in
            table.column(time)
            table.column(type)
            table.column(seconds)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // Archive
    func createTableArchive(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.Archive)
        
        let symbol = Expression<String>("symbol")
        let strategy = Expression<String>("strategy")
        let createdOn = Expression<Date>("createdOn")
        let closedOn = Expression<Date>("closedOn")
        let pnl = Expression<String>("pnl")
        let margin = Expression<String>("margin")
        let tradeStatus = Expression<String>("tradeStatus")
        let legData = Expression<String>("legData")
        let id = Expression<String>("id")
        
        
        let createTable = table.create { (table) in
            table.column(symbol)
            table.column(strategy)
            table.column(createdOn)
            table.column(closedOn)
            table.column(pnl)
            table.column(margin)
            table.column(tradeStatus)
            table.column(legData)
            table.column(id)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // ErrorLog
    func createTableErrorLog(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)){
        let table = Table(Constants.DBTables.ErrorLog)
        
        let time = Expression<Date>("time")
        let errorCode = Expression<Double>("errorCode")
        let msg = Expression<String>("msg")
        let activity = Expression<String>("activity")
        let version = Expression<String>("version")
        
        let createTable = table.create { (table) in
            table.column(time)
            table.column(errorCode)
            table.column(msg)
            table.column(activity)
            table.column(version)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    
    // FutMaster
    func createTableFutMaster(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)){
        let table = Table(Constants.DBTables.FutMaster)
        
        let symbol = Expression<String>("symbol")
        let expiry = Expression<Date>("expiry")
        let lot = Expression<String>("lot")
        
        let createTable = table.create { (table) in
            table.column(symbol)
            table.column(expiry)
            table.column(lot)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // GcmRefresh
    func createTableGcmRefresh(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.GcmRefresh)
        
        let gcmToken = Expression<String>("gcmToken")
        let syncStatus = Expression<Int>("syncStatus")
        
        let createTable = table.create { (table) in
            table.column(gcmToken)
            table.column(syncStatus)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // GlobalNotification
    func createTableGlobalNotification(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.GlobalNotification)
        
        let msgId = Expression<Int>("msgId")
        let msg = Expression<String>("msg")
        let dateCreated = Expression<Date>("dateCreated")
        
        
        let createTable = table.create { (table) in
            table.column(msgId)
            table.column(msg)
            table.column(dateCreated)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // HelpCase
    func createTableHelpCase(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.HelpCase)
        
        let caseId = Expression<Int>("caseId")
        let timeStamp = Expression<Date>("timeStamp")
        let caseSubject = Expression<String>("caseSubject")
        let comments = Expression<String>("comments")
        let product = Expression<String>("product")
        let typeUserSupport = Expression<Int>("typeUserSupport")
        let syncStatus = Expression<Int>("syncStatus")
        let localId = Expression<Int>("localId")
        let caseStatus = Expression<String>("caseStatus")
        let openClosed = Expression<Int>("openClosed")
        let closedOn = Expression<Date>("closedOn")
        
        
        let createTable = table.create { (table) in
            table.column(caseId)
            table.column(timeStamp)
            table.column(caseSubject)
            table.column(comments)
            table.column(product)
            table.column(typeUserSupport)
            table.column(syncStatus)
            table.column(localId)
            table.column(caseStatus)
            table.column(openClosed)
            table.column(closedOn)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // MyLibrary
    func createTableMyLibrary(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.MyLibrary)
        
        let no = Expression<Int>("no")
        let direction = Expression<Int>("direction")
        let proficiency = Expression<Int>("proficiency")
        let riskType = Expression<Int>("riskType")
        let optimizer = Expression<Int>("optimizer")
        let legs = Expression<Int>("legs")
        let riskSubType = Expression<Int>("riskSubType")
        let name = Expression<String>("name")
        let trade = Expression<String>("trade")
        let intro = Expression<String>("intro")
        let execute = Expression<String>("execute")
        let tradeText = Expression<String>("tradeText")
        let maxProfit = Expression<String>("maxProfit")
        let maxLoss = Expression<String>("maxLoss")
        let advantages = Expression<String>("advantages")
        let disadvantages = Expression<String>("disadvantages")
        
        
        let createTable = table.create { (table) in
            table.column(no)
            table.column(direction)
            table.column(proficiency)
            table.column(riskType)
            table.column(optimizer)
            table.column(legs)
            table.column(riskSubType)
            table.column(name)
            table.column(trade)
            table.column(intro)
            table.column(execute)
            table.column(tradeText)
            table.column(maxProfit)
            table.column(maxLoss)
            table.column(advantages)
            table.column(disadvantages)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // Notifications
    func createTableNotifications(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.Notifications)
        
        let id = Expression<Int>("id")
        let timeStamp = Expression<Date>("timeStamp")
        let msg = Expression<String>("msg")
        let type = Expression<Int>("type")
        let header = Expression<String>("header")
        let read = Expression<Int>("read")
        let alertVal = Expression<String>("alertVal")
        let mtm = Expression<String>("mtm")
        let activityId = Expression<String>("activityId")
        let bundleString = Expression<String>("bundleString")
        
        
        let createTable = table.create { (table) in
            table.column(id)
            table.column(timeStamp)
            table.column(msg)
            table.column(type)
            table.column(header)
            table.column(read)
            table.column(alertVal)
            table.column(mtm)
            table.column(activityId)
            table.column(bundleString)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // OptForecasts
    func createTableOptForecasts(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.OptForecasts)
        
        let symbol = Expression<String>("symbol")
        let direction = Expression<Int>("direction")
        let validTill = Expression<Date>("validTill")
        let level1 = Expression<String>("level1")
        let level2 = Expression<String>("level2")
        let level3 = Expression<String>("level3")
        let level4 = Expression<String>("level4")
        let id = Expression<Int>("id")
        let dateCreated = Expression<Date>("dateCreated")
        
        
        let createTable = table.create { (table) in
            table.column(symbol)
            table.column(direction)
            table.column(validTill)
            table.column(level1)
            table.column(level2)
            table.column(level3)
            table.column(level4)
            table.column(id)
            table.column(dateCreated)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // OptMaster
    func createTableOptMaster(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.OptMaster)
        
        let symbol = Expression<String>("symbol")
        let expiry = Expression<Date>("expiry")
        let lot = Expression<String>("lot")
        let strikes = Expression<String>("strikes")
        
        let createTable = table.create { (table) in
            table.column(symbol)
            table.column(expiry)
            table.column(lot)
            table.column(strikes)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // PageInfo
    func createTablePageInfo(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.PageInfo)
        
        let id = Expression<Int>("id")
        let pageName = Expression<String>("pageName")
        let segments = Expression<String>("segments")
        let label = Expression<String>("label")
        let value = Expression<String>("value")
        
        
        let createTable = table.create { (table) in
            table.column(id)
            table.column(pageName)
            table.column(segments)
            table.column(label)
            table.column(value)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    
    
    
    // PendingApis
    func createTablePendingApis(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.PendingApis)
        
        let apiNo = Expression<Int>("apiNo")
        let api = Expression<String>("api")
        let apiType = Expression<String>("apiType")
        
        let createTable = table.create { (table) in
            table.column(apiNo)
            table.column(api)
            table.column(apiType)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    
    
    // StrategistMaster
    func createTableStrategistMaster(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.StrategistMaster)
        
        let myKey = Expression<String>("myKey")
        let myVal = Expression<String>("myVal")
        
        let createTable = table.create { (table) in
            table.column(myKey)
            table.column(myVal)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // Books
    func createTableBooks(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.Books)
        
        let bookType = Expression<String>("bookType")
        let bookTitle = Expression<String>("bookTitle")
        
        let createTable = table.create { (table) in
            table.column(bookType)
            table.column(bookTitle)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    // StrategistResult
    func createTableStrategistResult(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.StrategistResult)
        
        let symbol = Expression<String>("symbol")
        let days = Expression<String>("days")
        let volMean = Expression<String>("volMean")
        let volUp = Expression<String>("volUp")
        let volDn = Expression<String>("volDn")
        let strategyName = Expression<String>("strategyName")
        let riskProfile = Expression<String>("riskProfile")
        let leg1 = Expression<String>("leg1")
        let leg2 = Expression<String>("leg2")
        let leg3 = Expression<String>("leg3")
        let leg4 = Expression<String>("leg4")
        let rewardToRisk = Expression<String>("rewardToRisk")
        let margin = Expression<String>("margin")
        let meanProfit = Expression<String>("meanProfit")
        let meanLoss = Expression<String>("meanLoss")
        let l1Mean = Expression<String>("l1Mean")
        let l1Best = Expression<String>("l1Best")
        let l1Worst = Expression<String>("l1Worst")
        let l2Mean = Expression<String>("l2Mean")
        let l2Best = Expression<String>("l2Best")
        let l2Worst = Expression<String>("l2Worst")
        let l3Mean = Expression<String>("l3Mean")
        let l3Best = Expression<String>("l3Best")
        let l3Worst = Expression<String>("l3Worst")
        let l4Mean = Expression<String>("l4Mean")
        let l4Best = Expression<String>("l4Best")
        let l4Worst = Expression<String>("l4Worst")
        let level1 = Expression<String>("level1")
        let level2 = Expression<String>("level2")
        let level3 = Expression<String>("level3")
        let level4 = Expression<String>("level4")
        let type = Expression<Int>("type")
        let tradeSpecific = Expression<String>("tradeSpecific")
        let no = Expression<Int>("no")
        let drawDn = Expression<String>("drawDn")
        let drawUp = Expression<String>("drawUp")
        let drawDnLvl = Expression<String>("drawDnLvl")
        let drawUpLvl = Expression<String>("drawUpLvl")
        let dataTime = Expression<Date>("dataTime")
        
        let createTable = table.create { (table) in
            table.column(symbol)
            table.column(days)
            table.column(volMean)
            table.column(volUp)
            table.column(volDn)
            table.column(strategyName)
            table.column(riskProfile)
            table.column(leg1)
            table.column(leg2)
            table.column(leg3)
            table.column(leg4)
            table.column(rewardToRisk)
            table.column(margin)
            table.column(meanProfit)
            table.column(meanLoss)
            table.column(l1Mean)
            table.column(l1Best)
            table.column(l1Worst)
            table.column(l2Mean)
            table.column(l2Best)
            table.column(l2Worst)
            table.column(l3Mean)
            table.column(l3Best)
            table.column(l3Worst)
            table.column(l4Mean)
            table.column(l4Best)
            table.column(l4Worst)
            table.column(level1)
            table.column(level2)
            table.column(level3)
            table.column(level4)
            table.column(type)
            table.column(tradeSpecific)
            table.column(no)
            table.column(drawDn)
            table.column(drawUp)
            table.column(drawDnLvl)
            table.column(drawUpLvl)
            table.column(dataTime)
        }
        
        do {
            try self.database.run(createTable)
            completion(true, nil)
        } catch {
            print(error)
            let err = NSError.init(type: .TableNotCreated)
            completion(false, err)
        }
    }
    
    
    func insertDefaultsInTableStrategistMaster(completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        
        var dataStrategistMaster = [String: String]()
        dataStrategistMaster["optDescription"] = "0"
        dataStrategistMaster["strategyReq"] = "0"
        dataStrategistMaster["optSpecificFormData"] = "0"
        dataStrategistMaster["dataType"] = "1"
        dataStrategistMaster["OptSetLotCapital"] = "1"
        dataStrategistMaster["OptSetRiskCapitalRatio"] = "50"
        dataStrategistMaster["OptSetCapital"] = "0"
        dataStrategistMaster["OptSetRisk"] = "0"
        dataStrategistMaster["OptSetUpdateCapital"] = "1"
        dataStrategistMaster["OptSetBestWorst"] = "0"
        dataStrategistMaster["OptSetAutoSaveForecast"] = "1"
        dataStrategistMaster["OptSetOverwriteForecast"] = "0"
        dataStrategistMaster["OptSetAutoImportForecast"] = "1"
        dataStrategistMaster["OptSetSortSummaryBy"] = "0"
        dataStrategistMaster["OptSetMaxExpiry"] = "0"
        dataStrategistMaster["OptSetsLegs"] = "0"
        dataStrategistMaster["OptSetsOutputs"] = "0"
        dataStrategistMaster["OptSetBackRatio"] = "0"
        dataStrategistMaster["OptSetRatio"] = "0"
        dataStrategistMaster["OptSetLimitedRisk"] = "0"
        dataStrategistMaster["OptSetUpdateRisk"] = "1"
        dataStrategistMaster["OptSetUpdateRiskCapitalRatio"] = "1"
        dataStrategistMaster["OptSetFiltration"] = "1"
        dataStrategistMaster["OptSetMinRoi"] = "0"
        dataStrategistMaster["managerDescription"] = "0"
        dataStrategistMaster["managerMaxArchive"] = "100"
        dataStrategistMaster["manSetSorting"] = "1"
        dataStrategistMaster["imei"] = "0"
        dataStrategistMaster["infoVersion"] = "0"
        dataStrategistMaster["optimizerPrice"] = "18000/3000"
        dataStrategistMaster["guide"] = "0"
        dataStrategistMaster["managerPrice"] = "18000/3000"
        dataStrategistMaster["managerLitePrice"] = "9000/1500"
        dataStrategistMaster["comboPrice"] = "25000/4200"
        dataStrategistMaster["lastLog"] = "1/1/2017"
        dataStrategistMaster["appVersion"] = "0"
        dataStrategistMaster["tax"] = "0.15"
        dataStrategistMaster["version"] = "1"
        dataStrategistMaster["users"] = "0"
        dataStrategistMaster["promo"] = "0"
        dataStrategistMaster["scripMaster"] = "0"
        dataStrategistMaster["versionLibrary"] = "0"
        dataStrategistMaster["token"] = "0"
        dataStrategistMaster["email"] = "0"
        dataStrategistMaster["userId"] = "0"
        dataStrategistMaster["homeDescription"] = "0"
        dataStrategistMaster["sAcType"] = "0"
        dataStrategistMaster["aAcType"] = "0"
        dataStrategistMaster["sValidity"] = "0"
        dataStrategistMaster["aValidity"] = "0"
        dataStrategistMaster["mobile"] = "0"
        dataStrategistMaster["firstName"] = "0"
        dataStrategistMaster["lastName"] = "0"
        dataStrategistMaster["usrExposure"] = "0"
        dataStrategistMaster["alertMethod"] = "0"
        dataStrategistMaster["notificationDeletion"] = "7"
        dataStrategistMaster["aOutputs"] = "0"
        dataStrategistMaster["aLegs"] = "0"
        dataStrategistMaster["sLegs"] = "1"
        dataStrategistMaster["sOutputs"] = "1"
        dataStrategistMaster["OptSetMinLots"] = "10"
        dataStrategistMaster["logLag"] = "60"
        dataStrategistMaster["backRatio"] = "2"
        dataStrategistMaster["ratio"] = "2"
        dataStrategistMaster["maxExpiry"] = "1"
        dataStrategistMaster["forecasts"] = "0"
        dataStrategistMaster["archive"] = "0"
        dataStrategistMaster["minRoi"] = "0.1"
        dataStrategistMaster["minLots"] = "10"
        
        let table = Table(Constants.DBTables.StrategistMaster)
        let myKey = Expression<String>("myKey")
        let myVal = Expression<String>("myVal")
        
        /*var insertedRows = 0
         
         
         for (key, value) in dataStrategistMaster {
         
         let insertData = table.insert(myKey <- key, myVal <- value)
         
         do {
         try self.database.run(insertData)
         //print("INSERTED DATA")
         insertedRows += 1
         
         if insertedRows == dataStrategistMaster.count {
         completion(true, nil)
         }
         } catch {
         print(error)
         insertedRows += 1
         
         if insertedRows == dataStrategistMaster.count {
         let err = NSError.init(type: .TableError)
         completion(false, err)
         }
         }
         }*/
        
        do {
            try self.database.transaction(.deferred, block: {
                var i = 0
                for (key, value) in dataStrategistMaster {
                    
                    let insertData = table.insert(myKey <- key, myVal <- value)
                    
                    do {
                        try self.database.run(insertData)
                        
                        i += 1
                        if i == dataStrategistMaster.count {
                            completion(true, nil)
                        }
                    } catch {
                        print("Error inserting default data in StrategistMaster: \(error)")
                    }
                }
            })
        } catch {
            print("Error inserting default data in StrategistMaster: \(error)")
            let error = NSError.init(type: .TableError)
            completion(false, error)
        }
    }
    
}

// MARK:- Empty Table

extension DatabaseManager {
    
    public func emptyTable(tableName: String, completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        //try db.run(users.delete())
        
        let table = Table(tableName)
        
        do {
            try self.database.run(table.delete())
            completion(true, nil)
        } catch {
            print("Error Deleting all rows in \(tableName): \(error)")
            let err = NSError.init(type: .TableError)
            completion(false, err)
        }
    }
    
    func emptyTables(tables: [String], completion: @escaping ((_ success: Bool, _ error: NSError?) -> Void)) {
        
        var counter: Int = 0
        for table in tables {
            DatabaseManager.sharedInstance.emptyTable(tableName: table, completion: { (success, error) in
                if let err = error {
                    counter += 1
                    print("Error: \(err.localizedDescription)")
                    
                    if counter == tables.count {
                        completion(true, nil)
                    }
                } else {
                    print("-> Table \"\(table)\" emptied successfully")
                    counter += 1
                    
                    if counter == tables.count {
                        completion(true, nil)
                    }
                }
            })
        }
    }
    
}// MARK:- Update Table

extension DatabaseManager {
    
    func bulkUpdateInStrategicMaster(keyValuePairs: [String: String], completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        
        print("-------------- bulkUpdateInStrategicMaster -----------------")
        //var counter = 0
        
        let opQueue = OperationQueue()
        opQueue.maxConcurrentOperationCount = 1
        opQueue.isSuspended = true
        
        let completionOperation = BlockOperation {
            print("COMPLETED")
            completion(true, nil)
        }
        
        let table = Table("StrategistMaster")
        
        let myKey = Expression<String>("myKey")
        let myVal = Expression<String>("myVal")
        
        for (key, value) in keyValuePairs {
            let operation = BlockOperation {
                let row = table.filter(myKey == key)
                let updateValue = row.update(myVal <- value)
                
                do {
                    if try self.database.run(updateValue) > 0 {
                        //completion(true, nil)
                        print("Updated \"\(key)\" <- \"\(value)\"")
                    } else {
                        print("Error: No row found")
                        
                        // insert new row if key is not found in the table
                        self.insertIntoStrategistMaster(key: key, value: value) { (success, error) in
                            if let error = error {
                                print("Error in setting \(key) : \(error)")
                                //let err = NSError.init(type: .Unknown)
                                //completion(false, err)
                            } else {
                                //completion(true, nil)
                                print("New Row Added \"\(key)\" <- \"\(value)\"")
                            }
                        }
                    }
                } catch {
                    print("Error in setting \(key) : \(error)")
                    //let error = NSError.init(type: .Unknown)
                    //completion(false, error)
                }
            }
            
            completionOperation.addDependency(operation)
            opQueue.addOperation(operation)
        }
        
        OperationQueue.main.addOperation(completionOperation)
        opQueue.isSuspended = false
    }
    
    func bulkInsertIntoStrategistMaster(rows: [[String: Any]], completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.StrategistMaster)
        
        let myKey = Expression<String>("myKey")
        let myVal = Expression<String>("myVal")
        
        do {
            try self.database.transaction(.deferred, block: {
                var i = 0
                for row in rows {
                    let insertData = table.insert(myKey <- row["key"] as! String, myVal <- row["value"] as! String)
                    
                    do {
                        try self.database.run(insertData)
                        
                        i += 1
                        if i == rows.count {
                            completion(true, nil)
                        }
                    } catch {
                        print("Error inserting data in StrategistMaster: \(error)")
                    }
                }
            })
        } catch {
            print("Error inserting data in StrategistMaster: \(error)")
            let error = NSError.init(type: .TableError)
            completion(false, error)
        }
    }
    
    func insertIntoStrategistMaster(key: String, value: String, completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table("StrategistMaster")
        
        let myKey = Expression<String>("myKey")
        let myVal = Expression<String>("myVal")
        
        let insertData = table.insert(myKey <- key, myVal <- value)
        
        do {
            try self.database.run(insertData)
            completion(true, nil)
        } catch {
            print("Error inserting data in StrategistMaster: \(error)")
            let error = NSError.init(type: .Unknown)
            completion(false, error)
        }
    }
    
    func bulkInsertIntoFutMaster(rowsFutMaster: [[String: Any]], completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.FutMaster)
        
        let symbol = Expression<String>("symbol")
        let expiry = Expression<Date>("expiry")
        let lot = Expression<String>("lot")
        
        do {
            try self.database.transaction(.deferred, block: {
                var i = 0
                for rowFut in rowsFutMaster {
                    let insertData = table.insert(symbol <- rowFut["symbol"] as! String, expiry <- rowFut["expiry"] as! Date, lot <- rowFut["lot"] as! String)
                    
                    do {
                        try self.database.run(insertData)
                        
                        i += 1
                        if i == rowsFutMaster.count {
                            completion(true, nil)
                        }
                    } catch {
                        print("Error inserting data in FutMaster: \(error)")
                    }
                }
            })
        } catch {
            print("Error inserting data in FutMaster: \(error)")
            let error = NSError.init(type: .TableError)
            completion(false, error)
        }
    }
    
    func bulkInsertIntoOptMaster(rowsOptMaster: [[String: Any]], completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.OptMaster)
        
        let symbol = Expression<String>("symbol")
        let expiry = Expression<Date>("expiry")
        let lot = Expression<String>("lot")
        let strikes = Expression<String>("strikes")
        
        do {
            try self.database.transaction(.deferred, block: {
                var i = 0
                for rowOpt in rowsOptMaster {
                    let insertData = table.insert(symbol <- rowOpt["symbol"] as! String, expiry <- rowOpt["expiry"] as! Date, lot <- rowOpt["lot"] as! String, strikes <- rowOpt["strikes"] as! String)
                    
                    do {
                        try self.database.run(insertData)
                        
                        i += 1
                        if i == rowsOptMaster.count {
                            completion(true, nil)
                        }
                    } catch {
                        print("Error inserting data in OptMaster: \(error)")
                    }
                }
            })
        } catch {
            print("Error inserting data in OptMaster: \(error)")
            let error = NSError.init(type: .TableError)
            completion(false, error)
        }
    }
    
    func bulkInsertIntoMyLibrary(rows: [[String: Any]], completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.MyLibrary)
        
        let no = Expression<Int>("no")
        let direction = Expression<Int>("direction")
        let proficiency = Expression<Int>("proficiency")
        let riskType = Expression<Int>("riskType")
        let optimizer = Expression<Int>("optimizer")
        let legs = Expression<Int>("legs")
        let riskSubType = Expression<Int>("riskSubType")
        let name = Expression<String>("name")
        let trade = Expression<String>("trade")
        let intro = Expression<String>("intro")
        let execute = Expression<String>("execute")
        let tradeText = Expression<String>("tradeText")
        let maxProfit = Expression<String>("maxProfit")
        let maxLoss = Expression<String>("maxLoss")
        let advantages = Expression<String>("advantages")
        let disadvantages = Expression<String>("disadvantages")
        
        do {
            try self.database.transaction(.deferred, block: {
                var i = 0
                for row in rows {
                    let insertData = table.insert(no <- row["no"] as! Int,
                                                  direction <- row["direction"] as! Int,
                                                  proficiency <- row["proficiency"] as! Int,
                                                  riskType <- row["riskType"] as! Int,
                                                  optimizer <- row["optimizer"] as! Int,
                                                  legs <- row["legs"] as! Int,
                                                  riskSubType <- row["riskSubType"] as! Int,
                                                  name <- row["name"] as! String,
                                                  trade <- row["trade"] as! String,
                                                  intro <- row["intro"] as! String,
                                                  execute <- row["execute"] as! String,
                                                  tradeText <- row["tradeText"] as! String,
                                                  maxProfit <- row["maxProfit"] as! String,
                                                  maxLoss <- row["maxLoss"] as! String,
                                                  advantages <- row["advantages"] as! String,
                                                  disadvantages <- row["disadvantages"] as! String)
                    
                    do {
                        try self.database.run(insertData)
                        
                        i += 1
                        if i == rows.count {
                            completion(true, nil)
                        }
                    } catch {
                        print("Error inserting data in MyLibrary: \(error)")
                    }
                }
            })
        } catch {
            print("Error inserting data in MyLibrary: \(error)")
            let error = NSError.init(type: .TableError)
            completion(false, error)
        }
    }
    
    func bulkInsertIntoPageInfo(rows: [[String: Any]], completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table(Constants.DBTables.PageInfo)
        
        let id = Expression<Int>("id")
        let pageName = Expression<String>("pageName")
        let segments = Expression<String>("segments")
        let label = Expression<String>("label")
        let value = Expression<String>("value")
        
        do {
            try self.database.transaction(.deferred, block: {
                var i = 0
                for row in rows {
                    let insertData = table.insert(id <- row["id"] as! Int,
                                                  pageName <- row["pageName"] as! String,
                                                  segments <- row["segments"] as! String,
                                                  label <- row["label"] as! String,
                                                  value <- row["value"] as! String)
                    
                    do {
                        try self.database.run(insertData)
                        
                        i += 1
                        if i == rows.count {
                            completion(true, nil)
                        }
                    } catch {
                        print("Error inserting data in PageInfo: \(error)")
                    }
                }
            })
        } catch {
            print("Error inserting data in PageInfo: \(error)")
            let error = NSError.init(type: .TableError)
            completion(false, error)
        }
    }
    
    func bulkInsertIntoOptForecasts(forecasts: [OptForecasts], completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        print("---------- bulkInsertIntoOptForecasts -------------")
        let table = Table(Constants.DBTables.OptForecasts)
        
        let expSymbol = Expression<String>("symbol")
        let expDirection = Expression<Int>("direction")
        let expValidTill = Expression<Date>("validTill")
        let expLevel1 = Expression<String>("level1")
        let expLevel2 = Expression<String>("level2")
        let expLevel3 = Expression<String>("level3")
        let expLevel4 = Expression<String>("level4")
        let expId = Expression<Int>("id")
        let expDateCreated = Expression<Date>("dateCreated")
        
        do {
            try self.database.transaction(.deferred, block: {
                var i = 0
                for forecast in forecasts {
                    
                    
                    if let symbol = forecast.symbol, let direction = forecast.direction, let validTill = forecast.validTill, let level1 = forecast.level1, let level2 = forecast.level2, let level3 = forecast.level3, let level4 = forecast.level4, let id = forecast.id, let dateCreated = forecast.dateCreated {
                        
                        let insertData = table.insert(expSymbol <- symbol,
                                                      expDirection <- direction,
                                                      expValidTill <- validTill,
                                                      expLevel1 <- level1,
                                                      expLevel2 <- level2,
                                                      expLevel3 <- level3,
                                                      expLevel4 <- level4,
                                                      expId <- id,
                                                      expDateCreated <- dateCreated)
                        
                        do {
                            try self.database.run(insertData)
                            
                            i += 1
                            if i == forecasts.count {
                                completion(true, nil)
                            }
                        } catch {
                            print("Error inserting data in PageInfo: \(error)")
                        }
                        
                    }
                }
            })
        } catch {
            print("Error inserting data in PageInfo: \(error)")
            let error = NSError.init(type: .TableError)
            completion(false, error)
        }
    }
    
    func updateValueForKeyInStrategistMaster(key: String, value: String, completion: @escaping((_ success: Bool, _ error: NSError?) -> Void)) {
        let table = Table("StrategistMaster")
        
        let myKey = Expression<String>("myKey")
        let myVal = Expression<String>("myVal")
        
        let row = table.filter(myKey == key)
        let updateValue = row.update(myVal <- value)
        
        do {
            if try self.database.run(updateValue) > 0 {
                completion(true, nil)
            } else {
                print("Error: No row found")
                
                // insert new row if key is not found in the table
                insertIntoStrategistMaster(key: key, value: value) { (success, error) in
                    if let error = error {
                        print("Error in setting \(key) : \(error)")
                        let err = NSError.init(type: .Unknown)
                        completion(false, err)
                    } else {
                        completion(true, nil)
                    }
                }
            }
        } catch {
            print("Error in setting \(key) : \(error)")
            let error = NSError.init(type: .Unknown)
            completion(false, error)
        }
    }
}

// MARK:- Getters & Setters

extension DatabaseManager {
    
    public func getUserId() -> String {
        return getValueForKeyInStrategistMaster(key: "userId")
        //return "47814"
    }
    
    public func getToken() -> String {
        let token = getValueForKeyInStrategistMaster(key: "token")
        return token
        //return "328"
    }
    
    public func getAppVersion() -> String {
        return getValueForKeyInStrategistMaster(key: "appVersion")
    }
    
    public func getMasterVersions() -> [String: String] {
        var masterVersions = [String: String]()
        
        // scripMaster | versionLibrary | version | infoVersion
        
        masterVersions["scripMaster"] = getValueForKeyInStrategistMaster(key: "scripMaster")
        masterVersions["versionLibrary"] = getValueForKeyInStrategistMaster(key: "versionLibrary")
        masterVersions["version"] = getValueForKeyInStrategistMaster(key: "version")
        masterVersions["infoVersion"] = getValueForKeyInStrategistMaster(key: "infoVersion")
        
        return masterVersions
    }
    
    public func getUserName() -> String {
        let firstName = getValueForKeyInStrategistMaster(key: "firstName")
        let lastName = getValueForKeyInStrategistMaster(key: "lastName")
        
        var fullName = "\(firstName)"
        
        if lastName != "0" {
            fullName.append(" \(lastName)")
        }
        
        return fullName
    }
    
    public func getAccountDetails() -> [String: String] {
        var accountDetails = [String: String]()
        
        accountDetails["sAcType"] = getValueForKeyInStrategistMaster(key: "sAcType")
        accountDetails["sValidity"] = getValueForKeyInStrategistMaster(key: "sValidity")
        accountDetails["sLegs"] = getValueForKeyInStrategistMaster(key: "sLegs")
        accountDetails["sOutputs"] = getValueForKeyInStrategistMaster(key: "sOutputs")
        accountDetails["aAcType"] = getValueForKeyInStrategistMaster(key: "aAcType")
        accountDetails["aValidity"] = getValueForKeyInStrategistMaster(key: "aValidity")
        accountDetails["aLegs"] = getValueForKeyInStrategistMaster(key: "aLegs")
        accountDetails["aOutputs"] = getValueForKeyInStrategistMaster(key: "aOutputs")
        
        return accountDetails
    }
    
    public func getValueForKeyInStrategistMaster(key: String) -> String {
        
        let table = Table(Constants.DBTables.StrategistMaster)
        
        let myKey = Expression<String>("myKey")
        let myVal = Expression<String>("myVal")
        
        if key.isEmpty {
            return "0"
        } else {
            do {
                if let row = try self.database.pluck(table.filter(myKey == key)) {
                    if let value = row[myVal] as? String {
                        return value
                    } else {
                        return "0"
                    }
                } else {
                    return "0"
                }
            } catch {
                return "0"
            }
        }
    }
}

// MARK:- StrategistResult

extension DatabaseManager {
    public func getAllStrategiesResults(forType type: Int, completion: @escaping((_ success: Bool, _ error: NSError?, _ strategiesResults: [StrategistResult]?) -> Void)) {
        
        let table = Table(Constants.DBTables.StrategistResult)
        let symbol = Expression<String>("symbol")
        let days = Expression<String>("days")
        let volMean = Expression<String>("volMean")
        let volUp = Expression<String>("volUp")
        let volDn = Expression<String>("volDn")
        let strategyName = Expression<String>("strategyName")
        let riskProfile = Expression<String>("riskProfile")
        let leg1 = Expression<String>("leg1")
        let leg2 = Expression<String>("leg2")
        let leg3 = Expression<String>("leg3")
        let leg4 = Expression<String>("leg4")
        let rewardToRisk = Expression<String>("rewardToRisk")
        let margin = Expression<String>("margin")
        let meanProfit = Expression<String>("meanProfit")
        let meanLoss = Expression<String>("meanLoss")
        let l1Mean = Expression<String>("l1Mean")
        let l1Best = Expression<String>("l1Best")
        let l1Worst = Expression<String>("l1Worst")
        let l2Mean = Expression<String>("l2Mean")
        let l2Best = Expression<String>("l2Best")
        let l2Worst = Expression<String>("l2Worst")
        let l3Mean = Expression<String>("l3Mean")
        let l3Best = Expression<String>("l3Best")
        let l3Worst = Expression<String>("l3Worst")
        let l4Mean = Expression<String>("l4Mean")
        let l4Best = Expression<String>("l4Best")
        let l4Worst = Expression<String>("l4Worst")
        let level1 = Expression<String>("level1")
        let level2 = Expression<String>("level2")
        let level3 = Expression<String>("level3")
        let level4 = Expression<String>("level4")
        let type = Expression<Int>("type")
        let tradeSpecific = Expression<String>("tradeSpecific")
        let no = Expression<Int>("no")
        let drawDn = Expression<String>("drawDn")
        let drawUp = Expression<String>("drawUp")
        let drawDnLvl = Expression<String>("drawDnLvl")
        let drawUpLvl = Expression<String>("drawUpLvl")
        let dataTime = Expression<Date>("dataTime")
        
        var results = [StrategistResult]()
        
        do {
            for row in try self.database.prepare(table) {
                
                let result = StrategistResult()
                result.symbol = row[symbol]
                result.days = row[days]
                result.volMean = row[volMean]
                result.volUp = row[volUp]
                result.volDn = row[volDn]
                result.strategyName = row[strategyName]
                result.riskProfile = row[riskProfile]
                result.leg1 = row[leg1]
                result.leg2 = row[leg2]
                result.leg3 = row[leg3]
                result.leg4 = row[leg4]
                result.rewardToRisk = row[rewardToRisk]
                result.margin = row[margin]
                result.meanProfit = row[meanProfit]
                result.meanLoss = row[meanLoss]
                result.l1Mean = row[l1Mean]
                result.l1Best = row[l1Best]
                result.l1Worst = row[l1Worst]
                result.l2Mean = row[l2Mean]
                result.l2Best = row[l2Best]
                result.l2Worst = row[l2Worst]
                result.l3Mean = row[l3Mean]
                result.l3Best = row[l3Best]
                result.l3Worst = row[l3Worst]
                result.l4Mean = row[l4Mean]
                result.l4Best = row[l4Best]
                result.l4Worst = row[l4Worst]
                result.level1 = row[level1]
                result.level2 = row[level2]
                result.level3 = row[level3]
                result.level4 = row[level4]
                result.type = row[type]
                result.tradeSpecific = row[tradeSpecific]
                result.no = row[no]
                result.drawDn = row[drawDn]
                result.drawUp = row[drawUp]
                result.drawDnLvl = row[drawDnLvl]
                result.drawUpLvl = row[drawUpLvl]
                result.dataTime = row[dataTime]
                
                results.append(result)
            }
            
            completion(true, nil, results)
            
        } catch {
            print("*** Error (getAllStrategiesResults): \(error.localizedDescription)")
            let err = NSError.init(type: OSOError.TableError)
            completion(false, err, nil)
        }
        
    }
}

// MARK:- OptionChain

extension DatabaseManager {
    
    func getOptionSymbols(completion: @escaping((_ success: Bool, _ error: NSError?, _ symbols: [String]?) -> Void)) {
        let table = Table(Constants.DBTables.OptMaster)
        
        let symbol = Expression<String>("symbol")
        
        do {
            var symbols = [String]()
            for row in try self.database.prepare(table.select(distinct:symbol).order(symbol.asc)) {
                //print("symbol: \(row[symbol])")
                symbols.append(row[symbol])
            }
            
            completion(true, nil, symbols)
        } catch {
            let err = NSError.init(type: .Unknown)
            completion(false, err, nil)
        }
    }
    
    func getOptionDataForSymbol(symbolName: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ data: [[String: Any]]?) -> Void)){
        let table = Table(Constants.DBTables.OptMaster)
        
        let symbol = Expression<String>("symbol")
        let expiry = Expression<Date>("expiry")
        let lot = Expression<String>("lot")
        let strikes = Expression<String>("strikes")
        
        var data = [[String: Any]]()
        
        do {
            for row in try self.database.prepare(table.select(expiry, lot, strikes).filter(symbol == symbolName).order(expiry.asc)) {
                var d = [String: Any]()
                d["expiry"] = row[expiry]
                d["lot"] = row[lot]
                d["strikes"] = row[strikes]
                data.append(d)
            }
            
            completion(true, nil, data)
        } catch {
            print("Error (getOptionDataForSymbol): \(error.localizedDescription)")
            let err = NSError.init(type: .Unknown)
            completion(false, err, nil)
        }
    }
    
}

// MARK:- AnalyzerSummary

extension DatabaseManager {
    
    public func checkIfPositionNameAlreadyExists(position: String) -> Bool {
        
        let table = Table(Constants.DBTables.AnalyzerSummary)
        let positionName = Expression<String>("positionName")
        
        do {
            let rows = Array(try self.database.prepare(table.select(positionName).filter(positionName == position)))
            
            if rows.count > 0 {
                return true
            } else {
                return false
            }
            
        } catch {
            print("Error: \(error.localizedDescription)")
            return true
        }
    }
    
    private func getPositionNames() -> [String] {
        
        let table = Table(Constants.DBTables.AnalyzerSummary)
        let positionName = Expression<String>("positionName")
        
        var positions = [String]()
        
        do {
            for row in try self.database.prepare(table.select(distinct: positionName)) {
                positions.append(row[positionName])
            }
            return positions
        } catch {
            return positions
        }
    }
    
    public func addPosition(analyzerSummaries: [AnalyzerSummary], completion: @escaping((_ success: Bool) -> Void)) {
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let expBookType = Expression<String>("bookType")
        let expPositionNo = Expression<String>("positionNo")
        let expSrno = Expression<String>("srno")
        let expPositionName = Expression<String>("positionName")
        let expDateCreated = Expression<Date>("dateCreated")
        let expPapi = Expression<String>("Papi")
        let expInstrument = Expression<String>("instrument")
        let expSymbol = Expression<String>("symbol")
        let expStrike = Expression<String>("strike")
        let expPrice = Expression<String>("price")
        let expQty = Expression<String>("qty")
        let expMargin = Expression<String>("margin")
        let expExpiry = Expression<String>("expiry")
        let expAlertOne = Expression<String>("alertOne")
        let expAlertTwo = Expression<String>("alertTwo")
        let expSyncNo = Expression<String>("syncNo")
        let expTradeStatus = Expression<String>("tradeStatus")
        let expExpired = Expression<String>("expired")
        let expOptType = Expression<String>("optType")
        let expDateLegClosed = Expression<String>("dateLegClosed")
        let expClosingPrice = Expression<String>("closingPrice")
        let expFixedProfit = Expression<String>("fixedProfit")
        let expSpreadTarget = Expression<String>("spreadTarget")
        let expSpreadStopLoss = Expression<String>("spreadStopLoss")
        
        var counter = 0
        
        for summary in analyzerSummaries {
            let insertData = table.insert(expPositionNo <- summary.positionNo!,
                                          expBookType <- summary.bookType!,
                                          expSrno <- summary.srno!,
                                          expPositionName <- summary.positionName!,
                                          expDateCreated <- summary.dateCreated!,
                                          expPapi <- summary.Papi!,
                                          expInstrument <- summary.instrument!,
                                          expSymbol <- summary.symbol!,
                                          expStrike <- summary.strike!,
                                          expPrice <- summary.price!,
                                          expQty <- summary.qty!,
                                          expMargin <- summary.margin!,
                                          expExpiry <- summary.expiry!,
                                          expAlertOne <- summary.alertOne!,
                                          expAlertTwo <- summary.alertTwo!,
                                          expSyncNo <- summary.syncNo!,
                                          expTradeStatus <- summary.tradeStatus ?? "0",
                                          expExpired <- summary.expired!,
                                          expOptType <- summary.optType!,
                                          expDateLegClosed <- summary.dateLegClosed!,
                                          expClosingPrice <- summary.closingPrice!,
                                          expFixedProfit <- summary.fixedProfit!,
                                          expSpreadTarget <- summary.spreadTarget!,
                                          expSpreadStopLoss <- summary.spreadStopLoss!)
            
            do {
                try self.database.run(insertData)
                counter += 1
            } catch {
                print("Error inserting data in AnalyzerSummary: \(error)")
                counter += 1
            }
        }
        
        if counter == analyzerSummaries.count {
            completion(true)
        } else {
            completion(false)
        }
    }
    
    public func addPositionInCallsHistory(analyzerSummaries: [AnalyzerSummary], completion: @escaping((_ success: Bool) -> Void)) {
        let table = Table(Constants.DBTables.CallHistory)
        
        let expBookType = Expression<String>("bookType")
        let expPositionNo = Expression<String>("positionNo")
        let expSrno = Expression<String>("srno")
        let expPositionName = Expression<String>("positionName")
        let expDateCreated = Expression<Date>("dateCreated")
        let expDateClosed = Expression<Date>("dateClosed")
        let expPapi = Expression<String>("Papi")
        let expInstrument = Expression<String>("instrument")
        let expSymbol = Expression<String>("symbol")
        let expStrike = Expression<String>("strike")
        let expPrice = Expression<String>("price")
        let expQty = Expression<String>("qty")
        let expMargin = Expression<String>("margin")
        let expExpiry = Expression<String>("expiry")
        let expAlertOne = Expression<String>("alertOne")
        let expAlertTwo = Expression<String>("alertTwo")
        let expSyncNo = Expression<String>("syncNo")
        let expTradeStatus = Expression<String>("tradeStatus")
        let expExpired = Expression<String>("expired")
        let expOptType = Expression<String>("optType")
        let expDateLegClosed = Expression<String>("dateLegClosed")
        let expInitiationPrice = Expression<String>("initiationPrice")
        let expClosingPrice = Expression<String>("closingPrice")
        let expFixedProfit = Expression<String>("fixedProfit")
        let expSpreadTarget = Expression<String>("spreadTarget")
        let expSpreadStopLoss = Expression<String>("spreadStopLoss")
        
        var counter = 0
        
        for summary in analyzerSummaries {
            let insertData = table.insert(expPositionNo <- summary.positionNo!,
                                          expBookType <- summary.bookType!,
                                          expSrno <- summary.srno!,
                                          expPositionName <- summary.positionName!,
                                          expDateCreated <- summary.dateCreated!,
                                          expDateClosed <- summary.dateClosed!,
                                          expPapi <- summary.Papi!,
                                          expInstrument <- summary.instrument!,
                                          expSymbol <- summary.symbol!,
                                          expStrike <- summary.strike!,
                                          expPrice <- summary.price!,
                                          expQty <- summary.qty!,
                                          expMargin <- summary.margin!,
                                          expExpiry <- summary.expiry!,
                                          expAlertOne <- summary.alertOne!,
                                          expAlertTwo <- summary.alertTwo!,
                                          expSyncNo <- summary.syncNo!,
                                          expTradeStatus <- summary.tradeStatus ?? "0",
                                          expExpired <- summary.expired!,
                                          expOptType <- summary.optType!,
                                          expDateLegClosed <- summary.dateLegClosed!,
                                          expInitiationPrice <- summary.initiationPrice!,
                                          expClosingPrice <- summary.closingPrice!,
                                          expFixedProfit <- summary.fixedProfit!,
                                          expSpreadTarget <- summary.spreadTarget!,
                                          expSpreadStopLoss <- summary.spreadStopLoss!)
            
            do {
                try self.database.run(insertData)
                counter += 1
            } catch {
                print("Error inserting data in AnalyzerSummary: \(error)")
                counter += 1
            }
        }
        
        if counter == analyzerSummaries.count {
            completion(true)
        } else {
            completion(false)
        }
    }
    
    public func deletePositions(positions: [String], bookType: String, completion: @escaping((_ success: Bool) -> Void)) {
        print("----------- deletePosition ----------")
        print("positions: \(positions)")
        
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let expPositionNo = Expression<String>("positionNo")
        let expBookType = Expression<String>("bookType")
        
        var ctr = 0
        
        for position in positions {
            do {
                let row = table.filter(expPositionNo == position && expBookType == bookType)
                
                if try self.database.run(row.delete()) > 0 {
                    ctr += 1
                    //completion(true)
                } else {
                    print("*** NO ROWS TO DELETE")
                    //completion(true)
                }
            } catch {
                completion(false)
            }
        }
        
        completion(ctr == positions.count)
    }
    
    public func deletePositionsFromOpenCalls(positions: [String], bookType: String) -> Bool {
        print("----------- deletePositionsFromOpenCalls ----------")
        print("positions: \(positions)")
        
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let expPositionNo = Expression<String>("positionNo")
        let expBookType = Expression<String>("bookType")
        
        var ctr = 0
        
        for position in positions {
            do {
                let row = table.filter(expPositionNo == position && expBookType == bookType)
                
                if try self.database.run(row.delete()) > 0 {
                    ctr += 1
                } else {
                    print("* NO ROWS TO DELETE")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        return positions.count == ctr
    }
    
    public func deletePositionsFromCallHistory(positions: [String], bookType: String) -> Bool {
        print("----------- deletePositionsFromCallHistory ----------")
        print("positions: \(positions)")
        
        let table = Table(Constants.DBTables.CallHistory)
        
        let expPositionNo = Expression<String>("positionNo")
        let expBookType = Expression<String>("bookType")
        
        var ctr = 0
        
        for position in positions {
            do {
                let row = table.filter(expPositionNo == position && expBookType == bookType)
                
                if try self.database.run(row.delete()) > 0 {
                    ctr += 1
                } else {
                    print("* NO ROWS TO DELETE")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        return positions.count == ctr
    }
    
    public func archivePosition(positionNo: String, mtm: String, tradeStatus: String, legs: [ManagerDetailLegs], id: String, completion: @escaping((_ success: Bool) -> Void)) {
        print("----------- archivePosition ----------")
        
        var archives = [Archive]()
        var summaries = [AnalyzerSummary]()
        
        getPositionDetails(positionNo: positionNo) { (error, analyzerSummaries) in
            if let err = error {
                print("*** Error: \(err.localizedDescription)")
                completion(false)
            } else {
                if let summaryArray = analyzerSummaries {
                    summaries = summaryArray
                }
                
                let archive = Archive()
                archive.id = id
                archive.closedOn = Date()
                archive.pnl = mtm
                archive.tradeStatus = tradeStatus
                
                if let dateCreated = summaries[0].dateCreated {
                    archive.createdOn = dateCreated
                }
                
                if let margin = summaries[0].margin {
                    archive.margin = margin
                }
                
                if let positionName = summaries[0].positionName {
                    archive.strategy = positionName
                }
                
                if let symbol = summaries[0].symbol {
                    archive.symbol = symbol
                }
                
                var legString: String = ""
                for i in 0..<legs.count {
                    let leg = legs[i]
                    
                    if let instrument = leg.instrument, let expiry = leg.expiry, let strike = leg.strike, let optType = leg.optType, let price = leg.price, let qty = leg.qty, let cmp = leg.cmp, let mtm = leg.mtm {
                        legString.append("\(instrument) \(expiry) \(strike) \(optType),\(price),\(qty),\(cmp),\(mtm)")
                        
                        if i < legs.count - 1 {
                            legString.append(";")
                        }
                    }
                }
                archive.legData = legString
                
                archives.append(archive)
                
                // insert archives in Archive Table
                self.insertIntoArchive(archives: archives, completion: { (success) in
                    if success {
                        // delete data from AnalyzerSummary
                        self.deletePositions(positions: [positionNo], bookType: "0", completion: { (deleted) in
                            if deleted {
                                completion(true)
                            } else {
                                completion(false)
                            }
                        })
                    } else {
                        completion(false)
                    }
                })
            }
        }
    }
    
    public func addAlert(positionNo: String, alertOne: String, alertTwo: String, completion: @escaping((_ success: Bool) -> Void)) {
        print("----------- addAlert ----------")
        print("positionNo: \(positionNo) | alertOne: \(alertOne) | alertTwo: \(alertTwo)")
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let expPositionNo = Expression<String>("positionNo")
        let expAlertOne = Expression<String>("alertOne")
        let expAlertTwo = Expression<String>("alertTwo")
        
        let row = table.filter(expPositionNo == positionNo)
        let updateValue = row.update(expAlertOne <- alertOne, expAlertTwo <- alertTwo)
        
        do {
            if try self.database.run(updateValue) > 0 {
                print("Row Updated")
                completion(true)
            } else {
                print("Error: No row found")
                completion(false)
            }
        } catch {
            print("Error in setting Alert")
            completion(false)
        }
    }
    
    public func getPositionDetailsForEdit(positionNo: String, completion: @escaping((_ error: NSError?, _ analyzerSummaries: [AnalyzerSummary]?, _ legsAllowed: Int?) -> Void)) {
        
        print("----------- getPositionDetailsForEdit ----------")
        
        self.getPositionDetails(positionNo: positionNo) { (error, summaries) in
            if let err = error {
                completion(err, nil, nil)
            } else {
                let legsAllowed = self.getValueForKeyInStrategistMaster(key: "aLegs")
                
                if let summaryArray = summaries, let legsAllowedInt = legsAllowed.int {
                    completion(nil, summaryArray, legsAllowedInt)
                } else {
                    let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Could not find the position details, please Try again."])
                    completion(err, nil, nil)
                }
            }
        }
        
    }
    
    public func getPositionDetails(positionNo: String, completion: @escaping((_ error: NSError?, _ summaries: [AnalyzerSummary]?) -> Void)) {
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let expBookType = Expression<String>("bookType")
        let expPositionNo = Expression<String>("positionNo")
        let expSrno = Expression<String>("srno")
        let expPositionName = Expression<String>("positionName")
        let expDateCreated = Expression<Date>("dateCreated")
        let expPapi = Expression<String>("Papi")
        let expInstrument = Expression<String>("instrument")
        let expSymbol = Expression<String>("symbol")
        let expStrike = Expression<String>("strike")
        let expPrice = Expression<String>("price")
        let expQty = Expression<String>("qty")
        let expMargin = Expression<String>("margin")
        let expExpiry = Expression<String>("expiry")
        let expAlertOne = Expression<String>("alertOne")
        let expAlertTwo = Expression<String>("alertTwo")
        let expSyncNo = Expression<String>("syncNo")
        let expTradeStatus = Expression<String>("tradeStatus")
        let expExpired = Expression<String>("expired")
        let expOptType = Expression<String>("optType")
        let dateLegClosed = Expression<String>("dateLegClosed")
        let closingPrice = Expression<String>("closingPrice")
        let fixedProfit = Expression<String>("fixedProfit")
        
        do {
            let rows = Array(try self.database.prepare(table.filter(expPositionNo == positionNo)))
            
            if rows.count > 0 {
                
                var summaries = [AnalyzerSummary]()
                
                for row in rows {
                    let summary = AnalyzerSummary()
                    
                    summary.bookType = row[expBookType]
                    summary.positionNo = row[expPositionNo]
                    summary.srno = row[expSrno]
                    summary.positionName = row[expPositionName]
                    summary.dateCreated = row[expDateCreated]
                    summary.Papi = row[expPapi]
                    summary.instrument = row[expInstrument]
                    summary.symbol = row[expSymbol]
                    summary.strike = row[expStrike]
                    summary.price = row[expPrice]
                    summary.qty = row[expQty]
                    summary.margin = row[expMargin]
                    summary.expiry = row[expExpiry]
                    summary.alertOne = row[expAlertOne]
                    summary.alertTwo = row[expAlertTwo]
                    summary.syncNo = row[expSyncNo]
                    summary.tradeStatus = row[expTradeStatus]
                    summary.expired = row[expExpired]
                    summary.optType = row[expOptType]
                    summary.dateLegClosed = row[dateLegClosed]
                    summary.closingPrice = row[closingPrice]
                    summary.fixedProfit = row[fixedProfit]
                    
                    summaries.append(summary)
                }
                
                completion(nil, summaries)
                
            } else {
                let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Could not find the position, please Try again."])
                completion(err, nil)
            }
        } catch {
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: error.localizedDescription])
            completion(err, nil)
        }
    }
    
    public func getPositionDetailsFromCallHistory(positionNo: String, completion: @escaping((_ error: NSError?, _ summaries: [AnalyzerSummary]?) -> Void)) {
        let table = Table(Constants.DBTables.CallHistory)
        
        let expBookType = Expression<String>("bookType")
        let expPositionNo = Expression<String>("positionNo")
        let expSrno = Expression<String>("srno")
        let expPositionName = Expression<String>("positionName")
        let expDateCreated = Expression<Date>("dateCreated")
        let expDateClosed = Expression<Date>("dateClosed")
        let expPapi = Expression<String>("Papi")
        let expInstrument = Expression<String>("instrument")
        let expSymbol = Expression<String>("symbol")
        let expStrike = Expression<String>("strike")
        let expPrice = Expression<String>("price")
        let expQty = Expression<String>("qty")
        let expMargin = Expression<String>("margin")
        let expExpiry = Expression<String>("expiry")
        let expAlertOne = Expression<String>("alertOne")
        let expAlertTwo = Expression<String>("alertTwo")
        let expSyncNo = Expression<String>("syncNo")
        let expTradeStatus = Expression<String>("tradeStatus")
        let expExpired = Expression<String>("expired")
        let expOptType = Expression<String>("optType")
        let expInitiationPrice = Expression<String>("initiationPrice")
        let expClosingPrice = Expression<String>("closingPrice")
        let expFixedProfit = Expression<String>("fixedProfit")
        
        do {
            let rows = Array(try self.database.prepare(table.filter(expPositionNo == positionNo)))
            
            if rows.count > 0 {
                
                var summaries = [AnalyzerSummary]()
                
                for row in rows {
                    let summary = AnalyzerSummary()
                    
                    summary.bookType = row[expBookType]
                    summary.positionNo = row[expPositionNo]
                    summary.srno = row[expSrno]
                    summary.positionName = row[expPositionName]
                    summary.dateCreated = row[expDateCreated]
                    summary.dateClosed = row[expDateClosed]
                    summary.Papi = row[expPapi]
                    summary.instrument = row[expInstrument]
                    summary.symbol = row[expSymbol]
                    summary.strike = row[expStrike]
                    summary.price = row[expPrice]
                    summary.qty = row[expQty]
                    summary.margin = row[expMargin]
                    summary.expiry = row[expExpiry]
                    summary.alertOne = row[expAlertOne]
                    summary.alertTwo = row[expAlertTwo]
                    summary.syncNo = row[expSyncNo]
                    summary.tradeStatus = row[expTradeStatus]
                    summary.expired = row[expExpired]
                    summary.optType = row[expOptType]
                    summary.initiationPrice = row[expInitiationPrice]
                    summary.closingPrice = row[expClosingPrice]
                    summary.fixedProfit = row[expFixedProfit]
                    
                    summaries.append(summary)
                }
                
                completion(nil, summaries)
                
            } else {
                let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "Could not find the position, please Try again."])
                completion(err, nil)
            }
        } catch {
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: error.localizedDescription])
            completion(err, nil)
        }
    }
    
    public func getSyncPositionAPI(forBookType bookType: String) -> String {
        print("----------- getSyncPositionAPI ------------")
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let expPositionNo = Expression<String>("positionNo")
        let expBookType = Expression<String>("bookType")
        //let expSrno = Expression<String>("srno")
        //let expExpired = Expression<String>("expired")
        //let expSyncNo = Expression<String>("syncNo")
        
        do {
            //let rows = Array(try self.database.prepare(table.select(expPositionNo, expSrno, expExpired, expSyncNo)))
            let rows = Array(try self.database.prepare(table.select(distinct: expPositionNo).filter(expBookType == bookType)))
            
            var syncAPI: String = ""
            
            if rows.count > 0 {
                for i in 0..<rows.count {
                    //syncAPI.append("\(rows[i][expPositionNo]),\(rows[i][expSrno]),\(rows[i][expExpired]),\(rows[i][expSyncNo])")
                    syncAPI.append("\(rows[i][expPositionNo])")
                    if i < (rows.count - 1) {
                        syncAPI.append(";")
                    }
                }
            } else {
                syncAPI = "0"
            }
            
            return syncAPI
            
        } catch {
            return "0"
        }
    }
    
    public func getSyncCallHistoryAPI(forBookType bookType: String) -> String {
        print("----------- getSyncCallHistoryAPI ------------")
        let table = Table(Constants.DBTables.CallHistory)
        
        let expPositionNo = Expression<String>("positionNo")
        let expBookType = Expression<String>("bookType")
        //let expSrno = Expression<String>("srno")
        //let expExpired = Expression<String>("expired")
        //let expSyncNo = Expression<String>("syncNo")
        
        do {
            //let rows = Array(try self.database.prepare(table.select(expPositionNo, expSrno, expExpired, expSyncNo)))
            let rows = Array(try self.database.prepare(table.select(distinct: expPositionNo).filter(expBookType == bookType)))
            
            var syncAPI: String = ""
            
            if rows.count > 0 {
                for i in 0..<rows.count {
                    //syncAPI.append("\(rows[i][expPositionNo]),\(rows[i][expSrno]),\(rows[i][expExpired]),\(rows[i][expSyncNo])")
                    syncAPI.append("\(rows[i][expPositionNo])")
                    if i < (rows.count - 1) {
                        syncAPI.append(";")
                    }
                }
            } else {
                syncAPI = "0"
            }
            
            return syncAPI
            
        } catch {
            return "0"
        }
    }
    
    public func deleteAllAnalyzerPositions(bookType: String? = nil) -> Bool {
        print("----------- deleteAllAnalyzerPositions ------------")
        let table = Table(Constants.DBTables.AnalyzerSummary)
        let expBookType = Expression<String>("bookType")
        
        do {
            if let bType = bookType {
                let rows = table.filter(expBookType == bType)
                
                if try self.database.run(rows.delete()) > 0 {
                    return true
                } else {
                    print("No rows to delete")
                    return true
                }
            } else {
                if try self.database.run(table.delete()) > 0 {
                    return true
                } else {
                    print("No rows to delete")
                    return true
                }
            }
        } catch {
            print("*** Error (deleteAllAnalyzerPositions): \(error.localizedDescription)")
            return false
        }
    }
    
    public func deleteAllFromCallsHistory(bookType: String? = nil) -> Bool {
        print("----------- deleteAllFromCallsHistory ------------")
        let table = Table(Constants.DBTables.CallHistory)
        let expBookType = Expression<String>("bookType")
        
        do {
            if let bType = bookType {
                let rows = table.filter(expBookType == bType)
                
                if try self.database.run(rows.delete()) > 0 {
                    return true
                } else {
                    print("No rows to delete")
                    return true
                }
            } else {
                if try self.database.run(table.delete()) > 0 {
                    return true
                } else {
                    print("No rows to delete")
                    return true
                }
            }
        } catch {
            print("*** Error (deleteAllAnalyzerPositions): \(error.localizedDescription)")
            return false
        }
    }
    
    public func getAnalyzerSummaryPositions(forBookType bookType: String, completion: @escaping((_ success: Bool, _ summaryList: [AnalyzerSummaryList]?) -> Void)) {
        print("----------- getAnalyzerSummaryPositions ----------")
        let table = Table(Constants.DBTables.AnalyzerSummary)
        
        let expBookType = Expression<String>("bookType")
        let expPositionNo = Expression<String>("positionNo")
        let expPositionName = Expression<String>("positionName")
        let expSymbol = Expression<String>("symbol")
        let expMargin = Expression<String>("margin")
        let expAlertOne = Expression<String>("alertOne")
        let expAlertTwo = Expression<String>("alertTwo")
        let expDateCreated = Expression<Date>("dateCreated")
        let expSpreadTarget = Expression<String>("spreadTarget")
        let expSpreadStopLoss = Expression<String>("spreadStopLoss")
        
        do {
            let rows = Array(try self.database.prepare(table.select(expBookType, expPositionNo, expPositionName, expSymbol, expMargin, expAlertOne, expAlertTwo, expDateCreated, expSpreadTarget, expSpreadStopLoss).filter(expBookType == bookType).group(expBookType, expPositionNo, expPositionName, expSymbol, expMargin, expAlertOne, expAlertTwo, expDateCreated, expSpreadTarget, expSpreadStopLoss)))
            
            var summaryList = [AnalyzerSummaryList]()
            
            for row in rows {
                let summary = AnalyzerSummaryList()
                summary.bookType = row[expBookType]
                summary.positionNo = row[expPositionNo]
                summary.positionName = row[expPositionName]
                summary.symbol = row[expSymbol]
                summary.margin = row[expMargin]
                summary.alertOne = row[expAlertOne]
                summary.alertTwo = row[expAlertTwo]
                summary.dateCreated = row[expDateCreated]
                summary.spreadTarget = row[expSpreadTarget]
                summary.spreadStopLoss = row[expSpreadStopLoss]
                
                summaryList.append(summary)
            }
            
            completion(true, summaryList)
            
        } catch {
            completion(false, nil)
        }
    }
    
    public func getAnalyzerSummaryPositionsFromCallHistory(forBookType bookType: String, completion: @escaping((_ success: Bool, _ summaryList: [AnalyzerSummaryList]?) -> Void)) {
        print("----------- getAnalyzerSummaryPositionsFromCallHistory ----------")
        let table = Table(Constants.DBTables.CallHistory)
        
        let expBookType = Expression<String>("bookType")
        let expPositionNo = Expression<String>("positionNo")
        let expPositionName = Expression<String>("positionName")
        let expSymbol = Expression<String>("symbol")
        let expMargin = Expression<String>("margin")
        let expAlertOne = Expression<String>("alertOne")
        let expAlertTwo = Expression<String>("alertTwo")
        let expDateCreated = Expression<Date>("dateCreated")
        let expDateClosed = Expression<Date>("dateClosed")
        let expSpreadTarget = Expression<String>("spreadTarget")
        let expSpreadStopLoss = Expression<String>("spreadStopLoss")
        
        do {
            let rows = Array(try self.database.prepare(table.select(expBookType, expPositionNo, expPositionName, expSymbol, expMargin, expAlertOne, expAlertTwo, expDateCreated, expDateClosed, expSpreadTarget, expSpreadStopLoss).filter(expBookType == bookType).group(expBookType, expPositionNo, expPositionName, expSymbol, expMargin, expAlertOne, expAlertTwo, expDateCreated, expDateClosed, expSpreadTarget, expSpreadStopLoss)))
            
            var summaryList = [AnalyzerSummaryList]()
            
            for row in rows {
                let summary = AnalyzerSummaryList()
                summary.bookType = row[expBookType]
                summary.positionNo = row[expPositionNo]
                summary.positionName = row[expPositionName]
                summary.symbol = row[expSymbol]
                summary.margin = row[expMargin]
                summary.alertOne = row[expAlertOne]
                summary.alertTwo = row[expAlertTwo]
                summary.dateCreated = row[expDateCreated]
                summary.dateClosed = row[expDateClosed]
                summary.spreadTarget = row[expSpreadTarget]
                summary.spreadStopLoss = row[expSpreadStopLoss]
                
                summaryList.append(summary)
            }
            
            completion(true, summaryList)
            
        } catch {
            completion(false, nil)
        }
    }
}

// MARK:- FutMaster

extension DatabaseManager {
    
    public func getExpiryFromFutMaster(forSymbol symbol: String) -> String? {
        let table = Table(Constants.DBTables.FutMaster)
        let expiry = Expression<Date>("expiry")
        let symbol = Expression<String>("symbol")
        
        do {
            var expiryArray = [Date]()
            
            for row in try self.database.prepare(table.select(expiry).filter(symbol == symbol).order(expiry.asc)) {
                expiryArray.append(row[expiry])
            }
            
            if let firstExpiry = expiryArray.first {
                return firstExpiry.string(withFormat: "dd-MMM-yy")
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
}

// MARK:- OptMaster

extension DatabaseManager {
    
    func getAllStrikesForSymbol(symbolName: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ strikes: [String]?) -> Void)) {
        
        let table = Table(Constants.DBTables.OptMaster)
        let expiry = Expression<Date>("expiry")
        let strikes = Expression<String>("strikes")
        let symbol = Expression<String>("symbol")
        
        do {
            var strikesArray = [String]()
            
            for row in try self.database.prepare(table.select(strikes).filter(symbol == symbolName).order(expiry.asc)) {
                strikesArray.append(row[strikes])
            }
            
            if let strikeString = strikesArray.first {
                completion(true, nil, strikeString.components(separatedBy: ","))
            } else {
                completion(true, nil, nil)
            }
        } catch {
            let err = NSError.init(type: .Unknown)
            completion(false, err, nil)
        }
    }
    
    func getOptMasterForSymbol(symbolName: String, completion: @escaping((_ success: Bool, _ error: NSError?, _ optMasters: [OptMaster]?) -> Void)) {
        
        let table = Table(Constants.DBTables.OptMaster)
        let expiry = Expression<Date>("expiry")
        let strikes = Expression<String>("strikes")
        let symbol = Expression<String>("symbol")
        let lot = Expression<String>("lot")
        
        do {
            var optMastersArray = [OptMaster]()
            
            for row in try self.database.prepare(table.filter(symbol == symbolName).order(expiry.asc)) {
                let optMaster = OptMaster()
                optMaster.expiry = row[expiry]
                optMaster.symbol = row[symbol]
                optMaster.lot = row[lot]
                optMaster.strikes = row[strikes].components(separatedBy: ",")
                optMastersArray.append(optMaster)
            }
            
            if optMastersArray.count > 0 {
                completion(true, nil, optMastersArray)
            } else {
                let err = NSError.init(type: .NoRecordsFound)
                completion(false, err, nil)
            }
        } catch {
            let err = NSError.init(type: .Unknown)
            completion(false, err, nil)
        }
        
    }
    
}

// MARK:- Lot

extension DatabaseManager {
    
    public func getLot(forSymbol symbol: String, inTable table: String) -> Int {
        let table = Table(table)
        let expSymbol = Expression<String>("symbol")
        let expLot = Expression<String>("lot")
        let expExpiry = Expression<String>("expiry")
        
        var lotValue: Int = -1
        
        do {
            let rows = Array(try self.database.prepare(table.select(expLot, expSymbol).filter(expSymbol == symbol).order(expExpiry.asc)))
            
            if rows.count > 0 {
                if let intValue = rows[0][expLot].int {
                    lotValue = intValue
                }
            }
        } catch {
            print("Row not found.")
        }
        
        return lotValue
    }
}


// MARK:- MyLibrary

extension DatabaseManager {
    
    public func getStrategiesFor(directionIndex: Int, riskIndex: Int, completion: @escaping((_ success: Bool, _ error: NSError?, _ strategies: [MyLibrary]?) -> Void)) {
        let table = Table(Constants.DBTables.MyLibrary)
        
        let no = Expression<Int>("no")
        let direction = Expression<Int>("direction")
        let proficiency = Expression<Int>("proficiency")
        let riskType = Expression<Int>("riskType")
        let legs = Expression<Int>("legs")
        let riskSubType = Expression<Int>("riskSubType")
        let name = Expression<String>("name")
        let optimizer = Expression<Int>("optimizer")
        let trade = Expression<String>("trade")
        let intro = Expression<String>("intro")
        let execute = Expression<String>("execute")
        let tradeText = Expression<String>("tradeText")
        let maxProfit = Expression<String>("maxProfit")
        let maxLoss = Expression<String>("maxLoss")
        let advantages = Expression<String>("advantages")
        let disadvantages = Expression<String>("disadvantages")
        
        var data = [MyLibrary]()
        
        do {
            
            if riskIndex == 1 || riskIndex == 2 {
                for row in try self.database.prepare(table.filter(direction == directionIndex && riskType == riskIndex).order(no.asc)) {
                    let myLibrary = MyLibrary()
                    myLibrary.no = row[no]
                    myLibrary.name = row[name]
                    myLibrary.proficiency = row[proficiency]
                    myLibrary.legs = row[legs]
                    myLibrary.riskSubType = row[riskSubType]
                    myLibrary.optimizer = row[optimizer]
                    myLibrary.trade = row[trade]
                    myLibrary.intro = row[intro]
                    myLibrary.execute = row[execute]
                    myLibrary.tradeText = row[tradeText]
                    myLibrary.maxProfit = row[maxProfit]
                    myLibrary.maxLoss = row[maxLoss]
                    myLibrary.advantages = row[advantages]
                    myLibrary.disadvantages = row[disadvantages]
                    data.append(myLibrary)
                }
            } else {
                for row in try self.database.prepare(table.filter(direction == directionIndex).order(no.asc)) {
                    let myLibrary = MyLibrary()
                    myLibrary.no = row[no]
                    myLibrary.name = row[name]
                    myLibrary.proficiency = row[proficiency]
                    myLibrary.legs = row[legs]
                    myLibrary.riskSubType = row[riskSubType]
                    myLibrary.optimizer = row[optimizer]
                    myLibrary.trade = row[trade]
                    myLibrary.intro = row[intro]
                    myLibrary.execute = row[execute]
                    myLibrary.tradeText = row[tradeText]
                    myLibrary.maxProfit = row[maxProfit]
                    myLibrary.maxLoss = row[maxLoss]
                    myLibrary.advantages = row[advantages]
                    myLibrary.disadvantages = row[disadvantages]
                    data.append(myLibrary)
                }
            }
            
            completion(true, nil, data)
        } catch {
            print("*** Error (getStrategiesFor): \(error.localizedDescription)")
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: "No strategies found."])
            completion(false, err, nil)
        }
    }
    
}

// MARK:- AddPosition Pre-Defined

extension DatabaseManager {
    
    public func getNameStrategyBuilder(strategyName: String, completion: @escaping((_ success: Bool, _ positionName: String?, _ legsAllowed: Int?) -> Void)) {
        print("---------- getNameStrategyBuilder ---------")
        var legsAllowed: Int = 1
        var positionName: String = ""
        
        let aOutputs = getValueForKeyInStrategistMaster(key: "aOutputs")
        let aLegs = getValueForKeyInStrategistMaster(key: "aLegs")
        let positions = getPositionNames()
        
        if let intValue = aLegs.int {
            legsAllowed = intValue
        }
        
        /*if positions.count >= aOutputs.int! {
            positionName = "Can't add more position, maximum eligible open position is \(aOutputs)."
            completion(false, positionName, legsAllowed)
        } else */
        if positions.count == 0 {
            positionName = strategyName
            completion(true, positionName, legsAllowed)
        } else {
            
            positionName = strategyName
            
            if !positions.contains(positionName) {
                completion(true, positionName, legsAllowed)
            } else {
                for i in 0..<positions.count {
                    positionName = "\(strategyName) \(i+1)"
                    
                    if !positions.contains(positionName) {
                        break
                    }
                }
                
                completion(true, positionName, legsAllowed)
            }
        }
    }
}

// MARK:- AddPosition Custom

extension DatabaseManager {
    
    public func getNameForPosition(completion: @escaping((_ error: NSError?, _ positionName: String?, _ legsAllowed: Int?) -> Void)) {
        print("---------- getNameForPosition ---------")
        var legsAllowed: Int = 1
        var positionName: String = ""
        
        let aOutputs = getValueForKeyInStrategistMaster(key: "aOutputs")
        let aLegs = getValueForKeyInStrategistMaster(key: "aLegs")
        let positions = getPositionNames()
        
        if let intValue = aLegs.int {
            legsAllowed = intValue
        }
        
        /*if positions.count >= aOutputs.int! {
            let message: String = "Can't add more position, maximum eligible open position is \(aOutputs)."
            let err = NSError(domain: SwifterSwift.appBundleID!, code: 1000, userInfo: [NSLocalizedDescriptionKey: message])
            completion(err, nil, nil)
        } else */
        if positions.count == 0 {
            positionName = "Position 1"
            completion(nil, positionName, legsAllowed)
        } else {
            
            positionName = "Position 1"
            
            if !positions.contains(positionName) {
                completion(nil, positionName, legsAllowed)
            } else {
                for i in 0..<(positions.count+1) {
                    positionName = "Position \(i+1)"
                    
                    if !positions.contains(positionName) {
                        break
                    }
                }
                
                completion(nil, positionName, legsAllowed)
            }
        }
    }
    
    public func getExpiriesStrikesAndLot(symbol: String, instrument: String, completion: @escaping((_ expiries: [String]?, _ strikes: [String]?, _ lots: [Int]?) -> Void)) {
        var expiries = [String]()
        var strikes = [String]()
        var lot: String = ""
        var lots = [Int]()
        
        if instrument.uppercased() == "FUT" {
            let table = Table(Constants.DBTables.FutMaster)
            let expSymbol = Expression<String>("symbol")
            let expExpiry = Expression<Date>("expiry")
            let expLot = Expression<String>("lot")
            
            do {
                let rows = Array(try self.database.prepare(table.select(expExpiry, expLot).filter(expSymbol == symbol).order(expExpiry.asc)))
                
                if rows.count > 0 {
                    for row in rows {
                        let date = row[expExpiry].string(withFormat: "dd-MMM-yy")
                        expiries.append(date)
                        
                        if let lot = row[expLot].int {
                            lots.append(lot)
                        } else {
                            lots.append(-1)
                        }
                    }
                    
                    completion(expiries, nil, lots)
                    
                    /*if let row = rows.first {
                        lot = row[expLot]
                        completion(expiries, nil, lot)
                    } else {
                        completion(nil, nil, nil)
                    }*/
                } else {
                    completion(nil, nil, nil)
                }
            } catch {
                print("*** Error: \(error.localizedDescription)")
                completion(nil, nil, nil)
            }
        } else if instrument.uppercased() == "OPT" {
            let table = Table(Constants.DBTables.OptMaster)
            let expSymbol = Expression<String>("symbol")
            let expExpiry = Expression<Date>("expiry")
            let expLot = Expression<String>("lot")
            let expStrikes = Expression<String>("strikes")
            
            do {
                let rows = Array(try self.database.prepare(table.select(expExpiry, expStrikes, expLot).filter(expSymbol == symbol).order(expExpiry.asc)))
                
                if rows.count > 0 {
                    for row in rows {
                        let date = row[expExpiry].string(withFormat: "dd-MMM-yy")
                        expiries.append(date)
                        
                        if let lot = row[expLot].int {
                            lots.append(lot)
                        } else {
                            lots.append(-1)
                        }
                    }
                    
                    if let row = rows.first {
                        lot = row[expLot]
                        strikes = row[expStrikes].components(separatedBy: ",")
                        completion(expiries, strikes, lots)
                    } else {
                        completion(nil, nil, nil)
                    }
                } else {
                    completion(nil, nil, nil)
                }
            } catch {
                print("*** Error: \(error.localizedDescription)")
                completion(nil, nil, nil)
            }
        } else {
            completion(nil, nil, nil)
        }
    }
}

// MARK:- Archive

extension DatabaseManager {
    
    func insertIntoArchive(archives: [Archive], completion: @escaping((_ success: Bool) -> Void)) {
        print("---------- insertIntoArchive ----------")
        let table = Table(Constants.DBTables.Archive)
        
        let symbol = Expression<String>("symbol")
        let strategy = Expression<String>("strategy")
        let createdOn = Expression<Date>("createdOn")
        let closedOn = Expression<Date>("closedOn")
        let pnl = Expression<String>("pnl")
        let margin = Expression<String>("margin")
        let tradeStatus = Expression<String>("tradeStatus")
        let legData = Expression<String>("legData")
        let id = Expression<String>("id")
        
        var counter = 0
        
        for archive in archives {
            let insertData = table.insert(symbol <- archive.symbol!,
                                          strategy <- archive.strategy!,
                                          createdOn <- archive.createdOn!,
                                          closedOn <- archive.closedOn!,
                                          pnl <- archive.pnl!,
                                          margin <- archive.margin!,
                                          tradeStatus <- archive.tradeStatus!,
                                          legData <- archive.legData!,
                                          id <- archive.id!)
            
            do {
                try self.database.run(insertData)
                counter += 1
            } catch {
                print("Error inserting data in Archive: \(error)")
                counter += 1
            }
        }
        
        if counter == archives.count {
            completion(true)
        } else {
            completion(false)
        }
    }
}

// MARK:- Book Types

extension DatabaseManager {
    
    public func getBooks() -> [OSOBook] {
        print("► getBooks()")
        var books = [OSOBook]()
        
        /*let bookElite = OSOBook()
        bookElite.bookType = "0"
        bookElite.title = "Elite"
        books.append(bookElite)
        
        let bookPremium = OSOBook()
        bookPremium.bookType = "1"
        bookPremium.title = "Premium"
        books.append(bookPremium)*/
        
        let table = Table(Constants.DBTables.Books)
        
        let expBookType = Expression<String>("bookType")
        let expBookTitle = Expression<String>("bookTitle")
        
        do {
            let rows = Array(try self.database.prepare(table))
            
            if rows.count > 0 {
                
                for row in rows {
                    let book = OSOBook()
                    
                    book.bookType = row[expBookType]
                    book.title = row[expBookTitle]
                    
                    books.append(book)
                }
                
            }
        } catch {
            print("Error: \(error.localizedDescription)")
        }
        
        return books
    }
    
    public func addBooks(books: [OSOBook]) -> Bool {
        
        let table = Table(Constants.DBTables.Books)
        
        let bookType = Expression<String>("bookType")
        let bookTitle = Expression<String>("bookTitle")
        
        var counter: Int = 0
        
        do {
            try self.database.transaction(.deferred, block: {
                
                for book in books {
                    if let type = book.bookType, let title = book.title {
                        let insertData = table.insert(bookType <- type, bookTitle <- title)
                        
                        do {
                            try self.database.run(insertData)
                            
                            counter += 1
                        } catch {
                            print("Error inserting data in \"\(Constants.DBTables.Books)\": \(error)")
                        }
                    }
                    
                }
            })
        } catch {
            print("Error inserting data in \"\(Constants.DBTables.Books)\": \(error)")
        }
        
        return counter == books.count
    }
    
}

// MARK:- Constraints

extension DatabaseManager {
    
    func getContraintsSettingForKey(keyName: String) -> String {
        return getValueForKeyInStrategistMaster(key: keyName)
    }
    
}
